VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "Sheet8"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = True
Option Explicit

Private Function MyMacroDisable() As Boolean
    MyMacroDisable = False
    
    On Error GoTo MacroDisable
    If IsFormatSheet(Me) Then
        GoTo MacroDisable
    ElseIf Me.Range("TTypeMatrix_自動フィルタ").Value <> "ON" Then
        GoTo MacroDisable
    End If
    On Error GoTo 0
    
    Exit Function
    
MacroDisable:
    MyMacroDisable = True
End Function

Private Sub Worksheet_BeforeDoubleClick(ByVal Target As Range, Cancel As Boolean)
    If MyMacroDisable() Then Exit Sub
    
    On Error GoTo MacroFail
    Call StatisticsTtypematrix.TTypeMatrixDataDisp
    Cancel = True

MacroFail:
End Sub
