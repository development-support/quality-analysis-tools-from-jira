Attribute VB_Name = "CalcEstimatedBugFromTestChart"
Option Explicit



Sub CalcEstimatedBugFromTestChart()
    Application.ScreenUpdating = False

    Dim strInSheetName As String
    Dim strAnalyzeSheetName As String
    Dim colRowLabel As Long
    Dim rowDate As Long

    ' Parameter
    strInSheetName = strConstTestDataSheetName
    strAnalyzeSheetName = strConstCalcEstimatedBugSheetName

    Sheets(strInSheetName).Select
    colRowLabel = 1
    rowDate = GetTargetStringRow(Columns(colRowLabel), "Date")

    ' ゴンペルツ演算
    Call PBForSim(strInSheetName, _
                  strAnalyzeSheetName, _
                  colRowLabel, _
                  rowDate)

End Sub

' ゴンペルツ演算
Private Sub PBForSim(ByVal strInSheetName As String, _
                     ByVal strAnalyzeSheetName As String, _
                     ByVal colRowLabel As Long, _
                     ByVal rowDate As Long)
    Dim varSettingParam As Variant
    Dim strByWeekOrDay As String
    Dim dtPredicted As Date
    Dim varTmp As Variant
    Dim dtStart As Date
    Dim dtEnd As Date
    Dim strDataFrequency As String
    Dim booGomperz As String
    
    Debug.Print "(PBForSim):strInSheetName = " & strInSheetName & _
                " strAnalyzeSheetName = " & strAnalyzeSheetName & _
                " colRowLabel = " & colRowLabel & _
                " rowDate = " & rowDate

    ' パラメータ取得
    strByWeekOrDay = "Day"

    'Sheets(strInSheetName).Select
    'dtStart = WorksheetFunction.Min(Rows(GetTargetStringRow(Columns(colRowLabel), "Date", True)))
    'dtPredicted = WorksheetFunction.Max(Rows(GetTargetStringRow(Columns(colRowLabel), "Date", True)))
    'dtEnd = Cells(GetTargetStringRow(Columns(colRowLabel), "Current Report Date") + 1, colRowLabel).Value
    Sheets(strConstJIRASheetName).Select
    dtStart = WorksheetFunction.Min(Columns(GetTargetStringColumn(Rows(1), "Created")))
    dtPredicted = WorksheetFunction.Max(Columns(GetTargetStringColumn(Rows(1), "Created")))
    dtEnd = dtPredicted
    Debug.Print "(PBForSim):dtStart = " & dtStart & " dtPredicted = " & dtPredicted & _
                " dtEnd = " & dtEnd

    If (DateDiff("d", dtStart, dtEnd) <= 0) Then
        If (MsgBox("2日以上のバグのサンプルが必要です。" & vbCr & _
                "'Start Report Date','Current Report Date'の設定を見直してください。", vbExclamation) = vbOK) Then
            Exit Sub
        End If
    End If

    ' 計算シート生成
    同名のワークシートを削除 strAnalyzeSheetName
    同名のワークシートが無ければ追加 strAnalyzeSheetName

    ' 日毎のバグを取得
    Call GetDailyTestsData(strInSheetName, _
                           strAnalyzeSheetName, _
                           dtStart, _
                           dtEnd, _
                           dtPredicted, _
                           strByWeekOrDay)

End Sub

' データの落とし込み
Private Sub GetDailyTestsData(ByVal strInSheetName As String, _
                              ByVal strOutSheetName As String, _
                              ByVal dtStart As Date, _
                              ByVal dtEnd As Date, _
                              ByVal dtPredicted As Date, _
                              ByVal strByWeekOrDay As String)
    ' Total Tests Planned
    Call EndDayWrite(strOutSheetName, _
                     dtStart, _
                     dtPredicted, _
                     strByWeekOrDay, _
                     1)

    Call ActualSamplingNoWrite(strOutSheetName, 1, 1, 2)

    Call NumberOfDailyTestsWrite(strInSheetName, _
                                 1, _
                                 "Total Tests Planned", _
                                 strOutSheetName, _
                                 1, _
                                 1, _
                                 1, _
                                 3)
    ' Planned Pass
    Call NumberOfDailyTestsWrite(strInSheetName, _
                                 1, _
                                 "Planned Pass", _
                                 strOutSheetName, _
                                 1, _
                                 1, _
                                 1, _
                                 4)

    ' Estimated Weekly Pass Rate %
    Call NumberOfDailyTestsWrite(strInSheetName, _
                                 1, _
                                 "Estimated * Pass Rate", _
                                 strOutSheetName, _
                                 1, _
                                 1, _
                                 1, _
                                 6)
    ' "Passed"の日付は"Total Tests Planned"の開始は同じで開始から終了の間に含まれるので削除
    Columns(5).Delete

    ' バーンダウンに変更
    Dim i As Integer
    Dim intEndRow As Integer
    Dim intTotalTestsPlannedCount As Integer
    ' テスト項目の総数を取得(途中で変わる可能性があるので最終日の値を取得)
    ' テスト総項目数
    intEndRow = GetTargetColumnEndRow(1)
    intTotalTestsPlannedCount = Cells(intEndRow, 3).Value

    ' バグ密度
    Dim intPlannedPassMinRow As Integer
    Dim intPlannedPassMaxRow As Integer
    Dim dblEstimatedPassRate As Double

    For i = 2 To intEndRow
        If ((Cells(i, 4) <> 0) And (Cells(i, 4) <> Cells(i, 3))) Then
            If (intPlannedPassMinRow = 0) Then
                intPlannedPassMinRow = i
            End If
            intPlannedPassMaxRow = i
        Else
            If (intPlannedPassMaxRow > 0) Then
                intPlannedPassMaxRow = intPlannedPassMaxRow - 1
                Exit For
            End If
        End If
    Next
    dblEstimatedPassRate = 1 - WorksheetFunction.Average(Range(Cells(intPlannedPassMinRow, 5), Cells(intPlannedPassMaxRow, 5)))

    ' 予定バグ数
    Dim dblPlannedBug As Long
    dblPlannedBug = intTotalTestsPlannedCount * dblEstimatedPassRate

    ' homeシートに反映
    Sheets(strConstConfigSheetName).Select
    Range("TotalTestPlan").Value = intTotalTestsPlannedCount
    Range("AverageBugDensity").Value = dblEstimatedPassRate
    Range("PlannedBug").Value = dblPlannedBug

End Sub


' 実績日の書き込み
Private Sub EndDayWrite(ByVal varInSheet As Variant, _
                        ByVal dtStart As Date, _
                        ByVal dtEnd As Date, _
                        ByVal strByWeekOrDay As String, _
                        ByVal intOutTargetColumn As Integer, _
                        Optional ByVal booCheck As Boolean = True)
    Dim i As Integer

    Call TargetSheetSelect(varInSheet)

    If (strByWeekOrDay = "Day") Then
        Cells(1, intOutTargetColumn).Value = "日付"

        For i = 0 To DateDiff("d", dtStart, dtEnd)
            Cells(i + 2, intOutTargetColumn).Value = DateAdd("d", i, dtStart)
        Next
    Else
        Cells(1, intOutTargetColumn).Value = "日付"

        For i = 0 To DateDiff("w", dtStart, dtEnd)
            Cells(i + 2, intOutTargetColumn).Value = DateAdd("d", 7 * i, dtStart)
        Next
    End If

End Sub

' テスト項目データ取得
Private Sub NumberOfDailyTestsWrite(ByVal strInSheetName As String, _
                                    ByVal intHeaderColumn As Integer, _
                                    ByVal strTargetString As String, _
                                    ByVal strOutSheetName As String, _
                                    ByVal intTargetColumn As Integer, _
                                    ByVal intHeaderRow As Integer, _
                                    ByVal intTargetRowColumnOffset As Integer, _
                                    ByVal intOutTargetColumn As Integer)
    Dim i, j As Integer
    Dim intStartRow As Integer
    Dim intCurRow As Integer
    Dim intLastRow As Integer
    Dim intTargetRow As Integer
    Dim dtTargetDate As Date

    ' Test Dataシートの対象行を抽出
    Sheets(strInSheetName).Select
    intTargetRow = GetTargetStringRow(Columns(intHeaderColumn), strTargetString)

    ' 出力シート
    Sheets(strOutSheetName).Select
    ' 日付行のHeaderを除く開始と終わりの行を抽出
    intStartRow = intHeaderRow + 1
    intCurRow = intStartRow
    intLastRow = GetTargetColumnEndRow(intTargetColumn)
    ' ヘッダを記載
    Cells(1, intOutTargetColumn).Value = strTargetString

    ' Test Data シートの対象行の値を出力シートへコピー
    ' intTargetColumnは日付
    Dim rowDate As Integer
    Dim intDateEndColumn As Integer

    Sheets(strInSheetName).Select
    rowDate = GetTargetStringRow(Columns(intHeaderColumn), "Date", True)
    intDateEndColumn = GetUsedRangeEndColumn

    For i = intTargetRowColumnOffset + 1 To GetUsedRangeEndColumn
        If (Trim$(Cells(rowDate, i)) = "") Then
            intDateEndColumn = i - 1
            Exit For
        End If
    Next
    
    Sheets(strOutSheetName).Select

    For i = intTargetRowColumnOffset + 1 To intDateEndColumn
        dtTargetDate = Sheets(strInSheetName).Cells(rowDate, i)
        'varTargetData = Sheets(strInSheetName).Cells(intTargetRow, i)
        For j = intCurRow To intLastRow
            If (DateDiff("d", Cells(j, intTargetColumn), dtTargetDate) = 0) Then
                If (Sheets(strInSheetName).Cells(intTargetRow, i).Value <> "") Then
                    Cells(j, intOutTargetColumn).Value = Trim$(Sheets(strInSheetName).Cells(intTargetRow, i).Value)
                ' 値の入ってない行は0で埋める
                Else
                    Cells(j, intOutTargetColumn).Value = 0
                End If
                intCurRow = j
                Exit For
            End If
        Next
    Next

    ' 空白を前日の値で穴埋め（初期値があるのが前提)
    Sheets(strOutSheetName).Select
    intLastRow = GetTargetColumnEndRow2(intOutTargetColumn)
    For i = intStartRow To intLastRow
        If (Cells(i, intOutTargetColumn) = "") Then
            Cells(i, intOutTargetColumn) = Cells(i - 1, intOutTargetColumn)
        End If
    Next

End Sub

' 実績サンプリング番号の書き込み
'　経過日数X
'  注：0から始まる
Private Sub ActualSamplingNoWrite(ByVal varInSheet As Variant, _
                                  ByVal intTargetColumn As Integer, _
                                  ByVal intHeaderRow As Integer, _
                                  ByVal intOutTargetColumn As Integer)
    Dim i As Integer
    Dim intStartRow As Integer
    Dim intLastRow As Integer

    intStartRow = intHeaderRow + 1
    intLastRow = GetTargetColumnEndRow(intTargetColumn)

    Call TargetSheetSelect(varInSheet)

    Cells(1, intOutTargetColumn).Value = "経過日数X"

    For i = intStartRow To intLastRow
        Cells(i, intOutTargetColumn).Value = i - intHeaderRow - 1
    Next

End Sub

' ロジスティック曲線算出
Private Sub LogisticCalc(ByVal strInSheetName As String, _
                         ByVal intTargetColumn As Integer, _
                         ByVal intHeaderRow As Integer, _
                         ByVal intOutTargetColumn As Integer, _
                         ByVal booGomperz As Boolean)
    Dim intStartRow As Integer
    Dim intLastRow As Integer
    Dim dblA As Double
    Dim dblB As Double
    Dim dblC As Double
    Dim strCalc As String
    Dim i As Integer

    If (booGomperz) Then
        ' 注：名前定義してない
        dblA = Sheets(strConstGomperzAnalyzeSheetName).Cells(13, 12).Value
    Else
        dblA = Sheets(strConstConfigSheetName).Range("LogisticsParamaterA").Value
    End If

    dblB = Sheets(strConstConfigSheetName).Range("LogisticsParamaterB").Value
    dblC = Sheets(strConstConfigSheetName).Range("LogisticsParamaterC").Value
    
    Sheets(strInSheetName).Select

    intStartRow = intHeaderRow + 1
    intLastRow = GetTargetColumnEndRow(intTargetColumn)

    If (booGomperz) Then
        Cells(1, intOutTargetColumn).Value = "バグ数(Gomperz)"
    Else
        Cells(1, intOutTargetColumn).Value = "バグ数(指標値)"
    End If

    ' a/{1+b*EXP(-c*X)}
    For i = intStartRow To intLastRow
        strCalc = "=" & dblA & "/(1 + " & dblB & "*EXP(-" & dblC & "*" & Cells(i, intTargetColumn).Value & "))"
        Cells(i, intOutTargetColumn).Value = Evaluate(strCalc)
    Next

End Sub


'　検索文字列の行番号を取得
Private Function GetTargetStringRow(ByVal rangeTarget As Range, _
                                    ByVal strTargetString As String, _
                                    Optional ByVal boofull As Boolean = False) As Integer
    Dim intReturn As Integer
    Dim Foundcell As Range

    If (boofull) Then
        Set Foundcell = rangeTarget.Find(what:=strTargetString, _
                                        LookIn:=xlValues, _
                                        LookAt:=xlWhole, _
                                        MatchCase:=False, _
                                        MatchByte:=False)
    Else
        Set Foundcell = rangeTarget.Find(what:=strTargetString, _
                                        LookIn:=xlValues, _
                                        LookAt:=xlPart, _
                                        MatchCase:=False, _
                                        MatchByte:=False)
    End If

    If Foundcell Is Nothing Then
        intReturn = 0
    Else
        intReturn = Foundcell.Row
    End If

    GetTargetStringRow = intReturn

End Function

'　検索文字列の列番号を取得
Private Function GetTargetStringColumn(ByVal rangeTarget As Range, _
                                       ByVal strTargetString As String) As Long
    Dim intReturn As Long
    Dim Foundcell As Range

    Set Foundcell = rangeTarget.Find(what:=strTargetString, _
                                     LookIn:=xlValues, _
                                     LookAt:=xlPart, _
                                     MatchCase:=False, _
                                     MatchByte:=False)
    If Foundcell Is Nothing Then
        intReturn = 0
    Else
        intReturn = Foundcell.Column
    End If

    GetTargetStringColumn = intReturn

End Function

Private Sub 同名のワークシートが無ければ追加(ByVal strOutSheetName As String)
    Dim i As Worksheet

    For Each i In Worksheets
        If (UCase(i.Name) = UCase(strOutSheetName)) Then
            Sheets(strOutSheetName).Select
            Exit Sub
        End If
    Next

    Sheets.Add After:=Sheets(Sheets(Sheets.Count).Name)
    ActiveSheet.Name = strOutSheetName

End Sub

Private Sub 同名のワークシートを削除(ByVal strOutSheetName As String)
    Dim i As Worksheet

    For Each i In Worksheets
        If (UCase(i.Name) = UCase(strOutSheetName)) Then
            Application.DisplayAlerts = False
            i.Delete
            Application.DisplayAlerts = True
            Exit For
        End If
    Next

End Sub

Private Sub 同名のワークシートを非表示(ByVal strOutSheetName As String)
    Dim i As Worksheet

    For Each i In Worksheets
        If (UCase(i.Name) = UCase(strOutSheetName)) Then
            i.Visible = False
            Exit For
        End If
    Next

End Sub

Private Sub 同名のグラフを削除(ByVal strOutGraphName As String)
    Dim i As Chart

    For Each i In Charts
        If (UCase(i.Name) = UCase(strOutGraphName)) Then
            Application.DisplayAlerts = False
            i.Delete
            Application.DisplayAlerts = True
            Exit For
        End If
    Next

End Sub

Private Sub TargetSheetSelect(ByVal varInSheet As Variant)

    If VarType(varInSheet) = vbString Then
        Sheets(varInSheet).Select
    Else
        varInSheet.Parent.Activate
        varInSheet.Select
    End If

End Sub

Private Function GetTargetColumnEndRow(ByVal intColumn As Integer, _
                                       Optional ByVal intRow As Integer = 1) As Integer
    Dim ret As Integer

    On Error GoTo overflow
    ' 空白時は、2回実行、オーバーフロー時は1
    ' integerの範囲を超えたらオーバーフローとする
    If (Cells(intRow, intColumn).Value = "") Then
        ret = Cells(intRow, intColumn).End(xlDown).Row
        ret = Cells(ret, intColumn).End(xlDown).Row
    Else
        ret = Cells(intRow, intColumn).End(xlDown).Row
    End If

    GetTargetColumnEndRow = ret
    Exit Function

overflow:
    GetTargetColumnEndRow = intRow

End Function

Private Function GetTargetColumnEndRow2(ByVal intColumn As Integer) As Integer
    Dim ret As Integer

    On Error GoTo overflow
    ' UsedRangeから検索、文字列があればその値
    ' 文字列が無ければ上に向かって移動
    If (Cells(GetUsedRangeEndRow, intColumn).Value <> "") Then
        ret = GetUsedRangeEndRow
    Else
        ret = Cells(GetUsedRangeEndRow, intColumn).End(xlUp).Row
    End If

    GetTargetColumnEndRow2 = ret
    Exit Function

overflow:
    GetTargetColumnEndRow2 = 1

End Function

Private Function GetUsedRangeEndRow() As Integer
    GetUsedRangeEndRow = Cells(1, 1).SpecialCells(xlCellTypeLastCell).Row
End Function

Private Function GetUsedRangeEndColumn() As Integer
    GetUsedRangeEndColumn = Cells(1, 1).SpecialCells(xlCellTypeLastCell).Column
End Function

