Attribute VB_Name = "GetTestData"
Option Explicit


Sub GetTestData()
    Dim strFilePath As String
    Dim strFileName As String
    Dim strSheetName As String
    Dim strCopyedSheetName As String
    Dim strRange As String
    Dim strConfigSheetName As String
    Dim booExistSheet As Boolean
    Dim i As Worksheet
    Dim strJobName As String
    Dim booEndMessage As Boolean
    
    Application.ScreenUpdating = False
    
    strJobName = "PB曲線&ゴンペルツ合成"
    JobBegin (strJobName & "中...")
    
    Sheets(strConstConfigSheetName).Select
    strConfigSheetName = strConstConfigSheetName
    
    strFilePath = Range("TestDataFilePath").Value
    strSheetName = Range("TestDataSheetName").Value
    ' シート削除
    同名のワークシートを削除 strSheetName
    同名のワークシートが無ければ追加 strSheetName
    'Sheets(strSheetName).Move After:=Sheets(strConfigSheetName)
    
    ' FileOpen
    If Dir(strFilePath) <> "" Then
        Workbooks.Open strFilePath
    Else
        MsgBox "存在しないパスが設定されています。" & vbCr & "TestChartのファイルパス設定を確認してください。", vbCritical, "Error"
        Sheets(strConstConfigSheetName).Select
        strJobName = ""
        booEndMessage = True
        JobEnd strJobName & "処理終了", MsgBoxShow:=booEndMessage
        End
    End If
    
    ' SheetCopy
    strFileName = Dir(strFilePath)
    Workbooks(strFileName).Activate
    booExistSheet = False
    For Each i In Worksheets
        If (UCase(i.Name) = UCase(strSheetName)) Then
            booExistSheet = True
            Exit For
        End If
    Next
    If booExistSheet = False Then
        MsgBox "存在しないシートが設定されています。" & vbCr & "TestChartのシート設定を確認してください。", vbCritical, "Error"
        Application.DisplayAlerts = False
        Workbooks(strFileName).Close
        Application.DisplayAlerts = True
        ThisWorkbook.Activate
        Sheets(strConstConfigSheetName).Select
        strJobName = ""
        booEndMessage = True
        JobEnd strJobName & "処理終了", MsgBoxShow:=booEndMessage
       End
    End If
    
    Sheets(strSheetName).Select
    strRange = ActiveSheet.UsedRange.Address
    ActiveSheet.UsedRange.Select
    Selection.Copy
    
    ThisWorkbook.Activate
    Cells(1, 1).Select
    Selection.PasteSpecial xlPasteValues
    
    ' FileClose
    Application.DisplayAlerts = False
    Workbooks(strFileName).Close
    Application.DisplayAlerts = True

    ' シート名変更
    ThisWorkbook.Activate
    同名のワークシートを削除 strConstTestDataSheetName
    Sheets(strSheetName).Select
    ActiveSheet.Name = strConstTestDataSheetName
    
    ' 1行目削除
    Rows(1).Delete
    
    strJobName = ""
    booEndMessage = False
    JobEnd strJobName & "処理終了", MsgBoxShow:=booEndMessage

End Sub


Private Sub 同名のワークシートを再表示(ByVal strOutSheetName As String)
    Dim i As Worksheet
    For Each i In Worksheets
        If (UCase(i.Name) = UCase(strOutSheetName)) Then
            Sheets(strOutSheetName).Visible = xlSheetVisible
        End If
    Next
End Sub


Private Sub 同名のワークシートをクリア(ByVal strOutSheetName As String)
    Dim i As Worksheet
    For Each i In Worksheets
        If (UCase(i.Name) = UCase(strOutSheetName)) Then
            Sheets(strOutSheetName).Select
            Cells.Select
            Selection.Clear
            Exit For
        End If
    Next
End Sub

Private Sub 同名のワークシートが無ければ追加(ByVal strOutSheetName As String)
    Dim i As Worksheet
    For Each i In Worksheets
        If (UCase(i.Name) = UCase(strOutSheetName)) Then
            Sheets(strOutSheetName).Select
            Exit Sub
        End If
    Next
    Sheets.Add Before:=Sheets(Sheets(Sheets.Count).Name)
    ActiveSheet.Name = strOutSheetName
End Sub

Private Sub 同名のワークシートを削除(ByVal strOutSheetName As String)
    Dim i As Worksheet
    For Each i In Worksheets
        If (UCase(i.Name) = UCase(strOutSheetName)) Then
            Application.DisplayAlerts = False
            i.Delete
            Application.DisplayAlerts = True
            Exit For
        End If
    Next
End Sub

Private Sub 同名のワークシートを非表示(ByVal strOutSheetName As String)
    Dim i As Worksheet
    For Each i In Worksheets
        If (UCase(i.Name) = UCase(strOutSheetName)) Then
            i.Visible = False
            Exit For
        End If
    Next
End Sub

Private Sub JobBegin(Optional strMsg As String = "")

    varSaveCalculation = Application.Calculation
    varSaveScreenUpdating = Application.ScreenUpdating
    
    Application.Calculation = xlCalculationManual
    
    If strMsg <> "" Then
        Application.StatusBar = strMsg
    End If
    
    Application.ScreenUpdating = False
    
End Sub

Private Sub JobEnd(Optional strMsg As String = "", Optional MsgBoxShow As Boolean = False)

    If Not IsEmpty(varSaveCalculation) Then
        If Application.Calculation <> varSaveCalculation Then
            Application.Calculation = varSaveCalculation
        End If
        varSaveCalculation = Empty
    End If
    If Not IsEmpty(varSaveScreenUpdating) Then
        Application.ScreenUpdating = varSaveScreenUpdating
        varSaveScreenUpdating = Empty
    End If
    
    If (strMsg <> "") And (MsgBoxShow = True) Then
        Application.StatusBar = Replace(strMsg, vbCrLf, " ")
        MsgBox strMsg, vbOKOnly
    End If
    
    Application.StatusBar = False
End Sub

