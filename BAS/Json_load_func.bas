Attribute VB_Name = "Json_load_func"
Sub Json_load(z As Integer, assign_name As String)
Attribute Json_load.VB_ProcData.VB_Invoke_Func = " \n14"
'
' Json_load Macro
'
' 追加:2016.08.01
' 更新:2016.08.02
' 更新:2017.05.31 ver0.2.38
'

  ' Read .json file
  Dim FSO As New FileSystemObject
  'Dim JsonTS As TextStream
  Dim JsonText, Statelog As String
  Dim Parsed As Dictionary
  Dim MaxRow, MaxCol As Integer
  Dim wsJira As Worksheet

  Set wsJira = Worksheets(gJIRASheetName)
  MaxCol = wsJira.Range("A1").End(xlToRight).Column

  '格納する変数を定義します｡
  Dim strPath, Target As String

  '変数にパスを格納します。
  strPath = ActiveWorkbook.Path

  'UTF-8として読み込む
  Target = strPath & "\JiraStatus.js"
  With CreateObject("ADODB.Stream")
    .Charset = "UTF-8"
    .Open
    .LoadFromFile Target
    JsonText = .ReadText
    .Close
  End With

  Dim i, ii, y As Long
  Dim Json As Object
  Dim intRejectedCount As Integer
  Dim strAutor As String

  Set Json = JsonConverter.ParseJson(JsonText)

  'Debug.Print Json("changelog")("histories").Count

  Statelog = "" '空にしておく
  Statelog_email = ""

  y = 0
  intRejectedCount = 0
  strAutor = ""

  For i = 1 To Json("changelog")("histories").Count
    '
    'Debug.Print Json("changelog")("histories")(i)("id") ' id
    'Debug.Print Json("changelog")("histories")(i)("created") ' created
    '
    For ii = 1 To Json("changelog")("histories")(i)("items").Count ' itemsCount
      'Debug.Print Json("changelog")("histories")(i)("items")(ii)("field") ' field
      If Json("changelog")("histories")(i)("items")(ii)("field") = "status" Then
        Statelog = Left(Json("changelog")("histories")(i)("created"), 10)

        'Debug.Print Json("changelog")("histories")(i)("items")(ii)("fromString") ' fromString
        Statelog = Statelog & ":status:" & Json("changelog")("histories")(i)("items")(ii)("fromString")
        'Debug.Print Json("changelog")("histories")(i)("items")(ii)("toString") ' toString
        Statelog = Statelog & "->" & Json("changelog")("histories")(i)("items")(ii)("toString")

        ' Log格納
        wsJira.Cells(z, MaxCol + y) = Statelog
        ' 掛線追加
        wsJira.Cells(z, MaxCol + y).Borders.LineStyle = xlDash

        ' カウント
        If (InStr(LCase$(Statelog), "open") = 0) And (InStr(LCase$(Statelog), "->in development") <> 0) Then
          intRejectedCount = intRejectedCount + 1
        End If
        ' 初期化
        Statelog = ""
        y = y + 1

        If Json("changelog")("histories")(i)("items")(ii)("toString") = "ready for test" Then
          '★★ Statusを変えた人（Display上の名称）
          strAutor = Json("changelog")("histories")(i)("author")("displayName")

          ' Log格納（常に上書きして最後のStatus変更の人にする）
          wsJira.Cells(z, MaxCol - 3) = strAutor
          ' 掛線追加
          wsJira.Cells(z, MaxCol - 3).Borders.LineStyle = xlDash
        End If
      End If
    Next
  Next

  ' Reject回数のCount
  wsJira.Cells(z, MaxCol - 1) = intRejectedCount
  ' 掛線追加
  wsJira.Cells(z, MaxCol - 1).Borders.LineStyle = xlDash

  'statelogに入っていないまたは入っているのがJira AdminだったらAssignee側の名前を使う
  If strAutor = "" Or strAutor = "Jira Admin" Then
    '元々入っているAssigneeをわりあてる
    wsJira.Cells(z, MaxCol - 3) = assign_name
    ' 掛線追加
    wsJira.Cells(z, MaxCol - 3).Borders.LineStyle = xlDash
  End If

  'Debug.Print JsonConverter.ConvertToJson(Json)
  'Debug.Print JsonConverter.ConvertToJson(Json, Whitespace:=2)

End Sub
