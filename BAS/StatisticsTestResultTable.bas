Attribute VB_Name = "StatisticsTestResultTable"
Option Explicit

Private Const strRedmineSheetName As String = "TestItem" ' Redmineダウンロードシート(トラッカーID=試験管理)
Private Const strTestResultTableName As String = "TestResultTable"
Private Const strTestResultTableSheetName As String = strTestResultTableName
Private Const strTestResultPivotTableName As String = "TestResultPivotTable"
Private Const strTestResultPivotTableSheetName As String = strTestResultPivotTableName
Private Const strJobName As String = "試験結果集計"

Sub TestResultTableExecute(Optional ByVal booEndMessage As Boolean = True)
    Application.ScreenUpdating = False

    MsgBox "この機能は、現在、使用できません。", vbCritical
    Exit Sub '注）「ST2_合同試験項目書」未対応のため、2016/5/30 機能停止。

    ' Statusバー表示
    StatisticsLibrary2.JobBegin (strJobName & "中...")

    ' 作業用シート生成
    StatisticsTestResultTable.SheetGenerate strRedmineSheetName, strTestResultTableSheetName
    ' テーブル作成
    StatisticsTestResultTable.TestResultTableGenerate strTestResultTableSheetName, strTestResultTableName
    ' Pivotテーブル作成
    StatisticsTestResultTable.TestResultPivotTableGenerate strTestResultTableName, strTestResultPivotTableSheetName, strTestResultPivotTableName
    ' 作業用シート非表示
    StatisticsLibrary.同名のワークシートを非表示 strTestResultTableName

    ' Statusバー表示
    StatisticsLibrary2.JobEnd strJobName & "処理終了", MsgBoxShow:=booEndMessage

End Sub

' 作業用シート生成
Private Sub SheetGenerate(ByVal strInSheetName As String, _
                          ByVal strOutSheetName As String)
    Sheets(strInSheetName).Select

    StatisticsLibrary.同名のワークシートを削除 strOutSheetName
    StatisticsLibrary.同名のワークシートが無ければコピー strInSheetName, strOutSheetName

End Sub

' 問題管理 - テーブル生成
Private Sub TestResultTableGenerate(ByVal strInSheetName As String, _
                                    ByVal strOutTableName As String)
    Dim intStartRow As Integer
    Dim intStartColumn As Integer
    Dim intEndRow As Integer
    Dim intEndColumn As Integer

    Sheets(strInSheetName).Select
    intStartRow = 2
    intStartColumn = 1
    intEndRow = GetTargetColumnEndRow(1)
    intEndColumn = GetTargetRowEndColumn(intStartRow)
    StatisticsLibrary.TableGenerate strInSheetName, _
                                    strOutTableName, _
                                    intStartRow, _
                                    intStartColumn, _
                                    intEndRow, _
                                    intEndColumn

End Sub

' ピボットテーブル生成
Private Sub TestResultPivotTableGenerate(ByVal strInTableName As String, _
                                         ByVal strOutSheetName As String, _
                                         ByVal strOutTableName As String)
    StatisticsLibrary.PivotTableGenerate strInTableName, _
                                         strOutSheetName, _
                                         strOutTableName

    ' パラメータ選択
    ActiveSheet.PivotTables(strOutSheetName).AddDataField ActiveSheet. _
        PivotTables(strOutSheetName).PivotFields("大項目名"), "データの個数 / 大項目名", xlCount
    With ActiveSheet.PivotTables(strOutSheetName).PivotFields("開発項目番号")
        .Orientation = xlRowField
        .Position = 1
    End With
    With ActiveSheet.PivotTables(strOutSheetName).PivotFields("大項目名")
        .Orientation = xlRowField
        .Position = 2
    End With
    With ActiveSheet.PivotTables(strOutSheetName).PivotFields("試験結果")
        .Orientation = xlColumnField
        .Position = 1
    End With

    ' 書式設定
    With ActiveSheet.PivotTables(strOutSheetName)
        .TableStyle2 = "PivotStyleMedium8"
        .ColumnGrand = True
        .RowGrand = False
        .SubtotalLocation xlAtBottom
    End With

End Sub

