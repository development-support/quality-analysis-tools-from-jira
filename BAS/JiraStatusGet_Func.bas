Attribute VB_Name = "JiraStatusGet_Func"
Sub JiraStatusGet()
Attribute JiraStatusGet.VB_ProcData.VB_Invoke_Func = " \n14"
  '
  ' JiraStatusGet Macro
  '
  ' 追加:2016.08.01
  ' 更新:2016.08.02
  ' 更新:2017.06.09  ver0.2.38
  '
  Dim url As String
  Dim Filename As String
  Dim MaxRow, MaxCol, z As Integer
  Dim assign_name As String

  Dim Key As String 'Hash用
  Dim Sheet1 As Worksheet
  Dim wsJira As Worksheet

  Application.ScreenUpdating = False
    
  '人名→会社をHashに入れて使う
  Set dicT = CreateObject("Scripting.Dictionary") ' Hash
  Set Sheet1 = Worksheets("会社別リスト") 'シート名は仮
  Set wsJira = Worksheets(gJIRASheetName)

  MaxRow = Sheet1.Cells(Rows.Count, 1).End(xlUp).Row '会社別リストの最終行
  With Sheet1
    For r = 2 To MaxRow ' 最終行
      Key = .Cells(r, 1).Value ' 人名（1列目固定)
      If Not dicT.Exists(Key) Then
        dicT.Add Key, .Cells(r, 2).Value '会社名(2列目固定）
      End If
    Next
  End With

  'クリア範囲を指定
  wsJira.Activate
  wsJira.Range("A1") = " "

  '使用済み最終行
  With ActiveSheet.UsedRange
    MaxRow = .Find("*", , xlFormulas, , xlByRows, xlPrevious).Row
    MaxCol = .Find("*", , xlFormulas, , xlByColumns, xlPrevious).Column
  End With

  ' 列名入力
  wsJira.Cells(1, MaxCol + 1) = "Author or Assignee"
  wsJira.Cells(1, MaxCol + 2) = "Company"
  wsJira.Cells(1, MaxCol + 3) = "Rejected Count"
  wsJira.Cells(1, MaxCol + 4) = "StatusLog"
  ' 掛線追加
  wsJira.Cells(1, MaxCol + 1).Borders.LineStyle = xlDash
  wsJira.Cells(1, MaxCol + 2).Borders.LineStyle = xlDash
  wsJira.Cells(1, MaxCol + 3).Borders.LineStyle = xlDash
  wsJira.Cells(1, MaxCol + 4).Borders.LineStyle = xlDash

  'MsgBox MaxCol
    
  '変数にパスを格納します。
  strPath = ""
  strPath = ActiveWorkbook.Path
  Filename = "\JiraStatus.js"
  strPath = strPath & Filename
    
  For i = 1 To MaxCol
    If wsJira.Cells(1, i) = "Assignee" Then
      assign_col = i
      Exit For
    End If
  Next

  For i = 2 To MaxRow
    'If Worksheets("Problem2").Cells(i, 4) = "Epic" Or Worksheets("Problem2").Cells(i, 4) = "Task" Or Worksheets("Problem2").Cells(i, 4) = "Story" Or Worksheets("Problem2").Cells(i, 4) = "Sub-task" Then

    With CreateObject("Wscript.Shell")
      ret = .Run("curl -Lkv -o " & strPath & _
                 " -v -H ""Content-Type: application/json"" -X GET -u okuda:lc2011SDAP ""http://rtx-swtl-jira.fnc.net.local/rest/api/2/issue/" & _
                 wsJira.Cells(i, 2) & "?expand=changelog&fields=status", 7, True)
    End With

    z = i
    assign_name = wsJira.Cells(i, assign_col)

    ' Module15へ
    Call Json_load(z, assign_name)

    'ここで会社を入れる
    Key = wsJira.Cells(i, MaxCol + 1)
    If dicT.Exists(Key) Then
      wsJira.Cells(i, MaxCol + 2) = dicT(Key) '会社名
    Else
      wsJira.Cells(i, MaxCol + 2) = "none"  '変換できなかった
    End If

    ' 掛線追加
    wsJira.Cells(i, MaxCol + 2).Borders.LineStyle = xlDash
  Next

End Sub
