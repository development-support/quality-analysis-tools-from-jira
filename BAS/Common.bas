Attribute VB_Name = "Common"
'************************************************
'  Version  2016.10.14  Create New
'  Version  2016.10.24  進捗率の計算を修正
'                       バグ習熟予測(gomperz)に名称変更
'  Version  2016.11.02  Module:CalcEstimatedBugFromTestChartの"Estimated Weely Pass Rate %"の文字列判断を→ "Estimated * Pass Rate"に変更
'                       Module:StatisticsGomperzForJiraのdtEndの指定を"Current Report Date"の指定日に変更
'  Version  2016.11.02  工程選択を変更
'           2016.11.10  Module:Common 問処Close数と残りを無くした代わりに、TestChart総項目数とゴンペルツ関連を追加
'                       Module:StatisticsGomperzForJira "Current Report Date"迄のリスト生成に変更
'                       Module:StatisticsGomperzForJira "How-Found"=未設定に対応
'                       Module:MargeTable,Module:StatisticsOtherCalc 表記の日本語<->英語対応の為変更
'  Version  2016.11.15  Module:MargeTable,Module:StatisticsOtherCalc 日本語表記の変更、グラフの軸を左右入れ替え
'                       Module:GetJiraData シート:Boardを削除し、FilterID直接入力に変更
'  Version  2016.11.16  Module:StatisticsGomperzForJira Created/Resolved列の日付以外のゴミを削除
'  Version  2016.11.17  Module:StatisticsOtherCalc 進捗率/Convergence Rateの桁数を修正
'  Version  2016.11.21  Module:StatisticsGomperzForJira,StatisticsOtherCalc 帳票なし/ゴンペルツ演算エラーの場合に対処
'  Version  2016.11.24  Module:StatisticsPBForTest,CalcEstimatedBugFromTestChart Dateの最終列番号抽出を修正
'  Version  2016.12.06  バグ改修予測(線形)を追加=Module:StatisticsLeastSquareMethodを追加
'  Version  2017.04.27  64bit対応 - 参照設定:Microsoft Object Library DAO 3.6を非参照
'  Version  2017.05.19  試験実施数の予実"Planed Attempts"、"Actual Attempts"の表示機能を追加
'  Version  2020.04.07  T700向けに品質分析ツールと統合、テストチャート無しでも動作するように改造
'  Version  2020.05.14  以下予測日をグラフ上に縦棒として表示する機能を追加。
'                       (1)ゴンペルツ-5%の到達日, (2)現時点のバグ問処解決予測日, (3)ゴンペルツ曲線による予測バグの解決予測日
'************************************************

Option Explicit

Public Const strConstConfigSheetName As String = "home"
Public Const strConstJIRASheetName As String = gJIRASheetName
Public Const strConstJIRASearchResultSheetName = "GomperzDataFromJIRA"
Public Const strConstGomperzAnalyzeSheetName = "Gomperz"

Public Const strConstTestDataSheetName As String = "DataFromTestChart"

Public Const strConstPBAnalyzeSheetName = "TableOfTestChart"

Public Const strConstMargeTableSheetName = "MargeTable"
Public Const strConstPBGraphSheetName = strConstConfigSheetName
Public Const strConstPBGraphName = "MargeGraph"

Public Const strConstCalcEstimatedBugSheetName = "CalcEstimatedBugFromTestChart"

Public varSaveCalculation  As Variant
Public varSaveScreenUpdating   As Variant

Private Const strSystemVersion = "2021.04.20"


' データ取得＆グラフ更新
Sub UpdateDataAndGraph()
    Dim strJobName As String
    Dim booEndMessage As Boolean

    Application.ScreenUpdating = False

    ' Versionの入力
    Sheets(strConstConfigSheetName).Range("Version").Value = strSystemVersion

    ' Worksheet全表示
    ワークシートを全表示

    ' Statusバー表示
    strJobName = "処理開始"
    JobBegin (strJobName & "中...")

    ' TestChartあり
    If (Sheets(strConstConfigSheetName).Range("TestDataFilePath").Value <> "") Then
        ' TestChartsDataの取得
        GetTestData.GetTestData
        ' Logistics演算パラメータ算出
        CalcEstimatedBugFromTestChart.CalcEstimatedBugFromTestChart
        ' TestChartDataの整理&Logistics演算
        StatisticsPBForTest.PBExecuteForSim
    Else
        If (Sheets(strConstConfigSheetName).Range("Final_Report_Date").Value = "") Then
          MsgBox "予測最終日付(Final Report Date)を入力して下さい"
          ' Statusバー表示
          strJobName = ""
          booEndMessage = False
          JobEnd strJobName
          Exit Sub
        End If

        ' TestChartデータのクリア
        Call ClearTestData
        ' JIRA最新日付〜予測最終日までのダミーTestChartデータを作成
        If (MakeDummyTestData = False) Then
          Exit Sub
        End If
    End If

    ' JIRAデータ取得
    'strJobName = "JIRAデータ取得"
    'JobBegin (strJobName & "中...")
    'GetJiraData.JiraGet

    ' ゴンペルツ演算
    StatisticsGomperzForJira.GomperzExecuteForSim

    ' 合成
    strJobName = "PB曲線&ゴンペルツ合成"
    JobBegin (strJobName & "中...")
    MargeTable.MargeTable

    ' その他の演算（図の左の項目に値を挿入)
    StatisticsOtherCalc.StatisticsOtherCalc

    ' homeに戻る
    Sheets(strConstConfigSheetName).Select
    Cells(1, 1).Select

    ' シート非表示
    '同名のワークシートを非表示 strConstJIRASheetName
    同名のワークシートを非表示 strConstJIRASearchResultSheetName
    同名のワークシートを非表示 strConstGomperzAnalyzeSheetName
    同名のワークシートを非表示 strConstTestDataSheetName
    同名のワークシートを非表示 strConstPBAnalyzeSheetName
    同名のワークシートを非表示 strConstMargeTableSheetName
    同名のワークシートを非表示 strConstCalcEstimatedBugSheetName

    ' Statusバー表示
    strJobName = ""
    booEndMessage = True
    JobEnd strJobName & "処理終了" & vbCr & "Version:" & strSystemVersion, MsgBoxShow:=booEndMessage

End Sub

' グラフ更新
Sub UpdateGraph()
    Dim strJobName As String
    Dim booEndMessage As Boolean

    Application.ScreenUpdating = False

    ' Versionの入力
    Sheets(strConstConfigSheetName).Range("Version").Value = strSystemVersion

    ' Worksheet全表示
    ワークシートを全表示

    ' Statusバー表示
    strJobName = "処理開始"
    JobBegin (strJobName & "中...")

    ' Logistics演算パラメータ算出
    CalcEstimatedBugFromTestChart.CalcEstimatedBugFromTestChart
    ' TestChartDataの整理&Logistics演算
    StatisticsPBForTest.PBExecuteForSim

    ' ゴンペルツ演算
    StatisticsGomperzForJira.GomperzExecuteForSim

    ' 合成
    strJobName = "PB曲線&ゴンペルツ合成"
    JobBegin (strJobName & "中...")
    MargeTable.MargeTable

    ' その他の演算（図の左の項目に値を挿入)
    StatisticsOtherCalc.StatisticsOtherCalc

    ' homeに戻る
    Sheets(strConstConfigSheetName).Select
    Cells(1, 1).Select

    ' シート非表示
    '同名のワークシートを非表示 strConstJIRASheetName
    同名のワークシートを非表示 strConstJIRASearchResultSheetName
    同名のワークシートを非表示 strConstGomperzAnalyzeSheetName
    同名のワークシートを非表示 strConstTestDataSheetName
    同名のワークシートを非表示 strConstPBAnalyzeSheetName
    同名のワークシートを非表示 strConstMargeTableSheetName
    同名のワークシートを非表示 strConstCalcEstimatedBugSheetName

    ' Statusバー表示
    strJobName = ""
    booEndMessage = True
    JobEnd strJobName & "処理終了" & vbCr & "Version:" & strSystemVersion, MsgBoxShow:=booEndMessage

End Sub

' TestChartから予定バグ数を算出
Sub CalcPlannedBugFromTestChart()
    Dim strJobName As String
    Dim booEndMessage As Boolean

    Application.ScreenUpdating = False
    strJobName = "予定バグ数算出"
    ' Statusバー表示
    JobBegin (strJobName & "中...")

    GetTestData.GetTestData
    CalcEstimatedBugFromTestChart.CalcEstimatedBugFromTestChart

    ' シート非表示
    同名のワークシートを非表示 strConstJIRASheetName
    同名のワークシートを非表示 strConstJIRASearchResultSheetName
    同名のワークシートを非表示 strConstGomperzAnalyzeSheetName
    同名のワークシートを非表示 strConstTestDataSheetName
    同名のワークシートを非表示 strConstPBAnalyzeSheetName
    同名のワークシートを非表示 strConstMargeTableSheetName
    同名のワークシートを非表示 strConstCalcEstimatedBugSheetName

    ' Statusバー表示
    booEndMessage = True
    JobEnd strJobName & "処理終了" & vbCr & strSystemVersion, MsgBoxShow:=booEndMessage

End Sub


Private Sub JobBegin(Optional strMsg As String = "")

    varSaveCalculation = Application.Calculation
    varSaveScreenUpdating = Application.ScreenUpdating

    Application.Calculation = xlCalculationManual

    If (strMsg <> "") Then
        Application.StatusBar = strMsg
    End If

    Application.ScreenUpdating = False

End Sub

Private Sub JobEnd(Optional strMsg As String = "", _
                   Optional MsgBoxShow As Boolean = False)

    If Not IsEmpty(varSaveCalculation) Then
        If Application.Calculation <> varSaveCalculation Then
            Application.Calculation = varSaveCalculation
        End If
        varSaveCalculation = Empty
    End If

    If Not IsEmpty(varSaveScreenUpdating) Then
        Application.ScreenUpdating = varSaveScreenUpdating
        varSaveScreenUpdating = Empty
    End If

    If (strMsg <> "") And (MsgBoxShow = True) Then
        Application.StatusBar = Replace(strMsg, vbCrLf, " ")
        MsgBox strMsg, vbOKOnly
    End If

    Application.StatusBar = False
End Sub

Private Sub ワークシートを全表示()
    Dim i As Worksheet

    For Each i In Worksheets
        Sheets(i.Name).Visible = xlSheetVisible
    Next
End Sub

Private Sub 同名のワークシートを非表示(ByVal strOutSheetName As String)
    Dim i As Worksheet

    For Each i In Worksheets
        If (UCase(i.Name) = UCase(strOutSheetName)) Then
            i.Visible = False
            Exit For
        End If
    Next
End Sub

Sub 同名のワークシートが無ければ追加(ByVal strOutSheetName As String)
    Dim i As Worksheet

    For Each i In Worksheets
        If (UCase(i.Name) = UCase(strOutSheetName)) Then
            Sheets(strOutSheetName).Select
            Exit Sub
        End If
    Next

    Sheets.Add After:=Sheets(Sheets(Sheets.Count).Name)
    ActiveSheet.Name = strOutSheetName
End Sub

Private Sub 同名のワークシートを削除(ByVal strOutSheetName As String)
    Dim i As Worksheet

    For Each i In Worksheets
        If (UCase(i.Name) = UCase(strOutSheetName)) Then
            Application.DisplayAlerts = False
            i.Delete
            Application.DisplayAlerts = True
            Exit For
        End If
    Next
End Sub

Private Function ClearTestData()
  Dim colMax As Long
  Dim rowMax As Long
  Dim i As Long
  Dim j As Long
  Dim wsTestChartData As Worksheet
  Dim wsTestChartTable As Worksheet

  Set wsTestChartData = Worksheets(strConstTestDataSheetName)
  Set wsTestChartTable = Worksheets(strConstPBAnalyzeSheetName)

  ' DataFromTestChartシートのクリア
  wsTestChartData.Activate
  colMax = Cells(1, Columns.Count).End(xlToLeft).Column
  Debug.Print "(ClearTestData):colMax = " & colMax

  ' Date行〜Weekly Pass Rate %行まで
  For i = 1 To 18
    ' 2列〜最終列まで
    For j = 2 To colMax
      Cells(i, j).Value = ""
    Next j
  Next i

  ' TableOfTestChartシートのクリア
  wsTestChartTable.Activate
  rowMax = Cells(Rows.Count, 1).End(xlUp).Row
  Debug.Print "(ClearTestData):rowMax = " & rowMax

  ' 2行〜最終行まで
  For i = 2 To rowMax
    ' 日付列〜バグ数(指標値)列まで
    For j = 1 To 8
      Cells(i, j).Value = ""
    Next j
  Next i

End Function

Private Function MakeDummyTestData() As Boolean
  Dim wshome As Worksheet
  Dim wsJIRA As Worksheet
  Dim wsTestChartData As Worksheet
  Dim wsTestChartTable As Worksheet
  Dim i As Long
  Dim cntDate As Long
  Dim cntTemp As Long
  Dim dummyRate As Long
  Dim JIRAEndDate As Date
  Dim StartReportDate As Date
  Dim CurrentReportDate As Date
  Dim booFinalReportDate As Boolean
  Dim FinalReportDate As Date

  Set wshome = Worksheets(strConstConfigSheetName)
  Set wsJIRA = Worksheets(strConstJIRASheetName)
  Set wsTestChartData = Worksheets(strConstTestDataSheetName)
  Set wsTestChartTable = Worksheets(strConstPBAnalyzeSheetName)

  wsJIRA.Activate
  JIRAEndDate = WorksheetFunction.Max(Columns(GetTargetStringColumn(Rows(1), "Created")))
  StartReportDate = WorksheetFunction.Max(Columns(GetTargetStringColumn(Rows(1), "Created"))) + 1
  CurrentReportDate = StartReportDate + 1
  booFinalReportDate = IsDate(wshome.Range("Final_Report_Date").Value)

  If (booFinalReportDate = True) Then
    FinalReportDate = wshome.Range("Final_Report_Date").Value
  Else
    MsgBox "予測最終日付(Final Report Date)の記述を見直して下さい。", vbCritical
    MakeDummyTestData = False
    Exit Function
  End If
  Debug.Print "(MakeDummyTestData):JIRAEndDate = " & JIRAEndDate & _
              " StartReportDate = " & StartReportDate & _
              " CurrentReportDate = " & CurrentReportDate & _
              " FinalReportDate = " & FinalReportDate

  ' 指定したFinalReportDateがJIRAデータの最終日付より過去の場合、エラーとする
  If (FinalReportDate < JIRAEndDate) Then
    MsgBox "予測最終日付(Final Report Date)が、JIRAデータ最終日(" & JIRAEndDate & _
           ")よりも過去になっています。" & vbCrLf & _
           "予測最終日付(Final Report Date)の日付を見直して下さい。", _
           vbCritical
    MakeDummyTestData = False
    Exit Function
  End If

  ' DataFromTestChartシートの更新
  wsTestChartData.Activate
  Cells(GetTargetStringRow(Columns(1), "Start Report Date") + 1, 1) = StartReportDate
  Cells(GetTargetStringRow(Columns(1), "Current Report Date") + 1, 1) = CurrentReportDate
  Cells(GetTargetStringRow(Columns(1), "Final Report Date") + 1, 1) = FinalReportDate

  cntDate = FinalReportDate - StartReportDate
  cntTemp = CurrentReportDate - StartReportDate
  Debug.Print "(MakeDummyTestData):cntDate = " & cntDate

  For i = 0 To cntDate
    ' Date
    Cells(1, 2 + i) = StartReportDate + i
    ' Total Tests Planned
    Cells(2, 2 + i) = cntDate + 1
    ' Original Planned Tests
    Cells(3, 2 + i) = cntDate + 1
    ' Estimated Weekly Pass Rate %
    Cells(4, 2 + i) = ((i + 2) - (i + 1)) / ((i + 2) - (i + 1))
    ' Planned Attempts
    Cells(5, 2 + i) = i + 1
    ' Planned Tests
    Cells(6, 2 + i) = i + 1
    ' Planned Pass
    Cells(7, 2 + i) = i + 1
    ' Actual Attempts
    Cells(8, 2 + i) = 0
    ' Passed
    Cells(9, 2 + i) = 0
    ' Blocked
    Cells(10, 2 + i) = 0
    ' Failed
    Cells(11, 2 + i) = 0
    ' Failed (Ready for ReTest)
    Cells(12, 2 + i) = 0
    ' Ready (Not Attempted)
    Cells(13, 2 + i) = 0
    ' Executed
    Cells(14, 2 + i) = 0
    ' % Executed
    Cells(15, 2 + i) = 0
    ' % Passed
    Cells(16, 2 + i) = 0
    ' Cumulative Pass Rate %
    Cells(17, 2 + i) = 0
    ' Weekly Pass Rate %
    Cells(18, 2 + i) = 0
  Next i

  ' TableOfTestChartシートの更新
  wsTestChartTable.Activate
  For i = 0 To cntDate
    ' 日付
    Cells(2 + i, 1) = StartReportDate + i
    ' 経過日数X
    Cells(2 + i, 2) = i
    ' Total Tests Planned
    Cells(2 + i, 3) = cntDate + 1
    ' Planned Attempts
    Cells(2 + i, 4) = cntDate - i
    ' Planned Pass
    Cells(2 + i, 5) = cntDate - i

    ' バグ数(指標値)
    dummyRate = (i + 1) / cntDate * 100
    If (dummyRate > 100) Then
      dummyRate = 100
    End If
    Cells(2 + i, 8) = dummyRate
  Next i

  For i = 0 To cntTemp
    ' Actual Attempts
    Cells(2 + i, 6) = cntDate + 1
    ' Passed
    Cells(2 + i, 7) = cntDate + 1
  Next i

  MakeDummyTestData = True

End Function

' 検索文字列の行番号を取得
Private Function GetTargetStringRow(ByVal rangeTarget As Range, _
                                    ByVal strTargetString As String, _
                                    Optional ByVal boofull As Boolean = False) As Integer
    Dim intReturn As Integer
    Dim Foundcell As Range

    If (boofull) Then
        Set Foundcell = rangeTarget.Find(what:=strTargetString, _
                                        LookIn:=xlValues, _
                                        LookAt:=xlWhole, _
                                        MatchCase:=False, _
                                        MatchByte:=False)
    Else
        Set Foundcell = rangeTarget.Find(what:=strTargetString, _
                                        LookIn:=xlValues, _
                                        LookAt:=xlPart, _
                                        MatchCase:=False, _
                                        MatchByte:=False)
    End If

    If Foundcell Is Nothing Then
        intReturn = 0
    Else
        intReturn = Foundcell.Row
    End If

    GetTargetStringRow = intReturn

End Function

' 検索文字列の列番号を取得
Private Function GetTargetStringColumn(ByVal rangeTarget As Range, _
                                       ByVal strTargetString As String) As Long
    Dim intReturn As Long
    Dim Foundcell As Range

    Set Foundcell = rangeTarget.Find(what:=strTargetString, _
                                     LookIn:=xlValues, _
                                     LookAt:=xlPart, _
                                     MatchCase:=False, _
                                     MatchByte:=False)
    If Foundcell Is Nothing Then
        intReturn = 0
    Else
        intReturn = Foundcell.Column
    End If

    GetTargetStringColumn = intReturn

End Function
