Attribute VB_Name = "StatisticsLibrary"
Option Explicit
Option Private Module

Public Enum 分割単位
    非分割 = 0
    出力ID毎 = 1
    工程毎 = 2
End Enum

'共通設定の列番号
Private Const CommonParamProcessNameColumn As Long = 1 '試験工程
Private Const CommonParamEnableColumn As Long = 2      '処理Enable
Private Const CommonParamOutIdColumn As Long = 3       '出力ID
Private Const CommonParamStartDateColumn As Long = 4   '工程開始日
Private Const CommonParamEndDateColumn As Long = 5     '工程終了予定日
Private Const CommonParamProcessCategoryColumn As Long = 6 '工程分類 ※L100,L120,JIRA用

' 工程別シート生成
'   wsInSheet         : 入力シート
'   NewSheetNamePrefix: 生成したシートのシート名の先頭に付加する文字列
Function ByProcessSheetGenerate( _
        ByVal wsInSheet As Worksheet _
        , Optional ByVal NewSheetNamePrefix As String = "" _
        , Optional ByVal OnlyEnable As Boolean = True _
        , Optional ByVal GroupBy As 分割単位 = 出力ID毎 _
        , Optional ByVal JobEndIfEmpty As Boolean = True _
        , Optional ByVal DeleteIfEmpty As Boolean = False _
        , Optional TargetProcess As Variant _
        , Optional ByRef 分割値格納配列 As Variant _
        ) As String()

    Dim intTargetColumn As Long '工程の列番号
    Dim intTargetRowOffset  As Long '入力シートのデータ開始行
    Dim intPrimaryKeyColumn As Long 'チケットID

    Dim varSettingParam As Variant
    Dim strProcessName() As String
    Dim strOutSheetName() As String
    Dim intOutSheetIndex() As Integer
    Dim intTargetColumnEndRow As Integer
    Dim strID As String
    Dim strName As String
    Dim strTmp As String
    Dim booFlag As Boolean
    Dim i As Integer
    Dim j As Integer
    Dim k As Integer
    Dim intSelectorColumn As Long

    ByProcessSheetGenerate = Split("")

    intTargetRowOffset = StatisticsLibrary2.numRedmineSheetDataRow

    ' 参照する項目の列番号を取得
    intPrimaryKeyColumn = StatisticsLibrary2.GetProbemColumnByItemName("ID") 'チケットID
    If intPrimaryKeyColumn < 1 Then
        Exit Function
    End If

    intTargetColumn = StatisticsLibrary2.GetProbemColumnByItemName("発見工程")
    If intTargetColumn < 1 Then
        Exit Function
    End If

    Call MySheetActivate(wsInSheet)
    intTargetColumnEndRow = GetTargetColumnEndRow(intPrimaryKeyColumn)
    If (intTargetColumnEndRow < intTargetRowOffset) Then
        If JobEndIfEmpty Then
            Call MsgBox("集計対象データがありません。処理を終了します。", vbExclamation)
            Exit Function
        End If
    End If

    Select Case ProblemDataBaseTypeGet()
    Case "JIRA"
        intSelectorColumn = GetDataColumn(RedmineSheetHeader(wsInSheet), "Issue Type")
        If intSelectorColumn < 1 Then
            Exit Function ' 不正な入力シート
        End If
    Case Else
        intSelectorColumn = 0
    End Select

    ' 工程名のリストを取得
    Dim strTmpList() As String
    If IsMissing(TargetProcess) Then
        strTmpList = GetProcessNameList(wsInSheet.Cells(intTargetRowOffset, intTargetColumn), intTargetColumnEndRow)
    Else
        strTmpList = TargetProcess
    End If

    If OnlyEnable Then
        j = 0
        ReDim strProcessName(0)
        For i = 1 To UBound(strTmpList)
            strName = strTmpList(i)
            If Not IsSpaceString(strName) Then
                varSettingParam = GetStringSettingParam("Common_setting", strSearchKey:=strName)
                If ArrayElementsCount(varSettingParam) < 2 Then
                    ' 未登録
                ElseIf varSettingParam(CommonParamEnableColumn) = "○" Then '「処理Enable」
                    j = j + 1
                    ReDim Preserve strProcessName(j)
                    strProcessName(j) = strName
                End If
            End If
        Next i
        If j < 1 Then
            Call MsgBox("集計対象工程の指定がありません。処理を終了します。", vbExclamation)
            Exit Function
        End If
    Else
        strProcessName = strTmpList
    End If
    Erase strTmpList

    ' 出力シート名のリストを生成
    k = 1
    ReDim intOutSheetIndex(UBound(strProcessName))
    ReDim strOutSheetName(0)
    For i = 1 To UBound(strProcessName)
        strName = strProcessName(i)
        strProcessName(i) = ConvertKeyString(strName)

        booFlag = False

        ' 工程とリンクした設定を取得
        varSettingParam = GetStringSettingParam("Common_setting", strSearchKey:=strName)
        strID = varSettingParam(CommonParamOutIdColumn) '「出力ID」
        Erase varSettingParam

        If (GroupBy = 非分割) And (k > 1) Then
            intOutSheetIndex(i) = 1
            booFlag = True

        Else
            strTmp = IIf(GroupBy <> 工程毎, strID, strName)
            strTmp = NewSheetNamePrefix & strTmp

            ' シート名にできないキャラクタ,空白を置換
            strTmp = 集計用出力シート名生成(strTmp)
            For j = 1 To UBound(strOutSheetName)
                If (strOutSheetName(j) = strTmp) Then
                    intOutSheetIndex(i) = j
                    booFlag = True
                    Exit For
                End If
            Next j
        End If

        If Not booFlag Then
            If Not IsMissing(分割値格納配列) Then
                ReDim Preserve 分割値格納配列(k)
                分割値格納配列(k) = IIf(GroupBy <> 工程毎, strID, strName)
            End If

            ReDim Preserve strOutSheetName(k)
            intOutSheetIndex(i) = k
            strOutSheetName(k) = strTmp
            k = k + 1
        End If
    Next i

    ' シート生成
    For i = 1 To UBound(strOutSheetName)
        同名のワークシートを削除 strOutSheetName(i)
        同名のワークシートが無ければ追加 strOutSheetName(i)
    Next

    ' シート毎にデータを置き直し(ヘッダ)
    For i = 1 To UBound(strOutSheetName)
        wsInSheet.Select
        Range(Cells(1, 1), Cells(intTargetRowOffset - 1, GetUsedRangeEndColumn())).Select
        Selection.Copy
        Sheets(strOutSheetName(i)).Select
        Cells(1, 1).Select
        ActiveSheet.Paste
        Application.CutCopyMode = False
    Next

    ' シート毎にデータを置き直し
    booFlag = True
    wsInSheet.Select
    For i = intTargetRowOffset To intTargetColumnEndRow
        wsInSheet.Select
        strTmp = ConvertKeyString(Cells(i, intTargetColumn).Value)
        Rows(i).Select
        Selection.Copy
        For j = 1 To UBound(strProcessName)
            If (strProcessName(j) = strTmp) Then
                k = intOutSheetIndex(j)
                If k > 0 Then
                    If intSelectorColumn > 0 Then
                        If Trim$(Cells(i, intSelectorColumn).Value) <> "Bug" Then
                            k = 0
                        End If
                    End If
                End If
                If k > 0 Then
                    booFlag = False
                    Sheets(strOutSheetName(k)).Select
                    Rows(GetTargetColumnEndRow(intPrimaryKeyColumn) + 1).Select
                    ActiveSheet.Paste
                End If
                Exit For
            End If
        Next
        Application.CutCopyMode = False
    Next

    If booFlag = True Then
        ' 有効データなし
        If JobEndIfEmpty Then
            Call MsgBox("集計対象データがありません。処理を終了します。", vbExclamation)
        End If

        If JobEndIfEmpty Or DeleteIfEmpty Then
            For i = 1 To UBound(strOutSheetName)
                同名のワークシートを削除 strOutSheetName(i)
            Next i
            ReDim strOutSheetName(0)
        End If
    End If

    ByProcessSheetGenerate = strOutSheetName

End Function

Function 分割シート取得( _
        ByVal wsInput As Worksheet, _
        ByVal strSplitByItem As String, _
        ByVal NewSheetNamePrefix As String, _
        ByRef strSplitName() As String, _
        ByRef varResult As Variant _
    ) As Variant

    Dim intTargetColumn As Long
    Dim wsProblem As Worksheet

    分割シート取得 = False

    intTargetColumn = StatisticsLibrary2.GetProbemColumnByItemName(strSplitByItem, NoMsg:=True)
    If intTargetColumn < 1 Then ' 分割項目のフィールドが無い場合は
        Set varResult = wsInput ' 元のシートを設定。
    Else
        varResult = SplitSheetGenerate( _
                    wsInput, _
                    intTargetColumn, _
                    NewSheetNamePrefix, _
                    分割値格納配列:=strSplitName)

        If UBound(varResult) < 1 Then '分割時にエラーが発生した場合は、
            Erase varResult
            Exit Function   ' Falseを返す。
        End If
    End If

    分割シート取得 = True
End Function

Sub 分割シート後処理(ByVal varTarget As Variant)
    Dim i As Long

    ' シートの削除
    If IsArray(varTarget) Then
        For i = 1 To UBound(varTarget)
            同名のワークシートを削除 varTarget(i)
        Next i
    End If

End Sub

' 分割シート生成
'   wsInSheet         : 入力シート
'   NewSheetNamePrefix: 生成したシートのシート名の先頭に付加する文字列
Function SplitSheetGenerate( _
            ByVal wsInSheet As Worksheet, _
            ByVal intTargetColumn As Long, _
            ByVal NewSheetNamePrefix As String, _
            Optional ByVal JobEndIfEmpty As Boolean = True, _
            Optional ByRef 分割値格納配列 As Variant _
        ) As String()

    Dim intTargetRowOffset  As Long '入力シートのデータ開始行
    Dim intPrimaryKeyColumn As Long 'チケットID

    Dim strSplitName() As String
    Dim strOutSheetName() As String
    Dim intOutSheetIndex() As Integer
    Dim intTargetColumnEndRow As Integer

    Dim strName As String
    Dim strTmp As String
    Dim booFlag As Boolean
    Dim i As Integer
    Dim j As Integer
    Dim k As Integer
    Dim intSelectorColumn As Long

    SplitSheetGenerate = Split("")

    intTargetRowOffset = StatisticsLibrary2.numRedmineSheetDataRow

    ' 参照する項目の列番号を取得
    intPrimaryKeyColumn = StatisticsLibrary2.GetProbemColumnByItemName("ID") 'チケットID
    If intPrimaryKeyColumn < 1 Then
        Exit Function
    End If

    wsInSheet.Select
    intTargetColumnEndRow = GetTargetColumnEndRow(intPrimaryKeyColumn)
    If (intTargetColumnEndRow < intTargetRowOffset) Then
        If JobEndIfEmpty Then
            Call MsgBox("集計対象データがありません。処理を終了します。", vbExclamation)
            Exit Function
        End If
    End If

    Select Case ProblemDataBaseTypeGet()
    Case "JIRA"
        intSelectorColumn = GetDataColumn(RedmineSheetHeader(wsInSheet), "Issue Type")
        If intSelectorColumn < 1 Then
            Exit Function ' 不正な入力シート
        End If
    Case Else
        intSelectorColumn = 0
    End Select

    ' 分割名のリストを取得
    strSplitName = UniqListGet(wsInSheet.Cells(intTargetRowOffset, intTargetColumn), intTargetColumnEndRow)

    ' 出力シート名のリストを生成
    k = 1
    ReDim intOutSheetIndex(UBound(strSplitName))
    ReDim strOutSheetName(0)
    For i = 1 To UBound(strSplitName)
        strName = strSplitName(i)
        booFlag = False

        strTmp = NewSheetNamePrefix & strName

        ' シート名にできないキャラクタ,空白を置換
        strTmp = 集計用出力シート名生成(strTmp)
        For j = 1 To UBound(strOutSheetName)
            If (strOutSheetName(j) = strTmp) Then
                intOutSheetIndex(i) = j
                booFlag = True
                Exit For
            End If
        Next j

        If Not booFlag Then
            If Not IsMissing(分割値格納配列) Then
                ReDim Preserve 分割値格納配列(k)
                分割値格納配列(k) = strName
            End If

            ReDim Preserve strOutSheetName(k)
            intOutSheetIndex(i) = k
            strOutSheetName(k) = strTmp
            k = k + 1
        End If
    Next i

    ' シート生成
    For i = 1 To UBound(strOutSheetName)
        同名のワークシートを削除 strOutSheetName(i)
        同名のワークシートが無ければ追加 strOutSheetName(i)
    Next

    ' シート毎にデータを置き直し(ヘッダ)
    For i = 1 To UBound(strOutSheetName)
        wsInSheet.Select
        Range(Cells(1, 1), Cells(intTargetRowOffset - 1, GetUsedRangeEndColumn())).Select
        Selection.Copy
        Sheets(strOutSheetName(i)).Select
        Cells(1, 1).Select
        ActiveSheet.Paste
        Application.CutCopyMode = False
    Next

    ' シート毎にデータを置き直し
    booFlag = True
    wsInSheet.Select
    For i = intTargetRowOffset To intTargetColumnEndRow
        wsInSheet.Select
        strTmp = Trim$(Cells(i, intTargetColumn).Value)
        Rows(i).Select
        Selection.Copy
        For j = 1 To UBound(strSplitName)
            If (strSplitName(j) = strTmp) Then
                k = intOutSheetIndex(j)
                If k > 0 Then
                    If intSelectorColumn > 0 Then
                        If Trim$(Cells(i, intSelectorColumn).Value) <> "Bug" Then
                            k = 0
                        End If
                    End If
                End If
                If k > 0 Then
                    booFlag = False
                    Sheets(strOutSheetName(k)).Select
                    Rows(GetTargetColumnEndRow(intPrimaryKeyColumn) + 1).Select
                    ActiveSheet.Paste
                End If
                Exit For
            End If
        Next
        Application.CutCopyMode = False
    Next

    If booFlag = True Then
        ' 有効データなし
        If JobEndIfEmpty Then
            Call MsgBox("集計対象データがありません。処理を終了します。", vbExclamation)
            For i = 1 To UBound(strOutSheetName)
                同名のワークシートを削除 strOutSheetName(i)
            Next i
            ReDim strOutSheetName(0)
         End If
    End If

    SplitSheetGenerate = strOutSheetName

End Function

' 工程名のリストを取得
Function GetProcessNameList( _
            ByVal objSrcTop As Range, _
            ByVal intSrcEndRow As Variant, _
            Optional ByVal 未設定を除く As Boolean = False _
        ) As String()

    Dim strResult() As String
    Dim i As Long

    Dim wsSrc As Worksheet
    Dim intSrcCol As Variant
    Dim intRow As Variant
    Dim strKey As String
    Dim strTmp As String
    Dim aa() As String

    ' 工程名取得
    aa = UniqListGet(objSrcTop, intSrcEndRow, KeyMode:=True, 未設定を除く:=未設定を除く)

    With objSrcTop
        Set wsSrc = .Worksheet
        intSrcCol = .Column
    End With

    ReDim strResult(UBound(aa))
    For i = 1 To UBound(aa)
        strKey = aa(i)
        For intRow = objSrcTop.Row To intSrcEndRow
            strTmp = Trim$(wsSrc.Cells(intRow, intSrcCol).Value)
            If ConvertKeyString(strTmp) = strKey Then
                strResult(i) = strTmp
                Exit For
            End If
        Next intRow
    Next i

    GetProcessNameList = strResult

End Function

' 工程名のシート名変換
Function ConvertSheetName(ByVal strInput As String) As String
    Dim strTmp As String
    strTmp = strInput
    strTmp = RegExpReplace(strTmp, "\{", "")
    strTmp = RegExpReplace(strTmp, "\}", "")
    strTmp = RegExpReplace(strTmp, "\[", "")
    strTmp = RegExpReplace(strTmp, "\]", "")
    strTmp = RegExpReplace(strTmp, "\(", "")
    strTmp = RegExpReplace(strTmp, "\)", "")
    strTmp = RegExpReplace(strTmp, "/", "")
    strTmp = RegExpReplace(strTmp, "-", "")
    strTmp = RegExpReplace(strTmp, " ", "_")

    If Len(strTmp) > 31 Then
        strTmp = Left$(strTmp, 28) & "..."
    End If

    ConvertSheetName = strTmp
End Function

' 検索キーの文字列変換
Function ConvertKeyString(strKey As String, Optional StripSpace = True) As String
    Dim strTmp As String
    Dim i   As Long

    strTmp = strKey
    strTmp = StrConv(strKey, 8) 'を全角文字(2バイト)半角文字(1バイト)に変換
    strTmp = StrConv(strTmp, 2) '文字列を小文字に変換

    ' 空白文字の変換
    strTmp = Replace(strTmp, vbTab, " ")
    strTmp = Replace(strTmp, vbLf, " ")
    strTmp = Replace(strTmp, vbCr, " ")
    strTmp = Trim$(strTmp)

    If StripSpace Then
        strTmp = Replace(strTmp, " ", "")
    Else
        Do
            i = Len(strTmp)
            strTmp = Replace(strTmp, "  ", " ") '連続する空白を１文字に変換
        Loop While Len(strTmp) <> i
    End If

    ConvertKeyString = strTmp
End Function

' 設定パラメータの読み取り
Public _
Function GetStringSettingParam(ByVal strTableTagName As String _
            , Optional ByVal strConfigSheetName As String = "" _
            , Optional ByVal intKeyColumn As Long = 1 _
            , Optional ByVal intGetColumn As Variant = Empty _
            , Optional ByVal strSearchKey As String = "" _
        ) As String()
    Dim rngRet() As Range
    Dim strRet() As String
    Dim i As Integer

    rngRet = GetRangeSettingParam(strTableTagName _
        , strConfigSheetName:=strConfigSheetName _
        , intKeyColumn:=intKeyColumn _
        , intGetColumn:=intGetColumn _
        , strSearchKey:=strSearchKey)

    If ArrayElementsCount(rngRet) > 0 Then
        ReDim strRet(UBound(rngRet))
        For i = 1 To UBound(rngRet)
            strRet(i) = Trim$(rngRet(i).Value)
        Next i
    End If
    Erase rngRet

    GetStringSettingParam = strRet

End Function

Public _
Function GetRangeSettingParam(ByVal strTableTagName As String _
        , Optional ByVal strConfigSheetName As String = "" _
        , Optional ByVal intKeyColumn As Long = 1 _
        , Optional ByVal intGetColumn As Variant = Empty _
        , Optional ByVal strSearchKey As String = "" _
        ) As Range()
    Const intDefaultGetColumn As Long = 2
    Dim rngRet() As Range
    Dim intTableStartRow As Long
    Dim intTableStartColumn As Long
    Dim intTableEndRow As Long
    Dim intTableEndColumn As Integer
    Dim intTargetColumn As Long
    Dim strTmp As String
    Dim strKey As String
    Dim wsConfig As Worksheet
    Dim i, j As Integer
    Dim rngTmp As Range

    ReDim rngRet(0)

    If intKeyColumn < 1 Then
        MsgBox "Bug!!", vbCritical
        GoTo Exit_Proc

    ElseIf strSearchKey <> "" Then
        strKey = ConvertKeyString(strSearchKey)
        If strKey = "" Then
            MsgBox "Bug!!", vbCritical
            GoTo Exit_Proc
        ElseIf Not IsEmpty(intGetColumn) Then
            MsgBox "Bug!!", vbCritical  '同時指定不可
            GoTo Exit_Proc
        End If

    ElseIf Not IsEmpty(intGetColumn) Then
        If intGetColumn < 1 Then
            MsgBox "Bug!!", vbCritical
            GoTo Exit_Proc
        End If
    Else
        intGetColumn = intDefaultGetColumn
    End If

    ' 設定シートを取得
    If strConfigSheetName = "" Then
        Set wsConfig = StatisticsLibrary2.ConfigSheetGet()
    Else
        Set wsConfig = 同名のワークシート取得(strConfigSheetName)
    End If
    If wsConfig Is Nothing Then
        GoTo Exit_Proc
    End If

    ' 設定表範囲を検出
    Set rngTmp = 名前の参照範囲取得(wsConfig, strTableTagName)
    If rngTmp Is Nothing Then
        GoTo Exit_Proc
    End If
    With rngTmp
        intTableStartRow = .Row + 1
        intTableStartColumn = .Column

        With .CurrentRegion
            intTableEndRow = .Cells(.Cells.Count).Row
            intTableEndColumn = .Cells(.Cells.Count).Column
        End With
    End With

    If intTableStartRow > intTableEndRow Then
        MsgBox wsConfig.Name & "シートの" & strTableTagName & "の指定にエラーがあります", vbCritical
        GoTo Exit_Proc
    End If

    ' キーの指定がない場合は、値の列を返す。
    If strSearchKey = "" Then
        intTargetColumn = intTableStartColumn + intGetColumn - 1

        ReDim rngRet(intTableEndRow - intTableStartRow + 1)
        For i = intTableStartRow To intTableEndRow
            Set rngRet(i - intTableStartRow + 1) = wsConfig.Cells(i, intTargetColumn)
        Next i
    Else
        ' キーの指定がある場合はキー列を検索し、検出した行を返す。
        intTargetColumn = intTableStartColumn + intKeyColumn - 1

        For i = intTableStartRow To intTableEndRow
            strTmp = ConvertKeyString(wsConfig.Cells(i, intTargetColumn).Value)
            If strKey = strTmp Then
                ReDim rngRet(intTableEndColumn - intTableStartColumn + 1)
                For j = intTableStartColumn To intTableEndColumn
                    Set rngRet(j - intTableStartColumn + 1) = wsConfig.Cells(i, j)
                Next j
                Exit For
            End If
        Next i
    End If

Exit_Proc:
    GetRangeSettingParam = rngRet

End Function

Sub TargetSheetSelect(ByVal varInSheet As Variant)
    If VarType(varInSheet) = vbString Then
        Sheets(varInSheet).Select
    Else
        varInSheet.Parent.Activate
        varInSheet.Select
    End If
End Sub

' 実績日の書き込み
Sub EndDayWrite(ByVal varInSheet As Variant, _
                ByVal dtStart As Date, _
                ByVal dtEnd As Date, _
                ByVal strByWeekOrDay As String, _
                ByVal intOutTargetColumn As Integer, _
                Optional ByVal booCheck As Boolean = True)
    Dim i As Integer

    Call TargetSheetSelect(varInSheet)
    If (strByWeekOrDay = "Day") Then
        If (DateDiff("d", dtStart, dtEnd) <= 0) And booCheck Then
            If (MsgBox("start_date列のサンプル数が不足しています。2日以上のサンプルが必要です。", vbExclamation) = vbOK) Then
                End
            End If
        End If
        For i = 0 To DateDiff("d", dtStart, dtEnd)
            Cells(i + 1, intOutTargetColumn).Select
            ActiveCell.Value = DateAdd("d", i, dtStart)
        Next
    Else
        If (DateDiff("w", dtStart, dtEnd) <= 0) And booCheck Then
            If (MsgBox("start_date列のサンプル数が不足しています。2週以上のサンプルが必要です。", vbExclamation) = vbOK) Then
               End
            End If
        End If
        For i = 0 To DateDiff("w", dtStart, dtEnd)
            Cells(i + 1, intOutTargetColumn).Select
            ActiveCell.Value = DateAdd("d", 7 * i, dtStart)
        Next
    End If

End Sub

' 実績サンプリング番号の書き込み
Sub ActualSamplingNoWrite(ByVal varInSheet As Variant, _
                          ByVal intTargetColumn As Integer, _
                          ByVal intOutTargetColumn As Integer)
    Dim i As Integer

    Call TargetSheetSelect(varInSheet)

    For i = 0 To GetTargetColumnEndRow(intTargetColumn) - 1
        Cells(i + 1, intOutTargetColumn).Select
        ActiveCell.Value = i + 1
    Next

End Sub

Function UniqListGet( _
            ByVal objSrcTop As Range, _
            ByVal intSrcEndRow As Variant, _
            Optional KeyMode As Boolean = False, _
            Optional 未設定を除く As Boolean = False, _
            Optional 結果ソート As Boolean = False) As String()

    Dim aa() As String
    Dim wsSrc As Worksheet
    Dim intSrcCol As Variant
    Dim strTmp As String
    Dim i As Variant
    Dim j As Long
    Dim k As Long
    Dim intFound As Variant

    With objSrcTop
        Set wsSrc = .Worksheet
        intSrcCol = .Column
    End With

    ReDim aa(0)
    k = 0
    For i = objSrcTop.Row To intSrcEndRow
        strTmp = Trim$(wsSrc.Cells(i, intSrcCol).Value)
        If IsSpaceString(strTmp) Then
            If 未設定を除く Then
                GoTo NextLoop
            End If

            strTmp = ""
        ElseIf KeyMode Then
            strTmp = ConvertKeyString(strTmp)
        End If

        intFound = 0
        For j = 1 To k
            If strTmp = aa(j) Then
                intFound = j
                Exit For
            End If
        Next j
        If intFound = 0 Then
            k = k + 1
            ReDim Preserve aa(k)
            aa(k) = strTmp
        End If
NextLoop:
    Next i
    
    If k > 1 Then
        If 結果ソート Then
            UniqListGet = MyStringArraySort(aa, 1)
            Exit Function
        End If
    End If
    
    UniqListGet = aa
End Function

' 数式の値を書き込み
Sub FormulaCalcWrite( _
            ByVal intTopRow As Variant, _
            ByVal intEndRow As Variant, _
            ByVal rngFormula As Range)

    Dim wsDst As Worksheet
    Dim intRowNum    As Variant

    Dim c1 As Range
    Dim c2 As Range

    Set wsDst = rngFormula.Worksheet
    intRowNum = intEndRow - intTopRow + 1

    For Each c1 In rngFormula
        Set c2 = wsDst.Cells(intTopRow, c1.Column)
        If c2.HasFormula Then
            ' 値を設定
            Call MyRangeValueCopy(c2.Resize(intRowNum), c2)
        End If
    Next c1

End Sub

' 列の抽出
'   rngDataTable: 出力先ヘッダ、または 出力領域
'   objSrcHeader: 参照先ヘッダ開始セル
'
Function SelectDataWrite( _
            ByVal rngDataTable As Range, _
            ByVal objSrcHeader As Range, _
            ByVal intSrcTopRow As Variant, _
            ByVal intSrcEndRow As Variant, _
            Optional ByVal レコード条件 As Range = Nothing, _
            Optional ByVal 計算値設定 As Range = Nothing, _
            Optional ByVal By項目名 As Boolean = False) As Variant

    Dim wsSrc As Worksheet
    Dim intSrcRowsNum As Variant   'データの行数

    Dim wsDst As Worksheet

    Dim intSelectMax As Long '抽出列数
    Dim intSrcColumn() As Variant

    Dim intDstMinRow    As Variant
    Dim intDstMaxRow    As Variant
    Dim intDstMinCol    As Variant
    Dim intDstMaxCol    As Variant

    Dim intTargetCol    As Variant

    Dim rngFieldSelector As Range
    Dim rngFormula(2) As Range

    Dim booCalc As Boolean
    Dim c1 As Range
    Dim c2 As Range
    Dim i As Variant
    Dim strMsg As String

    SelectDataWrite = False

    If rngDataTable Is Nothing Then
        MsgBox "Bug!! 出力先データ表の指定は省略できません", vbCritical
        End
    End If

    With rngDataTable
        strMsg = ""
        If .MergeCells Then
            strMsg = "Bug!! " & セルの位置表示取得(c1) & ": 結合セルは指定できません"
            
        ElseIf .Areas.Count > 1 Then
            strMsg = "Bug!! " & セルの位置表示取得(c1) & ": 複数領域は指定できません"
            
        End If

        If strMsg <> "" Then
            Call MyRangeSelect(rngDataTable.Rows(1))
            Call MsgBox(strMsg, vbCritical)
            Exit Function
        End If
    End With

    With rngDataTable
        Set rngFieldSelector = .Rows(1)
        If .Rows.Count > 1 Then
            Set rngFormula(0) = .Rows(2)
        Else
            Set rngFormula(0) = Nothing
        End If
    End With

    Set rngFormula(1) = 計算値設定
    Set rngFormula(2) = レコード条件

    booCalc = False
    For i = 0 To UBound(rngFormula)
        strMsg = ""
        Set c1 = rngFormula(i)
        If c1 Is Nothing Then
            ' OK
        ElseIf c1.MergeCells Then
            strMsg = "Bug!! " & セルの位置表示取得(c1) & ": 結合セルは指定できません"
            
        ElseIf c1.Areas.Count > 1 Then
            strMsg = "Bug!! " & セルの位置表示取得(c1) & ": 複数領域は指定できません"
            
        ElseIf c1.Rows.Count > 1 Then
            strMsg = "Bug!! " & セルの位置表示取得(c1) & ": 複数行は指定できません"
            
        ElseIf i = 0 Then
            booCalc = True

        ElseIf c1.Row <> rngFieldSelector.Row Then
            strMsg = "Bug!! " & セルの位置表示取得(c1) & ": ヘッダ行と異なる行は指定できません"

        Else
            Set rngFormula(i) = rngFormula(i).Offset(1)
            booCalc = True
        End If

        If strMsg <> "" Then
            Call MyRangeSelect(rngFormula(i))
            Call MsgBox(strMsg, vbCritical)
            Exit Function
        End If
    Next i

    With objSrcHeader
        Set wsSrc = .Worksheet
        intSrcRowsNum = intSrcEndRow - intSrcTopRow + 1
    End With

    ' 抽出列番号取得
    If SelectFieldColumnGet(rngFieldSelector, objSrcHeader, intSrcColumn, rngFormula(0), By項目名:=By項目名) = False Then
        Exit Function
    End If

    intSelectMax = UBound(intSrcColumn)

    With rngDataTable
        ' 出力位置取得
        Set wsDst = .Worksheet

        intDstMinRow = .Row + 1
        intDstMaxRow = .Row + intSrcRowsNum

        intDstMinCol = .Column
        intDstMaxCol = .Column + .Columns.Count - 1

        '' 初期設定
        Select Case .Rows.Count
        Case 1
            Set c1 = .Offset(1)
            Set c2 = c1
        Case 2
            Set c1 = .Rows(2)
            Set c2 = c1
        Case Else
            Set c1 = .Rows(2)
            Set c2 = .Rows(.Rows.Count)
        End Select

        ' 出力域クリア
        Call ClearRangeToEndRow(c2.Offset(1))

        If c1.Row < c2.Row Then
            ' データ表領域拡張
            Call MyRowsResize(wsDst.Range(c1, c2), intSrcRowsNum)
        End If

        Set c2 = c1.Offset(intSrcRowsNum - 1)

        ' 数式等をコピー
        If c1.Row < c2.Row Then
            c1.Copy wsDst.Range(c1.Offset(1), c2)
        End If
    End With

    '' 抽出列コピー
    For i = 0 To intSelectMax
        intTargetCol = rngFieldSelector.Cells(i + 1).Column
        Set c2 = wsDst.Cells(intDstMinRow, intTargetCol)

        Select Case intSrcColumn(i)
        Case Is = 0
            Call c2.Resize(intSrcRowsNum).ClearContents

        Case Is > 0
            Set c1 = wsSrc.Cells(intSrcTopRow, intSrcColumn(i))
            Call MyRangeValueCopy(c1.Resize(intSrcRowsNum), c2)
        End Select
    Next i

    '' 数式を値におきかえ
    If booCalc Then
        Call MyWsCalculate(wsDst)

        For i = 1 To UBound(rngFormula)
            If Not (rngFormula(i) Is Nothing) Then
                Call FormulaCalcWrite(intDstMinRow, intDstMaxRow, rngFormula(i))
            End If
        Next i
    End If

    '' 不要な行を削除
    If Not (レコード条件 Is Nothing) Then
        intTargetCol = レコード条件.Column

        For i = intDstMaxRow To intDstMinRow Step -1
            If wsDst.Cells(i, intTargetCol).Value = False Then
                With wsDst
                    Set c1 = .Cells(i, intDstMinCol)
                    Set c2 = .Cells(i, intDstMaxCol)
                    .Range(c1, c2).Delete Shift:=xlShiftUp
                End With
                intDstMaxRow = intDstMaxRow - 1
            End If
        Next i
    End If

    SelectDataWrite = Array(intDstMinRow, intDstMinCol, intDstMaxRow, intDstMaxCol)

End Function

' 抽出するデータの列番号を取得
Public _
Function SelectFieldColumnGet( _
            ByVal objFieldName As Range, _
            ByVal objSrcHeaderBase As Range, _
            ByRef aryDataCol() As Variant, _
            Optional ByVal objDataLine As Range = Nothing, _
            Optional ByVal By項目名 As Boolean = False) As Boolean

    Dim intMax  As Variant
    Dim i   As Long
    Dim strTmp As String
    Dim booSkip As Boolean

    SelectFieldColumnGet = False

    intMax = objFieldName.Cells.Count - 1
    ReDim aryDataCol(intMax)
    For i = 0 To intMax
        ' 抽出するデータの項目名を取得
        strTmp = Trim$(objFieldName.Cells(i + 1).Value)

        ' 抽出対象か判定
        booSkip = False
        If IsSpaceString(strTmp) Then
            booSkip = True ' 項目名無し

        ElseIf Not objDataLine Is Nothing Then
            If objDataLine.Cells(i + 1).HasFormula Then
                booSkip = True ' データ行に数式が設定されている列は抽出対象外
            End If
        End If

        If booSkip Then
            aryDataCol(i) = -1
        Else
            ' ヘッダ行を項目名で検索し、列番号を取得
            If By項目名 Then
                aryDataCol(i) = GetProbemColumnByItemName(strTmp, NoMsg:=True)
            Else
                aryDataCol(i) = GetProbemColumnByFieldName(strTmp, objSrcHeaderBase)
                If aryDataCol(i) < 1 Then
                    Exit Function
                End If
            End If

        End If
    Next i

    SelectFieldColumnGet = True

End Function

' シート名をプロジェクト名に変更(未使用)
Sub ChangeSheetName(ByVal strInSheetName As String)
    Dim i As Worksheet
    Dim strTmp As String

    For Each i In Worksheets
        If (InStr(i.Name, strInSheetName) > 0) Then
            Sheets(i.Name).Select
            strTmp = Cells(3, 3).Value
            同名のワークシートを削除 strTmp
            i.Name = strTmp
        End If
    Next
End Sub

' 同名のWorksheetの存在確認
Function CheckExistWorksheetsName(ByVal strSheetName As String)
    Dim w As Worksheet
    Dim booExist As Boolean

    booExist = False
    For Each w In Worksheets
        If (UCase(w.Name) = UCase(strSheetName)) Then
            booExist = True
            Exit For
        End If
    Next

    CheckExistWorksheetsName = booExist

End Function

'　配列の要素数
Function ArrayElementsCount(ByVal varInput As Variant) As Long
    If Not IsArray(varInput) Then
        ArrayElementsCount = -1
    Else
        On Error GoTo Error
        ArrayElementsCount = UBound(varInput) - LBound(varInput) + 1
    End If
    Exit Function

Error:
    ArrayElementsCount = 0
End Function

' 日付の最小値・最大値検出
Function GetTargetColumnDateMinMax(ByVal intTargetColumn As Integer, _
                                   Optional ByVal intTargetRowOffset As Integer = -1, _
                                   Optional ByVal booFormatFlag As Boolean = True, _
                                   Optional ByVal intTargetColumnEndRow As Integer = -1) As Variant

    Dim i As Integer
    Dim ret(2) As Variant
    Dim dtMin As Date
    Dim dtMax As Date
    Dim dtTmp As Date
    Dim intTargetColumnEndRowTmp As Integer
    Dim booFlag As Boolean

    Columns(intTargetColumn).Select
    Selection.NumberFormatLocal = "yyyy-mm-dd"

    If intTargetRowOffset < 0 Then
        intTargetRowOffset = StatisticsLibrary2.numRedmineSheetDataRow - 1
    End If

    If (intTargetColumnEndRow < 0) Then
        intTargetColumnEndRowTmp = GetTargetColumnEndRow(intTargetColumn)
    Else
        intTargetColumnEndRowTmp = intTargetColumnEndRow
    End If

    booFlag = False
    For i = intTargetRowOffset + 1 To intTargetColumnEndRowTmp
        If (Not (intTargetColumnEndRow < 0)) And (Cells(i, intTargetColumn).Value = "") Then
            GoTo Continue
        End If
        If (IsDate(Cells(i, intTargetColumn).Value)) Then
            dtTmp = Cells(i, intTargetColumn).Value
        Else
            If (MsgBox("日付の形式が異なります。", vbExclamation) = vbOK) Then
                End
            End If
        End If
        If (booFlag = False) Then
            dtMin = dtTmp
            dtMax = dtTmp
            booFlag = True
        Else
            If (dtTmp < dtMin) Then
                dtMin = dtTmp
            End If
            If (dtTmp > dtMax) Then
                dtMax = dtTmp
            End If
        End If
Continue:
    Next
    If (booFormatFlag) Then
        Columns(intTargetColumn).Select
        Selection.NumberFormatLocal = "@"
    End If
    ret(1) = dtMin
    ret(2) = dtMax

    GetTargetColumnDateMinMax = ret

End Function

Function DateIsInitial(ByVal dtTarget As Date) As Boolean
    Dim dtInitial As Date
    If dtTarget = dtInitial Then
        DateIsInitial = True
    Else
        DateIsInitial = False
    End If
End Function

Function 同名のワークシートの存在Check(ByVal strOutSheetName As String) As Boolean
    Dim i As Worksheet

    同名のワークシートの存在Check = False
    For Each i In Worksheets
        If (UCase(i.Name) = UCase(strOutSheetName)) Then
            同名のワークシートの存在Check = True
            Exit For
        End If
    Next
End Function

Sub 同名のワークシートをクリア(ByVal strOutSheetName As String)
    Dim i As Worksheet

    For Each i In Worksheets
        If (UCase(i.Name) = UCase(strOutSheetName)) Then
            Sheets(strOutSheetName).Select
            Cells.Select
            Selection.Clear
            Exit For
        End If
    Next
End Sub

Sub 同名のワークシートが無ければ追加(ByVal strOutSheetName As String)
    Dim i As Worksheet
    Dim objTmp As Object

    For Each i In Worksheets
        If (UCase(i.Name) = UCase(strOutSheetName)) Then
            Sheets(strOutSheetName).Select
            Exit Sub
        End If
    Next
    
    Set objTmp = 生成シートの挿入位置取得()
    Sheets.Add After:=objTmp
    ActiveSheet.Name = strOutSheetName
    Set objTmp = Nothing
End Sub

Sub 同名のワークシートが無ければコピー(ByVal strInSheetName As String, ByVal strOutSheetName As String)
    Dim i As Worksheet
    Dim booExist As Boolean
    Dim objTmp As Object

    booExist = False
    For Each i In Worksheets
        If (UCase(i.Name) = UCase(strInSheetName)) Then
            booExist = True
        End If
    Next
    If (booExist = False) Then
        MsgBox "シートが存在しません。:" & strInSheetName
        End
    End If
    For Each i In Worksheets
        If (UCase(i.Name) = UCase(strOutSheetName)) Then
            Sheets(strOutSheetName).Select
            Exit Sub
        End If
    Next

    Set objTmp = 生成シートの挿入位置取得()
    Sheets(strInSheetName).Copy After:=objTmp
    ActiveSheet.Name = strOutSheetName
    Set objTmp = Nothing
End Sub

Sub 同名のワークシートを削除(ByVal strOutSheetName As String)
    Dim i As Worksheet

    For Each i In Worksheets
        If (UCase(i.Name) = UCase(strOutSheetName)) Then
            Application.DisplayAlerts = False
            i.Delete
            Application.DisplayAlerts = True
            Exit For
        End If
    Next
End Sub

Sub 同名のワークシートを表示(ByVal strOutSheetName As String)
    Dim i As Worksheet

    For Each i In Worksheets
        If (UCase(i.Name) = UCase(strOutSheetName)) Then
            i.Visible = True
            Exit For
        End If
    Next
End Sub

Sub 同名のワークシートを非表示(ByVal strOutSheetName As String)
    Dim i As Worksheet

    For Each i In Worksheets
        If (UCase(i.Name) = UCase(strOutSheetName)) Then
            i.Visible = False
            Exit For
        End If
    Next
End Sub

Sub 選択セルの初期化()
    Dim i As Worksheet

    For Each i In Worksheets
        i.Select
        ActiveSheet.Cells(1, 1).Select
    Next
End Sub

Sub 同名のグラフを削除(ByVal strOutGraphName As String)
    Dim i As Chart

    For Each i In Charts
        If (UCase(i.Name) = UCase(strOutGraphName)) Then
            Application.DisplayAlerts = False
            i.Delete
            Application.DisplayAlerts = True
            Exit For
        End If
    Next
End Sub


Function GetTargetColumnStartRow(ByVal intColumn As Integer) As Integer
    Dim ret As Integer

    On Error GoTo overflow

    If (Cells(1, intColumn).Value = "") Then
        ret = Cells(1, intColumn).End(xlDown).Row
    Else
        ret = 1
    End If
    GetTargetColumnStartRow = ret
    Exit Function

overflow:
    GetTargetColumnStartRow = -1
End Function


Function GetTargetColumnEndRow(ByVal intColumn As Integer, Optional ByVal intRow As Integer = 1) As Integer
    Dim ret As Integer

    On Error GoTo overflow
    ' 空白時は、2回実行、オーバーフロー時は1
    ' integerの範囲を超えたらオーバーフローとする
    If (Cells(intRow, intColumn).Value = "") Then
        ret = Cells(intRow, intColumn).End(xlDown).Row
        ret = Cells(ret, intColumn).End(xlDown).Row
    Else
        ret = Cells(intRow, intColumn).End(xlDown).Row
    End If
    GetTargetColumnEndRow = ret
    Exit Function

overflow:
    GetTargetColumnEndRow = intRow
End Function


'　他と異なるので注意
Function GetUsedRangeEndColumn() As Integer
    GetUsedRangeEndColumn = Cells(1, 1).End(xlToRight).Column
End Function

Function GetTargetRowEndColumn(ByVal intRow As Integer, _
                               Optional ByVal intColumn As Integer = 1) As Integer
    Dim ret As Integer

    On Error GoTo overflow

    ' 空白時は、2回実行、オーバーフロー時は1
    ' integerの範囲を超えたらオーバーフローとする
    If (Cells(intRow, intColumn).Value = "") Then
        ret = Cells(intRow, intColumn).End(xlToRight).Column
        ret = Cells(intRow, ret).End(xlToRight).Column
    Else
        ret = Cells(intRow, intColumn).End(xlToRight).Column
    End If
    GetTargetRowEndColumn = ret
    Exit Function

overflow:
    GetTargetRowEndColumn = intColumn
End Function



Function GetRegExpMatch(ByVal strInput As String, _
                        ByVal strFind As String) As String
    Dim re As Variant

    Set re = CreateObject("VBScript.RegExp")

    With re
        .Pattern = strFind
        .IgnoreCase = True
        .Global = True
    End With

    Dim matches As Variant
    Set matches = re.Execute(strInput)

    If matches.Count > 0 Then
        GetRegExpMatch = matches(0).Value
    End If

End Function

Function GetRegExpMatches(ByVal strInput As String, _
                          ByVal strFind As String) As String()
    Dim re As Variant

    Set re = CreateObject("VBScript.RegExp")

    With re
        .Pattern = strFind
        .IgnoreCase = True
        .Global = True
    End With

    Dim match, matches As Variant
    Set matches = re.Execute(strInput)
    
    If (matches.Count > 0) Then
        Dim strResult() As String
        ReDim strResult(matches.Count - 1)

        Dim i As Integer
        For i = 0 To matches.Count - 1
            strResult(i) = matches(i).Value
        Next
        GetRegExpMatches = strResult
    End If

End Function

Function RegExpReplace(ByVal strInput As String, _
                       ByVal strFind As String, _
                       ByVal strReplace As String) As String
    Dim re As Variant

    Set re = CreateObject("VBScript.RegExp")

    With re
        .Pattern = strFind
        .IgnoreCase = True
        .Global = True
    End With

    Dim strResult As String
    strResult = re.Replace(strInput, strReplace)

    RegExpReplace = strResult

End Function


'　Chart名毎にグラフ結合
Sub JointGraphByChartName( _
            ByVal strOutSheetName As String, _
            ByVal strTargetChartName As String, _
            ByVal strGeneSource As String)

    同名のワークシートを削除 strOutSheetName
    同名のワークシートが無ければ追加 strOutSheetName

    ' 生成日時とデータソースを設定
    With ActiveSheet.Range("A1:B2")
        With .Columns(1)
            .HorizontalAlignment = xlHAlignRight
            .NumberFormatLocal = "@"
            .Cells(1).Value = "対象データ:"
            .Cells(2).Value = "実行日時:"
        End With
        With .Columns(2)
            .HorizontalAlignment = xlHAlignLeft
            .Cells(1).NumberFormatLocal = "@"
            .Cells(1).Value = strGeneSource
            .Cells(2).Value = dateJobStart
        End With
        .Columns.AutoFit
    End With

    Dim i, j As Integer
    Dim strTmp As String
    j = 1
    For i = 1 To Application.Charts.Count
        Charts(i).Select
        strTmp = Charts(i).Name
        ActiveChart.ChartArea.Copy
        If (ActiveChart.Name Like strTargetChartName) Then
             Sheets(strOutSheetName).Select
             ActiveSheet.Paste
             ActiveSheet.ChartObjects(ActiveSheet.ChartObjects.Count).Activate
             With Range(Cells((23 * (j - 1) + 3), 1), Cells((23 * (j - 1) + 3) + 21, 8))
                 ActiveChart.ChartArea.Left = .Left
                 ActiveChart.ChartArea.Top = .Top
                 ActiveChart.ChartArea.Width = .Width
                 ActiveChart.ChartArea.Height = .Height
            End With
            ActiveSheet.ChartObjects(ActiveSheet.ChartObjects.Count).Name = strTmp
            j = j + 1
        End If
        Application.CutCopyMode = False
    Next

    Call DeleteGraphByChartName(strTargetChartName)

    ' 出力がなければシート削除
    If (j = 1) Then
        同名のワークシートを削除 strOutSheetName
    End If

End Sub

Sub DeleteGraphByChartName(ByVal strTargetChartName As String)
    Dim i As Long

    For i = Application.Charts.Count To 1 Step -1
        If (Charts(i).Name Like strTargetChartName) Then
            Application.DisplayAlerts = False
            Charts(i).Delete
            Application.DisplayAlerts = True
        End If
    Next
End Sub

' テーブル生成
Sub TableGenerate(ByVal strInSheetName As String, _
                  ByVal strOutTableName As String, _
                  ByVal intStartRow As Integer, _
                  ByVal intStartColumn As Integer, _
                  ByVal intEndRow As Integer, _
                  ByVal intEndColumn As Integer)

    Sheets(strInSheetName).Select
    ActiveSheet.ListObjects.Add(xlSrcRange, _
                                Range(Cells(intStartRow, intStartColumn), _
                                      Cells(intEndRow, intEndColumn)), _
                                , _
                                xlYes).Name = strOutTableName
    Range(strOutTableName & "[#All]").Select

End Sub

' テーブルクリア
'   ExceptFormula: データの一行目に式がある場合は削除せず、数式以外のセルをクリアする。
Public _
Sub MyTableClear(ByVal objTable As ListObject, _
                 Optional ByVal ExceptFormula As Boolean = True)
    Dim i As Variant
    Dim j As Variant
    Dim c1 As Range

    With objTable.ListRows
        If .Count < 1 Then
            Exit Sub
        End If

        j = 1
        If ExceptFormula Then
            For Each c1 In .Item(1).Range
                If c1.HasFormula Then
                    j = 2
                Else
                    c1.Clear
                End If
            Next c1
        End If

        Call MyTableRowDelete(.Item(j))
    End With

End Sub

' 指定行以下のテーブル行を削除
Public _
Sub MyTableRowDelete(ByVal objTopRow As ListRow)
    Dim objTable As ListObject
    Dim i As Variant
    Dim j As Variant

    With objTopRow
        Set objTable = .Parent
        j = .Index
    End With
    With objTable.ListRows
        For i = .Count To j Step -1
            .Item(i).Delete
        Next i
    End With
End Sub

' テーブル取得
Public _
Function GetTableByName(ByVal テーブル名 As String, _
                        wsTarget As Worksheet, _
                        Optional ByVal NoMsg As Boolean = False) As ListObject

    Dim objTmp As ListObject
    Dim objFound As ListObject

    Set objFound = Nothing
    For Each objTmp In wsTarget.ListObjects
        If objTmp.Name = テーブル名 Then
            Set objFound = objTmp
            Exit For
        End If
    Next objTmp

    If (objFound Is Nothing) And (NoMsg = False) Then
        Call MsgBox(wsTarget.Name & "シートに「" & テーブル名 & "」というテーブルがありません。" _
            , vbCritical)
    End If

    Set GetTableByName = objFound
End Function

' ピボットテーブル生成
Sub PivotTableGenerate(ByVal strInTableName As String, _
                       ByVal strOutSheetName As String, _
                       ByVal strOutTableName As String)
    ' シート生成
    同名のワークシートを削除 strOutSheetName
    同名のワークシートが無ければ追加 strOutSheetName
    ' ピボットテーブル生成
    Sheets(strOutSheetName).Select
    ActiveWorkbook.PivotCaches.Create(SourceType:=xlDatabase, _
                       SourceData:=strInTableName, _
                       Version:=xlPivotTableVersion14).CreatePivotTable _
                       TableDestination:=strOutSheetName & "!R3C1", _
                       TableName:=strOutTableName, _
                       DefaultVersion:=xlPivotTableVersion14

End Sub

' 設定パラメータの読み取り (処理対象工程の最小開始・最大終了日取得)
Function GetProcessPeriod(ByRef strTargetProcess() As String) As Date()

    Dim ret() As Date
    Dim strProcessName As String
    Dim dtStart As Date
    Dim dtEnd  As Date
    Dim dtMin As Date
    Dim dtMax As Date
    Dim dtTmp As Date
    Dim i As Integer

    For i = 1 To UBound(strTargetProcess)
        strProcessName = strTargetProcess(i)

        ' 工程開始・終了日取得
        ret = GetProcessDate(strProcessName)
        dtStart = ret(1) '「工程開始日」
        dtEnd = ret(2) '「工程終了(予定)日」

        If Not DateIsInitial(dtStart) Then
            If DateIsInitial(dtMin) Then
                dtMin = dtStart
            ElseIf (dtStart < dtMin) Then
                dtMin = dtStart
            End If
        End If

        If Not DateIsInitial(dtEnd) Then
            If DateIsInitial(dtMax) Then
                dtMax = dtEnd
            ElseIf (dtMax < dtEnd) Then
                dtMax = dtEnd
            End If
        End If
    Next i

    ReDim ret(2)
    ret(1) = dtMin
    ret(2) = dtMax

    GetProcessPeriod = ret

End Function

' 設定パラメータの読み取り (工程開始・終了日取得)
Private _
Function GetProcessDate(ByVal strProcessName As String) As Date()

    Dim ret(2) As Date
    Dim varSettingParam As Variant
    Dim varStart As Variant
    Dim varEnd  As Variant
    Dim dtStart As Date
    Dim dtEnd   As Date

    ' 工程とリンクした設定を取得
    varSettingParam = GetRangeSettingParam("Common_setting", strSearchKey:=strProcessName)
    If ArrayElementsCount(varSettingParam) < 2 Then
        If (MsgBox("設定: 工程名「" & strProcessName & "」の記載がありません。", vbExclamation) = vbOK) Then
            End
        End If
    End If

    varStart = varSettingParam(CommonParamStartDateColumn).Value    '「工程開始日」
    varEnd = varSettingParam(CommonParamEndDateColumn).Value '「工程終了(予定)日」

    Erase varSettingParam

    If (varStart = "") Then
'        Call MsgBox("設定: 「" & strProcessName & "」の工程開始日が設定されていません。", vbExclamation)
'        End
    ElseIf Not IsDate(varStart) Then
        Call MsgBox("設定: 「" & strProcessName & "」の工程開始日が日付ではありません。", vbExclamation)
        End
    Else
        dtStart = varStart
    End If

    If (varEnd = "") Then
'        Call MsgBox("設定: 「" & strProcessName & "」の工程終了(予定)日が設定されていません。", vbExclamation)
'        End
    ElseIf Not IsDate(varEnd) Then
        Call MsgBox("設定: 「" & strProcessName & "」の工程終了(予定)日が日付ではありません。", vbExclamation)
        End
    Else
        dtEnd = varEnd
    End If

    If DateIsInitial(dtStart) Or DateIsInitial(dtEnd) Then
    ElseIf dtStart > dtEnd Then
        Call MsgBox("設定:「" & strProcessName & "」の工程開始日が終了日より後になっています。", vbExclamation)
        End
    End If

    ret(1) = dtStart
    ret(2) = dtEnd

    GetProcessDate = ret

End Function

' 設定パラメータの読み取り (工程分類)　※L100,L120,JIRA用
Function GetProcessCategory(ByVal strTargetName As String, _
                            Optional NoMsg As Boolean = False) As String

    Dim varSettingParam As Variant
    Dim strTmp  As String
    Dim booFound As Boolean

    ' 工程とリンクした設定を取得
    varSettingParam = GetRangeSettingParam("Common_setting", strSearchKey:=strTargetName)
    If ArrayElementsCount(varSettingParam) < 2 Then
        ' 設定Tableに該当工程が記述されていない場合
        If Not NoMsg Then
            Call MsgBox("設定: 工程名「" & strTargetName & "」が登録されていません。", vbExclamation)
            End
        End If
        strTmp = ""
        booFound = False
    Else
        strTmp = Trim$(varSettingParam(CommonParamProcessCategoryColumn).Value)    '「工程分類」
        booFound = True
    End If

    Erase varSettingParam

    If booFound And StatisticsLibrary2.IsSpaceString(strTmp) Then
        If Not NoMsg Then
            Call MsgBox("設定:工程名「" & strTargetName & "」に対する工程分類が設定されていません。", vbExclamation)
            End
        End If
    End If

    GetProcessCategory = strTmp

End Function

' 設定パラメータの読み取り - 処理対象の工程名リスト生成
'   ※生成リストの有効値は、インデクス=1 から
Function GetEnableProcessName(Optional ByVal strTargetID As String = "") As String()

    Dim varSettingParam As Variant
    Dim strAllProcess() As String
    Dim strEnableProcess() As String
    Dim strName As String
    Dim strEnable  As String
    Dim strID   As String
    Dim i As Long
    Dim j  As Long

    ' 工程のリストを取得
    strAllProcess = GetStringSettingParam("Common_setting", intGetColumn:=CommonParamProcessNameColumn)  '「試験工程」

    ReDim strEnableProcess(0)
    j = 0
    For i = 1 To UBound(strAllProcess)
        strName = strAllProcess(i)

        If strName = "" Then
            GoTo Continue
        End If

        ' 工程とリンクした設定を取得
        varSettingParam = GetStringSettingParam("Common_setting", strSearchKey:=strName)
        If ArrayElementsCount(varSettingParam) < 2 Then
            ' 設定Tableに該当工程が記述されていない場合
            MsgBox "Bug!!"
            End
        End If

        strEnable = varSettingParam(CommonParamEnableColumn)   '「処理Enable」
        strID = varSettingParam(CommonParamOutIdColumn) '「出力ID」

        Erase varSettingParam

        If strEnable <> "○" Then  '「処理Enable」
            GoTo Continue
        End If

        ' 抽出対象IDか判定
        Select Case strTargetID
        Case "", strID    '対象IDが未指定、または、一致
        Case Else
            GoTo Continue
        End Select

        ' リストに追加
        j = j + 1
        ReDim Preserve strEnableProcess(j)
        strEnableProcess(j) = strName

Continue:
    Next i

    GetEnableProcessName = strEnableProcess

End Function

Sub SetAllProcessEnable(str処理Enable As String)
    Dim varSettingParam As Variant
    Dim i As Long

    varSettingParam = GetRangeSettingParam("Common_setting", _
                                           intGetColumn:=CommonParamEnableColumn) '「処理Enable」
    For i = 1 To UBound(varSettingParam)
        varSettingParam(i).Value = str処理Enable
    Next i
End Sub

