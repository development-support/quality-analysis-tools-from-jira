Attribute VB_Name = "StatisticsLibrary2"
Option Explicit
Option Private Module
'-----------------------------------------------------------
' 【機能】
'    問題管理シートの集計で共通に使用するルーチン
'
'-----------------------------------------------------------

Public dateJobStart As Date '実行開始日時

Private strProblemSheetName As String  ' 問題管理シート名

Private numRedmineSheetHeadRow    As Long 'データシートの見出し行
Public numRedmineSheetDataRow     As Long 'データシートのデータ開始行

Private Const strFormatSheetPrefix  As String = "【書式】"

Private strTargetProjectName As String
Private strProblemDataBaseType As String

Private Const intProblemItemMax As Long = 9
Private strProblemItemName(intProblemItemMax) As String

Private strProblemFieldName(intProblemItemMax) As String
Private strProblemFieldAlias(intProblemItemMax) As String
Private intProblemFieldColumn(intProblemItemMax) As Long

Public _
Sub JobBegin(Optional strMsg As String = "", Optional booOnlyUpdate As Boolean = False)

    dateJobStart = Now()

    If booOnlyUpdate Then
    ElseIf ProjectInit() = False Then
        End
    End If

    Application.Calculation = xlCalculationManual

    If strMsg <> "" Then
        Application.StatusBar = strMsg & " - " & strProblemSheetName
    End If

    Application.ScreenUpdating = False

End Sub

Public _
Sub JobEnd(Optional strMsg As String = "", Optional MsgBoxShow As Boolean = False)

    Application.Calculation = xlCalculationAutomatic
    Application.ScreenUpdating = True

    If (strMsg <> "") And (MsgBoxShow = True) Then
        Application.StatusBar = Replace(strMsg, vbCrLf, " ")
        MsgBox strMsg, vbOKOnly
    End If

    Application.StatusBar = False

End Sub

Sub MyRangeSelect(ByVal objRange As Range)
    Application.ScreenUpdating = True

    Call MySheetActivate(objRange.Worksheet)
    objRange.Select
End Sub

Sub MySheetActivate(ByVal wsTarget As Worksheet)
    wsTarget.Parent.Activate
    wsTarget.Select
End Sub

Public _
Sub MySelectionReset(ByVal ws As Worksheet)
    Dim varSave(1)  As Variant

    varSave(0) = Application.ScreenUpdating
    Set varSave(1) = ws.Parent.ActiveSheet  '現在のActiveSheetを保存
    Application.ScreenUpdating = False

    '表示位置をリセット
    ws.Activate
    Application.GoTo Reference:=ws.Range("A1"), Scroll:=True

    varSave(1).Activate 'ActiveSheetを復帰
    Application.ScreenUpdating = varSave(0)

    Erase varSave
End Sub

Public _
Sub MyWsCalculate(ByVal wsTarget As Worksheet)
    If Application.Calculation <> xlCalculationAutomatic Then
        wsTarget.Calculate
    End If
End Sub

Public _
Sub MyFilterModeClear(ByVal wsTarget As Worksheet)
    If wsTarget.AutoFilter Is Nothing Then
        Exit Sub
    End If
    With wsTarget.AutoFilter
        If .FilterMode Then
            .ShowAllData
        End If
    End With
End Sub

Public Sub MyArrayClear(ByRef varTarget As Variant)
    If IsArray(varTarget) Then
        Erase varTarget
    End If
End Sub

Public _
Function MyStringArraySort(ByRef aryIn() As String, ByVal intStart As Long) As String()
    Dim aryOut As Variant
    Dim i As Long
    Dim j As Long
    Dim m As Long
    Dim strTmp As String

    aryOut = aryIn

    m = UBound(aryIn)

    For i = intStart To m
        strTmp = aryIn(i)
        For j = i + 1 To m
            If StrComp(strTmp, aryIn(j), vbBinaryCompare) < 0 Then
                aryIn(j) = strTmp
                strTmp = aryIn(j)
            End If
        Next j

        aryOut(i) = strTmp
    Next i

    MyStringArraySort = aryOut
End Function

Public Function MyFullPathGet(ByVal strPath As String) As String
    Dim objFSO As Object

    Set objFSO = CreateObject("Scripting.FileSystemObject")

    If objFSO.GetDriveName(strPath) = "" Then   '相対パスの場合
        MyFullPathGet = objFSO.GetAbsolutePathName(ThisWorkbook.Path & "\" & strPath)
            '注意）GetAbsolutePathNameは、指定したファイル名と実在するファイル名に
            '      大文字小文字の違いがある場合、実在するファイル名を返す。
    Else
        MyFullPathGet = strPath
    End If

    Set objFSO = Nothing

End Function

Public Function MyFileNameGet(ByVal strPath As String) As String
    Dim objFSO  As Object

    Set objFSO = CreateObject("Scripting.FileSystemObject")
    MyFileNameGet = objFSO.GetFileName(strPath)
    Set objFSO = Nothing

End Function

'=============================================================
'
'    問題管理シートの検索処理
'
'=============================================================
Public _
Function ProjectInit() As Boolean

    ProjectInit = False

    Dim wsProject As Worksheet

    Erase intProblemFieldColumn
    Erase strProblemFieldAlias

    strTargetProjectName = TargetProjectNameGet()
    strProblemDataBaseType = ProblemDataBaseTypeGet()

    ' 問題管理シートの形式設定
    Select Case strProblemDataBaseType
    Case "Redmine"
        strProblemSheetName = "Problem"    ' Redmineシート
        numRedmineSheetHeadRow = 2
        numRedmineSheetDataRow = 3

    Case "JIRA"
        strProblemSheetName = "Problem2"   ' JIRAシート
        numRedmineSheetHeadRow = 1
        numRedmineSheetDataRow = 2

    Case ""
        Exit Function

    Case Else
        Call MsgBox("DB種別「" & strProblemDataBaseType & "」は未対応です", vbCritical)
        Exit Function
    End Select

'    '---> 試験用
'    If ThisWorkbook.Name Like "*_Ver*.X.xlsm" Then
'        strProblemSheetName = strProblemSheetName & "(" & strTargetProjectName & ")"
'    End If
'    '<--- 試験用

    Set wsProject = 問題管理データシート取得()
    If wsProject Is Nothing Then
        Exit Function
    End If

    ' 問題管理シートの列定義設定
    Dim rngHeader As Range
    Set rngHeader = RedmineSheetHeader(wsProject)

    strProblemItemName(0) = "ID"
    strProblemItemName(1) = "ステータス"
    strProblemItemName(2) = "問題発生日"
    strProblemItemName(3) = "発見工程"
    strProblemItemName(4) = "問題完了日"
    strProblemItemName(5) = "問題発生機能"
    strProblemItemName(6) = "原因Gr"
    strProblemItemName(7) = "カテゴリ"
    strProblemItemName(8) = "トラッカ"
    strProblemItemName(9) = "重大度"

    Select Case strProblemDataBaseType
    Case "Redmine"
        strProblemFieldName(0) = "チケットID"
        strProblemFieldName(1) = "ステータス"
        strProblemFieldName(2) = "開始日"
        strProblemFieldName(3) = "(新規)発見工程"
        strProblemFieldName(4) = "(完了)承認日"
        strProblemFieldName(5) = "(解析)問題発生機能"
        strProblemFieldName(6) = "(解析)原因作りこみG"
        strProblemFieldName(7) = "カテゴリ"
        strProblemFieldName(8) = "トラッカーID"
        strProblemFieldName(9) = ""

        Select Case strTargetProjectName
        Case "L100"
            strProblemFieldName(3) = "発見工程"
            strProblemFieldName(4) = "承認日"
            strProblemFieldName(5) = ""
            strProblemFieldName(6) = "原因作りこみグループ"
            
        Case "L120"
            strProblemFieldName(3) = "発見工程"
            strProblemFieldName(4) = "承認日"
            strProblemFieldName(5) = "問題発生機能"
            strProblemFieldName(6) = "原因作りこみグループ"

        End Select

    Case "JIRA"
        strProblemFieldName(0) = "Key"
        strProblemFieldName(1) = "Status"
        strProblemFieldName(2) = "Created"
        strProblemFieldName(3) = "How-Found"
        strProblemFieldName(4) = "Resolved"
        strProblemFieldName(5) = "Component/s"
        strProblemFieldName(6) = ""
        strProblemFieldName(7) = ""
        strProblemFieldName(8) = "Issue Type"
        strProblemFieldName(9) = "Severity"

        If GetDataColumn(rngHeader, "Company", NoMsg:=True) > 0 Then '注意) この列が無い場合もあり。
            strProblemFieldName(7) = "Company"
        End If

        If GetDataColumn(rngHeader, "Key", NoMsg:=True) > 0 Then
            ' Aliasなし
        ElseIf GetDataColumn(rngHeader, "キー", NoMsg:=True) < 1 Then
            Call MsgBox(wsProject.Name & "シートのヘッダ行に """"Key"""" または """"キー"""" がありません", vbCritical)
            Exit Function
        Else
            strProblemFieldAlias(0) = "キー"
            strProblemFieldAlias(1) = "ステータス"
            strProblemFieldAlias(2) = "作成日"
            strProblemFieldAlias(3) = "How-Found"
            strProblemFieldAlias(4) = "解決日"
            strProblemFieldAlias(5) = "コンポーネント"
            strProblemFieldAlias(6) = ""
            strProblemFieldAlias(7) = strProblemFieldName(7)
            strProblemFieldAlias(8) = "Issue Type"
            strProblemFieldAlias(9) = "Severity"
        End If

    Case Else
        Call MsgBox("Bug!!", vbCritical)
        End
    End Select

    ' 問題管理シートの列番号取得
    If strProblemFieldAlias(0) = "" Then
        If RedmineSheetHeaderColumn(wsProject, strProblemFieldName, intProblemFieldColumn) = False Then
            Exit Function
        End If
    ElseIf RedmineSheetHeaderColumn(wsProject, strProblemFieldAlias, intProblemFieldColumn) = False Then
         Exit Function
    End If

    ProjectInit = True
End Function

' データシートの列番号取得
Public _
Function GetProbemColumnByItemName(ByVal strName As String, _
                                   Optional NoMsg As Boolean = False) As Long

    Dim intFound    As Long
    Dim i   As Long

    intFound = 0
    For i = 0 To UBound(strProblemItemName)
        If strProblemItemName(i) = strName Then
            intFound = intProblemFieldColumn(i)
            Exit For
        End If
    Next i

    If (intFound = 0) And (NoMsg = False) Then
        Call MsgBox("Bug!! 問題管理シートの「" & strName & "」項目に対応するフィールドは定義されていない", vbCritical)
    End If

    GetProbemColumnByItemName = intFound
End Function

Public _
Function GetProbemColumnByFieldName(ByVal strName As String, _
                                    ByVal rngHeader As Range) As Long

    Dim intFound    As Long
    Dim i   As Long

    intFound = 0
    For i = 0 To UBound(strProblemFieldName)
        If strProblemFieldName(i) = strName Then
            intFound = intProblemFieldColumn(i)
            Exit For
        End If
    Next i

    If intFound = 0 Then
        intFound = GetDataColumn(rngHeader, strName, NoMsg:=True)
    End If

    If intFound = 0 Then
        Dim r1 As Range
        Dim strTmp As String

        If strProblemDataBaseType = "Redmine" Then
            strTmp = strName & "_*"
            For Each r1 In rngHeader.Cells
                 If Trim$(r1.Value) Like strTmp Then
                    intFound = r1.Column
                    Exit For
                End If
            Next r1
        End If
    End If

    If intFound = 0 Then
        Call MsgBox(rngHeader.Worksheet.Name & _
                    "シートのヘッダ行に「" & strName & "」がありません", _
                    vbCritical)
    End If

    GetProbemColumnByFieldName = intFound
End Function

'=============================================================
'
'    セル範囲の処理
'
'=============================================================
' 指定されたセルを含む最小の範囲を返す。
Public Function MyCurrentRegion(ParamArray arrs()) As Range
    Dim pp  As Variant
    Dim rr  As Range
    Dim min_col As Long
    Dim max_col As Long
    Dim min_row As Long
    Dim max_row As Long
    Dim ws  As Worksheet

    With arrs(0).Areas(1)
        min_col = .Column
        max_col = .Column + .Columns.Count - 1
        min_row = .Row
        max_row = .Row + .Rows.Count - 1

        Set ws = .Worksheet
    End With

    For Each pp In arrs
        For Each rr In pp.Areas
            If min_col > rr.Column Then
                min_col = rr.Column
            ElseIf max_col < (rr.Column + rr.Columns.Count - 1) Then
                max_col = (rr.Column + rr.Columns.Count - 1)
            End If

            If min_row < rr.Row Then
                min_row = rr.Row
            ElseIf max_row < (rr.Row + rr.Rows.Count - 1) Then
                max_row = (rr.Row + rr.Rows.Count - 1)
            End If
        Next rr
    Next pp

    Set MyCurrentRegion = ws.Range(ws.Cells(min_row, min_col), ws.Cells(max_row, max_col))
End Function

' 指定された領域を列順に並べ変える
Public Function MyRegionColumnSort(ByRef rr As Range) As Boolean
    Dim r2  As Range
    Dim r3()    As Range
    Dim min_col As Long
    Dim max_col As Long
    Dim ii  As Long

    MyRegionColumnSort = False

    If rr Is Nothing Then
        Exit Function
    ElseIf rr.Areas.Count = 1 Then
        MyRegionColumnSort = True
        Exit Function
    End If

    Set r2 = MyCurrentRegion(rr)

    With r2
        min_col = .Column
        max_col = .Column + .Columns.Count - 1
    End With
    
    ReDim r3(min_col To max_col)
    For ii = min_col To max_col
        Set r3(ii) = Nothing
    Next ii

    For Each r2 In rr.Areas
        ii = r2.Column
        If r3(ii) Is Nothing Then
            Set r3(ii) = r2
        Else
            rr.Worksheet.Activate
            rr.Select
            Call MsgBox("同一列のセルは指定できません", vbCritical)
            GoTo Exit_Proc
        End If
    Next r2

    Set r2 = r3(min_col)
    For ii = min_col + 1 To max_col
        If Not (r3(ii) Is Nothing) Then
            Set r2 = Union(r2, r3(ii))
        End If
    Next ii

    Set rr = r2

    MyRegionColumnSort = True

Exit_Proc:
    Erase r3
    Set r2 = Nothing

End Function

Public _
Sub MyRowsResize(ByVal rngTarget As Range, _
                 ByVal 新行数 As Variant)
    Dim intOld As Variant

    intOld = rngTarget.Rows.Count

    Select Case intOld
    Case Is > 新行数
        rngTarget.Rows(2).Resize(intOld - 新行数).Delete Shift:=xlShiftUp
    Case Is < 新行数
        rngTarget.Rows(2).Resize(新行数 - intOld).Insert Shift:=xlShiftDown
    End Select

End Sub

'=============================================================
'
'    ワークブックの処理
'
'=============================================================
Public _
Function MyBookNameGet(ByVal strPath As String) As String
    Dim strTmp As String

    strTmp = MyFileNameGet(strPath)

    strTmp = Replace(strTmp, "[", "(")
    strTmp = Replace(strTmp, "]", ")")
    '注）ブック名(WorkBook.Name)の "["または"]"は、Excel 内では "(",")" に変換されている。
    '     WorkBook.Path 及び、WorkBook.FullName では、変換されない。
    
    MyBookNameGet = strTmp
End Function

Public _
Function MyRefBookOpen(ByVal strPath As String) As Workbook

    Dim wb As Workbook

    Set MyRefBookOpen = Nothing

    If MyOpenBookCheck(wb, strPath, ReuseEnable:=True) = False Then
        Exit Function
    ElseIf wb Is Nothing Then
        Set wb = MyBookOpen(strPath, ReadOnly:=True)
    End If

    Set MyRefBookOpen = wb
End Function

' オープン済みのブックを返す
Public _
Function MyOpenBookCheck(ByRef wbResult As Workbook, _
                         ByVal strPath As String, _
                         ByVal ReuseEnable As Boolean, _
                         Optional ReadOnly As Variant) As Boolean

    Dim strFullPath As String
    Dim strBookName As String
    Dim objBook As Workbook
    Dim strMsg As String

    MyOpenBookCheck = False
    Set wbResult = Nothing

    strBookName = MyBookNameGet(strPath)

    '既にオープンされているか調べる
    For Each objBook In Workbooks
        If UCase(objBook.Name) = UCase(strBookName) Then
            strFullPath = MyFullPathGet(strPath)

            If objBook.FullName <> strFullPath Then    '注）同名の別ファイルかもしれないので完全パスで比較する。
                strMsg = "同じブック名の別ファイル" & vbCrLf _
                    & objBook.FullName & vbCrLf & vbCrLf _
                    & "が開いているため、 & vbCrLf" _
                    & strFullPath & vbCrLf _
                    & "が開けません" & vbCrLf & vbCrLf _
                    & "クローズして再実行して下さい"

            ElseIf ReuseEnable = False Then
                strMsg = strFullPath & vbCrLf & vbCrLf _
                        & "がオープンされています。" & vbCrLf _
                        & "クローズして再実行して下さい"

            ElseIf Not IsMissing(ReadOnly) Then
                If objBook.ReadOnly <> ReadOnly Then
                    strMsg = strFullPath & vbCrLf & vbCrLf _
                            & "がオープンされています。" & vbCrLf _
                            & "クローズして再実行して下さい"
                End If
            End If

            If strMsg <> "" Then
                Application.ScreenUpdating = True
                objBook.Activate
                Call MsgBox(strMsg, vbCritical)

                Exit Function 'ＮＧ
            End If

            Set wbResult = objBook
            Exit For ' ＯＫ
        End If
    Next objBook

    MyOpenBookCheck = True

End Function

Public _
Function MyBookOpen(ByVal strPath As String, _
                    ByVal ReadOnly As Boolean, _
                    Optional ByVal UpdateLinks As Boolean = False) As Workbook

    Dim strFullPath As String
    Dim strBookName As String
    Dim objBook As Workbook
    Dim booSave As Boolean

    Set MyBookOpen = Nothing

    strFullPath = MyFullPathGet(strPath)
    strBookName = MyBookNameGet(strPath)

    On Error Resume Next
    booSave = Application.ScreenUpdating
    Application.ScreenUpdating = False
    Set objBook = Workbooks.Open(strFullPath, ReadOnly:=ReadOnly, UpdateLinks:=UpdateLinks)
    Application.ScreenUpdating = booSave

    If Err.Number <> 0 Then
        Call MsgBox(strFullPath & vbCrLf _
            & "がオープンできません" & vbCrLf & vbCrLf _
            & Err.Description, vbExclamation)

        Err.Clear
        Exit Function
    End If
    On Error GoTo 0

    If objBook.FullName <> strFullPath Then    '注）Excelのバグ(？)対処
        Call MsgBox(strFullPath & vbCrLf _
            & "の OpenでExcelに異常が発生しました。違うファイル" & vbCrLf _
            & objBook.FullName & vbCrLf _
            & "がOpenされました。" _
            , vbExclamation)

        Set objBook = Nothing
        End
    End If

    Set MyBookOpen = objBook

End Function

'=============================================================
'
'    ワークシートの処理
'
'=============================================================
' 生成パラメータシート取得
Public _
Function ConfigSheetGet() As Worksheet
    Set ConfigSheetGet = 同名のシート取得(ConfigSheetNameGet())
End Function

Public _
Function 問題管理データシート取得() As Worksheet
    Set 問題管理データシート取得 = 同名のシート取得(strProblemSheetName)
End Function

Public _
Function 同名のシート取得(ByVal strSheetName As String, _
                          Optional ByVal TargetBook As Workbook = Nothing, _
                          Optional ByVal NoMsg As Boolean = False) As Object
    Dim i

    If TargetBook Is Nothing Then
        Set TargetBook = ThisWorkbook
    End If

    For Each i In TargetBook.Sheets
        If (UCase(i.Name) = UCase(strSheetName)) Then
            Set 同名のシート取得 = i
            Exit Function
        End If
    Next i

    If NoMsg = False Then
        Call MsgBox(TargetBook.Name & "に「" & strSheetName & "」シートがありません", vbCritical)
    End If

    Set 同名のシート取得 = Nothing

End Function

Public _
Function 同名のワークシート取得(ByVal strSheetName As String, _
                                Optional ByVal TargetBook As Workbook = Nothing, _
                                Optional ByVal NoMsg As Boolean = False) As Worksheet
    Dim i As Worksheet

    If TargetBook Is Nothing Then
        Set TargetBook = ThisWorkbook
    End If

    For Each i In TargetBook.Worksheets
        If (UCase(i.Name) = UCase(strSheetName)) Then
            Set 同名のワークシート取得 = i
            Exit Function
        End If
    Next i

    If NoMsg = False Then
        Call MsgBox(TargetBook.Name & "に「" & strSheetName & "」シートがありません", vbCritical)
    End If

    Set 同名のワークシート取得 = Nothing

End Function

'データシートの見出し行を取得
Public _
Function RedmineSheetHeader(ByVal wsData As Worksheet) As Range
    Dim intRow As Variant
    Dim i As Variant

    intRow = numRedmineSheetHeadRow
    With wsData.UsedRange
        i = .Column + .Columns.Count - 1
    End With

    Do While IsSpaceString(wsData.Cells(intRow, i))
        i = i - 1
        If i < 1 Then
            Call MyRangeSelect(wsData.Rows(intRow))
            Call MsgBox("Bug!! ヘッダが空行です", vbCritical)
            End
        End If
    Loop

    Set RedmineSheetHeader = wsData.Cells(intRow, 1).Resize(, i)
End Function

'データシートのデータ行数を取得
Public _
Function RedmineSheetDataCount(ByVal wsData As Worksheet) As Long
    Dim ii  As Long
    Dim rr  As Range

    ii = GetProbemColumnByItemName("ID") 'IDの列番号取得
    If ii < 1 Then
        RedmineSheetDataCount = -1   '不正なシート
    Else
        Set rr = GetEndToDown(wsData.Cells(numRedmineSheetDataRow, ii))   'ID列の最後のセルを取得
        If rr Is Nothing Then
            RedmineSheetDataCount = 0
        Else
            RedmineSheetDataCount = rr.Row - numRedmineSheetDataRow + 1
        End If
    End If

End Function

' データシートの列番号取得
Public _
Function RedmineSheetHeaderColumn(ByVal wsData As Worksheet, _
                                  ByRef aryName() As String, _
                                  ByRef aryColumn() As Long) As Boolean

    Dim numCol      As Long
    Dim idx As Long
    Dim ss  As String
    Dim rngHeader   As Range

    RedmineSheetHeaderColumn = True

    Set rngHeader = RedmineSheetHeader(wsData)

    For idx = LBound(aryName) To UBound(aryName)
        ss = aryName(idx)
        If ss = "" Then
            numCol = 0
        Else
            numCol = GetProbemColumnByFieldName(ss, rngHeader)
            If numCol = 0 Then
                RedmineSheetHeaderColumn = False '取得できない項目名がある
                Exit For
            End If
        End If

        aryColumn(idx) = numCol
    Next idx

    Set rngHeader = Nothing

End Function

'データシートの最初と最後のデータの行番号を取得
Public _
Function RedmineSheetDataRangeGet(ByVal wsData As Worksheet, _
                                  ByRef topRow As Long, _
                                  ByRef EndRow As Long) As Long
    Dim numCount    As Long

    numCount = RedmineSheetDataCount(wsData)
    If numCount > 0 Then    'データ行がある場合
        topRow = numRedmineSheetDataRow
        EndRow = numRedmineSheetDataRow + numCount - 1

    Else    'データ行がない、または不正なシートの場合
        topRow = 0
        EndRow = 0
    End If

    RedmineSheetDataRangeGet = numCount 'データ行数を返す。
End Function

' データシートからプロジェクトIDを抽出
Public _
Function RedmineSheetProjectID(ByVal wsData As Worksheet) As String
    Dim numCol  As Long
    Dim ss As String

    Dim rngHeader As Range
    Set rngHeader = RedmineSheetHeader(wsData)

    Select Case RedmineSheetDataCount(wsData)
    Case Is < 0
        ss = "" '不正なデータシート
    Case 0
        ss = "" 'データ行が無い。
    Case Else
        numCol = GetProbemColumnByFieldName("プロジェクトID", rngHeader)
        If numCol = 0 Then
            ss = "" '不正なデータシート
        Else
            With wsData.Cells(numRedmineSheetDataRow, numCol)
                ss = Trim$(.Value)
                If ss = "" Then
                    Call MsgBox(セルの位置表示取得(.Cells(1, 1)) & " に有効な値がありません")
                End If
            End With
        End If
    End Select

    RedmineSheetProjectID = ss
End Function

'=============================================================
'
'    コピー処理
'
'=============================================================
' 非表示セルも含む Range.Copy
Sub MyRangeValueCopy(ByVal コピー元 As Range, ByVal コピー先 As Range)

    Dim intOfsRow As Variant
    Dim intOfsCol As Variant
    Dim wsDst As Worksheet
    Dim c1 As Range

    With コピー先.Cells(1, 1)
        intOfsRow = .Row - コピー元.Row
        intOfsCol = .Column - コピー元.Column
        Set wsDst = .Worksheet
    End With

    For Each c1 In コピー元.Cells
'        c1.Copy wsDst.Cells(c1.Row + intOfsRow, c1.Column + intOfsCol)
        With wsDst.Cells(c1.Row + intOfsRow, c1.Column + intOfsCol)
            If VarType(c1.Value) = vbString Then
                .NumberFormat = "@"
            Else
                .NumberFormat = .NumberFormat
            End If
            .Value = c1.Value
        End With
    Next c1

End Sub

'=============================================================
'
'    検索処理
'
'=============================================================
Function MyDataEndRowGet(ByVal StartCell As Range) As Variant

    Dim wsTarget  As Worksheet
    Dim IntMaxRow   As Variant
    Dim intTargetColumn As Variant
    Dim intFound    As Variant
    Dim i As Variant

    If IsSpaceString(StartCell.Value) Then
        intFound = 0
    Else
        Set wsTarget = StartCell.Worksheet
        IntMaxRow = wsTarget.Rows.Count
        intTargetColumn = StartCell.Column
        intFound = IntMaxRow
        For i = StartCell.Row + 1 To IntMaxRow
            If IsSpaceString(wsTarget.Cells(i, intTargetColumn).Value) Then
                intFound = i - 1
                Exit For
            End If
        Next i
    End If

    MyDataEndRowGet = intFound
End Function

' 行方向の検索
Public _
Function GetDataColumn(ByVal StartCell As Range, _
                       ByVal strSearch As String, _
                       Optional ByVal NoMsg As Boolean = False) As Long

    Dim wsTarget  As Worksheet
    Dim intTargetRow  As Variant
    Dim intEndCol   As Variant
    Dim intFound    As Variant
    Dim i   As Variant

    With StartCell
        Set wsTarget = .Worksheet
        intTargetRow = .Row
    End With

    intEndCol = wsTarget.Columns.Count

    intFound = 0
    For i = StartCell.Column To intEndCol
         If Trim$(wsTarget.Cells(intTargetRow, i)) = strSearch Then
            intFound = i
            Exit For
        End If
    Next i

    If (intFound = 0) And Not NoMsg Then
        Call MyRangeSelect(StartCell)
        Call MsgBox("'" & wsTarget.Name & "'シートの " & _
                    intTargetRow & "行目に「" & strSearch & "」がありません", _
                    vbCritical)
    End If

    Set wsTarget = Nothing

    GetDataColumn = intFound

End Function

' 有効データの下端のセルを取得 ※Range.End(xlDown)の替わり
Public _
Function GetEndToDown(ByVal StartCell As Range) As Range
    Dim ws  As Worksheet
    Dim cc  As Range
    Dim rr  As Range

    Set ws = StartCell.Worksheet

    Set rr = Nothing
    For Each cc In StartCell.Cells(1, 1).Resize(ws.Rows.Count - StartCell.Row + 1).Cells
         If Trim$(cc.Value) = "" Then
            Set rr = cc
            Exit For
        End If
    Next cc

    If rr Is Nothing Then
        Set rr = ws.Cells(ws.Rows.Count, StartCell.Column)
    ElseIf rr.Row > StartCell.Row Then
        Set rr = rr.Offset(-1)
    Else
       Set rr = Nothing
    End If

    Set ws = Nothing

    Set GetEndToDown = rr
End Function

' 有効データの上端のセルを取得 ※Range.End(xlUp)の替わり
Public _
Function GetEndToUp(ByVal StartCell As Range) As Range
    Dim cc  As Range
    Dim rr  As Range

    Set cc = StartCell.Cells(1, 1)
    Do While True
        If Trim$(cc.Value) <> "" Then
            Set rr = cc
            Exit Do
        ElseIf cc.Row = 1 Then
            Set rr = Nothing
            Exit Do
        End If
        
        Set cc = cc.Offset(-1)
    Loop

    Set GetEndToUp = rr
End Function

' セルの列名取得    ※未使用
Private _
Function セルの列名取得(ByVal rng As Range) As String
    Dim ss  As String
    Dim ii  As Long

    ss = rng.Cells(1, 1).Address(RowAbsolute:=True, ColumnAbsolute:=False) 'A$1形式
    ii = InStr(ss, "$")

    If ii < 1 Then ii = Len(ss) + 1 ' ←有りえないはずだが、念のため。

    セルの列名取得 = Left(ss, ii - 1)
End Function

Public _
Function セルの位置表示取得(ByVal rng As Range) As String
    セルの位置表示取得 = "'" & rng.Worksheet.Name & "!" & rng.Address
End Function

Public _
Function 名前の参照範囲取得(ByVal ws As Worksheet, strName As String, _
                            Optional ByVal NoMsg As Boolean = False) As Range

    If ws Is Nothing Then
        Call MsgBox("Bug!!", vbCritical)
        Set 名前の参照範囲取得 = Nothing
        Exit Function
    End If

    On Error Resume Next

    Set 名前の参照範囲取得 = ws.Range(strName)
    If Err.Number <> 0 Then
        If NoMsg = False Then
            Call MsgBox("'" & ws.Name & "'シートに'" & strName & "'の名前定義がありません", vbCritical)
        End If
        Err.Clear
        
        Set 名前の参照範囲取得 = Nothing
    End If

    On Error GoTo 0

End Function

Private Sub 非表示の名前定義を全て表示する()
    Dim objName As Object

    For Each objName In ThisWorkbook.Names
        If objName.Visible = False Then
            objName.Visible = True
        End If
    Next
End Sub

' Gaph出力シート作成
Public _
Function FormatSheetCopy(ByVal wsTempl As Worksheet, _
                         ByVal strSheetName As String) As Worksheet
    Dim varSave As Variant
    Dim wsGraph As Worksheet
    Dim Obj

    Set FormatSheetCopy = Nothing

    Call StatisticsLibrary.同名のワークシートを削除(strSheetName)

    Set Obj = 生成シートの挿入位置取得()
    If Obj Is Nothing Then
        Exit Function
    End If

    On Error GoTo Add_Error
    varSave = wsTempl.Visible
    wsTempl.Visible = xlSheetVisible
    wsTempl.Copy After:=Obj
    Set wsGraph = ActiveSheet
    wsTempl.Visible = varSave

    On Error GoTo Init_Error
    Call FormatSheetPropertySet(wsGraph, Visible:=True, ChangeEditable:=True)
    wsGraph.Name = strSheetName

    On Error GoTo 0

    Set Obj = Nothing

    Set FormatSheetCopy = wsGraph
    Exit Function

Add_Error:
    Call MsgBox("ワークシートの追加に失敗しました", vbCritical)
    Exit Function

Init_Error:
    Call MsgBox("ワークシートの初期化に失敗しました" & vbCrLf & vbCrLf _
        & Err.Description, vbCritical)

End Function

Public _
Function IsFormatSheet(ByVal ws As Worksheet) As Boolean
    If ws.Name Like (strFormatSheetPrefix & "*") Then
        IsFormatSheet = True
    Else
        IsFormatSheet = False
    End If
End Function

Public _
Sub FormatSheetAllVisible()
    Dim ws  As Worksheet

    For Each ws In ThisWorkbook.Worksheets
        If IsFormatSheet(ws) Then
            Call FormatSheetPropertySet(ws, Visible:=True)
        End If
    Next ws
End Sub

Public _
Sub FormatSheetAllUnVisible()
    Dim ws As Worksheet

    For Each ws In ThisWorkbook.Worksheets
        If IsFormatSheet(ws) Then
            Call FormatSheetPropertySet(ws, Visible:=False)
        End If
    Next ws
End Sub

' 出力書式シート表示／非表示
Public _
Sub FormatSheetVisibleReverse(ByVal strSheetNameSuffix As String, _
                              Optional doVisible As Variant)

    Dim strSheetName As String
    Dim objTmp As Object
    Dim i   As Long
    Dim booFirst As Boolean
    Dim NewValue As Boolean

    If IsMissing(doVisible) Then
        booFirst = True
    Else
        booFirst = False
        NewValue = doVisible
    End If

    i = 0
    strSheetName = strSheetNameSuffix & "*"
    For Each objTmp In ThisWorkbook.Sheets
        If objTmp.Name Like strSheetName Then
            If IsFormatSheet(objTmp) = False Then
                Call MsgBox("「" & objTmp.Name & "」は書式シートの正しい名前ではありません", vbCritical)
                Exit Sub
            End If

            If booFirst Then
                NewValue = IIf(objTmp.Visible = xlSheetVisible, False, True)
                booFirst = False
            End If

            Call FormatSheetPropertySet(objTmp, Visible:=NewValue)

            i = i + 1
        End If
    Next objTmp

    If i = 0 Then
        Call MsgBox("「" & strSheetNameSuffix & "」シートがありません", vbCritical)
    End If

End Sub

Private _
Sub FormatSheetPropertySet(ByVal ws As Worksheet, _
                           ByVal Visible As Boolean, _
                           Optional ByVal ChangeEditable As Boolean = False)

    Dim varSave As Variant

    If Visible Then
        varSave = Application.ScreenUpdating
        Application.ScreenUpdating = False
        ws.Visible = xlSheetVisible
        If ChangeEditable Then
            ws.Unprotect
        End If
'        ws.EnableCalculation = True
'        ws.EnableFormatConditionsCalculation = True
        ws.Activate
        Application.ScreenUpdating = varSave
    Else
        ws.Visible = xlSheetHidden
        ws.Protect
'        ws.EnableCalculation = False
'        ws.EnableFormatConditionsCalculation = False
    End If
End Sub

'=============================================================
'
'    セルの処理
'
'=============================================================

'指定範囲のセルの値を配列に変換する｡
'   ※複数行選択は不可
Public _
Function Row2StringArray(ByVal rngList As Range, _
                         ByRef strList() As String) As Boolean
    Dim min_col As Long
    Dim max_col   As Long
    Dim c1  As Range
    Dim ii  As Long
    Dim Flag    As Boolean

    min_col = rngList.Worksheet.Columns.Count
    max_col = 0

    For Each c1 In rngList.Cells
        If min_col > c1.Column Then
            min_col = c1.Column
        ElseIf max_col < c1.Column Then
            max_col = c1.Column
        End If
    Next c1

    ReDim strList(max_col - min_col)
    For ii = 0 To UBound(strList)
        strList(ii) = ""
    Next ii

    Flag = False
    For Each c1 In rngList.Cells
        ii = c1.Column - min_col
        strList(ii) = Trim$(c1.Value)
        If strList(ii) <> "" Then
            Flag = True
        End If
    Next c1

    If Flag = False Then
        Call MsgBox(セルの位置表示取得(rngList) & "に有効な値がありません", vbCritical)
    End If

    Row2StringArray = Flag

End Function

Public _
Function セル値を加算(ByVal rng As Range, _
                      Optional num As Long = 1) As Boolean
    If IsNumeric(rng.Value) Then
        rng.Value = CLng(rng.Value) + num
        セル値を加算 = True
    Else
        rng.Worksheet.Activate
        rng.Activate
        Call MsgBox(セルの位置表示取得(rng) & "の値が数値ではありません", vbCritical)
        セル値を加算 = False
    End If

End Function

'指定範囲をシート最終行までクリアする｡
Public _
Function ClearRangeToEndRow(ByVal rr As Range) As Range
    Dim end_row As Long
    Dim end_col As Long

    end_col = rr.Column + rr.Columns.Count - 1

    With rr.Worksheet
        end_row = .Rows.Count

        .Range(rr, .Cells(end_row, end_col)).Clear
    End With
End Function

Public _
Function GetDateValue(ByVal objRange As Range) As Variant
    Dim varTmp As Variant

    varTmp = objRange.Value
    If IsSpaceString(varTmp) Then
        varTmp = Null  '未設定
    ElseIf VarType(varTmp) <> vbDate Then
        If IsDate(varTmp) Then
            varTmp = CDate(varTmp)
        Else
            varTmp = False '値エラー
        End If
    End If

    GetDateValue = varTmp
End Function

'=============================================================
'
'    文字列処理
'
'=============================================================
Public _
Function IsSpaceString(ByVal 値 As String) As Boolean

    ' 空白文字の除去
    If ReplaceSpaceChars(値, "") = "" Then
        IsSpaceString = True
    Else
        IsSpaceString = False
    End If

End Function

Public _
Function ReplaceSpaceChars(ByVal 変換対象 As String, _
                           ByVal 置換文字 As String) As String
    Dim strTmp As String
    Dim i As Long

    strTmp = 変換対象

    ' 空白文字を半角空白に置換
    strTmp = Replace(strTmp, "　", " ") '全角空白
    strTmp = Replace(strTmp, vbTab, " ")
    strTmp = Replace(strTmp, vbLf, " ")
    strTmp = Replace(strTmp, vbCr, " ")
    strTmp = Replace(strTmp, vbBack, " ")

    '連続する空白を１文字に変換
    Do
        i = Len(strTmp)
        strTmp = Replace(strTmp, "  ", " ")
    Loop While Len(strTmp) <> i

    ReplaceSpaceChars = Replace(strTmp, " ", 置換文字)

End Function
