Attribute VB_Name = "StatisticsPB"
Option Explicit

Private Const strConstSplitSheetPrefix As String = "C_"
Private Const strConstProcessSheetPrefix As String = "P_"

Private Const strConstGraphNamePrefix As String = "G_PB_"
Private Const strConstDataNamePrefix As String = "Data_PB_"

Private Const strDefNameConfig As String = "PB_Category_setting"

' Redmine試験管理シートのフォーマット
Private Const intConstTestItemSheetDataRow As Long = 3  'データ開始行
Private Const intConstTestPrimaryKeyColumnNumber As Long = 1  '主キーの列番号
Private Const intConstTestStartDayColumnNumber As Integer = 22  '試験開始日の列番号
Private Const intConstTestEndDayColumnNumber As Integer = 24  ' 試験完了日の列番号
Private Const intConstFirstEstimateTestEndDayColumnNumber As Integer = 19 ' 完了予定日(初期計画)

' ST試験項目書の小項目一覧シートのフォーマット
Private Const intConstTestItem2SheetDataRow As Long = 4 'データ開始行
Private Const intConstTestItem2PrimaryKeyColumn As Long = 1 '主キーの列番号
Private Const intConstTestItem2StartDayColumn As Integer = 14   '「1st Attempt」の列番号
Private Const intConstTestItem2EndDayColumn As Integer = 16  ' 「1st Pass」の列番号

Private Const strConstTestItem2Sheet1Name As String = "小項目一覧"
Private Const strConstTestItem2Sheet2Name As String = "大項目一覧"

' ST試験項目書の大項目一覧シートのフォーマット
Private Const intConstTestItem3SheetDataRow As Long = 6 'データ開始行
Private Const intConstTestItem3PrimaryKeyColumn As Long = 1 '主キーの列番号
Private Const intConstTestItem3StartDayColumn As Long = 6  '予定期間開始日の列番号
Private Const intConstTestItem3EndDayColumn As Long = 7  '予定期間終了日の列番号
Private Const intConstTestItem3SmallItemNumberColumn As Long = 9 '「小項目数」の列番号

Private Const strJobName As String = "PB曲線生成"

'試験管理データシートのデータ開始行、終了行
Private intTestItemSheetDataRow As Long
Private intTestItemSheetEndDataRow As Long

Private booTestItemIsRedmine As Boolean '試験管理データが Redmineシートの場合、True

Sub PBExecute(Optional ByVal booEndMessage As Boolean = True)

    Dim wsProblem   As Worksheet '問題管理データシート
    Dim strSheetName As String
    Dim varInputSheet As Variant '問題管理データシート/分割シート
    Dim strSplitName() As String
    Dim i As Long

    If ProblemDataBaseTypeGet() = "Redmine" Then
        Select Case TargetProjectNameGet()
        Case "L100", "T200"
            Call 未実装(booEndMessage)
            Exit Sub
        End Select
    End If

    If booEndMessage Then
        If MsgBox(strJobName & "を開始します。", vbOKCancel) <> vbOK Then
            Exit Sub
        End If
    End If

    ' Statusバー表示
    StatisticsLibrary2.JobBegin (strJobName & "中...")

    Set wsProblem = 問題管理データシート取得()
    If wsProblem Is Nothing Then
        booEndMessage = False
        GoTo Exit_Proc
    End If

    If (ProblemDataBaseTypeGet() = "Redmine") And (TargetProjectNameGet() = "T3xx") Then
        If 分割シート取得(wsProblem, "カテゴリ", strConstSplitSheetPrefix, strSplitName, varInputSheet) = False Then
            booEndMessage = False
            GoTo Exit_Proc
        End If
    Else
        Set varInputSheet = wsProblem
    End If

    If IsArray(varInputSheet) Then
        For i = 1 To UBound(varInputSheet)
            If strSplitName(i) = "" Then
                ' カテゴリの設定がないレコードは無視する。
            Else
                strSheetName = varInputSheet(i)
                Set wsProblem = StatisticsLibrary2.同名のシート取得(strSheetName)
                If PB_Main(wsProblem, True, strSplitName(i)) < 0 Then
                    booEndMessage = False
                    Exit For
                End If
            End If
        Next i

        Call 分割シート後処理(varInputSheet)

    ElseIf PB_Main(wsProblem) < 0 Then
        booEndMessage = False
    End If

Exit_Proc:
    ' Statusバー表示
    StatisticsLibrary2.JobEnd strJobName & "処理終了", MsgBoxShow:=booEndMessage

End Sub

Private _
Function PB_Main(ByVal wsProblem As Worksheet, _
                 Optional ByVal IsSplitSheet As Boolean = False, _
                 Optional ByVal strSplitName As String = "") As Long

    Dim intResult As Long
    Dim strAnalyzeSheetName() As String
    Dim intTargetColumn As Integer
    Dim i As Integer

    Dim strGeneSource As String
    Dim strSep As String
    Dim strDataSheetName As String ' 計算シート
    Dim strGraphName As String
    Dim strResultSheetName As String
    Dim strTargetDescription As String

    'PB曲線の設定値
    Dim varTargetProcess As Variant
    Dim strByWeekOrDay As String
    Dim dtProcessStart As Date  '工程開始日
    Dim dtProcessEnd As Date    '工程終了(予定)日
    Dim intTestParamA As Integer
    Dim intTestParamB As Integer
    Dim dblTestParamC As Double
    Dim strTestItemFormat As String '試験管理シートのフォーマット種別
    Dim strTestSheetName    As String '試験管理シート
    Dim strTestFileName     As String

    intResult = -1 '実行エラー

    strGeneSource = TargetProjectNameGet()
    If IsSplitSheet Then
        strGeneSource = strSplitName & "/" & strGeneSource
        strSep = "_" & strSplitName
    Else
        strSep = ""
    End If

    Call DeleteGraphByChartName(strConstGraphNamePrefix & "*")

    '' PB曲線の設定を取得
    Select Case PB_ConfigGet(IsSplitSheet, _
                             strSplitName, _
                             varTargetProcess, _
                             strByWeekOrDay, _
                             dtProcessStart, _
                             dtProcessEnd, _
                             intTestParamA, _
                             intTestParamB, _
                             dblTestParamC, _
                             strTestItemFormat, _
                             strTestSheetName, _
                             strTestFileName)
    Case Is < 0
        GoTo Exit_Proc
    Case Is = 0
        intResult = 0  ' 実行設定なし
        GoTo Exit_Proc
    End Select

    '' 工程別シート生成
    '   注）試験管理データは工程が判別できないので、問題管理データの工程別分割はせず対象工程のデータ抽出のみ行う。
    strAnalyzeSheetName = ByProcessSheetGenerate(wsProblem _
                                               , NewSheetNamePrefix:=strConstProcessSheetPrefix _
                                               , GroupBy:=非分割 _
                                               , OnlyEnable:=False _
                                               , TargetProcess:=varTargetProcess _
                                               , JobEndIfEmpty:=False)

    If UBound(strAnalyzeSheetName) < 1 Then
        GoTo Exit_Proc
    End If

    '' 参照する項目の列番号を取得
    ' 問題管理の開始日の列番号
    intTargetColumn = StatisticsLibrary2.GetProbemColumnByItemName("問題発生日")
    If intTargetColumn < 1 Then
        GoTo Exit_Proc
    End If

    '' PB
    strDataSheetName = 集計用出力シート名生成(strConstDataNamePrefix & strSep)
    strGraphName = 集計用出力シート名生成(strConstGraphNamePrefix & "_") ' 注) グラフシートはグラフ結合後に削除される
    strTargetDescription = Join(varTargetProcess, ",")
    If IsSplitSheet Then
        strTargetDescription = strSplitName & "の" & strTargetDescription
    End If

    If PB(strByWeekOrDay, _
          dtProcessStart, _
          dtProcessEnd, _
          intTestParamA, _
          intTestParamB, _
          dblTestParamC, _
          strTargetDescription, _
          strAnalyzeSheetName(1), _
          intTargetColumn, _
          Mid(Join(varTargetProcess, ","), 2), _
          strGraphName, _
          strDataSheetName, _
          strTestItemFormat, _
          strTestSheetName, _
          strTestFileName) = False Then
        GoTo Exit_Proc
    End If

    ThisWorkbook.Activate

    ' グラフ結合
    strResultSheetName = 集計用出力シート名生成("Graph_PB" & strSep)
    JointGraphByChartName strResultSheetName, strGraphName & "*", strGeneSource

    intResult = 1 '正常終了

    ' 工程別シートの削除
    For i = 1 To UBound(strAnalyzeSheetName)
        同名のワークシートを削除 strAnalyzeSheetName(i)
    Next

Exit_Proc:
    Call MyArrayClear(strAnalyzeSheetName)
    Call MyArrayClear(varTargetProcess)

    PB_Main = intResult
End Function

'' PB曲線の設定を取得
Private Function PB_ConfigGet(ByVal IsSplitSheet As Boolean, _
                              ByVal strSplitName As String, _
                              ByRef varTargetProcess As Variant, _
                              ByRef strByWeekOrDay As String, _
                              ByRef dtProcessStart As Date, _
                              ByRef dtProcessEnd As Date, _
                              ByRef intTestParamA As Integer, _
                              ByRef intTestParamB As Integer, _
                              ByRef dblTestParamC As Double, _
                              ByRef strTestItemFormat As String, _
                              ByRef strTestSheetName As String, _
                              ByRef strTestFileName As String) As Long

    Dim intResult As Long
    Dim varSettingParam As Variant
    Dim objTestItemBook As Workbook
    Dim varTmp As Variant
    Dim strTmp As String
    Dim i As Integer

    Dim strEstimatedPeriodSheetName As String   '予定件数シート名

    intResult = -1

    strByWeekOrDay = "Day" '固定

    '' 入力指定取得
    varSettingParam = GetStringSettingParam("PB_setting")
    strTestItemFormat = varSettingParam(1)
    strTestSheetName = varSettingParam(2)

    varTargetProcess = Split("," & varSettingParam(3), ",") '注) 添え字が　1から始まる配列を返す
    For i = 1 To UBound(varTargetProcess)
        strTmp = Trim$(varTargetProcess(i))
        If IsSpaceString(strTmp) Then
            Call MsgBox(strJobName + "設定: 工程指定が空", vbExclamation)
            GoTo Exit_Proc
        End If
        varTargetProcess(i) = strTmp
    Next i

    '' 生成パラメータ取得
    If Not IsSplitSheet Then
        varSettingParam = GetRangeSettingParam("PB_general_setting", strSearchKey:="生成パラメータ")
        If ArrayElementsCount(varSettingParam) < 2 Then
            Call MsgBox("設定: PB曲線の設定に「生成パラメータ」の記載がありません。", vbExclamation)
            GoTo Exit_Proc
        End If
    Else
        varSettingParam = GetRangeSettingParam("PB_Category_setting", strSearchKey:=strSplitName)
        If ArrayElementsCount(varSettingParam) < 2 Then
            intResult = 0
            GoTo Exit_Proc
        End If
    End If

    strTestFileName = varSettingParam(7).Value
    If IsSpaceString(strTestFileName) Then
        If Not IsSplitSheet Then
            Call MsgBox(strJobName + "設定: 必要な設定(試験管理ファイル名)がありません。", vbExclamation)
        Else
            intResult = 0
        End If
        GoTo Exit_Proc
    End If

    For i = 1 To 6
        If IsSpaceString(varSettingParam(i).Value) Then
            Call MsgBox(strJobName + "設定: 必要な設定がありません。", vbExclamation)
            GoTo Exit_Proc
        End If

        Select Case i
        Case 2, 3
            varTmp = GetDateValue(varSettingParam(i))
            If VarType(varTmp) <> vbDate Then ' 指定なし
                Call MsgBox(strJobName + "設定: 日付けを設定する欄に日付け以外が設定されています。", vbExclamation)
                GoTo Exit_Proc
            End If
            If i = 2 Then
                dtProcessStart = varTmp
            Else
                dtProcessEnd = varTmp
            End If

        Case 4 To 6
            If Not IsNumeric(varSettingParam(i).Value) Then
                Call MsgBox(strJobName + "設定: 数値を設定する欄に数値以外が設定されています。", vbExclamation)
                GoTo Exit_Proc
            End If
        End Select
    Next i

    intTestParamA = varSettingParam(4).Value
    intTestParamB = varSettingParam(5).Value
    dblTestParamC = varSettingParam(6).Value

    '' 試験管理シート名取得
    If UCase(strTestItemFormat) = "試験項目書" Then
        strTestSheetName = strConstTestItem2Sheet1Name

    ElseIf strTestSheetName = "" Then
        Call MsgBox(strJobName + "設定: 必要な設定(試験工程シート名)がありません。", vbExclamation)
        GoTo Exit_Proc

    End If

    intResult = 1

Exit_Proc:
    Erase varSettingParam

    PB_ConfigGet = intResult
End Function

' PB
Private Function PB(ByVal strByWeekOrDay As String, _
                    ByVal dtProcessStart As Date, _
                    ByVal dtProcessEnd As Date, _
                    ByVal intTestParamA As Integer, _
                    ByVal intTestParamB As Integer, _
                    ByVal dblTestParamC As Double, _
                    ByVal strTargetDescription As String, _
                    ByVal strRedmineSheetName As String, _
                    ByVal intTargetColumn As Integer, _
                    ByVal strGraphTitle As String, _
                    ByVal strGraphName As String, _
                    ByVal strAnalyzeSheetName As String, _
                    ByVal strTestItemFormat As String, _
                    ByVal strTestSheetName As String, _
                    ByVal strTestFileName As String) As Boolean

    Dim intTarget0Column As Integer
    Dim intTarget1Column As Integer
    Dim intTarget2Column As Integer
    Dim intTarget3Column As Integer

    Dim objProblemSheet As Worksheet    '問題管理シート
    Dim objTestItemSheet    As Worksheet '試験管理シート
    Dim objTestItemBook As Workbook
    Dim objAnalyzeSheet As Worksheet
    Dim varTmp As Variant

    Dim dtTestActualStart As Date '試験完了実績日の最小値
    Dim dtTestActualEnd As Date '試験完了実績日の最大値
    Dim dtProblemStart As Date  '問題発生日の最小値
    Dim dtProblemEnd As Date    '問題発生日の最大値
    Dim intTotalNumOfTestItems As Long  '試験総項目数

    Dim r As Range

    Dim intDataEndRow As Long

    Dim strEstimatedPeriodSheetName As String   '予定件数シート名
    Dim objEstimatedPeriodSheet As Worksheet    '予定件数シート
    Dim booProblemExists As Boolean

    PB = False

    strEstimatedPeriodSheetName = ""
    Set objTestItemBook = Nothing

    '' 試験管理シート取得
    If UCase(strTestItemFormat) = "試験項目書" Then
        strEstimatedPeriodSheetName = strConstTestItem2Sheet2Name
    End If

    If strTestFileName = "" Then
        Set objTestItemBook = ThisWorkbook
    Else
        Set objTestItemBook = MyRefBookOpen(strTestFileName)
        If objTestItemBook Is Nothing Then
            GoTo Error_Exit
        End If
    End If

    Set objTestItemSheet = 同名のワークシート取得(strTestSheetName, objTestItemBook)
    If objTestItemSheet Is Nothing Then
        GoTo Error_Exit
    End If

    If strEstimatedPeriodSheetName <> "" Then
        Set objEstimatedPeriodSheet = 同名のワークシート取得(strEstimatedPeriodSheetName, objTestItemBook)
        If objEstimatedPeriodSheet Is Nothing Then
            GoTo Error_Exit
        End If
    Else
        Set objEstimatedPeriodSheet = Nothing
    End If

    Select Case UCase(strTestItemFormat)
    Case UCase("Redmine")
        booTestItemIsRedmine = True

        ' 試験管理シートのデータ開始行
        intTestItemSheetDataRow = intConstTestItemSheetDataRow

        ' 試験管理シートの主キーの列番号
        intTarget0Column = intConstTestPrimaryKeyColumnNumber
        ' 試験管理シートの開始日の列番号
        intTarget1Column = intConstTestStartDayColumnNumber
        ' 試験管理シートの試験完了実績日の列番号
        intTarget2Column = intConstTestEndDayColumnNumber
        ' 試験管理シートの試験完了予定日の列番号
        intTarget3Column = intConstFirstEstimateTestEndDayColumnNumber
    Case UCase("試験項目書")
        booTestItemIsRedmine = False
        ' 試験管理シートのデータ開始行
        intTestItemSheetDataRow = intConstTestItem2SheetDataRow

        ' 試験管理シートの主キーの列番号
        intTarget0Column = intConstTestItem2PrimaryKeyColumn
        ' 試験管理シートの開始日の列番号
        intTarget1Column = intConstTestItem2StartDayColumn '「1st Attempt」
        ' 試験管理シートの試験完了実績日の列番号
        intTarget2Column = intConstTestItem2EndDayColumn   '「1st Pass」

        ' 試験管理シートの試験完了予定日の列番号
        intTarget3Column = intConstTestItem3EndDayColumn '予定期間終了日の列

    Case Else
        Call MsgBox(strJobName + "設定: 試験管理シート種別「" & strTestItemFormat & "」は未対応です", vbExclamation)
        GoTo Error_Exit

    End Select

    With objTestItemSheet
        Set r = StatisticsLibrary2.GetEndToUp(.Cells(.Rows.Count, intTarget0Column))
        If r Is Nothing Then
            intTestItemSheetEndDataRow = 0
        ElseIf r.Row < intTestItemSheetDataRow Then
            intTestItemSheetEndDataRow = 0
        Else
            intTestItemSheetEndDataRow = r.Row
        End If

        If intTestItemSheetEndDataRow < intTestItemSheetDataRow Then
            Call MsgBox("試験管理シート(" & .Name & ")に有効なデータがありません。", vbCritical)
            GoTo Error_Exit
        End If
    End With

    ' 試験総項目の抽出
    intTotalNumOfTestItems = intTestItemSheetEndDataRow - intTestItemSheetDataRow + 1

    ' 試験完了実績日の最小値と最大値を取得
    Call TargetSheetSelect(objTestItemSheet)
    varTmp = GetTargetColumnDateMinMax(intTarget2Column _
                , intTargetRowOffset:=intTestItemSheetDataRow - 1 _
                , intTargetColumnEndRow:=intTestItemSheetEndDataRow _
                , booFormatFlag:=booTestItemIsRedmine)
    dtTestActualStart = varTmp(1)
    dtTestActualEnd = varTmp(2)

    ' 工程期間を実績日付で補整
    If Not DateIsInitial(dtTestActualEnd) Then
        If dtProcessStart > dtTestActualStart Then
            dtProcessStart = dtTestActualStart
        End If
        If dtProcessEnd < dtTestActualEnd Then
            dtProcessEnd = dtTestActualEnd
        End If
    End If

    ' 問題管理シートから問題発生期間を取得
    Set objProblemSheet = ThisWorkbook.Worksheets(strRedmineSheetName)

    Call TargetSheetSelect(objProblemSheet)
    booProblemExists = True
    varTmp = GetTargetColumnDateMinMax(intTargetColumn)
    dtProblemStart = varTmp(1)
    dtProblemEnd = varTmp(2)
    If DateIsInitial(dtProblemStart) Then
        If (MsgBox(strTargetDescription & "の問題管理データがありません。" & vbCrLf _
                & "問題件数なしでグラフを出力しますか？", vbQuestion + vbOKCancel) = vbCancel) Then
            GoTo Error_Exit
        End If
        booProblemExists = False

    ElseIf DateDiff("d", dtProblemStart, dtProblemEnd) < 2 Then
        If (MsgBox(strTargetDescription & "のサンプル数が不足しています。" & vbCrLf _
                & "問題件数なしでグラフを出力しますか？", vbQuestion + vbOKCancel) = vbCancel) Then
            GoTo Error_Exit
        End If
        booProblemExists = False

    Else
        ' 実績最終日を補整
        If Not DateIsInitial(dtTestActualEnd) Then
            Select Case dtProblemEnd
            Case Is < dtTestActualEnd
                dtProblemEnd = dtTestActualEnd
            Case Is > dtTestActualEnd
                dtTestActualEnd = dtProblemEnd
            End Select
        End If

        ' 工程期間を実績日付で補整
        If dtProcessStart > dtProblemStart Then
            dtProcessStart = dtProblemStart
        End If

        If dtProcessEnd < dtProblemEnd Then
            dtProcessEnd = dtProblemEnd
        End If
    End If

    ' 計算シート生成
    Set objAnalyzeSheet = PBSheetGenerate(strAnalyzeSheetName)

  If booProblemExists Then
    ' 問題発生実績計算
    EndDayWrite objAnalyzeSheet, dtProcessStart, dtProblemEnd, strByWeekOrDay, 1
    ActualSamplingNoWrite objAnalyzeSheet, 1, 2
    NumberOfDailyBugWrite objProblemSheet, objAnalyzeSheet, intTargetColumn, dtProcessStart, dtProblemEnd, strByWeekOrDay, 3
  End If

    ' 問題発生予測値計算
    EndDayWrite objAnalyzeSheet, dtProcessStart, dtProcessEnd, strByWeekOrDay, 5
    ActualSamplingNoWrite objAnalyzeSheet, 5, 6
    PBPredictedCalc objAnalyzeSheet, intTestParamA, intTestParamB, dblTestParamC, 6, 7

    ' 試験完了予定数
  If objEstimatedPeriodSheet Is Nothing Then
    TestCompletionWrite strByWeekOrDay, _
                        dtProcessStart, _
                        objTestItemSheet, _
                        objAnalyzeSheet, _
                        intTarget3Column, _
                        10
  Else
        Set r = StatisticsLibrary2.GetEndToDown(objEstimatedPeriodSheet.Cells(intConstTestItem3SheetDataRow, intConstTestItem3PrimaryKeyColumn))
        If r Is Nothing Then
            intDataEndRow = intConstTestItem3SheetDataRow - 1
        Else
            intDataEndRow = r.Row
        End If

        TestEstimatedPeriodWrite strByWeekOrDay, _
                            dtProcessStart, _
                            objInSheet:=objEstimatedPeriodSheet, _
                            objOutSheet:=objAnalyzeSheet, _
                            intTargetRowTop:=intConstTestItem3SheetDataRow, _
                            intTargetRowEnd:=intDataEndRow, _
                            intStartDateColumn:=intConstTestItem3StartDayColumn, _
                            intEndDateColumn:=intConstTestItem3EndDayColumn, _
                            intItemNumberColumn:=intConstTestItem3SmallItemNumberColumn, _
                            intOutTargetColumn:=10
  End If

    ' 試験完了予定数を減算(残試験項目数算出）
    TestCompletionSubWrite objAnalyzeSheet, 10

    ' 試験完了実績数
  If Not DateIsInitial(dtTestActualEnd) Then
    TestCompletionActualWrite strByWeekOrDay, _
                            dtProcessStart, _
                            dtTestActualEnd, _
                            objTestItemSheet, _
                            objAnalyzeSheet, _
                            intTarget2Column, _
                            15
  End If

    ' 試験完了実績数を減算(残試験項目数算出）
    TestCompletionSubWrite objAnalyzeSheet, 15, intTotalNumOfTestItems

    ' テーブルをマージ
    PBTableGenerate strByWeekOrDay, objAnalyzeSheet, 1, 5, 10, 15, 20

    ' グラフ
    PBGenerateGraph strGraphTitle, strGraphName, objAnalyzeSheet, 20

    PB = True

Error_Exit:
    ' 計算用シートの非表示
'    同名のワークシートを非表示 objAnalyzeSheet.Name

    If objTestItemBook Is Nothing Then
    ElseIf objTestItemBook Is ThisWorkbook Then
    Else
        objTestItemBook.Close SaveChanges:=False
    End If

    Set objTestItemSheet = Nothing
    Set objTestItemBook = Nothing
End Function

'　日毎の試験項目数(累積)
Private Sub NumberOfDailyTestWrite(ByVal objInSheet As Worksheet, _
                                   ByVal objOutSheet As Worksheet, _
                                   ByVal intTargetColumn As Integer, _
                                   ByVal dtStart As Date, _
                                   ByVal dtEnd As Date, _
                                   ByVal strByWeekOrDay As String, _
                                   ByVal intOutTargetColumn As Integer)

    Call NumberOfDailyWrite(objInSheet, _
                            objOutSheet, _
                            intTargetColumn, _
                            dtStart, _
                            dtEnd, _
                            strByWeekOrDay, _
                            intOutTargetColumn, _
                            booTestItemIsRedmine)
End Sub

'　日毎のバグ数(累積)
Private Sub NumberOfDailyBugWrite(ByVal objInSheet As Worksheet, _
                                  ByVal objOutSheet As Worksheet, _
                                  ByVal intTargetColumn As Integer, _
                                  ByVal dtStart As Date, _
                                  ByVal dtEnd As Date, _
                                  ByVal strByWeekOrDay As String, _
                                  ByVal intOutTargetColumn As Integer)
               
    Call NumberOfDailyWrite(objInSheet, _
                            objOutSheet, _
                            intTargetColumn, _
                            dtStart, _
                            dtEnd, _
                            strByWeekOrDay, _
                            intOutTargetColumn, _
                            True)
End Sub

'　日毎の件数(累積)
Private Sub NumberOfDailyWrite(ByVal objInSheet As Worksheet, _
                               ByVal objOutSheet As Worksheet, _
                               ByVal intTargetColumn As Integer, _
                               ByVal dtStart As Date, _
                               ByVal dtEnd As Date, _
                               ByVal strByWeekOrDay As String, _
                               ByVal intOutTargetColumn As Integer, _
                               ByVal booFormatFlag As Boolean)

    Dim intCumulativeValue As Integer
    Dim i As Integer
    Dim j As Integer

    objInSheet.Parent.Activate
    objInSheet.Select

    Columns(intTargetColumn).NumberFormatLocal = "yyyy-mm-dd"
    intCumulativeValue = 0

    For i = 0 To DateDiff("d", dtStart, dtEnd)
        intCumulativeValue = intCumulativeValue + WorksheetFunction.CountIf(Columns(intTargetColumn), DateAdd("d", i, dtStart))
        If (strByWeekOrDay = "Day") Then
            objOutSheet.Cells(i + 1, intOutTargetColumn).Value = intCumulativeValue
        Else
            If (i Mod 7 = 6) Or (i = DateDiff("d", dtStart, dtEnd)) Then
                objOutSheet.Cells(j + 1, intOutTargetColumn).Value = intCumulativeValue
                j = j + 1
            End If
       End If
    Next

    If booFormatFlag Then
        Columns(intTargetColumn).NumberFormatLocal = "@"
    End If

End Sub

' 計算用シート生成
Private Function PBSheetGenerate(ByVal strOutSheetName As String) As Worksheet

    '同名のワークシートをクリア strOutSheetName
    同名のワークシートを削除 strOutSheetName
    同名のワークシートが無ければ追加 strOutSheetName

    Set PBSheetGenerate = ActiveSheet
End Function


' 予測値の算出(PB用)
Private Sub PBPredictedCalc(ByVal objInSheet As Worksheet, _
                            ByVal intA As Integer, _
                            ByVal dblB As Double, _
                            ByVal dblC As Double, _
                            ByVal intSamplingNumColumn As Integer, _
                            ByVal intOutTargetColumn As Integer)
    Dim i As Integer
    Dim strTmp As String
    objInSheet.Parent.Activate
    objInSheet.Select
    ' 予測値
    For i = 1 To GetTargetColumnEndRow(intSamplingNumColumn)
        strTmp = intA & "/ " & "(1+" & dblB & "*EXP(-" & dblC & "*" & Cells(i, intSamplingNumColumn).Value & "))"
        Cells(i, intOutTargetColumn).Value = Evaluate(strTmp)
    Next

End Sub

' 試験完了予定数(範囲指定から取得)
Private Sub TestEstimatedPeriodWrite(ByVal strByWeekOrDay As String, _
                                     ByVal dtProcessStart As Date, _
                                     ByVal objInSheet As Worksheet, _
                                     ByVal objOutSheet As Worksheet, _
                                     ByVal intTargetRowTop As Long, _
                                     ByVal intTargetRowEnd As Long, _
                                     ByVal intStartDateColumn As Integer, _
                                     ByVal intEndDateColumn As Integer, _
                                     ByVal intItemNumberColumn As Integer, _
                                     ByVal intOutTargetColumn As Integer)

    Dim dtMin As Date
    Dim dtMax As Date
    Dim dtVal As Variant

    dtMin = dtProcessStart

    '' 全大項目の試験期間の最小値と最大値を取得
    Call TargetSheetSelect(objInSheet)
    dtVal = GetTargetColumnDateMinMax(intStartDateColumn, _
                intTargetRowOffset:=intTargetRowTop - 1, _
                intTargetColumnEndRow:=intTargetRowEnd, _
                booFormatFlag:=False)
    If StatisticsLibrary.DateIsInitial(dtVal(1)) Then
        Exit Sub
    End If

    If dtMin > dtVal(1) Then
        dtMin = dtVal(1)
    End If
    dtMax = dtVal(2)

    dtVal = GetTargetColumnDateMinMax(intEndDateColumn, _
                intTargetRowOffset:=intTargetRowTop - 1, _
                intTargetColumnEndRow:=intTargetRowEnd, _
                booFormatFlag:=False)
    If StatisticsLibrary.DateIsInitial(dtVal(1)) Then
        Exit Sub
    End If

    If dtMin > dtVal(1) Then
        dtMin = dtVal(1)
    End If
    If dtMax < dtVal(2) Then
        dtMax = dtVal(2)
    End If

    EndDayWrite objOutSheet, dtMin, dtMax, "Day", intOutTargetColumn
    ActualSamplingNoWrite objOutSheet, intOutTargetColumn, intOutTargetColumn + 1
'    NumberOfDailyTestWrite objInSheet, objOutSheet, intStartDateColumn, dtMin, dtMax, "Day", intOutTargetColumn + 2

    ''大項目毎に、一日当たりの予定件数を設定
    Dim intRowOfs As Long
    Dim varStart As Variant '開始予定日
    Dim varEnd As Variant   '終了予定日
    Dim intTotal As Long    '総項目数

    Dim intDays As Long '試験予定日数
    Dim intEstimatedNum As Long    '予定件数
    Dim varEstimatedNum As Variant
    Dim dateTmp As Date
    Dim intSum As Long

    Dim i As Long
    Dim j As Long

    For i = intTargetRowTop To intTargetRowEnd
        With objInSheet.Rows(i)
            intTotal = .Columns(intItemNumberColumn).Value  '小項目数
            varStart = StatisticsLibrary2.GetDateValue(.Columns(intStartDateColumn))
            varEnd = StatisticsLibrary2.GetDateValue(.Columns(intEndDateColumn))
        End With
        If (VarType(varStart) <> vbDate) Or (VarType(varEnd) <> vbDate) Then
            intDays = 0
        ElseIf varStart > varEnd Then
            intDays = 0
        Else
            intDays = DateDiff("d", varStart, varEnd) + 1
        End If

        If (intTotal > 0) And (intDays > 0) Then
            varEstimatedNum = intTotal / intDays
            intSum = 0
            For j = 1 To intDays
                dateTmp = DateAdd("d", j - 1, varStart)
                intRowOfs = GetTableRow(objOutSheet.Cells(1, intOutTargetColumn), dateTmp) '日付け列検索
                If intRowOfs > 0 Then
                    If j < intDays Then
                        intEstimatedNum = Int((varEstimatedNum * j) - intSum)
                    Else
                        intEstimatedNum = intTotal - intSum
                    End If
                    intSum = intSum + intEstimatedNum

                    With objOutSheet.Cells(intRowOfs, intOutTargetColumn + 2)
                        .Value = .Value + intEstimatedNum
                    End With
                End If
            Next j
        End If
    Next i

    ''予定件数を累積件数に置き換え
    Dim intCumulativeValue As Integer

    intDays = DateDiff("d", dtMin, dtMax) + 1
    intCumulativeValue = 0

    For i = 1 To intDays
        With objOutSheet.Cells(i, intOutTargetColumn + 2)
            intCumulativeValue = intCumulativeValue + .Value
            .Value = intCumulativeValue
        End With
    Next i

End Sub

Private _
Function GetTableRow(ByVal objTop As Range, _
                     ByVal dateSearch As Date) As Long
    Dim c1 As Range

    Set c1 = objTop
    Do While c1.Value <> ""
        Select Case DateDiff("d", c1.Value, dateSearch)
        Case Is = 0
            GetTableRow = c1.Row
            Exit Function
        Case Is < 0
            Exit Do
        End Select
        Set c1 = c1.Offset(1)
    Loop

    GetTableRow = 0
    Call MsgBox("Bug!!!")
End Function

' 試験完了予定数 ※Redmine用
Private Sub TestCompletionWrite(ByVal strByWeekOrDay As String, _
                                ByVal dtProcessStart As Date, _
                                ByVal objInSheet As Worksheet, _
                                ByVal objOutSheet As Worksheet, _
                                ByVal intTargetColumn As Integer, _
                                ByVal intOutTargetColumn As Integer, _
                                Optional ByVal intTargetRowOffset As Integer = 2)
    Dim i, j As Integer
    Dim dtMin, dtMax As Date
    Dim dtVal As Variant

    Call TargetSheetSelect(objInSheet)
    dtVal = GetTargetColumnDateMinMax(intTargetColumn, intTargetRowOffset:=intTargetRowOffset)
    dtMin = dtVal(1)
    dtMax = dtVal(2)

    If dtMin > dtProcessStart Then
        dtMin = dtProcessStart
    End If

    EndDayWrite objOutSheet, dtMin, dtMax, strByWeekOrDay, intOutTargetColumn
    ActualSamplingNoWrite objOutSheet, intOutTargetColumn, intOutTargetColumn + 1
    NumberOfDailyBugWrite objInSheet, objOutSheet, intTargetColumn, dtMin, dtMax, strByWeekOrDay, intOutTargetColumn + 2

End Sub

' 試験完了実績数
Private Sub TestCompletionActualWrite(ByVal strByWeekOrDay As String, _
                                      ByVal dtMin As Date, _
                                      ByVal dtMax As Date, _
                                      ByVal objInSheet As Worksheet, _
                                      ByVal objOutSheet As Worksheet, _
                                      ByVal intTargetColumn As Integer, _
                                      ByVal intOutTargetColumn As Integer, _
                                      Optional ByVal intTargetRowOffset As Integer = 2)

    Call TargetSheetSelect(objInSheet)

    ' 全て記入なし
    If StatisticsLibrary.DateIsInitial(dtMin) Then
        'なにもしない
    Else
        EndDayWrite objOutSheet, dtMin, dtMax, strByWeekOrDay, intOutTargetColumn, False
        ActualSamplingNoWrite objOutSheet, intOutTargetColumn, intOutTargetColumn + 1
        NumberOfDailyTestWrite objInSheet, objOutSheet, intTargetColumn, dtMin, dtMax, strByWeekOrDay, intOutTargetColumn + 2
    End If

End Sub

' 試験完了予定数(総数からの減算)
Private Sub TestCompletionSubWrite(ByVal objInSheet As Worksheet, _
                                   ByVal intTargetColumn As Integer, _
                                   Optional ByVal intSum As Integer = -1)
    Dim i As Integer

    objInSheet.Parent.Activate
    objInSheet.Select

    If (Cells(1, intTargetColumn).Value = "") Then
        Exit Sub
    End If

    Range(Cells(1, intTargetColumn), Cells(GetTargetColumnEndRow(intTargetColumn), intTargetColumn + 2)).Select
    Selection.Cut
    Cells(2, intTargetColumn).Select
    ActiveSheet.Paste
    Application.CutCopyMode = False

    Cells(1, intTargetColumn).Value = Cells(2, intTargetColumn).Value - 1
    ActualSamplingNoWrite objInSheet, intTargetColumn, intTargetColumn + 1
    Cells(1, intTargetColumn + 2).Value = 0

    If (intSum < 0) Then
        intSum = Cells(GetTargetColumnEndRow(intTargetColumn + 2), intTargetColumn + 2).Value
    End If
    For i = 1 To GetTargetColumnEndRow(intTargetColumn + 2)
        Cells(i, intTargetColumn + 3).Value = intSum - Cells(i, intTargetColumn + 2).Value
    Next

End Sub

' PB曲線グラフ用テーブル生成
Private Sub PBTableGenerate(ByVal strByWeekOrDay As String, _
                            ByVal objInSheet As Worksheet, _
                            ByVal intTable1FirstColumn As Integer, _
                            ByVal intTable2FirstColumn As Integer, _
                            ByVal intTable3FirstColumn As Integer, _
                            ByVal intTable4FirstColumn As Integer, _
                            ByVal intOutTargetColumn As Integer)
    Dim i As Integer
    Dim j As Integer
    Dim intDateColumn As Long
    Dim varTabledt As Variant
    Dim dtMin As Date
    Dim dtMax As Date

    Call TargetSheetSelect(objInSheet)

    ' グラフのX軸となる日程の最小値、最大値を求める
    j = 0
    For i = 1 To 4
        Select Case i
        Case 1
            intDateColumn = intTable1FirstColumn
        Case 2
            intDateColumn = intTable2FirstColumn
        Case 3
            intDateColumn = intTable3FirstColumn
        Case 4
            intDateColumn = intTable4FirstColumn
        End Select

        If (Cells(1, intDateColumn) <> "") Then
            varTabledt = GetTargetColumnDateMinMax(intDateColumn, 0, False)
            If Not DateIsInitial(varTabledt(1)) Then
                j = j + 1
                If j = 1 Then
                    dtMin = varTabledt(1)
                    dtMax = varTabledt(2)
                Else
                    If (varTabledt(1) < dtMin) Then
                         dtMin = varTabledt(1)
                     End If
                     If (varTabledt(2) > dtMax) Then
                         dtMax = varTabledt(2)
                     End If
                End If
            End If
        End If
    Next i

    If j = 0 Then
        Call MsgBox("有効なデータがありません。", vbCritical)
        End
    End If

    EndDayWrite objInSheet, dtMin, dtMax, strByWeekOrDay, intOutTargetColumn
    ' 日付の軸に合わせてコピー
    MatchDateCopy intOutTargetColumn, intTable1FirstColumn, intTable1FirstColumn + 2, intOutTargetColumn + 1
    MatchDateCopy intOutTargetColumn, intTable2FirstColumn, intTable2FirstColumn + 2, intOutTargetColumn + 2
    MatchDateCopy intOutTargetColumn, intTable3FirstColumn, intTable3FirstColumn + 3, intOutTargetColumn + 3
    MatchDateCopy intOutTargetColumn, intTable4FirstColumn, intTable4FirstColumn + 3, intOutTargetColumn + 4

End Sub

' 日付の一致するセルを軸を合わせてコピー
Private Sub MatchDateCopy(ByVal intMajorScaleColumn As Integer, _
                          ByVal intTargetScaleColumn As Integer, _
                          ByVal intTargetCopyColumn As Integer, _
                          ByVal intTargetPasteColumn As Integer)
    Dim i As Integer

    If Cells(1, intTargetScaleColumn) = "" Then
        Exit Sub
    End If

    For i = 1 To GetTargetColumnEndRow(intMajorScaleColumn)
        If (Cells(i, intMajorScaleColumn).Value = Cells(1, intTargetScaleColumn).Value) Then
            Range(Cells(1, intTargetCopyColumn), Cells(GetTargetColumnEndRow(intTargetCopyColumn), intTargetCopyColumn)).Select
            Selection.Copy
            Cells(i, intTargetPasteColumn).Select
            ActiveCell.PasteSpecial
            Application.CutCopyMode = False
            Exit For
        End If
    Next
End Sub

' グラフ
Private Sub PBGenerateGraph(ByVal strGraphTitle As String, _
                            ByVal strGraphName As String, _
                            ByVal objInSheet As Worksheet, _
                            ByVal intTargetColumn As Integer)

    Dim strInSheetName As String
    Dim XRange As Range
    Dim ActualRange As Range
    Dim PredictedRange As Range
    Dim TestActualRange As Range
    Dim TestPredictedRange As Range
    Dim intNumOfActualPoint As Integer
    Dim intNumOfPredictedPoint As Integer
    Dim intNumOfTestActualPoint As Integer
    Dim intNumOfTestPredictedPoint As Integer
    Dim i As Integer
    Dim z As Series

    Call TargetSheetSelect(objInSheet)
    strInSheetName = objInSheet.Name

    Set XRange = Range(Cells(1, intTargetColumn), Cells(GetTargetColumnEndRow(intTargetColumn), intTargetColumn))
    Set ActualRange = Range(Cells(1, intTargetColumn + 1), Cells(GetTargetColumnEndRow(intTargetColumn), intTargetColumn + 1))
    Set PredictedRange = Range(Cells(1, intTargetColumn + 2), Cells(GetTargetColumnEndRow(intTargetColumn), intTargetColumn + 2))
    Set TestPredictedRange = Range(Cells(1, intTargetColumn + 3), Cells(GetTargetColumnEndRow(intTargetColumn), intTargetColumn + 3))
    Set TestActualRange = Range(Cells(1, intTargetColumn + 4), Cells(GetTargetColumnEndRow(intTargetColumn), intTargetColumn + 4))

    intNumOfActualPoint = GetTargetColumnEndRow(intTargetColumn + 1)
    intNumOfPredictedPoint = GetTargetColumnEndRow(intTargetColumn + 2)
    intNumOfTestActualPoint = GetTargetColumnEndRow(intTargetColumn + 4)
    intNumOfTestPredictedPoint = GetTargetColumnStartRow(intTargetColumn + 3)

    同名のグラフを削除 strGraphName

    Dim mychart As Chart
    Set mychart = Charts.Add(After:=Sheets(Sheets(Sheets.Count).Name))
    ' 上記で最後のシートの後ろに置きたいが、仕様でおけない(Beforeと同じ)ので置き換え
    Sheets(mychart.Index).Move After:=Sheets(Sheets.Count)

    With mychart
        ' グラフType設定
        .ChartType = xlLineMarkers
        ' グラフ名設定
        .Name = strGraphName
        ' Title設定
        .HasTitle = True
        .ChartTitle.text = "PB曲線 " & "[" & strGraphTitle & "]"
        .ChartTitle.Format.TextFrame2.TextRange.Font.Size = 16
        .ChartTitle.Format.TextFrame2.TextRange.Font.Bold = msoTrue
        '.ChartTitle.Format.TextFrame2.TextRange.Font.Name = "Arial"

        ' 初回は削除して新規作成(Seriesの初期値が変動するため・・・)
        For Each z In .SeriesCollection
          z.Delete
        Next
        For i = 1 To 4
            .SeriesCollection.NewSeries
        Next

        ' 少なくとも1軸目の線にX値を入れないとX軸に値が入らない
        ' 全てにいれてもよい
        .SeriesCollection(1).XValues = XRange
        .SeriesCollection(1).Values = ActualRange
        .SeriesCollection(2).XValues = XRange
        .SeriesCollection(2).Values = PredictedRange
        .SeriesCollection(3).XValues = XRange
        .SeriesCollection(3).Values = TestActualRange
        .SeriesCollection(4).XValues = XRange
        .SeriesCollection(4).Values = TestPredictedRange

        .SeriesCollection(1).AxisGroup = 2
        .SeriesCollection(2).AxisGroup = 2
        .SeriesCollection(3).AxisGroup = 1
        .SeriesCollection(4).AxisGroup = 1

        .SeriesCollection(1).Name = "実績バグ"
        .SeriesCollection(2).Name = "予測バグ"
        .SeriesCollection(3).Name = "実績残項目"
        .SeriesCollection(4).Name = "予測残項目"

        .Axes(xlCategory, xlPrimary).HasTitle = False
        '.Axes(xlCategory, xlPrimary).AxisTitle.Characters.Text = ""
        .Axes(xlCategory, xlPrimary).TickLabels.NumberFormatLocal = "yyyy/mm/dd"
        .Axes(xlValue, xlPrimary).HasTitle = True
        .Axes(xlValue, xlPrimary).AxisTitle.Characters.text = "残試験項目数"
        .Axes(xlValue, xlPrimary).TickLabels.NumberFormatLocal = "G/標準"
        .Axes(xlValue, xlSecondary).HasTitle = True
        .Axes(xlValue, xlSecondary).AxisTitle.Characters.text = "バグ検出数"
        .Axes(xlValue, xlSecondary).TickLabels.NumberFormatLocal = "G/標準"

        ' 実績バグの線に対する設定
        With .SeriesCollection(1)
            .Format.Line.ForeColor.RGB = RGB(255, 0, 0)
            .Format.Fill.ForeColor.RGB = RGB(255, 0, 0)
            .Format.Line.DashStyle = msoLineSolid
            .Border.Weight = xlMedium
            .MarkerStyle = xlSquare
            .Smooth = False
            .MarkerSize = 7
            .Shadow = False
            .Points(intNumOfActualPoint).ApplyDataLabels
            .Points(intNumOfActualPoint).DataLabel.NumberFormat = "#,##0_ "
        End With

        ' 予測バグの線に対する設定
        With .SeriesCollection(2)
            .Format.Line.ForeColor.RGB = RGB(255, 124, 128)
            .Format.Line.DashStyle = msoLineSysDash
            .Border.Weight = xlMedium
            .MarkerStyle = xlNone
            .Smooth = False
            .MarkerSize = 7
            .Shadow = False
            .Points(intNumOfPredictedPoint).ApplyDataLabels
            .Points(intNumOfPredictedPoint).DataLabel.NumberFormat = "#,##0_ "
        End With

      If intNumOfTestPredictedPoint > 0 Then
        ' 実績残試験項目数
        With .SeriesCollection(3)
            .Format.Line.ForeColor.RGB = RGB(0, 0, 255)
            .Format.Fill.ForeColor.RGB = RGB(0, 0, 255)
            .Format.Line.DashStyle = msoLineSolid
            .Border.Weight = xlMedium
            .MarkerStyle = xlSquare
            .Smooth = False
            .MarkerSize = 7
            .Shadow = False
            .Points(intNumOfTestActualPoint).ApplyDataLabels
            .Points(intNumOfTestActualPoint).DataLabel.NumberFormat = "#,##0_ "
        End With

        ' 予測残試験項目数
        With .SeriesCollection(4)
            .Format.Line.ForeColor.RGB = RGB(0, 102, 255)
            .Format.Line.DashStyle = msoLineSysDash
            .Border.Weight = xlMedium
            .MarkerStyle = xlNone
            .Smooth = False
            .MarkerSize = 7
            .Shadow = False
            ' 総項目数をプロット
            .Points(intNumOfTestPredictedPoint).ApplyDataLabels
            .Points(intNumOfTestPredictedPoint).DataLabel.NumberFormat = "#,##0_ "
            ' プロット位置=左
            .Points(intNumOfTestPredictedPoint).DataLabel.Position = xlLabelPositionLeft

        End With
      End If

        ' プロットエリアの背景色
        .PlotArea.Format.Fill.ForeColor.RGB = RGB(255, 255, 204)

        ' テキストボックス
'        With .TextBoxes.Add(550, 500, 50, 20)
'            '.Select
'            .AutoSize = True
'            .Text = ""
'        End With

    End With

End Sub

Private Sub 同名のワークシートが無ければ追加(ByVal strOutSheetName As String)
    Dim i As Worksheet

    For Each i In Worksheets
        If (UCase(i.Name) = UCase(strOutSheetName)) Then
            Sheets(strOutSheetName).Select
            Exit Sub
        End If
    Next

    Sheets.Add After:=Sheets(Sheets(Sheets.Count).Name)
    ActiveSheet.Name = strOutSheetName

End Sub
