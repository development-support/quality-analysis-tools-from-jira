Attribute VB_Name = "StatisticsTookDays_old"
Option Explicit
'-----------------------------------------------------------
' 【機能】
' 仕掛日数毎の問題件数の集計とグラフ化
'
'    ※仕掛日数の算出方法
'        �@完了問題
'            仕掛日数 = 問題完了日 - 問題発生日
'        �A未完了問題
'            仕掛日数 = 集計日 - 問題発生日
'
' 完了問題､未完了問題別に仕掛日数毎に問題件数を集計する
' 仕掛日数毎に棒グラフを作成する
'
'【変更履歴】
' 2016.3.10 仕様変更: 原因グループ別出力機能廃止
' 2016.3.10 仕様変更: 出力単位毎の集計日指定機能廃止
' 2017.5.17 英語対応: TookDaysDataCountの完了/未完了をDone/Undoneに変更
'
'-----------------------------------------------------------

Private Const strTemplateSheetName  As String = "【書式】仕掛日数別問題件数"

Private Const strGraphNamePrefix    As String = "TookDays"

Private Const strConstSplitSheetPrefix As String = "C_"
Private Const strConstProcessSheetPrefix As String = "P_"

Private Const strGeneSourceAddr As String = "C1"  '実行ソース出力位置
Private Const strGeneTimeAddr As String = "C2" '実行日時出力位置

Private strGeneSource As String

'' 参照する定義名
Private Const strDefName_SelectField As String = "TookDays_参照項目"  '抽出項目
Private Const strDefName_DataField As String = "TookDays_データ項目"  '集計元データ
Private Const strDefName_GraphData As String = "TookDays_集計結果"    'グラフデータ

Private Const strDefName_Date As String = "TookDays_集計日"
Private Const strShapeName_Date As String = "TextBox_集計日"

' 集計表
Private Const strDefName_Axis0 As String = "TookDays_横軸"
Private Const strDefName_Axis1 As String = "TookDays_系列1"
Private Const strDefName_Axis2 As String = "TookDays_系列2"

Public _
Sub TookDaysExecute_old(Optional ByVal booEndMessage As Boolean = True)
    Const strJobName    As String = "仕掛日数別問題件数の集計"
    Dim wsData  As Worksheet    '入力データシート
    Dim strSheetName As String
    Dim varInputSheet As Variant '問題管理データシート/分割シート
    Dim strSplitName() As String
    Dim i As Integer

    If booEndMessage Then
        If MsgBox(strJobName & "を開始します。", vbOKCancel) <> vbOK Then
            Exit Sub
        End If
    End If

    ' 前処理
    Call StatisticsLibrary2.JobBegin(strJobName & "中...")

    ' 入力シート取得
    Set wsData = StatisticsLibrary2.問題管理データシート取得()
    If wsData Is Nothing Then
        booEndMessage = False
        GoTo Exit_Proc
    End If

    If (ProblemDataBaseTypeGet() = "Redmine") And (TargetProjectNameGet() = "T3xx") Then
        If 分割シート取得(wsData, "カテゴリ", strConstSplitSheetPrefix, strSplitName, varInputSheet) = False Then
            booEndMessage = False
            GoTo Exit_Proc
        End If
    ElseIf ProblemDataBaseTypeGet() = "JIRA" Then
        If 分割シート取得(wsData, "カテゴリ", strConstSplitSheetPrefix, strSplitName, varInputSheet) = False Then
            booEndMessage = False
            GoTo Exit_Proc
        End If
    Else
        Set varInputSheet = wsData
    End If

    If IsArray(varInputSheet) Then
        Dim wsData2  As Worksheet    '分割データシート
        For i = 1 To UBound(varInputSheet)
            strSheetName = varInputSheet(i)
            Set wsData2 = StatisticsLibrary2.同名のシート取得(strSheetName)
            If TookDaysMain(wsData2, True, strSplitName(i)) = False Then
                booEndMessage = False
                Exit For
            End If
        Next i

        Call 分割シート後処理(varInputSheet)

    ElseIf ProblemDataBaseTypeGet() = "JIRA" Then

    ElseIf TookDaysMain(wsData) = False Then
        booEndMessage = False
    End If

    ' 全体出力 ※JIRAの場合、分割出力の有無に関わらず全体出力する。
    If ProblemDataBaseTypeGet() = "JIRA" Then
        If TookDaysMain(wsData) = False Then
            booEndMessage = False
        End If
    End If

Exit_Proc:
    Call StatisticsLibrary2.JobEnd(strJobName & "処理終了", MsgBoxShow:=booEndMessage)
End Sub

Private _
Function TookDaysMain(ByVal wsData As Worksheet, _
                      Optional ByVal IsSplitSheet As Boolean = False, _
                      Optional ByVal strSplitName As String = "") As Boolean

    Dim strAnalyzeSheetName() As String
    Dim strProcessID() As String
    Dim strSep As String
    Dim i As Integer

    TookDaysMain = False

    strGeneSource = TargetProjectNameGet()
    If IsSplitSheet Then
        strGeneSource = strSplitName & "/" & strGeneSource
        strSep = "_" & strSplitName
    Else
        strSep = ""
    End If

    strAnalyzeSheetName = StatisticsLibrary.ByProcessSheetGenerate( _
                                        wsData _
                                        , JobEndIfEmpty:=IIf(IsSplitSheet, False, True) _
                                        , 分割値格納配列:=strProcessID _
                                        , NewSheetNamePrefix:=strConstProcessSheetPrefix)
    Select Case UBound(strAnalyzeSheetName)
    Case Is < 0  'エラー発生
        GoTo Exit_Proc
    Case 0  ' 有効データなし
        TookDaysMain = True
        GoTo Exit_Proc
    End Select

    ' 仕掛日数別問題件数集計
    If TookDaysCount(strAnalyzeSheetName, strProcessID, strSep) = False Then
        GoTo Exit_Proc
    End If

    '' 後処理
    ' 工程別シートの削除
    For i = 1 To UBound(strAnalyzeSheetName)
        Call 同名のワークシートを削除(strAnalyzeSheetName(i))
    Next

    TookDaysMain = True

Exit_Proc:
    Set wsData = Nothing

    Erase strAnalyzeSheetName
End Function

' 出力シート名生成
Private _
Function TookDaysOutNameGet(ByVal strSuffix As String) As String
    TookDaysOutNameGet = 集計用出力シート名生成(strGraphNamePrefix & "_" & strSuffix)
End Function

' 出力書式シート表示／非表示
Public _
Sub TookDaysFormatSheetDisp()
    Call StatisticsLibrary2.FormatSheetVisibleReverse(strTemplateSheetName)
End Sub

Private _
Function TookDaysCount(ByRef strAnalyzeSheetName() As String, _
                       ByRef strProcessID() As String, _
                       ByVal strSuffix As String) As Boolean

    Dim varSettingParam As Variant
    Dim rngDate As Range    '集計日
    Dim wsData  As Worksheet
    Dim wsTempl As Worksheet   '出力書式シート
    Dim wsGraph  As Worksheet
    Dim aryDataName()   As String   '抽出するデータの項目名
    Dim aryDataCol()    As Long     '抽出するデータの列番号
    Dim strRedmineSheetName As String   '入力シート名
    Dim strGraphName    As String   '出力シート名
    Dim dateNow As Date '現在日
    Dim dateJob As Date '集計日
    Dim varDate As Variant
    Dim rr  As Range
    Dim r2  As Range
    Dim r3  As Range
    Dim numCount    As Long ' 有効データ数
    Dim i   As Long
    Dim intTableGap As Variant

    TookDaysCount = False

    '---------------------------------------------------
    ' 工程共通の前処理
    '---------------------------------------------------
    dateNow = dateJobStart

    ' 仕掛日数別問題件数集計の設定を取得
    varSettingParam = StatisticsLibrary.GetRangeSettingParam("TookDays_setting")

    Set rngDate = varSettingParam(1)
    varDate = rngDate.Value '集計日

    Erase varSettingParam

    If Trim$(varDate) = "" Then ' 指定なし
        dateJob = dateNow
    ElseIf VarType(varDate) = vbDate Then
        dateJob = varDate ' 正常値
    ElseIf IsDate(varDate) Then
        dateJob = CDate(varDate) ' 正常値
    Else
        Call MsgBox(StatisticsLibrary2.セルの位置表示取得(rngDate) & " の集計日の値が不正です", vbCritical)
        GoTo Exit_Proc
    End If

    ' 出力書式シート取得
    Select Case ProblemDataBaseTypeGet()
    Case "JIRA"
        Set wsTempl = StatisticsLibrary2.同名のシート取得(strTemplateSheetName & "(JIRA)")

    Case Else
        Set wsTempl = StatisticsLibrary2.同名のシート取得(strTemplateSheetName)
    End Select

    If wsTempl Is Nothing Then
        GoTo Exit_Proc
    End If

    ' Redmineシートから抽出するデータの項目名を取得
    If TookDaysSelectName(wsTempl, aryDataName) = False Then
        GoTo Exit_Proc  '不正なシート
    End If

    ReDim aryDataCol(UBound(aryDataName))

    '---------------------------------------------------
    ' 工程毎の処理
    '---------------------------------------------------
    For i = 1 To UBound(strAnalyzeSheetName)
        '---------------------------------------------------
        ' 前処理
        '---------------------------------------------------
        ' Redmineシート取得
        strRedmineSheetName = strAnalyzeSheetName(i)
        Set wsData = StatisticsLibrary2.同名のシート取得(strRedmineSheetName, NoMsg:=True)
        If wsData Is Nothing Then
           GoTo Continue
        End If

        strGraphName = TookDaysOutNameGet(strProcessID(i) & strSuffix) '出力シート名生成
        If strGraphName = "" Then
            GoTo Exit_Proc
        End If

        Call StatisticsLibrary.同名のワークシートを削除(strGraphName)

        Select Case StatisticsLibrary2.RedmineSheetDataCount(wsData)
        Case Is < 0 '不正なシート
            GoTo Exit_Proc
        Case 0
            GoTo Continue   ' 有効データが無い
        End Select

        ' 出力シート生成
        Set wsGraph = StatisticsLibrary2.FormatSheetCopy(wsTempl, strGraphName)
        If wsGraph Is Nothing Then
            GoTo Exit_Proc
        End If

        ' 生成日時とデータソースを設定
        With wsGraph.Range(strGeneSourceAddr)
            .NumberFormatLocal = "@"
            .Value = strProcessID(i) & "/" & strGeneSource
        End With
        With wsGraph.Range(strGeneTimeAddr)
            .Value = dateJobStart
        End With

        ' 集計日設定
        Set rr = StatisticsLibrary2.名前の参照範囲取得(wsGraph, strDefName_Date)
        If rr Is Nothing Then
            GoTo Exit_Proc  '不正なシート
        End If

        rr.Value = dateJob

        Call MyFilterModeClear(wsGraph) '注）フィルター選択されていると列コピーで全体がコピーされないため。

        ' 出力先取得
        Set r2 = StatisticsLibrary2.名前の参照範囲取得(wsGraph, strDefName_DataField)  '集計元データ
        If r2 Is Nothing Then
            GoTo Exit_Proc  '不正なシート
        End If

        Set r3 = StatisticsLibrary2.名前の参照範囲取得(wsGraph, strDefName_GraphData) '集計結果の格納先
        If r3 Is Nothing Then
            GoTo Exit_Proc  '不正なシート
        End If

        ' Redmineシートから抽出するデータの列番号を取得
        If TookDaysSelectColumn(wsData, aryDataName, aryDataCol) = False Then
            GoTo Exit_Proc
        End If

        '---------------------------------------------------
        ' Redmineシートからデータ抽出
        '---------------------------------------------------
        numCount = TookDaysDataSelect(wsData, aryDataCol, r2)
        If numCount < 0 Then
            GoTo Exit_Proc  '異常発生
        End If

        '-------------------------------------------------
        '   データ表の位置を補整
        '     ※データ表のフィルタ設定で集計表内の行が非表示になると
        '       その行がグラフに表示されなくなるため。
        '-------------------------------------------------
        intTableGap = r2.Row + numCount - r3.Row + 1
        If intTableGap > 0 Then
            r3.Offset(-1).Resize(intTableGap).Insert Shift:=xlShiftDown
        End If

        '---------------------------------------------------
        ' データ集計
        '---------------------------------------------------
        numCount = TookDaysDataCount(r2, r3, dateJob, numCount)
        If numCount < 0 Then
            GoTo Exit_Proc  '異常発生
        End If

        '------------------------
        ' グラフ定義補整
        '------------------------
        If TookDaysGraphAdjust(wsGraph, numCount) = False Then
            GoTo Exit_Proc  '異常発生
        End If

        Call MySelectionReset(wsGraph)
        Call MyWsCalculate(wsGraph)

Continue:
    Next i
    TookDaysCount = True

Exit_Proc:
    Erase aryDataName
    Erase aryDataCol
    Set wsTempl = Nothing

End Function

' Redmineデータシートから抽出するデータの名称格納位置を取得
Private _
Function TookDaysSelectName(ByVal ws As Worksheet, _
                            ByRef aryDataName() As String) As Boolean
    Dim rr  As Range
    Dim r2  As Range
    Dim aa() As String
    Dim nn  As Long
    Dim ii  As Long

    TookDaysSelectName = False

    Set rr = StatisticsLibrary2.名前の参照範囲取得(ws, strDefName_SelectField) '抽出データの格納先
    If StatisticsLibrary2.MyRegionColumnSort(rr) = False Then
        GoTo Exit_Proc  '不正なシート
    ElseIf StatisticsLibrary2.Row2StringArray(rr, aa) = False Then
        GoTo Exit_Proc  '不正なシート
    End If

    Set r2 = StatisticsLibrary2.名前の参照範囲取得(ws, strDefName_DataField)
    If r2 Is Nothing Then
        GoTo Exit_Proc  '不正なシート
    ElseIf (rr.Column > r2.Column) Or (rr.Row <> r2.Row) Then
        Call MsgBox("書式シート(" & ws.Name & ")内の名前定義の値が不正です。", vbCritical)
        GoTo Exit_Proc  '不正なシート
    End If

    nn = r2.Column - rr.Column
    ReDim aryDataName(UBound(aa) + nn)

    For ii = 0 To nn
        aryDataName(ii) = ""
    Next ii

    For ii = 0 To UBound(aa)
        aryDataName(nn + ii) = aa(ii)
    Next ii

    TookDaysSelectName = True

Exit_Proc:
    Erase aa

End Function

' Redmineデータシート抽出するデータの列番号を取得
Private _
Function TookDaysSelectColumn(ByVal wsData As Worksheet, _
                              ByRef aryDataName() As String, _
                              ByRef aryDataCol() As Long) As Boolean

    TookDaysSelectColumn = False
    If StatisticsLibrary2.RedmineSheetHeaderColumn(wsData, aryDataName, aryDataCol) = False Then
        TookDaysSelectColumn = False
    Else
        TookDaysSelectColumn = True
    End If

End Function

' データシートから使用するデータを抽出
Private _
Function TookDaysDataSelect(ByVal wsData As Worksheet, _
                            ByRef aryDataCol() As Long, _
                            ByVal OutHeader As Range) As Long

    Dim rowDataTop  As Long
    Dim rowDataEnd  As Long
    Dim in_col  As Variant
    Dim in_row  As Long
    Dim out_col As Long
    Dim out_base    As Range
    Dim out_next    As Range
    Dim numCount    As Long ' 有効データ数

    TookDaysDataSelect = -1

    Set out_base = OutHeader.Offset(1)   '最初のデータ出力位置

    ' 出力域クリア   注）書式コピー用に一行目のデータは残す
    Call StatisticsLibrary2.ClearRangeToEndRow(out_base.Offset(1))

    ' 抽出対象の列の値を取り出して出力域に格納
    Call StatisticsLibrary2.RedmineSheetDataRangeGet(wsData, rowDataTop, rowDataEnd)

    Set out_next = out_base
    For in_row = rowDataTop To rowDataEnd
        With wsData.Rows(in_row)
            If out_next.Row > out_base.Row Then
                out_base.Copy Destination:=out_next
            End If

            out_col = 0
            For Each in_col In aryDataCol
                out_col = out_col + 1
                If in_col > 0 Then
                    out_next.Columns(out_col).Value = .Columns(in_col).Value
                End If
            Next in_col
        End With

        Set out_next = out_next.Offset(1)
Continue:
    Next in_row

    numCount = out_next.Row - OutHeader.Row - 1
    If numCount < 1 Then  '有効データが無い場合
        out_base.ClearContents  '一行目のデータをクリア。
    End If

    ' 抽出したデータ数を返す。
    TookDaysDataSelect = numCount

End Function

' 集計
Private _
Function TookDaysDataCount(ByVal InHeader As Range, _
                           ByVal OutHeader As Range, _
                           ByVal dateJob As Date, _
                           ByVal numCount As Long) As Long
    Dim numIdCol    As Long '「ID」列番号
    Dim numDay1Col  As Long '「問題発生日」列番号
    Dim numDay2Col  As Long '「問題完了日」列番号
    Dim numDaysCol  As Long '「仕掛日数」列番号
    Dim numStatCol  As Long '「完了/未完了」列番号
    Dim rngDay1 As Range
    Dim rngDay2 As Range
    Dim rngDays As Range
    Dim rngStat As Range
    Dim rr  As Range
    Dim Flag    As Boolean

    TookDaysDataCount = -1

    If numCount < 1 Then  '有効データ無し
        GoTo Exit_Proc
    End If

    With InHeader.Offset(-1)
        numIdCol = StatisticsLibrary2.GetDataColumn(.Cells(1, 1), "ID") - .Column + 1
        If numIdCol < 1 Then Exit Function
    
        numDay1Col = StatisticsLibrary2.GetDataColumn(.Cells(1, 1), "問題発生日") - .Column + 1
        If numDay1Col < 1 Then Exit Function
        
        numDay2Col = StatisticsLibrary2.GetDataColumn(.Cells(1, 1), "問題完了日") - .Column + 1
        If numDay2Col < 1 Then Exit Function
    End With
    With InHeader
        numDaysCol = StatisticsLibrary2.GetDataColumn(.Cells(1, 1), "仕掛日数") - .Column + 1
        If numDaysCol < 1 Then Exit Function
        
        numStatCol = StatisticsLibrary2.GetDataColumn(.Cells(1, 1), "完/未完") - .Column + 1
        If numStatCol < 1 Then Exit Function
    End With

    '-------------------------------
    ' 仕掛日数算出
    '-------------------------------
    Flag = False

    Set rr = InHeader.Offset(1)
    Do While rr.Columns(numIdCol).Value <> ""
        rr.Borders.LineStyle = xlContinuous
        Set rngDay1 = rr.Columns(numDay1Col)
        Set rngDay2 = rr.Columns(numDay2Col)
        Set rngDays = rr.Columns(numDaysCol)
        Set rngStat = rr.Columns(numStatCol)

        If Not IsDate(rngDay1.Value) Then
            rngDay1.Font.Color = vbRed
            rngDays.ClearContents
            rngStat.ClearContents
        Else
            If IsDate(rngDay2.Value) Then
                'rngStat.Value = "完了"
                rngStat.Value = "Done"
                rngDays.Value = DateDiff("d", rngDay1.Value, rngDay2.Value)
            Else
                'rngStat.Value = "未完了"
                rngStat.Value = "Undone"
                rngDays.Value = DateDiff("d", rngDay1.Value, dateJob)
            End If

            Flag = True ' 有効データ発見
        End If

        Set rr = rr.Offset(1)
    Loop

    If Flag = False Then    '仕掛日数が算出可能なデータが無い場合
        numCount = 0
        GoTo Exit_Proc
    End If

    '-------------------------------
    ' 入力データをステータス及び仕掛日数の降順にソート
    '-------------------------------
    If numCount > 1 Then
        With InHeader.Offset(1)
            .Resize(numCount).Sort Header:=xlNo, Orientation:=xlTopToBottom _
                , key1:=.Columns(numStatCol), Order1:=xlDescending _
                , Key2:=.Columns(numDaysCol), Order2:=xlDescending
        End With
    End If

    '-------------------------------
    ' 仕掛日数毎の問題件数カウント
    '       ※書式シート内の関数で算出される
    '-------------------------------
    Set rr = OutHeader.Offset(1)

    ' 出力域初期化
    Call StatisticsLibrary2.ClearRangeToEndRow(rr.Offset(numCount))
    If numCount > 1 Then
        rr.Copy Destination:=rr.Offset(1).Resize(numCount - 1) '一行目の書式をコピー
    End If

    ' 仕掛日数のリスト生成
    InHeader.Columns(numDaysCol).Offset(1).Resize(numCount).Copy _
        Destination:=rr.Cells(1, 1) '仕掛日数のリストをコピー

    If numCount > 1 Then
        rr.Resize(numCount).Sort Header:=xlNo, Orientation:=xlTopToBottom _
            , key1:=rr.Columns(1), Order1:=xlAscending   '仕掛日数の昇順にソート

        Set rr = OutHeader.Offset(1)
        rr.Resize(numCount).RemoveDuplicates Columns:=1, Header:=xlNo  '重複除去
        
        numCount = StatisticsLibrary2.GetEndToDown(OutHeader).Row - OutHeader.Row   '仕掛日数の個数
    End If

'    ' 式を値に置き換える
'    With rr.Resize(numCount)
'        .Worksheet.Calculate
'        .Value = .Value
'    End With

Exit_Proc:
    If numCount < 1 Then
        ' 出力域クリア。一行目の罫線は残す。
        OutHeader.Offset(1).ClearContents
        Call StatisticsLibrary2.ClearRangeToEndRow(OutHeader.Offset(2))
    End If

    TookDaysDataCount = numCount  '有効データ数を返す。
End Function

' グラフの補整
Private _
Function TookDaysGraphAdjust(ByVal wsGraph As Worksheet, _
                             ByVal numCount As Long) As Boolean
    Const numAxis   As Long = 2
    Dim rngAxis(numAxis) As Range
    Dim numAxisYmax As Long
    Dim numMajorUnit As Long
    Dim objChart    As ChartObject
    Dim objSrs  As Series
    Dim objShape    As Shape
    Dim objTextBox  As TextBox
    Dim ss  As String
    Dim ii  As Long
    Dim rr  As Range

    TookDaysGraphAdjust = False

    On Error GoTo Exit_Proc

     ' 最初のグラフを取り出す。
    Select Case wsGraph.ChartObjects.Count
    Case 0
        Call MsgBox("'" & wsGraph.Name & "'シートにグラフがありません", vbCritical)
        GoTo Exit_Proc  '不正なシート
    Case Is > 1
        Call MsgBox("'" & wsGraph.Name & "'シートに複数のグラフがあります", vbCritical)
        GoTo Exit_Proc  '不正なシート
    End Select

    Set objChart = wsGraph.ChartObjects(1)

    ' 集計日の埋め込みが名前参照になっている旧書式シートの対応 --->
    On Error Resume Next
    Set objShape = wsGraph.Shapes(strShapeName_Date)
    If Not objShape Is Nothing Then
        objShape.DrawingObject.Formula = "=" & strDefName_Date
        Set objShape = Nothing
    End If
    On Error GoTo Exit_Proc
    '<--- 集計日の埋め込みが名前参照になっている旧書式シートの対応

    For ii = 0 To numAxis
        Select Case ii
        Case 0
            ss = strDefName_Axis0
        Case 1
            ss = strDefName_Axis1
        Case 2
            ss = strDefName_Axis2
        End Select

        Set rngAxis(ii) = StatisticsLibrary2.名前の参照範囲取得(wsGraph, ss)
        If rngAxis(ii) Is Nothing Then
            GoTo Exit_Proc  '不正なシート
        ElseIf rngAxis(ii).Cells.Count > 1 Then
            Call MsgBox("'" & wsGraph.Name & "!" & ss & " の名前定義が不正です", vbCritical)
            GoTo Exit_Proc  '不正なシート
        End If
    Next ii

    If numCount > 0 Then
        ' 値の範囲取得
        For ii = 0 To numAxis
            Set rngAxis(ii) = rngAxis(ii).Offset(1).Resize(numCount)
        Next ii

        ' 縦軸最大値算出
        numAxisYmax = 0
        For ii = 1 To numAxis
            For Each rr In rngAxis(ii)
                If numAxisYmax < rr.Value Then
                    numAxisYmax = rr.Value
                End If
            Next rr
        Next ii

        numMajorUnit = 10 ^ (Len(CStr(numAxisYmax)) - 1)

        If objChart.Chart.SeriesCollection.Count <> numAxis Then
            Call MsgBox("'" & wsGraph.Name & "'シートのグラフの系列数が " & numAxis & " ではありません", vbCritical)
            GoTo Exit_Proc  '不正なシート
        End If

        With objChart.Chart.Axes(xlValue)
            .MajorUnit = numMajorUnit
            .MinorUnit = IIf(numMajorUnit <= 1, 1, numMajorUnit / 10)
        End With

        ' グラフ内の系列毎に処理する。
        For ii = 1 To objChart.Chart.SeriesCollection.Count
            ' データ範囲設定
            Set objSrs = objChart.Chart.SeriesCollection(ii)
            ss = rngAxis(ii).Rows(1).Offset(-1).Address(External:=True) '凡例に表示される名前
            ss = ss & "," & rngAxis(0).Address(External:=True)    '項目軸に表示されるラベル
            ss = ss & "," & rngAxis(ii).Address(External:=True)   'プロットされる値
            ss = ss & "," & ii     '系列のプロット順
            objSrs.Formula = "=SERIES(" & ss & ")"
        Next ii
    End If

    On Error GoTo 0

    TookDaysGraphAdjust = True

Exit_Proc:
    If Err.Number <> 0 Then
        Call MsgBox("グラフ補整でエラー発生。" & vbCrLf & vbCrLf & Err.Description, vbCritical)
    End If

    Set objChart = Nothing
    Set objSrs = Nothing
End Function

