Attribute VB_Name = "StatisticsGomperzForJira"
' Version 0.1 2016/08/01 Create New
'          0.2 2016/08/03 バグの行を"Failed"→"Daily Bug"(<Phase Specific Data>を上書き)に変更。
'          0.3 2016/09/02 Gomperz曲線をZN準拠に変更
' *********　"T300_progress_chart_FJST_Plan_Ver0.3.xlsm"からコピー *********
' Version 0.4 ソフトPMO集計向けに変更(JIRAからデータ取り込み+バグ解決済累積を追加)

Option Explicit


Sub GomperzExecuteForSim()
    Dim strJobName As String
    Dim strInSheetName As String
    Dim strAnalyzeSheetName As String
    Dim colRowLabel As Long
    Dim booEndMessage As Boolean

    Application.ScreenUpdating = False

    ' Parameter
    strJobName = "ゴンペルツ曲線集計"
    strInSheetName = strConstJIRASearchResultSheetName
    strAnalyzeSheetName = strConstGomperzAnalyzeSheetName

'    Sheets(strInSheetName).Select
    colRowLabel = 1
    booEndMessage = False

    ' Statusバー表示
    JobBegin (strJobName & "中...")

    ' JIRAデータからGomperzデータ抽出
    Call GomperzDataFromJiraDataDBAccess

    ' ゴンペルツ演算
    Call gomperzForSim(strInSheetName, strAnalyzeSheetName, colRowLabel)

    ' Statusバー表示
    JobEnd strJobName & "処理終了", MsgBoxShow:=booEndMessage

End Sub

' JIRAデータからGomperzデータ抽出
Private Sub GomperzDataFromJiraDataDBAccess()
    Dim objCon As New ADODB.Connection
    Dim objRS As New ADODB.Recordset

    Dim strSQL As String
    Dim strTableAddress As String
    Dim strTargetProcessSetting As String

    Dim dtStartDay As Date
    Dim dtEndDay As Date
    Dim booStartDay As Boolean
    Dim booEndDay As Boolean
    Dim strAffectsVersion As String
    Dim booAffectsVersion As Boolean
    Dim strLabels As String
    Dim booLabels As Boolean
    Dim dtEnd As Date

    ' 期間抽出設定取得
    ' 入力規則があるので開始終了のつじつまのみCheck
    booStartDay = IsDate(Sheets(strConstConfigSheetName).Range("JIRAStartDay").Value)
    booEndDay = IsDate(Sheets(strConstConfigSheetName).Range("JIRAEndDay").Value)
    dtStartDay = Sheets(strConstConfigSheetName).Range("JIRAStartDay").Value
    dtEndDay = Sheets(strConstConfigSheetName).Range("JIRAEndDay").Value
    Debug.Print "(GomperzDataFromJiraDataDBAccess):[before] booStartDay = " & booStartDay & _
                " booEndDay = " & booEndDay & " dtStartDay = " & dtStartDay & _
                " dtEndDay = " & dtEndDay

    ' Current Report Dateまでを実績の期間とする為の追加
    Sheets(strConstTestDataSheetName).Select
    dtEnd = Cells(GetTargetStringRow(Columns(1), "Current Report Date") + 1, 1).Value

    Debug.Print "(GomperzDataFromJiraDataDBAccess):dtEnd(from TestChart) = " & dtEnd

    If (booEndDay = True) Then
        If DateDiff("d", dtEndDay, dtEnd) < 0 Then
            dtEndDay = dtEnd
        End If
    Else
        booEndDay = True
        dtEndDay = dtEnd
    End If

    If (booStartDay = True) And (booEndDay = True) Then
        If DateDiff("d", dtEndDay, dtStartDay) >= 0 Then
            MsgBox "JIRA抽出期間設定の開始と終了の日付を見直してください。", vbCritical
            Sheets(strConstConfigSheetName).Select
            End
        End If
    End If

    Debug.Print "(GomperzDataFromJiraDataDBAccess):[after] dtStartDay = " & dtStartDay & _
                " dtEndDay = " & dtEndDay

    ' JIRAからデータが出力されない場合（他にも条件がありそうだが・・・）
    If ((Sheets(strConstJIRASheetName).Cells(2, 1) = "No Issues Found") Or _
        (Sheets(strConstJIRASheetName).Cells(1, 1) = "")) Then
        同名のワークシートを削除 strConstJIRASearchResultSheetName
        同名のワークシートが無ければ追加 strConstJIRASearchResultSheetName
        Exit Sub
    End If

    ' AffectsVersion取得
    booAffectsVersion = Not IsEmpty(Sheets(strConstConfigSheetName).Range("JIRAAffectsVersion").Value)
    strAffectsVersion = Sheets(strConstConfigSheetName).Range("JIRAAffectsVersion").Value
    ' 簡易Check(列内に対象データの存在確認
    Sheets(strConstJIRASheetName).Select
    If (GetTargetStringRow(Columns(GetTargetStringColumn(Rows(1), "Affects Version/s")), strAffectsVersion) = 0) Then
        MsgBox "AffectsVerisionの設定を見直してください。対象文字列が存在しません。", vbCritical
        Sheets(strConstConfigSheetName).Select
        End
    End If

    ' Labels取得
    booLabels = Not IsEmpty(Sheets(strConstConfigSheetName).Range("JIRALabels").Value)
    strLabels = Sheets(strConstConfigSheetName).Range("JIRALabels").Value
    ' 簡易Check(列内に対象データの存在確認
    Sheets(strConstJIRASheetName).Select
    If (GetTargetStringRow(Columns(GetTargetStringColumn(Rows(1), "Labels")), strLabels) = 0) Then
        MsgBox "Labelsの設定を見直してください。対象文字列が存在しません。", vbCritical
        Sheets(strConstConfigSheetName).Select
        End
    End If

    ' VIT/ST2設定値取得
    Sheets(strConstConfigSheetName).Select
    strTargetProcessSetting = Range("JIRAHowFound").Value

    ' JIRAのデータからSQLで対象データを抽出
    Sheets(strConstJIRASheetName).Select
    strTableAddress = Range(Cells.CurrentRegion.Address).Address(False, False)

    objCon.Open "Driver={Microsoft Excel Driver (*.xls, *.xlsx, *.xlsm, *.xlsb)};" & _
                "DBQ=" & ThisWorkbook.FullName & ";" & _
                "ReadOnly=1"

    strSQL = "SELECT [Key], [Issue Type], [Status], [Created], [Resolved], [How-Found] FROM [" & _
             strConstJIRASheetName & "$] " & _
             "WHERE [Issue Type] = 'Bug'"

    ' VIT/ST2設定反映
    If (strTargetProcessSetting = "ST2") Then
        strSQL = strSQL & " and ""How-Found"" = 'ST2'"
    ElseIf (strTargetProcessSetting = "VIT") Then
        strSQL = strSQL & " and ""How-Found"" IN ('HIT','HW-SW Integration','VIT')"
    ElseIf (strTargetProcessSetting = "VIT-SIM") Then
        strSQL = strSQL & " and ""How-Found"" IN ('VIT-SIM')"
    ElseIf (strTargetProcessSetting = "VIT+VIT-SIM") Then
        strSQL = strSQL & " and ""How-Found"" IN ('HIT','HW-SW Integration','VIT','VIT-SIM')"
    ElseIf (strTargetProcessSetting = "未設定") Then
        ' なにもしない
    End If

    ' AffectsVersion 設定反映
    If (booAffectsVersion) Then
        strSQL = strSQL & " and ""Affects Version/s"" like " & "'%" & strAffectsVersion & "%'"
    End If

    ' Labels設定反映
    If (booLabels) Then
        strSQL = strSQL & " and ""Labels"" like " & "'%" & strLabels & "%'"
    End If

    ' 期間抽出設定反映
    If booStartDay And booEndDay Then
        strSQL = strSQL & " and [Created] >= #" & dtStartDay & "#" & _
                 " and [Created] <= #" & dtEndDay & "#" & " ORDER BY [Created] DESC"
    ElseIf booStartDay Then
        strSQL = strSQL & " and [Created] >= #" & dtStartDay & "#" & " ORDER BY [Created] DESC"
    ElseIf booEndDay Then
        strSQL = strSQL & " and [Created] <= #" & dtEndDay & "#" & " ORDER BY [Created] DESC"
    Else
        strSQL = strSQL & " ORDER BY [Created] DESC"
    End If
    Set objRS = objCon.Execute(strSQL)

    'SQL実行結果の判定
    If Err.Number <> 0 Then
       Call prcSqlError(objRS)
    End If

    同名のワークシートを削除 strConstJIRASearchResultSheetName
    同名のワークシートが無ければ追加 strConstJIRASearchResultSheetName

    Cells(1, 1).Value = "Key"
    Cells(1, 2).Value = "Issue Type"
    Cells(1, 3).Value = "Status"
    Cells(1, 4).Value = "Created"
    Cells(1, 5).Value = "Resolved"
    Cells(1, 6).Value = "How-Found"

    Dim Foundcell As Range
    Dim SearchArea As Range
    Dim intTargetRow As Integer
    Dim strItemNo As String
    Dim strItemName As String

    Sheets(strConstJIRASearchResultSheetName).Select
    Cells(2, 1).CopyFromRecordset objRS
    Columns(4).NumberFormatLocal = "yyyy/mm/dd"
    Columns(5).NumberFormatLocal = "yyyy/mm/dd"

    'レコードセットをクローズします
    objRS.Close
    objCon.Close

    'オブジェクトの破棄
    Set objRS = Nothing
    Set objCon = Nothing

    Dim intEndRow As Integer
    Dim i As Integer

    ' 日付以外のごみ（時間）を削除
    intEndRow = GetTargetColumnEndRow(4)
    For i = 2 To intEndRow
        Cells(i, 4) = RegExpReplace(Cells(i, 4), "(\d{4}/\d{2}/\d{2}).*", "$1")
        Cells(i, 5) = RegExpReplace(Cells(i, 5), "(\d{4}/\d{2}/\d{2}).*", "$1")
    Next

    ' ResolvedのCurrent Report Dateの日付以降を空欄に変更
    ' dtEndの値を他に持ち込むのが面倒なのでここに作成
    'intEndRow = GetTargetColumnEndRow(5)
    For i = 2 To intEndRow
        If (DateDiff("d", dtEndDay, Cells(i, 5)) > 0) Then
            Cells(i, 5) = Empty
        End If
    Next

End Sub


' エラー処理
Private Sub prcSqlError(ByVal objADO As Object)
     Dim objErrItem As Object

     Set objErrItem = objADO.Errors.Item(0)

     Debug.Print "Description=" & objErrItem.Description
     Debug.Print "HelpContext=" & objErrItem.HelpContext
     Debug.Print "HelpFile=" & objErrItem.HelpFile
     Debug.Print "NativeError=" & objErrItem.NativeError
     Debug.Print "Number=" & objErrItem.Number
     Debug.Print "Source=" & objErrItem.Source
     Debug.Print "SQLState=" & objErrItem.SqlState

     Set objErrItem = Nothing

End Sub

' ゴンペルツ演算
Private Sub gomperzForSim(ByVal strInSheetName As String, _
                          ByVal strAnalyzeSheetName As String, _
                          ByVal colRowLabel As Long)
    Dim varSettingParam As Variant
    Dim strByWeekOrDay As String
    Dim dtPredicted As Date
    Dim varTmp As Variant
    Dim dtStart As Date
    Dim dtEnd As Date
    Dim strDataFrequency As String
    Dim booGomperz As String

    Dim dtPredictedCreated As Date
    Dim dtPredictedResolved As Date

    Debug.Print "(gomperzForSim):strInSheetName = " & strInSheetName & _
                " strAnalyzeSheetName = " & strAnalyzeSheetName & _
                " colRowLabel = " & colRowLabel

    ' パラメータ取得
    strByWeekOrDay = "Day"

    Sheets(strConstTestDataSheetName).Select
    dtStart = WorksheetFunction.Min(Rows(GetTargetStringRow(Columns(colRowLabel), "Date", True)))
    dtPredicted = WorksheetFunction.Max(Rows(GetTargetStringRow(Columns(colRowLabel), "Date", True)))
    dtEnd = Cells(GetTargetStringRow(Columns(colRowLabel), "Current Report Date") + 1, colRowLabel).Value

    Debug.Print "(gomperzForSim):[before] dtStart = " & dtStart & " dtPredicted = " & dtPredicted & _
                " dtEnd = " & dtEnd

    Sheets(strInSheetName).Select
    If (WorksheetFunction.Min(Columns(4)) <> 0) And (WorksheetFunction.Min(Columns(4)) < dtStart) Then
        dtStart = WorksheetFunction.Min(Columns(4))
    End If

    If (dtPredicted < dtEnd) Then
        dtPredicted = dtEnd
    End If

    Debug.Print "(gomperzForSim):[after] dtStart = " & dtStart & " dtPredicted = " & dtPredicted & _
                " dtEnd = " & dtEnd

    If (DateDiff("d", dtStart, dtEnd) <= 0) Then
        If (MsgBox("2日以上のバグのサンプルが必要です。" & vbCr & _
                   "JIRA Dataを見直してください。", _
                   vbExclamation) = vbOK) Then
            Exit Sub
        End If
    End If

    ' 計算シート生成
    同名のワークシートを削除 strAnalyzeSheetName
    同名のワークシートが無ければ追加 strAnalyzeSheetName

    ' 日毎のバグを取得
    Call GetDailyBugData(strInSheetName, _
                         strAnalyzeSheetName, _
                         dtStart, _
                         dtEnd, _
                         strByWeekOrDay)

    ' ゴンペルツパラメータ演算
    If (GetGomperzParameter(strAnalyzeSheetName, 1)) Then
        ' ゴンペルツ曲線生成
        Call GomperzGenerate(strAnalyzeSheetName, _
                             1, _
                             dtStart, _
                             dtPredicted, _
                             strByWeekOrDay)
        ' グラフ用テーブル生成
        Call GraphTableGenerate(strAnalyzeSheetName, _
                                1, _
                                dtStart, _
                                dtPredicted, _
                                strByWeekOrDay, _
                                True)
        ' 解決済バグ数追加
        Call GetCumlativeResolvedBugData(strInSheetName, _
                                         strAnalyzeSheetName, _
                                         dtStart, _
                                         dtEnd, _
                                         strByWeekOrDay)
        ' ゴンペルツ上下限界
        Call GetGomperzBugMinMax(strAnalyzeSheetName, 1)
    Else

        ' グラフ用テーブル生成
        Call GraphTableGenerate(strAnalyzeSheetName, _
                                1, _
                                dtStart, _
                                dtPredicted, _
                                strByWeekOrDay, _
                                False)
        ' 解決済バグ数追加
        Call GetCumlativeResolvedBugData(strInSheetName, _
                                         strAnalyzeSheetName, _
                                         dtStart, _
                                         dtEnd, _
                                         strByWeekOrDay)
        ' ゴンペルツ上下限界
        Call GetGomperzBugMinMax(strAnalyzeSheetName, 1)
    End If

End Sub

' データの落とし込み
Private Sub GetDailyBugData(ByVal strInSheetName As String, _
                            ByVal strOutSheetName As String, _
                            ByVal dtStart As Date, _
                            ByVal dtEnd As Date, _
                            ByVal strByWeekOrDay As String)

    Call EndDayWrite(strOutSheetName, _
                     dtStart, _
                     dtEnd, _
                     strByWeekOrDay, _
                     1)

    Call ActualSamplingNoWrite(strOutSheetName, 1, 1, 2)

    Call NumberOfDailyBugWrite(strInSheetName, _
                               4, _
                               strOutSheetName, _
                               1, _
                               1, _
                               "検出件数(fi)", _
                               dtStart, _
                               dtEnd, _
                               strByWeekOrDay, _
                               3)

End Sub


' 実績日の書き込み
Private Sub EndDayWrite(ByVal varInSheet As Variant, _
                        ByVal dtStart As Date, _
                        ByVal dtEnd As Date, _
                        ByVal strByWeekOrDay As String, _
                        ByVal intOutTargetColumn As Integer, _
                        Optional ByVal booCheck As Boolean = True)
    Dim i As Integer

    Call TargetSheetSelect(varInSheet)

    If (strByWeekOrDay = "Day") Then
        Cells(1, intOutTargetColumn).Value = "日付"

        For i = 0 To DateDiff("d", dtStart, dtEnd)
            Cells(i + 2, intOutTargetColumn).Value = DateAdd("d", i, dtStart)
        Next
    Else
        Cells(1, intOutTargetColumn).Value = "日付"

        For i = 0 To DateDiff("w", dtStart, dtEnd)
            Cells(i + 2, intOutTargetColumn).Value = DateAdd("d", 7 * i, dtStart)
        Next
    End If

End Sub

' 実績サンプリング番号の書き込み
' 時間(ti)
Private Sub ActualSamplingNoWrite(ByVal varInSheet As Variant, _
                                  ByVal intTargetColumn As Integer, _
                                  ByVal intHeaderRow As Integer, _
                                  ByVal intOutTargetColumn As Integer)
    Dim i As Integer
    Dim intStartRow As Integer
    Dim intLastRow As Integer

    intStartRow = intHeaderRow + 1
    intLastRow = GetTargetColumnEndRow(intTargetColumn)

    Call TargetSheetSelect(varInSheet)

    Cells(1, intOutTargetColumn).Value = "時間(ti)"

    For i = intStartRow To intLastRow
        Cells(i, intOutTargetColumn).Value = i - intHeaderRow
    Next

End Sub


' 検出件数(fi)
Private Sub NumberOfDailyBugWrite(ByVal strInSheetName As String, _
                                  ByVal intTargetDateColumn As Integer, _
                                  ByVal strOutSheetName As String, _
                                  ByVal intTargetColumn As Integer, _
                                  ByVal intHeaderRow As Integer, _
                                  ByVal strHeaderString As String, _
                                  ByVal dtStart As Date, _
                                  ByVal dtEnd As Date, _
                                  ByVal strByWeekOrDay As String, _
                                  ByVal intOutTargetColumn As Integer)
    Dim i, j As Integer
    Dim intStartRow As Integer
    Dim intLastRow As Integer
    Dim intCumulativeValue As Integer

    Sheets(strOutSheetName).Select

    intStartRow = intHeaderRow + 1
    intLastRow = GetTargetColumnEndRow(intTargetColumn)

    Cells(1, intOutTargetColumn).Value = strHeaderString

    For i = 0 To DateDiff("d", dtStart, dtEnd)
        Sheets(strInSheetName).Select
        intCumulativeValue = intCumulativeValue + WorksheetFunction.CountIf(Columns(intTargetDateColumn), DateAdd("d", i, dtStart))

        If (strByWeekOrDay = "Day") Then
            Sheets(strOutSheetName).Select
            Cells(i + intStartRow, intOutTargetColumn).Value = intCumulativeValue
            intCumulativeValue = 0
        Else
            If (i Mod 7 = 6) Or (i = DateDiff("d", dtStart, dtEnd)) Then
                Sheets(strOutSheetName).Select
                Cells(j + intStartRow, intOutTargetColumn).Value = intCumulativeValue
                j = j + 1
                intCumulativeValue = 0
            End If
       End If
    Next

End Sub

' ゴンペルツパラメータ取得
Private Function GetGomperzParameter(ByVal strInOutSheetName As String, _
                                     ByVal intHeaderRow As Integer) As Boolean
    Dim ret As Boolean

    ret = False

    ' バグ数=0を除く
    Call WriteExceptFor0(strInOutSheetName, intHeaderRow, 2, 4)
    ' 累積検出件数Fi(Σfi)
    Call NumberOfCumlativeBugWrite(strInOutSheetName, _
                                   5, _
                                   intHeaderRow, _
                                   "累積検出件数Fi(Σfi)", _
                                   6)

    ' ゴンペルツパラメータ算出
    ret = GomperzParameterCalc(strInOutSheetName, intHeaderRow)
    ' 不一致係数算出
    If (ret) Then
        ret = MismatchFactorCalc(strInOutSheetName, intHeaderRow)
    End If

    GetGomperzParameter = ret

End Function


' 検出件数0を除いた時間(ti)
' 検出件数0を除いた検出件数(fi)
Private Sub WriteExceptFor0(ByVal strInOutSheetName As String, _
                            ByVal intHeaderRow As Integer, _
                            ByVal intInTiColumn As Integer, _
                            ByVal intOutTiColumn As Integer)
    Dim i, j As Integer
    Dim intStartRow As Integer
    Dim intLastRow As Integer

    intStartRow = intHeaderRow + 1
    intLastRow = GetTargetColumnEndRow(intInTiColumn)

    Sheets(strInOutSheetName).Select
    Cells(1, intOutTiColumn).Value = "時間(ti)-検出件数0を除く"
    Cells(1, intOutTiColumn + 1).Value = "検出件数(fi)-検出件数0を除く"

    For i = intStartRow To intLastRow
        If (Cells(i, intInTiColumn + 1).Value <> 0) Then
            j = j + 1
            Cells(j + intHeaderRow, intOutTiColumn).Value = Cells(i, intInTiColumn).Value
            Cells(j + intHeaderRow, intOutTiColumn + 1).Value = Cells(i, intInTiColumn + 1).Value
        End If
    Next

End Sub

' 累積検出件数Fi(Σfi)
Private Sub NumberOfCumlativeBugWrite(ByVal strInOutSheetName As String, _
                                      ByVal intTargetColumn As Integer, _
                                      ByVal intHeaderRow As Integer, _
                                      ByVal strHeaderString As String, _
                                      ByVal intOutTargetColumn As Integer)
    Dim i As Integer

    Sheets(strInOutSheetName).Select
    Cells(1, intOutTargetColumn).Value = strHeaderString

    For i = 1 + intHeaderRow To GetTargetColumnEndRow(intTargetColumn)
        If (i = 1 + intHeaderRow) Then
            Cells(i, intOutTargetColumn).Value = Cells(i, intTargetColumn).Value
        Else
            Cells(i, intOutTargetColumn).Value = Cells(i - 1, intOutTargetColumn).Value + Cells(i, intTargetColumn).Value
        End If
    Next

End Sub


'　実績値から算出
'  ZN102-006準拠
Private Function GomperzParameterCalc(ByVal strInSheetName As String, _
                                      ByVal intHeaderRow As Integer) As Boolean
    Dim i As Integer
    Dim tmp As String
    Dim intStartRow As Integer
    Dim intLastRow As Integer

    On Error GoTo ErrorHandler

    GomperzParameterCalc = False

    Sheets(strInSheetName).Select

    intStartRow = intHeaderRow + 1
    intLastRow = GetTargetColumnEndRow(4)

    Cells(intHeaderRow, 7).Value = "ti^2"
    Cells(intHeaderRow, 8).Value = "Yi=ln(fi/Fi)"
    Cells(intHeaderRow, 9).Value = "ti×Yi"

    For i = intStartRow To intLastRow
        '　ti^2
        Cells(i, 7).Value = Cells(i, 4).Value ^ 2
        ' Yi=ln(fi/Fi)
        tmp = "LN(" & Cells(i, 5).Address & "/" & Cells(i, 6).Address & ")"
        Cells(i, 8).Value = Evaluate(tmp)
        ' ti×Yi
        Cells(i, 9).Value = Cells(i, 4).Value * Cells(i, 8).Value
    Next

    ' ゴンペルツパラメータ演算
    Cells(1, 11).Value = "ゴンペルツパラメータ"
    Cells(1, 12).Value = "値"
    ' Σti
    Cells(2, 11).Value = "Σti"
    tmp = "SUM(" & Cells(intStartRow, 4).Address & ":" & Cells(intLastRow, 4).Address & ")"
    Cells(2, 12).Value = Evaluate(tmp)
    ' Σfi
    Cells(3, 11).Value = "Σfi"
    Cells(3, 12).Value = Cells(intLastRow, 6).Value
    ' Σti^2
    Cells(4, 11).Value = "Σti^2"
    tmp = "SUM(" & Cells(intStartRow, 7).Address & ":" & Cells(intLastRow, 7).Address & ")"
    Cells(4, 12).Value = Evaluate(tmp)
    ' ΣYi
    Cells(5, 11).Value = "ΣYi"
    tmp = "SUM(" & Cells(intStartRow, 8).Address & ":" & Cells(intLastRow, 8).Address & ")"
    Cells(5, 12).Value = Evaluate(tmp)
    ' Σti×Yi
    Cells(6, 11).Value = "Σti×Yi"
    tmp = "SUM(" & Cells(intStartRow, 9).Address & ":" & Cells(intLastRow, 9).Address & ")"
    Cells(6, 12).Value = Evaluate(tmp)
    ' n
    ' fi=0以外の個数
    Cells(7, 11).Value = "n"
    Cells(7, 12).Value = intLastRow - intHeaderRow
    ' D
    ' D = (n*Σ(ti×Yi) - ΣtiΣYi) / (n*Σ(ti^2) - (Σti)^2)
    ' ここはfi=0以外をのぞかないのかな?
    ' 除いてよいらしい。もう一度再計算しよう・・。8/26
    Cells(8, 11).Value = "D"
    Cells(8, 12).Value = (Cells(7, 12).Value * Cells(6, 12).Value - Cells(2, 12).Value * Cells(5, 12).Value) / (Cells(7, 12).Value * Cells(4, 12).Value - Cells(2, 12) ^ 2)
    ' C
    ' C = (ΣYi - D*Σti) / n
    Cells(9, 11).Value = "C"
    Cells(9, 12).Value = (Cells(5, 12).Value - Cells(8, 12).Value * Cells(2, 12).Value) / Cells(7, 12).Value
    ' a
    ' a = exp(exp(C)/D)
    Cells(10, 11).Value = "a"
    tmp = "EXP(EXP(" & Cells(9, 12).Address & ")/" & Cells(8, 12).Address & ")"
    Cells(10, 12).Value = Evaluate(tmp)
    ' b
    ' b = exp(D)
    Cells(11, 11).Value = "b"
    tmp = "EXP(" & Cells(8, 12).Address & ")"
    Cells(11, 12).Value = Evaluate(tmp)
    ' t
    Cells(12, 11).Value = "t"
    Cells(12, 12).Value = Cells(GetTargetColumnEndRow(2), 2).Value
    ' N
    ' N=exp(ln(Σfi)-b^t*ln(a))
    Cells(13, 11).Value = "N"
    tmp = "EXP(LN(" & Cells(3, 12).Address & ")-((" & Cells(11, 12).Address & "^" & Cells(12, 12).Address & ")*(LN(" & Cells(10, 12).Address & "))))"
    Cells(13, 12).Value = Evaluate(tmp)
    ' 95%限界最大バグ数
    Cells(14, 11).Value = "95%限界最大バグ数"
    tmp = Cells(13, 12).Address & "+1.64*SQRT(" & Cells(13, 12).Address & ")"
    Cells(14, 12).Value = Evaluate(tmp)
    ' 95%限界最小バグ数
    Cells(15, 11).Value = "95%限界最小バグ数"
    tmp = Cells(13, 12).Address & "-1.64*SQRT(" & Cells(13, 12).Address & ")"
    Cells(15, 12).Value = Evaluate(tmp)
    ' 収束率F/N
    Cells(16, 11).Value = "収束率"
    Cells(16, 12).Value = Format((Cells(3, 12).Value / Cells(13, 12).Value), "0.000 %")

    GomperzParameterCalc = True

    Exit Function
ErrorHandler:
'    MsgBox "現状データではゴンペルツ演算できません"
'    End

End Function

' 不一致係数の算出
Private Function MismatchFactorCalc(ByVal strInSheetName As String, _
                                    ByVal intHeaderRow As Integer) As Boolean
    Dim i As Integer
    Dim intStartRow As Integer
    Dim intLastRow As Integer
    Dim a As Double
    Dim b As Double
    Dim n As Double
    Dim t As Integer
    Dim tmp As String

    On Error GoTo ErrorHandler

    MismatchFactorCalc = False

    Sheets(strInSheetName).Select

    intStartRow = intHeaderRow + 1
    intLastRow = GetTargetColumnEndRow(1)

    ' t,fiをコピー,Σfiを算出
    Cells(1, 14).Value = "時間(ti)"
    Cells(1, 15).Value = "検出件数(fi)"
    Cells(1, 16).Value = "累積検出件数Fi(Σfi)"

    For i = intStartRow To intLastRow
        ' t
        Cells(i, 14).Value = Cells(i, 2).Value
        ' fi
        Cells(i, 15).Value = Cells(i, 3).Value
        ' Σfi
        If (i = intStartRow) Then
            Cells(i, 16).Value = Cells(i, 3).Value
        Else
            Cells(i, 16).Value = Cells(i - 1, 16).Value + Cells(i, 3).Value
        End If
    Next

    ' N
    n = Cells(13, 12).Value
    ' a
    a = Cells(10, 12).Value
    ' b
    b = Cells(11, 12).Value

    ' ゴンペルツ曲線:F=N×(a^(b^t))
    Cells(1, 17).Value = "F"
    Cells(1, 18).Value = "Σfiの2乗"
    Cells(1, 19).Value = "Fの2乗"
    Cells(1, 20).Value = "誤差の2乗"

    For i = intStartRow To intLastRow
        ' t
        t = Cells(i, 14).Value
        ' ゴンペルツ曲線(予測値):F=N×(a^(b^t))
        Cells(i, 17).Value = n * (a ^ (b ^ t))
        ' 実績値:Σfiの2乗
        Cells(i, 18).Value = Cells(i, 16).Value ^ 2
        ' 予測値:Fの2乗
        Cells(i, 19).Value = Cells(i, 17).Value ^ 2
        ' 誤差:(実測値Σfi-予測値F)の2乗
        Cells(i, 20).Value = (Cells(i, 16).Value - Cells(i, 17).Value) ^ 2
    Next

    ' 不一致係数
    ' = Σ誤差の2乗^(1/2) / (Σ実績値の2乗^(1/2) + Σ予測値の2乗^(1/2))
    Cells(17, 11).Value = "不一致係数"
    tmp = "SUM(" & Cells(intStartRow, 20).Address & ":" & Cells(intLastRow, 20).Address & ")^0.5" & _
          "/" & _
          "(SUM(" & Cells(intStartRow, 18).Address & ":" & Cells(intLastRow, 18).Address & ")^0.5 +" & _
          " SUM(" & Cells(intStartRow, 19).Address & ":" & Cells(intLastRow, 19).Address & ")^0.5)"
    Cells(17, 12).Value = "=" & tmp

    MismatchFactorCalc = True

    Exit Function
ErrorHandler:
'    MsgBox "現状データではゴンペルツ演算できません"
'    End

End Function

' ゴンペルツ曲線生成
Private Sub GomperzGenerate(ByVal strInSheetName As String, _
                            ByVal intHeaderRow As Integer, _
                            ByVal dtStart As Date, _
                            ByVal dtEnd As Date, _
                            ByVal strByWeekOrDay As String)
    Dim i As Integer
    Dim intStartRow As Integer
    Dim intLastRow As Integer
    Dim a As Double
    Dim b As Double
    Dim n As Double
    Dim t As Integer

    EndDayWrite strInSheetName, dtStart, dtEnd, strByWeekOrDay, 22
    ActualSamplingNoWrite strInSheetName, 22, 1, 23

    Sheets(strInSheetName).Select

    intStartRow = intHeaderRow + 1
    intLastRow = GetTargetColumnEndRow(23)

    ' N
    n = Cells(13, 12).Value
    ' a
    a = Cells(10, 12).Value
    ' b
    b = Cells(11, 12).Value

    ' ゴンペルツ曲線(予測値):F=N×(a^(b^t))
    Cells(1, 24).Value = "F"

    For i = intStartRow To intLastRow
        ' t
        t = Cells(i, 23).Value
        ' ゴンペルツ曲線(予測値):F=N×(a^(b^t))
        Cells(i, 24).Value = n * (a ^ (b ^ t))
    Next

End Sub

' グラフ用テーブル生成
Private Sub GraphTableGenerate(ByVal strInSheetName As String, _
                               ByVal intHeaderRow As Integer, _
                               ByVal dtStart As Date, _
                               ByVal dtEnd As Date, _
                               ByVal strByWeekOrDay As String, _
                               ByVal booGomperz As Boolean)
    Dim i As Integer
    Dim intStartRow As Integer
    Dim intLastRow As Integer

    Sheets(strInSheetName).Select

    ' 16列目をコピー
    intStartRow = intHeaderRow + 1

    ' 日付
'    Cells(1, 26).Value = "日付"
'    intLastRow = GetTargetColumnEndRow(22)
'    For i = intStartRow To intLastRow
'        Cells(i, 26).Value = Cells(i, 22).Value
'    Next
    EndDayWrite strInSheetName, dtStart, dtEnd, strByWeekOrDay, 26

    '　実績値:累積検出件数Fi(Σfi)
    Cells(1, 27).Value = "実測値"
    intLastRow = GetTargetColumnEndRow(3)

    For i = intStartRow To intLastRow
        ' Σfi
        If (i = intStartRow) Then
            Cells(i, 27).Value = Cells(i, 3).Value
        Else
            Cells(i, 27).Value = Cells(i - 1, 27).Value + Cells(i, 3).Value
        End If
    Next

    '　ゴンペルツ曲線(予測値):F=N×(a^(b^t))
    If (booGomperz) Then
        Cells(1, 28).Value = "予測値"
        intLastRow = GetTargetColumnEndRow(24)

        For i = intStartRow To intLastRow
            Cells(i, 28).Value = Cells(i, 24).Value
        Next
    End If

End Sub

' 解析済バグデータの落とし込み
Private Sub GetCumlativeResolvedBugData(ByVal strInSheetName As String, _
                                        ByVal strOutSheetName As String, _
                                        ByVal dtStart As Date, _
                                        ByVal dtEnd As Date, _
                                        ByVal strByWeekOrDay As String)

    ' Resolvedの累計値テーブルに追加
    Call NumberOfDailyBugWrite(strInSheetName, _
                               5, _
                               strOutSheetName, _
                               26, _
                               1, _
                               "解析済バグ数", _
                               dtStart, _
                               dtEnd, _
                               strByWeekOrDay, _
                               29)
    ' 累積検出件数Fi(Σfi)
    Call NumberOfCumlativeBugWrite(strOutSheetName, _
                                   29, _
                                   1, _
                                   "解析済バグ累積数", _
                                   30)
    Columns(29).Delete

End Sub

' ±5%のゴンペルツ上下限界値の書き込み
Private Sub GetGomperzBugMinMax(ByVal strInSheetName As String, _
                                ByVal intHeaderRow As Integer)
                        
    Dim i As Integer
    Dim intStartRow As Integer
    Dim intLastRow As Integer

    Sheets(strInSheetName).Select

    intStartRow = intHeaderRow + 1
    intLastRow = GetTargetColumnEndRow(26)

    Cells(1, 30).Value = "潜在バグ数"
    Cells(1, 31).Value = "上限値(+5%)"
    Cells(1, 32).Value = "下限値(-5%)"

    For i = intStartRow To intLastRow
        Cells(i, 30).Value = Cells(13, 12).Value
        Cells(i, 31).Value = Cells(14, 12).Value
        Cells(i, 32).Value = Cells(15, 12).Value
    Next

End Sub

'　検索文字列の行番号を取得
Private Function GetTargetStringRow(ByVal rangeTarget As Range, _
                                    ByVal strTargetString As String, _
                                    Optional ByVal boofull As Boolean = False) As Long
    Dim intReturn As Long
    Dim Foundcell As Range

    If (boofull) Then
        Set Foundcell = rangeTarget.Find(what:=strTargetString, _
                                         LookIn:=xlValues, _
                                         LookAt:=xlWhole, _
                                         MatchCase:=False, _
                                         MatchByte:=False)
    Else
        Set Foundcell = rangeTarget.Find(what:=strTargetString, _
                                         LookIn:=xlValues, _
                                         LookAt:=xlPart, _
                                         MatchCase:=False, _
                                         MatchByte:=False)
    End If

    If Foundcell Is Nothing Then
        intReturn = 0
    Else
        intReturn = Foundcell.Row
    End If

    GetTargetStringRow = intReturn

End Function

'　検索文字列の列番号を取得
Private Function GetTargetStringColumn(ByVal rangeTarget As Range, _
                                       ByVal strTargetString As String) As Long
    Dim intReturn As Long
    Dim Foundcell As Range

    Set Foundcell = rangeTarget.Find(what:=strTargetString, _
                                     LookIn:=xlValues, _
                                     LookAt:=xlPart, _
                                     MatchCase:=False, _
                                     MatchByte:=False)
    If Foundcell Is Nothing Then
        intReturn = 0
    Else
        intReturn = Foundcell.Column
    End If

    GetTargetStringColumn = intReturn

End Function


Private Sub JobBegin(Optional strMsg As String = "")

    varSaveCalculation = Application.Calculation
    varSaveScreenUpdating = Application.ScreenUpdating

    Application.Calculation = xlCalculationManual

    If strMsg <> "" Then
        Application.StatusBar = strMsg
    End If

    Application.ScreenUpdating = False

End Sub

Private Sub JobEnd(Optional strMsg As String = "", _
                   Optional MsgBoxShow As Boolean = False)

    If Not IsEmpty(varSaveCalculation) Then
        If Application.Calculation <> varSaveCalculation Then
            Application.Calculation = varSaveCalculation
        End If

        varSaveCalculation = Empty
    End If

    If Not IsEmpty(varSaveScreenUpdating) Then
        Application.ScreenUpdating = varSaveScreenUpdating
        varSaveScreenUpdating = Empty
    End If

    If (strMsg <> "") And (MsgBoxShow = True) Then
        Application.StatusBar = Replace(strMsg, vbCrLf, " ")
        MsgBox strMsg, vbOKOnly
    End If

    Application.StatusBar = False

End Sub

Private Sub 同名のワークシートが無ければ追加(ByVal strOutSheetName As String)
    Dim i As Worksheet

    For Each i In Worksheets
        If (UCase(i.Name) = UCase(strOutSheetName)) Then
            Sheets(strOutSheetName).Select
            Exit Sub
        End If
    Next

    Sheets.Add After:=Sheets(Sheets(Sheets.Count).Name)
    ActiveSheet.Name = strOutSheetName

End Sub

Private Sub 同名のワークシートを削除(ByVal strOutSheetName As String)
    Dim i As Worksheet

    For Each i In Worksheets
        If (UCase(i.Name) = UCase(strOutSheetName)) Then
            Application.DisplayAlerts = False
            i.Delete
            Application.DisplayAlerts = True
            Exit For
        End If
    Next

End Sub

Private Sub 同名のワークシートを非表示(ByVal strOutSheetName As String)
    Dim i As Worksheet

    For Each i In Worksheets
        If (UCase(i.Name) = UCase(strOutSheetName)) Then
            i.Visible = False
            Exit For
        End If
    Next

End Sub

Private Sub TargetSheetSelect(ByVal varInSheet As Variant)

    If VarType(varInSheet) = vbString Then
        Sheets(varInSheet).Select
    Else
        varInSheet.Parent.Activate
        varInSheet.Select
    End If

End Sub


Private Function GetTargetColumnEndRow(ByVal intColumn As Integer, _
                                       Optional ByVal intRow As Integer = 1) As Integer
    Dim ret As Integer

    On Error GoTo overflow
    ' 空白時は、2回実行、オーバーフロー時は1
    ' integerの範囲を超えたらオーバーフローとする
    If (Cells(intRow, intColumn).Value = "") Then
        ret = Cells(intRow, intColumn).End(xlDown).Row
        ret = Cells(ret, intColumn).End(xlDown).Row
    Else
        ret = Cells(intRow, intColumn).End(xlDown).Row
    End If

    GetTargetColumnEndRow = ret
    Exit Function

overflow:
    GetTargetColumnEndRow = intRow

End Function

Private Function RegExpReplace(ByVal strInput As String, _
                               ByVal strFind As String, _
                               ByVal strReplace As String) As String
    Dim re As Variant
    Dim strResult As String

    Set re = CreateObject("VBScript.RegExp")

    With re
        .Pattern = strFind
        .IgnoreCase = True
        .Global = True
    End With

    strResult = re.Replace(strInput, strReplace)
    RegExpReplace = strResult

End Function

