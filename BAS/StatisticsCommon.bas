Attribute VB_Name = "StatisticsCommon"
Option Explicit

Public Const strStatGenSheetNamePrefix As String = "_" '集計機能生成シートのシート名prefix

'' シート名
Private Const strConstConfigSheetName As String = "集計" '設定シート

Private Const strConstSimpleStatSheetName As String = "簡易集計結果" '簡易集計の書式＆出力シート

'' 定義名
Private Const strDefName_TargetProject As String = "対象プロジェクト"
Private Const strDefName_ProblemDBtype As String = "問題管理DB種別"

Private Const strTableName_SimpleStat As String = "簡易集計用テーブル"

Private Declare PtrSafe Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)


Public Sub 未実装(booShowMsgBox As Boolean)
    If booShowMsgBox Then
        MsgBox "このボタンは" & vbCrLf _
             & "指定されたDB/プロジェクトに対して未実装です", vbCritical
    End If
End Sub

Sub T600_AllExcecute_Auto()
    Dim strPath As String
    Dim strBaseFileName As String
    Dim strExtension As String
    Dim strTimeStamp As String
    Dim strFullFilePath As String
    
    Call T600_AllExcecute(False)
    
    'Dim fso As Object
    'Set fso = CreateObject("Scripting.FileSystemObject")
    
    'Dim fullPath As String
    'fullPath = ThisWorkbook.FullName
    
    'strPath = fso.GetParentFolderName(fullPath)
    'strBaseFileName = fso.GetBaseName(fullPath)
    'strExtension = fso.GetExtensionName(fullPath)
    'strTimeStamp = Format(Now, "yyyymmdd_HHNN")
    'strFullFilePath = strPath & "\" & strBaseFileName & "_" & strTimeStamp
    
    'Set fso = Nothing
    
    'ActiveWorkbook.SaveAs FileName:=strFullFilePath
    'ActiveWorkbook.Close
    
End Sub


Sub T600_AllExcecute(Optional ByVal booEndMessage As Boolean = True)
    ' Statusバー表示
    Const strJobName As String = "T600用 JiraImport+集計 連続処理"
    StatisticsLibrary2.JobBegin strJobName & "中..."
    Call JiraGet(False)
    Call StatisticsExecute(False)
    StatisticsLibrary2.JobEnd strJobName & "処理終了", MsgBoxShow:=booEndMessage
    ' ありえないがSaveAsが無いと言われるので、おまじない
    DoEvents
    Sleep 10000
    DoEvents
End Sub



Sub StatisticsExecute(Optional ByVal booEndMessage As Boolean = True)
    Const strJobName As String = "全統計処理"
    
    If (booEndMessage) Then
        If MsgBox(strJobName & "を開始します。", vbOKCancel) <> vbOK Then
            Exit Sub
        End If
    End If
    
    ' Statusバー表示
    StatisticsLibrary2.JobBegin strJobName & "中..."
    
'    GomperzExecute False  '2018/10/02 停止(齊藤)
'    PBExecute False: ThisWorkbook.Activate     '2016/2/23 削除
    TTypeMatrixExecute False
    ProblemsByFuncExecute False
    AssigneeExecute False
    TookDaysExecute False
    IssuesByFAExecute False
    'TATByComponentsExecute False   '下記をFunctionalAreaを追加したのでComponentsをマスク　10/4
    TATByFunctionalAreaExecute False
'    TestResultTableExecute False'注）「ST2_合同試験項目書」未対応のため、2016/5/30 削除。
    TATByAssigneeExecute False ' 10/18 追加
    
    ' Statusバー表示
    StatisticsLibrary2.JobEnd strJobName & "処理終了", MsgBoxShow:=booEndMessage

End Sub

Sub 簡易集計()
    Const strJobName    As String = "簡易集計"
    Dim wsSrc  As Worksheet    '入力データシート
    Dim wsDst(1)  As Worksheet    '出力データシート
    Dim strSheetName As String
    Dim i As Long
    Dim pvt As PivotTable
    
    Dim varTmp As Variant
    Dim rowSrcTop  As Long
    Dim rowSrcEnd  As Long
    Dim rngSrcHeader As Range
    Dim rngDstHeader As Range
    
    If MsgBox(strJobName & "を開始します。", vbOKCancel) <> vbOK Then
        Exit Sub
    End If
    
    ' 前処理
    Call StatisticsLibrary2.JobBegin(strJobName & "中...")
    
    ' 入力シート取得
    Set wsSrc = StatisticsLibrary2.問題管理データシート取得()
    If wsSrc Is Nothing Then
        GoTo Error_End
    End If
    
    ' レコード数取得
    Call StatisticsLibrary2.RedmineSheetDataRangeGet(wsSrc, rowSrcTop, rowSrcEnd)
    
    ' 出力シート取得
    For i = 0 To 1
        Select Case i
        Case 0
            strSheetName = strConstSimpleStatSheetName
        Case 1
            strSheetName = strConstConfigSheetName
        Case Else
            MsgBox "Bug!!", vbCritical
        End Select
        Set wsDst(i) = StatisticsLibrary2.同名のシート取得(strSheetName)
        If wsDst(i) Is Nothing Then
            GoTo Error_End
        End If
    Next i
    
    ' データコピー
    Set rngSrcHeader = RedmineSheetHeader(wsSrc)
    Set rngDstHeader = wsDst(0).Cells(1, 1).CurrentRegion.Rows(1).Resize(2)
    varTmp = SelectDataWrite(rngDstHeader, rngSrcHeader, rowSrcTop, rowSrcEnd, By項目名:=True)
    If Not IsArray(varTmp) Then
        GoTo Error_End
    End If
    
    ' テーブルサイズ調整
    Dim objTable As ListObject
    Set objTable = GetTableByName(strTableName_SimpleStat, wsDst(0))
    If objTable Is Nothing Then
        GoTo Error_End
    End If
    With objTable
        varTmp = varTmp(2) - varTmp(0) + 1 'データ行数
        If varTmp < .ListRows.Count Then
            Call MyTableRowDelete(.ListRows(varTmp + 1))
        End If
    End With
    
    ' 出力シート書式調整
    With wsDst(0).UsedRange
        .WrapText = False
        .Rows(1).WrapText = True
    End With
    
    ' ピボットテーブルをすべて更新
    On Error Resume Next
    For i = 0 To 1
        For Each pvt In wsDst(i).PivotTables
            pvt.PivotCache.Refresh
            If Err.Number <> 0 Then
                MsgBox "ピボットテーブル(" & pvt.Name & ")の更新に失敗しました" & vbCrLf & vbCrLf _
                        & Err.Description, vbExclamation
                Err.Clear
            End If
        Next
    Next i
    On Error GoTo 0
    
    Call MyWsCalculate(wsDst(1))
    
    Call StatisticsLibrary2.JobEnd(strJobName & "処理終了", MsgBoxShow:=True)
    Exit Sub
    
Error_End:
    StatisticsLibrary2.JobEnd strJobName & "中止", MsgBoxShow:=False
    
End Sub

Sub Grf更新()
    Const strJobName    As String = "Grf更新"
    Dim wsSrc  As Worksheet    '入力データシート
    Dim wsDst(2)  As Worksheet    '出力データシート
    Dim strSheetName As String
    Dim i As Long
    Dim pvt As PivotTable
    
    Dim varTmp As Variant
    Dim rowSrcTop  As Long
    Dim rowSrcEnd  As Long
    Dim rngSrcHeader As Range
    Dim rngDstHeader As Range
    
    If TargetProjectNameGet() <> "S100" Then
        Call 未実装(True)
        Exit Sub
    End If
    
    If MsgBox(strJobName & "を開始します。", vbOKCancel) <> vbOK Then
        Exit Sub
    End If
    
    ' 前処理
    Call StatisticsLibrary2.JobBegin(strJobName & "中...")
    
    ' 入力シート取得
    Set wsSrc = StatisticsLibrary2.問題管理データシート取得()
    If wsSrc Is Nothing Then
        GoTo Error_End
    End If
    
    ' レコード数取得
    Call StatisticsLibrary2.RedmineSheetDataRangeGet(wsSrc, rowSrcTop, rowSrcEnd)
    
    ' 出力シート取得
    For i = 0 To 2
        Select Case i
        Case 0
            strSheetName = "ProblemWK"
        Case 1
            strSheetName = "データ解析(PTR)"
        Case 2
            strSheetName = "Grf"
        Case Else
            MsgBox "Bug!!", vbCritical
        End Select
        Set wsDst(i) = StatisticsLibrary2.同名のシート取得(strSheetName)
        If wsDst(i) Is Nothing Then
            GoTo Error_End
        End If
    Next i
    
    ' データコピー ※ヘッダが二行以上あるとピボットテーブルのデータソースで
    '                範囲指定が必要になるので、余分なヘッダ行を除去する。
    Set rngSrcHeader = RedmineSheetHeader(wsSrc)
    Set rngDstHeader = wsDst(0).Cells(1, 1).CurrentRegion.Rows(1).Resize(2)
    varTmp = SelectDataWrite(rngDstHeader, rngSrcHeader, rowSrcTop, rowSrcEnd)
    If Not IsArray(varTmp) Then
        GoTo Error_End
    End If
    
    ' 出力シート書式調整
    With wsDst(0).UsedRange
        .WrapText = False
        .Rows(1).WrapText = True
    End With
    
    ' ピボットテーブルをすべて更新
    On Error Resume Next
    For Each pvt In wsDst(1).PivotTables
        pvt.PivotCache.Refresh
        If Err.Number <> 0 Then
            MsgBox "ピボットテーブル(" & pvt.Name & ")の更新に失敗しました" & vbCrLf & vbCrLf _
                    & Err.Description, vbExclamation
            Err.Clear
        End If
    Next
    On Error GoTo 0
    
    For i = 1 To 2
        Call MyWsCalculate(wsDst(i))
    Next i
    
    Call MySheetActivate(wsDst(2))
    
    Call StatisticsLibrary2.JobEnd(strJobName & "処理終了", MsgBoxShow:=True)
    Exit Sub
    
Error_End:
    StatisticsLibrary2.JobEnd strJobName & "中止", MsgBoxShow:=False
    
End Sub

Public _
Function 集計用出力シート名生成(Id As String) As String
    集計用出力シート名生成 = StatisticsLibrary.ConvertSheetName(strStatGenSheetNamePrefix & Id)
End Function

Private Function IsMyGenerateSheet(objSheet As Object) As Boolean
    If objSheet.Name Like strStatGenSheetNamePrefix & "*" Then
        IsMyGenerateSheet = True
    Else
        IsMyGenerateSheet = False
    End If
End Function

Sub StatisticsResultAllClear()
    
    Dim wb As Workbook
    Dim i As Long
    Dim intDeleteCount As Long
    Dim intHidden   As Long
            
    Application.StatusBar = False
    
    Set wb = ThisWorkbook
    
    ' 削除するシート数をカウント
    intDeleteCount = 0
    intHidden = 0
    For i = wb.Sheets.Count To 1 Step -1
        If IsMyGenerateSheet(wb.Sheets(i)) Then
            intDeleteCount = intDeleteCount + 1
            
            If wb.Sheets(i).Visible <> xlSheetVisible Then
                intHidden = intHidden + 1
            End If
        End If
    Next i
    If intDeleteCount = 0 Then
        MsgBox "削除するシートはありません", vbInformation
        Exit Sub
    End If
    
    If MsgBox("削除シート数: " & intDeleteCount & "  (非表示 " & intHidden & ")" & vbCr & vbCr _
            & "削除を開始します", vbOKCancel) = vbCancel Then
        Exit Sub
    End If
    
    ' シートの削除
    Application.ScreenUpdating = False
    
    For i = wb.Sheets.Count To 1 Step -1
        Application.DisplayAlerts = False
        If IsMyGenerateSheet(wb.Sheets(i)) Then
            wb.Sheets(i).Delete
        End If
        Application.DisplayAlerts = True
    Next i
    
    Application.ScreenUpdating = True
'    MsgBox "削除終了"

End Sub

Sub 集計用非表示シートを全て表示()
    
    Dim wb As Workbook
    Dim i As Long
    Dim intHidden   As Long
            
    Application.StatusBar = False
    
    Set wb = ThisWorkbook
    
    ' 非表示シートのカウント
    intHidden = 0
    For i = 1 To wb.Sheets.Count
        If IsMyGenerateSheet(wb.Sheets(i)) Then
            If wb.Sheets(i).Visible <> xlSheetVisible Then
                intHidden = intHidden + 1
            End If
        End If
    Next i
    If intHidden = 0 Then
        MsgBox "非表示シートはありません", vbInformation
        Exit Sub
    End If
    
    If MsgBox("非表示シート(シート数 " & intHidden & ")を表示します", vbOKCancel) = vbCancel Then
        Exit Sub
    End If
    
    ' シートの表示
    Application.ScreenUpdating = False
    For i = 1 To wb.Sheets.Count
        If IsMyGenerateSheet(wb.Sheets(i)) Then
            If wb.Sheets(i).Visible <> xlSheetVisible Then
                wb.Sheets(i).Visible = xlSheetVisible
            End If
        End If
    Next i
    
    Application.ScreenUpdating = True
    MsgBox "表示終了"

End Sub

' 集計対象プロジェクト名取得
Public _
Function TargetProjectNameGet() As String
    Dim strTmp As String
    strTmp = GetConfigValue(strDefName_TargetProject, NullEnable:=False)
    Select Case strTmp
    Case "S100", "L100", "L120", "C200", "T200", "T3xx"
        ' ＯＫ
    Case ""
        End
    Case Else
        Call MsgBox("プロジェクト「" & strTmp & "」は未対応です", vbCritical)
        End
    End Select
    
    TargetProjectNameGet = strTmp
End Function

' 問題管理DB種別取得
Public _
Function ProblemDataBaseTypeGet() As String
    Dim strTmp As String
    
    strTmp = GetConfigValue(strDefName_ProblemDBtype, NullEnable:=False)
    Select Case strTmp
    Case "JIRA", "Redmine"
        ' ＯＫ
    Case ""
        End
    Case Else
        Call MsgBox("DB種別「" & strTmp & "」は未対応です", vbCritical)
        End
    End Select
    
    Select Case GetConfigValue(strDefName_TargetProject, NullEnable:=True)
    Case "C200"
        strTmp = "JIRA"
        
    End Select

    ProblemDataBaseTypeGet = strTmp
End Function

Private _
Function GetConfigValue( _
            ByVal strAddress As String, _
            ByVal NullEnable As Boolean, _
            Optional ByVal ConfigSheet As Worksheet = Nothing) As String
            
    Dim ws As Worksheet
    Dim rngTmp As Range
    Dim strTmp As String
    
    GetConfigValue = ""
    
    If ConfigSheet Is Nothing Then
        Set ws = ConfigSheetGet()
        If ws Is Nothing Then
            End
        End If
    Else
        Set ws = ConfigSheet
    End If
    
    Set rngTmp = 名前の参照範囲取得(ws, strAddress)
    If rngTmp Is Nothing Then
        End
    End If
    
    strTmp = Trim$(rngTmp.Value)
    If IsSpaceString(strTmp) Then
        strTmp = ""
        If Not NullEnable Then
            Call MsgBox(ws.Name & "シートの " _
                      & strAddress & " に有効な値が設定されていません", vbCritical)
        End If
    End If
    
    GetConfigValue = strTmp
End Function

' 生成パラメータシート名取得
Public _
Function ConfigSheetNameGet() As String
    ConfigSheetNameGet = strConstConfigSheetName
End Function

Public _
Function 生成シートの挿入位置取得() As Object
    Set 生成シートの挿入位置取得 = 同名のシート取得(strConstConfigSheetName)
End Function

Public _
Sub StatisticsProcessSelectAll()
'あーー
Application.Calculation = xlCalculationManual
    Call StatisticsLibrary.SetAllProcessEnable("○")
Application.Calculation = xlCalculationAutomatic
End Sub

Public _
Sub StatisticsProcessSelectClear()
'あーー
Application.Calculation = xlCalculationManual
    Call StatisticsLibrary.SetAllProcessEnable(vbNullString)
Application.Calculation = xlCalculationAutomatic
End Sub
