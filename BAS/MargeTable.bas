Attribute VB_Name = "MargeTable"
Option Explicit

Enum eColGraph
  xDate = 1
  JiraActualBug = 2
  GomperzPredictedBug = 3
  JiraActualResolvedBug = 4
  GomperzPredictedBugN = 5
  GomperzPredictedBugMax = 6
  GomperzPredictedBugMin = 7
  TestPlannedAttempts = 8
  TestPlannedPass = 9
  TestActualAttempts = 10
  TestPassed = 11
  LogisticsPredictedBug = 12
  LeastSquaresActualResolvedBug = 13
  DLineGomperzPredictedBugMin = 14
  DLineCurPredictedResolvedBug = 15
  DLineCrossAtGomperzAndPredicted = 16
End Enum


Sub MargeTable()
    Dim dtPBMin As Date
    Dim intPBMinRow As Integer
    Dim intEndRow As Integer
    Dim intEndColumn As Integer
    Dim i As Integer
    Dim dtStartDay As Date
    Dim dtEndDay As Date
    Dim dtEnd As Date
    Dim booStartDay As Boolean
    Dim booEndDay As Boolean
    Dim intStartDayRow As Integer
    Dim intEndDayRow As Integer
    Dim dActualBug As Double
    Dim dGomperz As Double
    Dim dLowerLimit As Double
    Dim dSlope As Double
    Dim dIntercept As Double
    Dim dCalcValue As Double

    Application.ScreenUpdating = False

    同名のワークシートを削除 strConstMargeTableSheetName
    同名のワークシートが無ければ追加 strConstMargeTableSheetName

    ' Gomperzの期間はPBの期間以上ある様に組んである
    ' Gomperzの軸にPBのデータを持ってくる

    ' Gomperzデータをコピー
    Sheets(strConstGomperzAnalyzeSheetName).Select
    Range(Columns(26), Columns(32)).Copy
    Sheets(strConstMargeTableSheetName).Select
    Cells(1, 1).Select
    ActiveSheet.Paste

    ' TestCharts
    ' TestChartあり
    If (Sheets(strConstConfigSheetName).Range("TestDataFilePath").Value <> "") Then
        ' 日付の先頭を合わせてコピー
        dtPBMin = WorksheetFunction.Min(Sheets(strConstPBAnalyzeSheetName).Columns(1))
        ' 対象行を抽出
        'intPBMinRow = GetTargetStringRow(Columns(1), CStr(dtPBMin))
        ' Dateは上記で検索できないので下記で抽出
        Sheets(strConstMargeTableSheetName).Select
        For i = 2 To GetUsedRangeEndRow
            If (DateDiff("d", CDate(Cells(i, eColGraph.xDate)), dtPBMin) = 0) Then
                intPBMinRow = i
                Exit For
            End If
        Next
        ' PBからPlanned Pass,Passed,バグ数指標値をコピー
        If (intPBMinRow = 0) Then
            ' ヘッダコピー
            Sheets(strConstPBAnalyzeSheetName).Select
            intEndRow = GetUsedRangeEndRow
            intEndColumn = GetUsedRangeEndColumn
            Range(Cells(1, 4), Cells(1, intEndColumn)).Copy
            Sheets(strConstMargeTableSheetName).Select
            Cells(1, eColGraph.TestPlannedAttempts).Select
            ActiveSheet.Paste
            
            ' データコピー
            Sheets(strConstPBAnalyzeSheetName).Select
            Range(Cells(2, 4), Cells(intEndRow, intEndColumn)).Copy
            Sheets(strConstMargeTableSheetName).Select
            Cells(2, eColGraph.TestPlannedAttempts).Select
            ActiveSheet.Paste
            ' 日程
            Sheets(strConstPBAnalyzeSheetName).Select
            Range(Cells(1, 1), Cells(intEndRow, 1)).Copy
            Sheets(strConstMargeTableSheetName).Select
            Cells(1, eColGraph.xDate).Select
            ActiveSheet.Paste
            
        ' 通常
        Else
            ' ヘッダコピー
            Sheets(strConstPBAnalyzeSheetName).Select
            intEndRow = GetUsedRangeEndRow
            intEndColumn = GetUsedRangeEndColumn
            Range(Cells(1, 4), Cells(1, intEndColumn)).Copy
            Sheets(strConstMargeTableSheetName).Select
            Cells(1, eColGraph.TestPlannedAttempts).Select
            ActiveSheet.Paste
            ' データコピー
            Sheets(strConstPBAnalyzeSheetName).Select
            Range(Cells(2, 4), Cells(intEndRow, intEndColumn)).Copy
            Sheets(strConstMargeTableSheetName).Select
            Cells(intPBMinRow, eColGraph.TestPlannedAttempts).Select
            ActiveSheet.Paste
        End If
    End If

    ' バグ改修予測(線形)用データをテーブルに追加用
    If (Sheets(strConstConfigSheetName).Range("LeastSquaresInsertSetting").Value = "あり") Then
        Sheets(strConstMargeTableSheetName).Select
        Columns(4).Copy (Columns(eColGraph.LeastSquaresActualResolvedBug))
        Cells(1, eColGraph.LeastSquaresActualResolvedBug).Value = "解析済バグ累積数-改修予測用"

        ' 期間修正
        booStartDay = IsDate(Sheets(strConstConfigSheetName).Range("LeastSquaresStartDay").Value)
        booEndDay = IsDate(Sheets(strConstConfigSheetName).Range("LeastSquaresEndDay").Value)
        dtStartDay = Sheets(strConstConfigSheetName).Range("LeastSquaresStartDay").Value
        dtEndDay = Sheets(strConstConfigSheetName).Range("LeastSquaresEndDay").Value
        Debug.Print "(MargeTable):[before] booStartDay = " & booStartDay & " booEndDay = " & booEndDay & _
                    " dtStartDay = " & dtStartDay & " dtEndDay = " & dtEndDay

        ' Current Report Dateまでを実績の期間とする為の追加
        Sheets(strConstTestDataSheetName).Select
        dtEnd = Cells(GetTargetStringRow(Columns(1), "Current Report Date") + 1, 1).Value
        Debug.Print "(MargeTable):dtEnd(from TestChart) = " & dtEnd

        If (booEndDay = True) Then
            If DateDiff("d", dtEndDay, dtEnd) < 0 Then
                dtEndDay = dtEnd
            End If
        Else
            booEndDay = True
            dtEndDay = dtEnd
        End If

        If (booStartDay = True) And (booEndDay = True) Then
            If DateDiff("d", dtEndDay, dtStartDay) >= 0 Then
                'MsgBox "バグ改修予測の開始と終了又は""current Report Date""の日付を見直してください。", _
                '       vbCritical
                MsgBox "バグ改修予測の開始と終了の日付を見直してください。", vbCritical
                Sheets(strConstConfigSheetName).Select
                End
            End If
        End If

        Debug.Print "(MargeTable):[after] dtStartDay = " & dtStartDay & " dtEndDay = " & dtEndDay

        Sheets(strConstMargeTableSheetName).Select
        intEndRow = GetTargetColumnEndRow(eColGraph.xDate)

        If (booStartDay = True) Then
            For i = 2 To intEndRow
                If (DateDiff("d", Cells(i, eColGraph.xDate), dtStartDay) = 0) Then
                    intStartDayRow = i
                    Exit For
                End If
            Next
            If (intStartDayRow > 0) Then
                For i = 2 To intStartDayRow - 1
                    Cells(i, eColGraph.LeastSquaresActualResolvedBug) = Empty
                Next
            ElseIf (intStartDayRow = 0) Then
                intStartDayRow = 2
            End If
        Else
          ' バグ改修予測(線形)の抽出期間設定(Resolved)の開始日が設定されていない場合
          intStartDayRow = 2
        End If

        If (booEndDay = True) Then
            For i = 2 To intEndRow
                If (DateDiff("d", Cells(i, eColGraph.xDate), dtEndDay) = 0) Then
                    intEndDayRow = i
                    Exit For
                End If
            Next
            If (intEndDayRow > 0) Then
                For i = intEndDayRow + 1 To intEndRow
                    Cells(i, eColGraph.LeastSquaresActualResolvedBug) = Empty
                Next
            End If
        End If

        Debug.Print "(MargeTable):intStartDayRow = " & intStartDayRow & " intEndDayRow = " & intEndDayRow

        ' 現時点のバグ数に到達する、バグ問処解決予測(線形)の値からの降下線用データ
        ' (DLineCurPredictedResolvedBug)
        Cells(1, eColGraph.DLineCurPredictedResolvedBug) = "降下線(バグ改修予測)"

        ' バグ問処解決予測(線形)の値が、現時点のバグ数を超えるポイントを算出
        dActualBug = Cells(intEndDayRow, eColGraph.JiraActualBug)

        For i = intEndDayRow + 1 To intEndRow
            dCalcValue = WorksheetFunction.Forecast_Linear(Cells(i, eColGraph.xDate), _
                             Range(Cells(intStartDayRow, eColGraph.LeastSquaresActualResolvedBug), _
                                   Cells(intEndDayRow, eColGraph.LeastSquaresActualResolvedBug)), _
                             Range(Cells(intStartDayRow, eColGraph.xDate), _
                                   Cells(intEndDayRow, eColGraph.xDate)))
            If ((dActualBug - dCalcValue) < 0) Then
                Cells(i, eColGraph.DLineCurPredictedResolvedBug) = dCalcValue
                Debug.Print "(MargeTable):Date of DLineCurPredictedResolvedBug = " & _
                            Cells(i, eColGraph.xDate) & vbCrLf & _
                            "             dActualBug = " & dActualBug & " dCalcValue = " & dCalcValue
                Exit For
            End If
        Next i

        ' Gomperz表示ありの場合のみ処理
        If (Sheets(strConstConfigSheetName).Range("GomperzInsertSetting").Value = "あり") Then
            ' ゴンペルツ曲線とバグ問処解決予測(線形)の交点からの降下線データ
            ' (DLineCrossAtGomperzAndPredicted)
            Cells(1, eColGraph.DLineCrossAtGomperzAndPredicted) = "降下線(Gomperz/バグ改修予測の交点)"

            For i = intEndDayRow + 1 To intEndRow
                dGomperz = Cells(i, eColGraph.GomperzPredictedBug)
                dCalcValue = WorksheetFunction.Forecast_Linear(Cells(i, eColGraph.xDate), _
                                 Range(Cells(intStartDayRow, eColGraph.LeastSquaresActualResolvedBug), _
                                       Cells(intEndDayRow, eColGraph.LeastSquaresActualResolvedBug)), _
                                 Range(Cells(intStartDayRow, eColGraph.xDate), _
                                       Cells(intEndDayRow, eColGraph.xDate)))
                If ((dGomperz - dCalcValue) < 0) Then
                    Cells(i, eColGraph.DLineCrossAtGomperzAndPredicted) = dCalcValue
                    Debug.Print "(MargeTable):Date of DLineCrossAtGomperzAndPredicted = " & _
                                Cells(i, eColGraph.xDate) & vbCrLf & _
                                "             dGomperz = " & dGomperz & " dCalcValue = " & dCalcValue
                    Exit For
               End If
            Next i
        End If

    End If

    ' Gomperz表示ありの場合のみ処理
    If (Sheets(strConstConfigSheetName).Range("GomperzInsertSetting").Value = "あり") Then
        ' ゴンペルツ-5%とゴンペルツ曲線との交点からの降下線用データ
        ' (DLineGomperzPredictedBugMin)
        Sheets(strConstMargeTableSheetName).Select

        Cells(1, eColGraph.DLineGomperzPredictedBugMin) = "降下線(下限値(-5%))"
        intEndRow = GetTargetColumnEndRow(eColGraph.xDate)
        dLowerLimit = Cells(2, eColGraph.GomperzPredictedBugMin)

        For i = 2 To intEndRow
            dGomperz = Cells(i, eColGraph.GomperzPredictedBug)

            ' ゴンペルツ曲線の値がゴンペルツ-5%を超えたポイントを算出
            If ((dLowerLimit - dGomperz) < 0) Then
                Cells(i, eColGraph.DLineGomperzPredictedBugMin) = dLowerLimit
                Debug.Print "(MargeTable):Date of DLineGomperzPredictedBugMin = " & _
                            Cells(i, eColGraph.xDate)
                Exit For
            End If
        Next i

    End If

    ' グラフ作成
    GenerateGraph

End Sub

' グラフ
Private Sub GenerateGraph()
    Dim objHush As Object
    Set objHush = New Scripting.Dictionary
    Dim varObj As Variant
    Dim intTmp As Integer
    Dim intLastRow As Integer
    Dim booGomperz As Boolean
    Dim strInSheetName As String
    Dim strGraphName As String
    Dim intNumOfTestPlannedAttemptsPoint As Integer
    Dim intNumOfTestPlannedPassPoint As Integer
    Dim i, j As Integer
    Dim z As Series

    ' Gomperz表示あり・なし判断
    booGomperz = False
    If (Sheets(strConstConfigSheetName).Range("GomperzInsertSetting").Value = "あり") Then
        ' 潜在バグ数check(EmptyかErrorだったらゴンペルツなし)
        If (IsEmpty(Sheets(strConstGomperzAnalyzeSheetName).Cells(13, 12)) Or _
            IsError(Sheets(strConstGomperzAnalyzeSheetName).Cells(13, 12))) Then
            MsgBox ("ゴンペルツ曲線を生成できませんので、ゴンペルツなしで表示します。")
            booGomperz = False
        Else
            booGomperz = True
        End If
    End If

    strInSheetName = strConstMargeTableSheetName
    strGraphName = strConstPBGraphName

    Sheets(strInSheetName).Select
    intLastRow = GetUsedRangeEndRow

'   これ使うと値が入る
'    ' グラフ表示Range取得
'    objHush("X") = Range(Cells(2, 1), Cells(GetTargetColumnEndRow2(1), 1))
'    ' 試行あり・なし
'    If (Sheets(strConstConfigSheetName).Range("TestDataAttemptsSetting").Value = "あり") Then
'        objHush("TestPlannedAttempts") = Range(Cells(2, 8), Cells(GetTargetColumnEndRow2(8), 8))
'        objHush("TestActualAttempts") = Range(Cells(2, 10), Cells(GetTargetColumnEndRow2(10), 10))
'    End If
'    objHush("TestPlannedPass") = Range(Cells(2, 9), Cells(GetTargetColumnEndRow2(9), 9))
'    objHush("TestPassed") = Range(Cells(2, 11), Cells(GetTargetColumnEndRow2(11), 11))
'    ' Logisticsあり・なし
'    If (Sheets(strConstConfigSheetName).Range("LogisticsInsertSetting").Value = "あり") Then
'        objHush("LogisticsPredictedBug") = Range(Cells(2, 12), Cells(GetTargetColumnEndRow2(12), 12))
'    End If
'    ' ゴンペルツあり・なし
'    If (booGomperz) Then
'        objHush("GomperzPredictedBug") = Range(Cells(2, 3), Cells(GetTargetColumnEndRow2(3), 3))
'        objHush("GomperzPredictedBugN") = Range(Cells(2, 5), Cells(GetTargetColumnEndRow2(5), 5))
'        objHush("GomperzPredictedBugMax") = Range(Cells(2, 6), Cells(GetTargetColumnEndRow2(6), 6))
'        objHush("GomperzPredictedBugMin") = Range(Cells(2, 7), Cells(GetTargetColumnEndRow2(7), 7))
'    End If
'    objHush("JiraActualBug") = Range(Cells(2, 2), Cells(GetTargetColumnEndRow2(2), 2))
'    objHush("JiraActualResolvedBug") = Range(Cells(2, 4), Cells(GetTargetColumnEndRow2(4), 4))
'    ' バグ改修予測あり・なし
'    If (Sheets(strConstConfigSheetName).Range("LeastSquaresInsertSetting").Value = "あり") Then
'        objHush("LeastSquaresActualResolvedBug") = Range(Cells(2, 13), Cells(intLastRow, 13))
'    End If

    ' グラフ表示Range取得
    objHush("X") = Range(Cells(2, eColGraph.xDate), _
                         Cells(GetTargetColumnEndRow2(eColGraph.xDate), _
                               eColGraph.xDate)).Address
    ' TestChartあり
    If (Sheets(strConstConfigSheetName).Range("TestDataFilePath").Value <> "") Then
        ' 試行あり・なし
        If (Sheets(strConstConfigSheetName).Range("TestDataAttemptsSetting").Value = "あり") Then
            objHush("TestPlannedAttempts") = Range(Cells(2, eColGraph.TestPlannedAttempts), _
                                                   Cells(GetTargetColumnEndRow2(eColGraph.TestPlannedAttempts), _
                                                         eColGraph.TestPlannedAttempts)).Address
            objHush("TestActualAttempts") = Range(Cells(2, eColGraph.TestActualAttempts), _
                                                  Cells(GetTargetColumnEndRow2(eColGraph.TestActualAttempts), _
                                                        eColGraph.TestActualAttempts)).Address
        End If
        objHush("TestPlannedPass") = Range(Cells(2, eColGraph.TestPlannedPass), _
                                           Cells(GetTargetColumnEndRow2(eColGraph.TestPlannedPass), _
                                                 eColGraph.TestPlannedPass)).Address
        objHush("TestPassed") = Range(Cells(2, eColGraph.TestPassed), _
                                      Cells(GetTargetColumnEndRow2(eColGraph.TestPassed), _
                                            eColGraph.TestPassed)).Address
        ' Logisticsあり・なし
        If (Sheets(strConstConfigSheetName).Range("LogisticsInsertSetting").Value = "あり") Then
            objHush("LogisticsPredictedBug") = Range(Cells(2, eColGraph.LogisticsPredictedBug), _
                                                     Cells(GetTargetColumnEndRow2(eColGraph.LogisticsPredictedBug), _
                                                           eColGraph.LogisticsPredictedBug)).Address
        End If
    End If
    ' ゴンペルツあり・なし
    If (booGomperz) Then
        objHush("GomperzPredictedBug") = Range(Cells(2, eColGraph.GomperzPredictedBug), _
                                               Cells(GetTargetColumnEndRow2(eColGraph.GomperzPredictedBug), _
                                                     eColGraph.GomperzPredictedBug)).Address
        objHush("GomperzPredictedBugN") = Range(Cells(2, eColGraph.GomperzPredictedBugN), _
                                                Cells(GetTargetColumnEndRow2(eColGraph.GomperzPredictedBugN), _
                                                      eColGraph.GomperzPredictedBugN)).Address
        objHush("GomperzPredictedBugMax") = Range(Cells(2, eColGraph.GomperzPredictedBugMax), _
                                                  Cells(GetTargetColumnEndRow2(eColGraph.GomperzPredictedBugMax), _
                                                        eColGraph.GomperzPredictedBugMax)).Address
        objHush("GomperzPredictedBugMin") = Range(Cells(2, eColGraph.GomperzPredictedBugMin), _
                                                  Cells(GetTargetColumnEndRow2(eColGraph.GomperzPredictedBugMin), _
                                                  eColGraph.GomperzPredictedBugMin)).Address
        objHush("DLineGomperzPredictedBugMin") = Range(Cells(2, eColGraph.DLineGomperzPredictedBugMin), _
                                                       Cells(intLastRow, _
                                                             eColGraph.DLineGomperzPredictedBugMin)).Address
    End If
    objHush("JiraActualBug") = Range(Cells(2, eColGraph.JiraActualBug), _
                                     Cells(GetTargetColumnEndRow2(eColGraph.JiraActualBug), _
                                           eColGraph.JiraActualBug)).Address
    objHush("JiraActualResolvedBug") = Range(Cells(2, eColGraph.JiraActualResolvedBug), _
                                             Cells(GetTargetColumnEndRow2(eColGraph.JiraActualResolvedBug), _
                                             eColGraph.JiraActualResolvedBug)).Address
    ' バグ改修予測あり・なし
    If (Sheets(strConstConfigSheetName).Range("LeastSquaresInsertSetting").Value = "あり") Then
        objHush("LeastSquaresActualResolvedBug") = Range(Cells(2, eColGraph.LeastSquaresActualResolvedBug), _
                                                         Cells(intLastRow, _
                                                               eColGraph.LeastSquaresActualResolvedBug)).Address
        objHush("DLineCurPredictedResolvedBug") = Range(Cells(2, eColGraph.DLineCurPredictedResolvedBug), _
                                                        Cells(intLastRow, _
                                                              eColGraph.DLineCurPredictedResolvedBug)).Address
        ' バグ改修予測あり、ゴンペルツありの場合のみ
        If (booGomperz) Then
            objHush("DLineCrossAtGomperzAndPredicted") = Range(Cells(2, eColGraph.DLineCrossAtGomperzAndPredicted), _
                                                               Cells(intLastRow, _
                                                                     eColGraph.DLineCrossAtGomperzAndPredicted)).Address
        End If
    End If

    ' 開始Pointの取得(Seriesの最初に値を表示する為に取得)
    ' 最後のPointはHush->UBoundで取得
    intNumOfTestPlannedAttemptsPoint = GetTargetColumnStartRow2(eColGraph.TestPlannedAttempts) - 1
    intNumOfTestPlannedPassPoint = GetTargetColumnStartRow2(eColGraph.TestPlannedPass) - 1

    シート上のグラフを全削除 strConstPBGraphSheetName

    Dim chartobj As ChartObject
    Set chartobj = Sheets(strConstPBGraphSheetName).ChartObjects.Add(10, 10, 100, 100)
    chartobj.Activate
    chartobj.Name = strGraphName
    Dim mychart As Chart
    Set mychart = ActiveChart

    With Range(Cells(2, 5), Cells(32, 22))
         mychart.ChartArea.Left = .Left
         mychart.ChartArea.Top = .Top
         mychart.ChartArea.Width = .Width
         mychart.ChartArea.Height = .Height
    End With

    Sheets(strInSheetName).Select

    With mychart
        ' グラフType設定
        .ChartType = xlLineMarkers
        ' Title設定
        .HasTitle = False

        ' 初回は削除して新規作成(Seriesの初期値が変動するため・・・)
        For Each z In .SeriesCollection
          z.Delete
        Next
        For i = 1 To objHush.Count - 1
            .SeriesCollection.NewSeries
        Next

        i = 0
        For Each varObj In objHush
            If (varObj <> "X") Then
                i = i + 1
                .SeriesCollection(i).XValues = Range(objHush("X"))
                .SeriesCollection(i).Values = Range(objHush(varObj))

                ' 1/2軸の設定
                 ' TestChartあり
                If (Sheets(strConstConfigSheetName).Range("TestDataFilePath").Value <> "") Then
                   If (InStr(varObj, "Test") > 0) Then
                        .SeriesCollection(i).AxisGroup = 1
                    Else
                        .SeriesCollection(i).AxisGroup = 2
                    End If
                Else
                    .SeriesCollection(i).AxisGroup = 1
                End If

                ' Seriesの設定
                'intTmp = UBound(objHush(varObj))
                intTmp = GetTargetColumnEndRow2(Range(objHush(varObj)).Column) - 1
                With .SeriesCollection(i)
                    Select Case varObj
                            ' Test Planned Attempts
                            Case "TestPlannedAttempts"
                                .Format.Line.ForeColor.RGB = RGB(150, 150, 150)
                                .Format.Line.DashStyle = msoLineSysDash
                                .Border.Weight = xlMedium
                                .MarkerStyle = xlNone
                                .Smooth = False
                                .MarkerSize = 7
                                .Shadow = False
                                intTmp = intNumOfTestPlannedPassPoint
                                .Points(intTmp).ApplyDataLabels
                                .Points(intTmp).DataLabel.NumberFormat = "#,##0_ "
                                ' プロット位置=左
                                .Points(intTmp).DataLabel.Position = xlLabelPositionLeft
                                ' 英語
                                If (Sheets(strConstConfigSheetName).Range("LanguageSetting").Value = "英語") Then
                                    .Name = "Test Planned Attempts"
                                ' 日本語
                                Else
                                    .Name = "テスト試行予定"
                                End If

                            ' Test Actual Attempts
                            Case "TestActualAttempts"
                                .Format.Line.ForeColor.RGB = RGB(77, 77, 77)
                                .Format.Fill.ForeColor.RGB = RGB(77, 77, 77)
                                .Format.Line.DashStyle = msoLineSolid
                                .Border.Weight = xlThick
                                .MarkerStyle = xlNone
                                .Smooth = False
                                .MarkerSize = 7
                                .Shadow = False
                                ' 総項目数をプロット
                                .Points(intTmp).ApplyDataLabels
                                .Points(intTmp).DataLabel.NumberFormat = "#,##0_ "
                                ' 英語
                                If (Sheets(strConstConfigSheetName).Range("LanguageSetting").Value = "英語") Then
                                    .Name = "Test Actual Attempts"
                                ' 日本語
                                Else
                                    .Name = "テスト試行実績"
                                End If

                            ' TestChart - Test Planned Pass
                            Case "TestPlannedPass"
                                .Format.Line.ForeColor.RGB = RGB(0, 102, 255)
                                .Format.Line.DashStyle = msoLineSysDash
                                .Border.Weight = xlMedium
                                .MarkerStyle = xlNone
                                .Smooth = False
                                .MarkerSize = 7
                                .Shadow = False
                                intTmp = intNumOfTestPlannedPassPoint
                                .Points(intTmp).ApplyDataLabels
                                .Points(intTmp).DataLabel.NumberFormat = "#,##0_ "
                                ' プロット位置=左
                                .Points(intTmp).DataLabel.Position = xlLabelPositionLeft
                                ' 英語
                                If (Sheets(strConstConfigSheetName).Range("LanguageSetting").Value = "英語") Then
                                    .Name = "Test Planned Pass"
                                ' 日本語
                                Else
                                    .Name = "テストパス予定"
                                End If

                            ' TestChart - Test Passed
                            Case "TestPassed"
                                .Format.Line.ForeColor.RGB = RGB(0, 0, 255)
                                .Format.Fill.ForeColor.RGB = RGB(0, 0, 255)
                                .Format.Line.DashStyle = msoLineSolid
                                .Border.Weight = xlThick
                                .MarkerStyle = xlNone
                                .Smooth = False
                                .MarkerSize = 7
                                .Shadow = False
                                ' 総項目数をプロット
                                .Points(intTmp).ApplyDataLabels
                                .Points(intTmp).DataLabel.NumberFormat = "#,##0_ "
                                ' プロット位置=左
                                '.Points(intTmp).DataLabel.Position = xlLabelPositionLeft
                                ' 英語
                                If (Sheets(strConstConfigSheetName).Range("LanguageSetting").Value = "英語") Then
                                    .Name = "Test Passed"
                                ' 日本語
                                Else
                                    .Name = "テストパス実績"
                                End If

                            ' Logistics曲線
                            Case "LogisticsPredictedBug"
                                .Format.Line.ForeColor.RGB = RGB(128, 0, 128)
                                .Format.Line.DashStyle = msoLineSysDash
                                .Border.Weight = xlMedium
                                .MarkerStyle = xlNone
                                .Smooth = False
                                .MarkerSize = 7
                                .Shadow = False
                                ' 総項目数をプロット
                                .Points(intTmp).ApplyDataLabels
                                .Points(intTmp).DataLabel.NumberFormat = "#,##0_ "
                                ' プロット位置=左
                                '.Points(intTmp).DataLabel.Position = xlLabelPositionLeft
                                ' 英語
                                If (Sheets(strConstConfigSheetName).Range("LanguageSetting").Value = "英語") Then
                                    .Name = "Estimate Bugs of Planned(Logistics)"
                                ' 日本語
                                Else
                                    .Name = "バグ検出予測(Logistics)"
                                End If

                            ' Gomperz曲線
                            Case "GomperzPredictedBug"
                                .Format.Line.ForeColor.RGB = RGB(255, 0, 0)
                                .Format.Line.DashStyle = msoLineSysDash
                                .Border.Weight = xlMedium
                                .MarkerStyle = xlNone
                                .Smooth = False
                                .MarkerSize = 7
                                .Shadow = False
                                .Points(intTmp).ApplyDataLabels
                                .Points(intTmp).DataLabel.NumberFormat = "#,##0_ "
                                ' 英語
                                If (Sheets(strConstConfigSheetName).Range("LanguageSetting").Value = "英語") Then
                                    .Name = "Estimate Bugs of Learning(Gomperz)"
                                ' 日本語
                                Else
                                    .Name = "バグ習熟予測(Gomperz)"
                                End If

                            ' Gomperz潜在バグ数
                            Case "GomperzPredictedBugN"
                                .Format.Line.ForeColor.RGB = RGB(0, 128, 0)
                                .Format.Line.DashStyle = msoLineSolid
                                .Border.Weight = xlThick
                                .MarkerStyle = xlNone
                                .Smooth = False
                                .MarkerSize = 7
                                .Shadow = False
                                .Points(intTmp).ApplyDataLabels
                                .Points(intTmp).DataLabel.NumberFormat = "#,##0_ "
                                ' 英語
                                If (Sheets(strConstConfigSheetName).Range("LanguageSetting").Value = "英語") Then
                                    .Name = "Latent Bugs(Gomperz)"
                                ' 日本語
                                Else
                                    .Name = "潜在バグ数(Gomperz)"
                                End If

                            ' Gomperz +5%
                            Case "GomperzPredictedBugMax"
                                .Format.Line.ForeColor.RGB = RGB(0, 128, 0)
                                .Format.Line.DashStyle = msoLineDashDot
                                .Border.Weight = xlMedium
                                .MarkerStyle = xlNone
                                .Smooth = False
                                .MarkerSize = 7
                                .Shadow = False
                                .Points(intTmp).ApplyDataLabels
                                .Points(intTmp).DataLabel.NumberFormat = "#,##0_ "
                                ' 英語
                                If (Sheets(strConstConfigSheetName).Range("LanguageSetting").Value = "英語") Then
                                    .Name = "Latent Bugs(Gomperz)+5%"
                                ' 日本語
                                Else
                                .Name = "潜在バグ数(Gomperz)+5%"
                                End If

                            ' Gomperz -5%
                            Case "GomperzPredictedBugMin"
                                .Format.Line.ForeColor.RGB = RGB(0, 128, 0)
                                .Format.Line.DashStyle = msoLineDashDot
                                .Border.Weight = xlMedium
                                .MarkerStyle = xlNone
                                .Smooth = False
                                .MarkerSize = 7
                                .Shadow = False
                                .Points(intTmp).ApplyDataLabels
                                .Points(intTmp).DataLabel.NumberFormat = "#,##0_ "
                                ' 英語
                                If (Sheets(strConstConfigSheetName).Range("LanguageSetting").Value = "英語") Then
                                    .Name = "Latent Bugs(Gomperz)-5%"
                                ' 日本語
                                Else
                                    .Name = "潜在バグ数(Gomperz)-5%"
                                End If

                            ' Resolved Bugs
                            Case "JiraActualResolvedBug"
                                .Format.Line.ForeColor.RGB = RGB(255, 102, 0)
                                .Format.Line.DashStyle = msoLineSolid
                                .Border.Weight = xlThick
                                '.MarkerStyle = xlSquare
                                .MarkerStyle = xlNone
                                .Smooth = False
                                .MarkerSize = 7
                                .Shadow = False
                                .Points(intTmp).ApplyDataLabels
                                .Points(intTmp).DataLabel.NumberFormat = "#,##0_ "
                                ' 英語
                                If (Sheets(strConstConfigSheetName).Range("LanguageSetting").Value = "英語") Then
                                    .Name = "Resolved Bugs"
                                ' 日本語
                                Else
                                    .Name = "バグ問処解決数"
                                End If

                            ' Bugs
                            Case "JiraActualBug"
                                .Format.Line.ForeColor.RGB = RGB(255, 0, 0)
                                .Format.Fill.ForeColor.RGB = RGB(255, 0, 0)
                                .Format.Line.DashStyle = msoLineSolid
                                .Border.Weight = xlThick
                                '.MarkerStyle = xlSquare
                                .MarkerStyle = xlNone
                                .Smooth = False
                                .MarkerSize = 7
                                .Shadow = False
                                .Points(intTmp).ApplyDataLabels
                                .Points(intTmp).DataLabel.NumberFormat = "#,##0_ "
                                ' 英語
                                If (Sheets(strConstConfigSheetName).Range("LanguageSetting").Value = "英語") Then
                                    .Name = "Bugs"
                                ' 日本語
                                Else
                                    .Name = "バグ問処発行数"
                                End If

                            ' バグ改修予測
                            Case "LeastSquaresActualResolvedBug"
                                ' 改修予測算出範囲
                                .MarkerStyle = xlNone 'Markerを消す
                                .Format.Line.Visible = msoFalse '線を消す
                                
                                ' バグ問処解決予測(線形)
                                .Trendlines.Add
                                With .Trendlines(1)
                                    ' 英語
                                    If (Sheets(strConstConfigSheetName).Range("LanguageSetting").Value = "英語") Then
                                        .Name = "Estimate Resolved Bugs(Linear)"
                                    Else
                                        .Name = "バグ問処解決数予測(線形)"
                                    End If
                                    .Format.Line.ForeColor.RGB = RGB(255, 102, 0)
                                    .Format.Line.DashStyle = msoLineSysDash
                                    .Border.Weight = xlMedium
                                End With
                                ' 英語
                                If (Sheets(strConstConfigSheetName).Range("LanguageSetting").Value = "英語") Then
                                    .Name = "calculation range of Estimate resolved bug"
                                ' 日本語
                                Else
                                    .Name = "改修予測算出範囲"
                                End If

                                 ' 凡例-改修予測算出範囲の削除
                                 mychart.Legend.LegendEntries(i).Delete

                            ' ゴンペルツ-5%とゴンペルツ曲線との交点からの降下線
                            Case "DLineGomperzPredictedBugMin"
                                ' グラフのタイプを集合縦棒に変更
                                .ChartType = xlColumnClustered
                                ' 線の色等を設定
                                .Format.Line.ForeColor.RGB = RGB(255, 0, 255)
                                .Format.Fill.ForeColor.RGB = RGB(255, 0, 255)
                                .Format.Line.DashStyle = msoLineSysDot
                                .Border.Weight = xlMedium
                                .MarkerStyle = xlNone
                                If (intTmp > 0) Then
                                    .Points(intTmp).ApplyDataLabels
                                    .Points(intTmp).DataLabel.NumberFormat = "#,##0_ "
                                End If

                                ' 英語
                                If (Sheets(strConstConfigSheetName).Range("LanguageSetting").Value = "英語") Then
                                    .Name = "Estimated date of Gompertz-5% arrival"
                                ' 日本語
                                Else
                                    .Name = "ゴンペルツ-5%の到達日"
                                End If

                            ' 現時点のバグ数に到達する、バグ問処解決予測(線形)の値からの降下線
                            Case "DLineCurPredictedResolvedBug"
                                ' グラフのタイプを集合縦棒に変更
                                .ChartType = xlColumnClustered
                                ' 線の色等を設定
                                .Format.Line.ForeColor.RGB = RGB(0, 255, 0)
                                .Format.Fill.ForeColor.RGB = RGB(0, 255, 0)
                                .Format.Line.DashStyle = msoLineSysDot
                                .Border.Weight = xlMedium
                                .MarkerStyle = xlNone
                                If (intTmp > 0) Then
                                    .Points(intTmp).ApplyDataLabels
                                    .Points(intTmp).DataLabel.NumberFormat = "#,##0_ "
                                End If

                                ' 英語
                                If (Sheets(strConstConfigSheetName).Range("LanguageSetting").Value = "英語") Then
                                    .Name = "Estimated date of current bugs resolution"
                                ' 日本語
                                Else
                                    .Name = "現時点のバグ問処解決予測日"
                                End If
                            ' ゴンペルツ曲線とバグ問処解決予測(線形)の交点からの降下線
                            Case "DLineCrossAtGomperzAndPredicted"
                                ' グラフのタイプを集合縦棒に変更
                                .ChartType = xlColumnClustered
                                ' 線の色等を設定
                                .Format.Line.ForeColor.RGB = RGB(51, 102, 255)
                                .Format.Fill.ForeColor.RGB = RGB(51, 102, 255)
                                .Format.Line.DashStyle = msoLineSysDot
                                .Border.Weight = xlMedium
                                .MarkerStyle = xlNone
                                If (intTmp > 0) Then
                                    .Points(intTmp).ApplyDataLabels
                                    .Points(intTmp).DataLabel.NumberFormat = "#,##0_ "
                                End If

                                ' 英語
                                If (Sheets(strConstConfigSheetName).Range("LanguageSetting").Value = "英語") Then
                                    .Name = "Estimated date of Gompertz prediction bug resolution"
                                ' 日本語
                                Else
                                    .Name = "ゴンペルツ曲線による予測バグの解決予測日"
                                End If

                            ' System Error
                            Case Else
                                MsgBox ("System Error!")

                    End Select
                End With
            End If
        Next

         ' プロットエリアの背景色
        .PlotArea.Format.Fill.ForeColor.RGB = RGB(255, 255, 204)

        ' 軸の設定
        ' 英語
        If (Sheets(strConstConfigSheetName).Range("LanguageSetting").Value = "英語") Then
            ' TestChartあり
            If (Sheets(strConstConfigSheetName).Range("TestDataFilePath").Value <> "") Then
                .Axes(xlCategory, xlPrimary).HasTitle = False
                .Axes(xlCategory, xlPrimary).TickLabels.NumberFormatLocal = "m/d"
                .Axes(xlValue, xlPrimary).HasTitle = True
                .Axes(xlValue, xlPrimary).AxisTitle.Characters.text = "Tests"
                .Axes(xlValue, xlPrimary).AxisTitle.Format.TextFrame2.TextRange.Font.Size = 12
                .Axes(xlValue, xlPrimary).TickLabels.NumberFormatLocal = "G/標準"
                .Axes(xlValue, xlSecondary).HasTitle = True
                .Axes(xlValue, xlSecondary).AxisTitle.Characters.text = "Bugs"
                .Axes(xlValue, xlSecondary).AxisTitle.Format.TextFrame2.TextRange.Font.Size = 12
                .Axes(xlValue, xlSecondary).TickLabels.NumberFormatLocal = "G/標準"
                ' 最少値=0
                .Axes(xlValue, xlSecondary).MinimumScale = 0

            ' TestChartなし
            Else
                .Axes(xlCategory, xlPrimary).HasTitle = False
                .Axes(xlCategory, xlPrimary).TickLabels.NumberFormatLocal = "m/d"
                .Axes(xlValue, xlPrimary).HasTitle = True
                .Axes(xlValue, xlPrimary).AxisTitle.Characters.text = "Bugs"
                .Axes(xlValue, xlPrimary).AxisTitle.Format.TextFrame2.TextRange.Font.Size = 12
                .Axes(xlValue, xlPrimary).TickLabels.NumberFormatLocal = "G/標準"
                ' 最少値=0
                .Axes(xlValue, xlPrimary).MinimumScale = 0
            End If

        ' 日本語
        Else
            ' TestChartあり
            If (Sheets(strConstConfigSheetName).Range("TestDataFilePath").Value <> "") Then
                .Axes(xlCategory, xlPrimary).HasTitle = False
                .Axes(xlCategory, xlPrimary).TickLabels.NumberFormatLocal = "m/d"
                .Axes(xlValue, xlPrimary).HasTitle = True
                .Axes(xlValue, xlPrimary).AxisTitle.Characters.text = "残項目数"
                .Axes(xlValue, xlPrimary).AxisTitle.Orientation = xlVertical
                .Axes(xlValue, xlPrimary).TickLabels.NumberFormatLocal = "G/標準"
                .Axes(xlValue, xlSecondary).HasTitle = True
                .Axes(xlValue, xlSecondary).AxisTitle.Characters.text = "バグ検出数"
                .Axes(xlValue, xlSecondary).AxisTitle.Orientation = xlVertical
                .Axes(xlValue, xlSecondary).TickLabels.NumberFormatLocal = "G/標準"
                ' 最少値=0
                .Axes(xlValue, xlSecondary).MinimumScale = 0

            ' TestChartなし
            Else
                .Axes(xlCategory, xlPrimary).HasTitle = False
                .Axes(xlCategory, xlPrimary).TickLabels.NumberFormatLocal = "m/d"
                .Axes(xlValue, xlPrimary).HasTitle = True
                .Axes(xlValue, xlPrimary).AxisTitle.Characters.text = "バグ検出数"
                .Axes(xlValue, xlPrimary).AxisTitle.Orientation = xlVertical
                .Axes(xlValue, xlPrimary).TickLabels.NumberFormatLocal = "G/標準"
                ' 最少値=0
                .Axes(xlValue, xlPrimary).MinimumScale = 0
            End If
        End If
    End With

End Sub

' グラフ
Private Sub GenerateGraph_old()
    Dim booGomperz As Boolean
    Dim strInSheetName As String
    Dim strGraphName As String
    Dim intLastRow As Integer
    Dim XRange As Range
    Dim GomperzActualBugRange As Range
    Dim GomperzPredictedBugRange As Range
    Dim GomperzActualResolvedBugRange As Range
    Dim GomperzPredictedBugNRange As Range
    Dim GomperzPredictedBugMaxRange As Range
    Dim GomperzPredictedBugMinRange As Range
    Dim TestPlannedPassRange As Range
    Dim TestPassedRange As Range
    Dim TestPredictedBugRange As Range
    Dim LeastSquaresResolvedBugRange As Range
    Dim intNumOfGomperzActualBugPoint As Integer
    Dim intNumOfGomperzPredictedBugPoint As Integer
    Dim intNumOfGomperzActualResolvedBugPoint As Integer
    Dim intNumOfGomperzPredictedBugNPoint As Integer
    Dim intNumOfGomperzPredictedBugMaxPoint As Integer
    Dim intNumOfGomperzPredictedBugMinPoint As Integer
    Dim intNumOfTestPlannedPassPoint As Integer
    Dim intNumOfTestPassedPoint As Integer
    Dim intNumOfTestPredictedBugPoint As Integer
    Dim i, j As Integer
    Dim z As Series

    ' Gomperz表示あり・なし判断
    booGomperz = False
    If (Sheets(strConstConfigSheetName).Range("GomperzInsertSetting").Value = "あり") Then
        ' 潜在バグ数check(EmptyかErrorだったらゴンペルツなし)
        If (IsEmpty(Sheets(strConstGomperzAnalyzeSheetName).Cells(13, 12)) Or IsError(Sheets(strConstGomperzAnalyzeSheetName).Cells(13, 12))) Then
            MsgBox ("ゴンペルツ曲線を生成できませんので、ゴンペルツなしで表示します。")
            booGomperz = False
        Else
            booGomperz = True
        End If
    End If

    strInSheetName = strConstMargeTableSheetName
    strGraphName = strConstPBGraphName

    Sheets(strInSheetName).Select
    intLastRow = GetUsedRangeEndRow

    Set XRange = Range(Cells(2, 1), Cells(intLastRow, 1))
    Set GomperzActualBugRange = Range(Cells(2, 2), Cells(intLastRow, 2))
    Set GomperzPredictedBugRange = Range(Cells(2, 3), Cells(intLastRow, 3))
    Set GomperzActualResolvedBugRange = Range(Cells(2, 4), Cells(intLastRow, 4))
    Set GomperzPredictedBugNRange = Range(Cells(2, 5), Cells(intLastRow, 5))
    Set GomperzPredictedBugMaxRange = Range(Cells(2, 6), Cells(intLastRow, 6))
    Set GomperzPredictedBugMinRange = Range(Cells(2, 7), Cells(intLastRow, 7))
    Set TestPlannedPassRange = Range(Cells(2, 8), Cells(intLastRow, 8))
    Set TestPassedRange = Range(Cells(2, 9), Cells(intLastRow, 9))
    Set TestPredictedBugRange = Range(Cells(2, 10), Cells(intLastRow, 10))

    intNumOfGomperzActualBugPoint = GetTargetColumnEndRow2(2) - 1
    intNumOfGomperzPredictedBugPoint = GetTargetColumnEndRow2(3) - 1
    intNumOfGomperzActualResolvedBugPoint = GetTargetColumnEndRow2(4) - 1
    intNumOfGomperzPredictedBugNPoint = GetTargetColumnEndRow2(5) - 1
    intNumOfGomperzPredictedBugMaxPoint = GetTargetColumnEndRow2(6) - 1
    intNumOfGomperzPredictedBugMinPoint = GetTargetColumnEndRow2(7) - 1
    intNumOfTestPlannedPassPoint = GetTargetColumnStartRow2(8) - 1
    intNumOfTestPassedPoint = GetTargetColumnEndRow2(9) - 1
    intNumOfTestPredictedBugPoint = GetTargetColumnEndRow2(10) - 1

    ' バグ改修予測(線形)用
    If (Sheets(strConstConfigSheetName).Range("LeastSquaresInsertSetting").Value = "あり") Then
        Set LeastSquaresResolvedBugRange = Range(Cells(2, 11), Cells(intLastRow, 11))
    End If

    シート上のグラフを全削除 strConstPBGraphSheetName

    Dim chartobj As ChartObject
    Set chartobj = Sheets(strConstPBGraphSheetName).ChartObjects.Add(10, 10, 100, 100)
    chartobj.Activate
    chartobj.Name = strGraphName
    Dim mychart As Chart
    Set mychart = ActiveChart

    With Range(Cells(2, 5), Cells(32, 22))
         mychart.ChartArea.Left = .Left
         mychart.ChartArea.Top = .Top
         mychart.ChartArea.Width = .Width
         mychart.ChartArea.Height = .Height
    End With

    With mychart
        ' グラフType設定
        .ChartType = xlLineMarkers
        ' Title設定
        .HasTitle = False
        
        ' 初回は削除して新規作成(Seriesの初期値が変動するため・・・)
        For Each z In .SeriesCollection
          z.Delete
        Next

        ' Gomperzあり
        If (booGomperz) Then
            ' バグ改修予測(線形)あり
            If (Sheets(strConstConfigSheetName).Range("LeastSquaresInsertSetting").Value = "あり") Then
                For i = 1 To 10
                    .SeriesCollection.NewSeries
                Next
                .SeriesCollection(1).XValues = XRange
                .SeriesCollection(1).Values = TestPlannedPassRange
                .SeriesCollection(2).XValues = XRange
                .SeriesCollection(2).Values = TestPassedRange
                .SeriesCollection(3).XValues = XRange
                .SeriesCollection(3).Values = GomperzActualBugRange
                .SeriesCollection(4).XValues = XRange
                .SeriesCollection(4).Values = TestPredictedBugRange
                .SeriesCollection(5).XValues = XRange
                .SeriesCollection(5).Values = GomperzPredictedBugRange
                .SeriesCollection(6).XValues = XRange
                .SeriesCollection(6).Values = GomperzPredictedBugNRange
                .SeriesCollection(7).XValues = XRange
                .SeriesCollection(7).Values = GomperzPredictedBugMaxRange
                .SeriesCollection(8).XValues = XRange
                .SeriesCollection(8).Values = GomperzPredictedBugMinRange
                .SeriesCollection(9).XValues = XRange
                .SeriesCollection(9).Values = GomperzActualResolvedBugRange
                .SeriesCollection(10).XValues = XRange
                .SeriesCollection(10).Values = LeastSquaresResolvedBugRange

                .SeriesCollection(1).AxisGroup = 1
                .SeriesCollection(2).AxisGroup = 1
                .SeriesCollection(3).AxisGroup = 2
                .SeriesCollection(4).AxisGroup = 2
                .SeriesCollection(5).AxisGroup = 2
                .SeriesCollection(6).AxisGroup = 2
                .SeriesCollection(7).AxisGroup = 2
                .SeriesCollection(8).AxisGroup = 2
                .SeriesCollection(9).AxisGroup = 2
                .SeriesCollection(10).AxisGroup = 2

                ' 英語
                If (Sheets(strConstConfigSheetName).Range("LanguageSetting").Value = "英語") Then
                    .SeriesCollection(1).Name = "Test Planned Pass" ' "TestPlannedPass"
                    .SeriesCollection(2).Name = "Test Passed" ' "TestPassed"
                    .SeriesCollection(3).Name = "Bugs"
                    .SeriesCollection(4).Name = "Estimate Bugs of Planned(Logistics)"
                    .SeriesCollection(5).Name = "Estimate Bugs of Learning(Gomperz)"
                    .SeriesCollection(6).Name = "Latent Bugs(Gomperz)"
                    .SeriesCollection(7).Name = "Latent Bugs(Gomperz)+5%"
                    .SeriesCollection(8).Name = "Latent Bugs(Gomperz)-5%"
                    .SeriesCollection(9).Name = "Resolved Bugs"
                    .SeriesCollection(10).Name = "calculation range of Estimate resolved bug"

                ' 日本語
                Else
                    .SeriesCollection(1).Name = "テスト項目消化予測" ' "TestPlannedPass"
                    .SeriesCollection(2).Name = "テスト項目消化実績" ' "TestPassed"
                    .SeriesCollection(3).Name = "バグ問処発行数"
                    .SeriesCollection(4).Name = "バグ検出予測(Logistics)"
                    .SeriesCollection(5).Name = "バグ習熟予測(Gomperz)"
                    .SeriesCollection(6).Name = "潜在バグ数(Gomperz)"
                    .SeriesCollection(7).Name = "潜在バグ数(Gomperz)+5%"
                    .SeriesCollection(8).Name = "潜在バグ数(Gomperz)-5%"
                    .SeriesCollection(9).Name = "バグ問処解決数"
                    .SeriesCollection(10).Name = "改修予測算出範囲"

                End If

                ' 凡例-改修予測算出範囲の削除
                .Legend.LegendEntries(10).Delete

            ' バグ改修予測(線形)なし
            Else
                For i = 1 To 9
                    .SeriesCollection.NewSeries
                Next
                .SeriesCollection(1).XValues = XRange
                .SeriesCollection(1).Values = TestPlannedPassRange
                .SeriesCollection(2).XValues = XRange
                .SeriesCollection(2).Values = TestPassedRange
                .SeriesCollection(3).XValues = XRange
                .SeriesCollection(3).Values = GomperzActualBugRange
                .SeriesCollection(4).XValues = XRange
                .SeriesCollection(4).Values = TestPredictedBugRange
                .SeriesCollection(5).XValues = XRange
                .SeriesCollection(5).Values = GomperzPredictedBugRange
                .SeriesCollection(6).XValues = XRange
                .SeriesCollection(6).Values = GomperzPredictedBugNRange
                .SeriesCollection(7).XValues = XRange
                .SeriesCollection(7).Values = GomperzPredictedBugMaxRange
                .SeriesCollection(8).XValues = XRange
                .SeriesCollection(8).Values = GomperzPredictedBugMinRange
                .SeriesCollection(9).XValues = XRange
                .SeriesCollection(9).Values = GomperzActualResolvedBugRange

                .SeriesCollection(1).AxisGroup = 1
                .SeriesCollection(2).AxisGroup = 1
                .SeriesCollection(3).AxisGroup = 2
                .SeriesCollection(4).AxisGroup = 2
                .SeriesCollection(5).AxisGroup = 2
                .SeriesCollection(6).AxisGroup = 2
                .SeriesCollection(7).AxisGroup = 2
                .SeriesCollection(8).AxisGroup = 2
                .SeriesCollection(9).AxisGroup = 2

                ' 英語
                If (Sheets(strConstConfigSheetName).Range("LanguageSetting").Value = "英語") Then

                    .SeriesCollection(1).Name = "Test Planned Pass" ' "TestPlannedPass"
                    .SeriesCollection(2).Name = "Test Passed" ' "TestPassed"
                    .SeriesCollection(3).Name = "Bugs"
                    .SeriesCollection(4).Name = "Estimate Bugs of Planned(Logistics)"
                    .SeriesCollection(5).Name = "Estimate Bugs of Learning(Gomperz)"
                    .SeriesCollection(6).Name = "Latent Bugs(Gomperz)"
                    .SeriesCollection(7).Name = "Latent Bugs(Gomperz)+5%"
                    .SeriesCollection(8).Name = "Latent Bugs(Gomperz)-5%"
                    .SeriesCollection(9).Name = "Resolved Bugs"

                ' 日本語
                Else
                    .SeriesCollection(1).Name = "テスト項目消化予測" ' "TestPlannedPass"
                    .SeriesCollection(2).Name = "テスト項目消化実績" ' "TestPassed"
                    .SeriesCollection(3).Name = "バグ問処発行数"
                    .SeriesCollection(4).Name = "バグ検出予測(Logistics)"
                    .SeriesCollection(5).Name = "バグ習熟予測(Gomperz)"
                    .SeriesCollection(6).Name = "潜在バグ数(Gomperz)"
                    .SeriesCollection(7).Name = "潜在バグ数(Gomperz)+5%"
                    .SeriesCollection(8).Name = "潜在バグ数(Gomperz)-5%"
                    .SeriesCollection(9).Name = "バグ問処解決数"

                End If

            End If

        ' Gomperzなし
        Else
            ' バグ改修予測(線形)あり
            If (Sheets(strConstConfigSheetName).Range("LeastSquaresInsertSetting").Value = "あり") Then
                For i = 1 To 6
                    .SeriesCollection.NewSeries
                Next
                .SeriesCollection(1).XValues = XRange
                .SeriesCollection(1).Values = TestPlannedPassRange
                .SeriesCollection(2).XValues = XRange
                .SeriesCollection(2).Values = TestPassedRange
                .SeriesCollection(3).XValues = XRange
                .SeriesCollection(3).Values = GomperzActualBugRange
                .SeriesCollection(4).XValues = XRange
                .SeriesCollection(4).Values = TestPredictedBugRange
                .SeriesCollection(5).XValues = XRange
                .SeriesCollection(5).Values = GomperzActualResolvedBugRange
                .SeriesCollection(6).XValues = XRange
                .SeriesCollection(6).Values = LeastSquaresResolvedBugRange

                .SeriesCollection(1).AxisGroup = 1
                .SeriesCollection(2).AxisGroup = 1
                .SeriesCollection(3).AxisGroup = 2
                .SeriesCollection(4).AxisGroup = 2
                .SeriesCollection(5).AxisGroup = 2
                .SeriesCollection(6).AxisGroup = 2

                ' 英語
                If (Sheets(strConstConfigSheetName).Range("LanguageSetting").Value = "英語") Then
                    .SeriesCollection(1).Name = "Test Planned Pass" ' "TestPlannedPass"
                    .SeriesCollection(2).Name = "Test Passed" ' "TestPassed"
                    .SeriesCollection(3).Name = "Bugs"
                    .SeriesCollection(4).Name = "Estimate Bugs of Planned(Logistics)"
                    .SeriesCollection(5).Name = "Resolved Bugs"
                    .SeriesCollection(6).Name = "calculation range of Estimate resolved bug"

                ' 日本語
                Else
                    .SeriesCollection(1).Name = "テスト項目消化予測" ' "TestPlannedPass"
                    .SeriesCollection(2).Name = "テスト項目消化実績" ' "TestPassed"
                    .SeriesCollection(3).Name = "バグ問処発行数"
                    .SeriesCollection(4).Name = "バグ検出予測(Logistics)"
                    .SeriesCollection(5).Name = "バグ問処解決数"
                    .SeriesCollection(6).Name = "改修予測算出範囲"
                End If

                ' 凡例-改修予測算出範囲の削除
                .Legend.LegendEntries(6).Delete

            ' バグ改修予測(線形)なし
            Else
                For i = 1 To 5
                    .SeriesCollection.NewSeries
                Next
                .SeriesCollection(1).XValues = XRange
                .SeriesCollection(1).Values = TestPlannedPassRange
                .SeriesCollection(2).XValues = XRange
                .SeriesCollection(2).Values = TestPassedRange
                .SeriesCollection(3).XValues = XRange
                .SeriesCollection(3).Values = GomperzActualBugRange
                .SeriesCollection(4).XValues = XRange
                .SeriesCollection(4).Values = TestPredictedBugRange
                .SeriesCollection(5).XValues = XRange
                .SeriesCollection(5).Values = GomperzActualResolvedBugRange

                .SeriesCollection(1).AxisGroup = 1
                .SeriesCollection(2).AxisGroup = 1
                .SeriesCollection(3).AxisGroup = 2
                .SeriesCollection(4).AxisGroup = 2
                .SeriesCollection(5).AxisGroup = 2

                ' 英語
                If (Sheets(strConstConfigSheetName).Range("LanguageSetting").Value = "英語") Then
                    .SeriesCollection(1).Name = "Test Planned Pass" ' "TestPlannedPass"
                    .SeriesCollection(2).Name = "Test Passed" ' "TestPassed"
                    .SeriesCollection(3).Name = "Bugs"
                    .SeriesCollection(4).Name = "Estimate Bugs of Planned(Logistics)"
                    .SeriesCollection(5).Name = "Resolved Bugs"

                ' 日本語
                Else
                    .SeriesCollection(1).Name = "テスト項目消化予測" ' "TestPlannedPass"
                    .SeriesCollection(2).Name = "テスト項目消化実績" ' "TestPassed"
                    .SeriesCollection(3).Name = "バグ問処発行数"
                    .SeriesCollection(4).Name = "バグ検出予測(Logistics)"
                    .SeriesCollection(5).Name = "バグ問処解決数"
                End If

            End If

        End If

        ' 軸の設定
        ' 英語
        If (Sheets(strConstConfigSheetName).Range("LanguageSetting").Value = "英語") Then
            .Axes(xlCategory, xlPrimary).HasTitle = False
            .Axes(xlCategory, xlPrimary).TickLabels.NumberFormatLocal = "m/d"
            .Axes(xlValue, xlPrimary).HasTitle = True
            .Axes(xlValue, xlPrimary).AxisTitle.Characters.text = "Tests"
            .Axes(xlValue, xlPrimary).AxisTitle.Format.TextFrame2.TextRange.Font.Size = 12
            .Axes(xlValue, xlPrimary).TickLabels.NumberFormatLocal = "G/標準"
            .Axes(xlValue, xlSecondary).HasTitle = True
            .Axes(xlValue, xlSecondary).AxisTitle.Characters.text = "Bugs"
            .Axes(xlValue, xlSecondary).AxisTitle.Format.TextFrame2.TextRange.Font.Size = 12
            .Axes(xlValue, xlSecondary).TickLabels.NumberFormatLocal = "G/標準"
            ' 最少値=0
            .Axes(xlValue, xlSecondary).MinimumScale = 0

        ' 日本語
        Else
            .Axes(xlCategory, xlPrimary).HasTitle = False
            .Axes(xlCategory, xlPrimary).TickLabels.NumberFormatLocal = "m/d"
            .Axes(xlValue, xlPrimary).HasTitle = True
            .Axes(xlValue, xlPrimary).AxisTitle.Characters.text = "残項目数"
            .Axes(xlValue, xlPrimary).AxisTitle.Orientation = xlVertical
            .Axes(xlValue, xlPrimary).TickLabels.NumberFormatLocal = "G/標準"
            .Axes(xlValue, xlSecondary).HasTitle = True
            .Axes(xlValue, xlSecondary).AxisTitle.Characters.text = "バグ検出数"
            .Axes(xlValue, xlSecondary).AxisTitle.Orientation = xlVertical
            .Axes(xlValue, xlSecondary).TickLabels.NumberFormatLocal = "G/標準"
            ' 最少値=0
            .Axes(xlValue, xlSecondary).MinimumScale = 0

        End If
        
        For i = 1 To .SeriesCollection.Count
            If (.SeriesCollection(i).Name = "Bugs") Or (.SeriesCollection(i).Name = "バグ問処発行数") Then
                ' Gomperz実績バグの線に対する設定
                With .SeriesCollection(i)
                    .Format.Line.ForeColor.RGB = RGB(255, 0, 0)
                    .Format.Fill.ForeColor.RGB = RGB(255, 0, 0)
                    .Format.Line.DashStyle = msoLineSolid
                    .Border.Weight = xlThick
                    '.MarkerStyle = xlSquare
                    .MarkerStyle = xlNone
                    .Smooth = False
                    .MarkerSize = 7
                    .Shadow = False
                    .Points(intNumOfGomperzActualBugPoint).ApplyDataLabels
                    .Points(intNumOfGomperzActualBugPoint).DataLabel.NumberFormat = "#,##0_ "
                End With

            ElseIf (.SeriesCollection(i).Name = "Resolved Bugs") Or (.SeriesCollection(i).Name = "バグ問処解決数") Then
                ' Gomperz解決済バグの線に対する設定
                With .SeriesCollection(i)
                    .Format.Line.ForeColor.RGB = RGB(255, 102, 0)
                    .Format.Line.DashStyle = msoLineSolid
                    .Border.Weight = xlThick
                    '.MarkerStyle = xlSquare
                    .MarkerStyle = xlNone
                    .Smooth = False
                    .MarkerSize = 7
                    .Shadow = False
                    .Points(intNumOfGomperzActualResolvedBugPoint).ApplyDataLabels
                    .Points(intNumOfGomperzActualResolvedBugPoint).DataLabel.NumberFormat = "#,##0_ "
                End With

            ElseIf (.SeriesCollection(i).Name = "calculation range of Estimate resolved bug") Or _
                   (.SeriesCollection(i).Name = "改修予測算出範囲") Then
                ' 改修予測算出範囲
                With .SeriesCollection(i)
                    .MarkerStyle = xlNone 'Markerを消す
                    .Format.Line.Visible = msoFalse '線を消す
                End With

                ' バグ問処解決予測(線形)
                .SeriesCollection(i).Trendlines.Add
                With .SeriesCollection(i).Trendlines(1)
                    ' 英語
                    If (Sheets(strConstConfigSheetName).Range("LanguageSetting").Value = "英語") Then
                        .Name = "Estimate Resolved Bugs(Linear)"
                    Else
                        .Name = "バグ問処解決数予測(線形)"
                    End If
                    .Format.Line.ForeColor.RGB = RGB(255, 102, 0)
                    .Format.Line.DashStyle = msoLineSysDash
                    .Border.Weight = xlMedium
                End With

            ElseIf (.SeriesCollection(i).Name = "Test Planned Pass") Or (.SeriesCollection(i).Name = "テスト項目消化予測") Then
                ' TestChart予測残試験項目数
                With .SeriesCollection(i)
                    .Format.Line.ForeColor.RGB = RGB(0, 102, 255)
                    .Format.Line.DashStyle = msoLineSysDash
                    .Border.Weight = xlMedium
                    .MarkerStyle = xlNone
                    .Smooth = False
                    .MarkerSize = 7
                    .Shadow = False
                    .Points(intNumOfTestPlannedPassPoint).ApplyDataLabels
                    .Points(intNumOfTestPlannedPassPoint).DataLabel.NumberFormat = "#,##0_ "
                    ' プロット位置=左
                    .Points(intNumOfTestPlannedPassPoint).DataLabel.Position = xlLabelPositionLeft
                End With

            ElseIf (.SeriesCollection(i).Name = "Test Passed") Or (.SeriesCollection(i).Name = "テスト項目消化実績") Then
                ' TestChart実績残試験項目数
                With .SeriesCollection(i)
                    .Format.Line.ForeColor.RGB = RGB(0, 0, 255)
                    .Format.Fill.ForeColor.RGB = RGB(0, 0, 255)
                    .Format.Line.DashStyle = msoLineSolid
                    .Border.Weight = xlThick
                    .MarkerStyle = xlNone
                    .Smooth = False
                    .MarkerSize = 7
                    .Shadow = False
                    ' 総項目数をプロット
                    .Points(intNumOfTestPassedPoint).ApplyDataLabels
                    .Points(intNumOfTestPassedPoint).DataLabel.NumberFormat = "#,##0_ "
                    ' プロット位置=左
                    .Points(intNumOfTestPassedPoint).DataLabel.Position = xlLabelPositionLeft
                End With

            ElseIf (.SeriesCollection(i).Name = "Estimate Bugs of Planned(Logistics)") Or (.SeriesCollection(i).Name = "バグ検出予測(Logistics)") Then
                ' TestChart予測バグ数(Logistics)
                With .SeriesCollection(i)
                    .Format.Line.ForeColor.RGB = RGB(128, 0, 128)
                    .Format.Line.DashStyle = msoLineSysDash
                    .Border.Weight = xlMedium
                    .MarkerStyle = xlNone
                    .Smooth = False
                    .MarkerSize = 7
                    .Shadow = False
                    ' 総項目数をプロット
                    .Points(intNumOfTestPredictedBugPoint).ApplyDataLabels
                    .Points(intNumOfTestPredictedBugPoint).DataLabel.NumberFormat = "#,##0_ "
                    ' プロット位置=左
                    .Points(intNumOfTestPredictedBugPoint).DataLabel.Position = xlLabelPositionLeft
                End With

            ElseIf (.SeriesCollection(i).Name = "Estimate Bugs of Learning(Gomperz)") Or (.SeriesCollection(i).Name = "バグ習熟予測(Gomperz)") Then
                ' Gomperz予測バグの線に対する設定
                 With .SeriesCollection(i)
                     .Format.Line.ForeColor.RGB = RGB(255, 0, 0)
                     .Format.Line.DashStyle = msoLineSysDash
                     .Border.Weight = xlMedium
                     .MarkerStyle = xlNone
                     .Smooth = False
                     .MarkerSize = 7
                     .Shadow = False
                     .Points(intNumOfGomperzPredictedBugPoint).ApplyDataLabels
                     .Points(intNumOfGomperzPredictedBugPoint).DataLabel.NumberFormat = "#,##0_ "
                 End With

            ElseIf (.SeriesCollection(i).Name = "Latent Bugs(Gomperz)") Or (.SeriesCollection(i).Name = "潜在バグ数(Gomperz)") Then
                ' Gomperz潜在バグ数
                With .SeriesCollection(i)
                    .Format.Line.ForeColor.RGB = RGB(0, 128, 0)
                    .Format.Line.DashStyle = msoLineSolid
                    .Border.Weight = xlThick
                    .MarkerStyle = xlNone
                    .Smooth = False
                    .MarkerSize = 7
                    .Shadow = False
                    .Points(intNumOfGomperzPredictedBugNPoint).ApplyDataLabels
                    .Points(intNumOfGomperzPredictedBugNPoint).DataLabel.NumberFormat = "#,##0_ "
                End With

            ElseIf (.SeriesCollection(i).Name = "Latent Bugs(Gomperz)+5%") Or (.SeriesCollection(i).Name = "潜在バグ数(Gomperz)+5%") Then
                ' Gomperz上限値
                With .SeriesCollection(i)
                    .Format.Line.ForeColor.RGB = RGB(0, 128, 0)
                    .Format.Line.DashStyle = msoLineDashDot
                    .Border.Weight = xlMedium
                    .MarkerStyle = xlNone
                    .Smooth = False
                    .MarkerSize = 7
                    .Shadow = False
                    .Points(intNumOfGomperzPredictedBugMaxPoint).ApplyDataLabels
                    .Points(intNumOfGomperzPredictedBugMaxPoint).DataLabel.NumberFormat = "#,##0_ "
                End With

            ElseIf (.SeriesCollection(i).Name = "Latent Bugs(Gomperz)-5%") Or (.SeriesCollection(i).Name = "潜在バグ数(Gomperz)-5%") Then
                ' Gomperz下限値
                With .SeriesCollection(i)
                    .Format.Line.ForeColor.RGB = RGB(0, 128, 0)
                    .Format.Line.DashStyle = msoLineDashDot
                    .Border.Weight = xlMedium
                    .MarkerStyle = xlNone
                    .Smooth = False
                    .MarkerSize = 7
                    .Shadow = False
                    .Points(intNumOfGomperzPredictedBugMinPoint).ApplyDataLabels
                    .Points(intNumOfGomperzPredictedBugMinPoint).DataLabel.NumberFormat = "#,##0_ "
                End With

            Else

            End If
        Next

        ' プロットエリアの背景色
        .PlotArea.Format.Fill.ForeColor.RGB = RGB(255, 255, 204)

        ' テキストボックス
'        With .TextBoxes.Add(550, 500, 50, 20)
'            '.Select
'            .AutoSize = True
'            .Text = ""
'        End With

    End With

End Sub

'　検索文字列の行番号を取得
Private Function GetTargetStringRow(ByVal rangeTarget As Range, _
                                    ByVal strTargetString As String) As Integer
    Dim intReturn As Integer
    Dim Foundcell As Range

    Set Foundcell = rangeTarget.Find(what:=strTargetString, _
                                    LookIn:=xlValues, _
                                    LookAt:=xlPart, _
                                    MatchCase:=False, _
                                    MatchByte:=False)
    If Foundcell Is Nothing Then
        intReturn = 0
    Else
        intReturn = Foundcell.Row
    End If

    GetTargetStringRow = intReturn

End Function

'　検索文字列の列番号を取得
Private Function GetTargetStringColumn(ByVal rangeTarget As Range, _
                                       ByVal strTargetString As String) As Long
    Dim intReturn As Long
    Dim Foundcell As Range

    Set Foundcell = rangeTarget.Find(what:=strTargetString, _
                                     LookIn:=xlValues, _
                                     LookAt:=xlPart, _
                                     MatchCase:=False, _
                                     MatchByte:=False)
    If Foundcell Is Nothing Then
        intReturn = 0
    Else
        intReturn = Foundcell.Column
    End If

    GetTargetStringColumn = intReturn

End Function

Private Sub 同名のワークシートが無ければ追加(ByVal strOutSheetName As String)
    Dim i As Worksheet

    For Each i In Worksheets
        If (UCase(i.Name) = UCase(strOutSheetName)) Then
            Sheets(strOutSheetName).Select
            Exit Sub
        End If
    Next

    Sheets.Add After:=Sheets(Sheets(Sheets.Count).Name)
    ActiveSheet.Name = strOutSheetName

End Sub

Private Sub 同名のワークシートを削除(ByVal strOutSheetName As String)
    Dim i As Worksheet

    For Each i In Worksheets
        If (UCase(i.Name) = UCase(strOutSheetName)) Then
            Application.DisplayAlerts = False
            i.Delete
            Application.DisplayAlerts = True
            Exit For
        End If
    Next

End Sub

Private Sub 同名のワークシートを非表示(ByVal strOutSheetName As String)
    Dim i As Worksheet

    For Each i In Worksheets
        If (UCase(i.Name) = UCase(strOutSheetName)) Then
            i.Visible = False
            Exit For
        End If
    Next

End Sub

Private Sub シート上のグラフを全削除(ByVal strOutSheetName As String)
    Dim i As Integer

    Sheets(strOutSheetName).Select

    With ActiveSheet
        For i = .ChartObjects.Count To 1 Step -1
            Application.DisplayAlerts = False
            .ChartObjects(i).Delete
            Application.DisplayAlerts = True
        Next
    End With

End Sub

Private Sub TargetSheetSelect(ByVal varInSheet As Variant)

    If VarType(varInSheet) = vbString Then
        Sheets(varInSheet).Select
    Else
        varInSheet.Parent.Activate
        varInSheet.Select
    End If

End Sub

Private Function GetTargetColumnEndRow(ByVal intColumn As Integer, _
                                       Optional ByVal intRow As Integer = 1) As Integer
    Dim ret As Integer

    On Error GoTo overflow
    ' 空白時は、2回実行、オーバーフロー時は1
    ' integerの範囲を超えたらオーバーフローとする
    If (Cells(intRow, intColumn).Value = "") Then
        ret = Cells(intRow, intColumn).End(xlDown).Row
        ret = Cells(ret, intColumn).End(xlDown).Row
    Else
        ret = Cells(intRow, intColumn).End(xlDown).Row
    End If

    GetTargetColumnEndRow = ret
    Exit Function

overflow:
    GetTargetColumnEndRow = intRow

End Function

Private Function GetTargetColumnEndRow2(ByVal intColumn As Integer) As Integer
    Dim ret As Integer

    On Error GoTo overflow
    ' UsedRangeから検索、文字列があればその値
    ' 文字列が無ければ上に向かって移動
    If (Cells(GetUsedRangeEndRow, intColumn).Value <> "") Then
        ret = GetUsedRangeEndRow
    Else
        ret = Cells(GetUsedRangeEndRow, intColumn).End(xlUp).Row
    End If

    GetTargetColumnEndRow2 = ret
    Exit Function

overflow:
    GetTargetColumnEndRow2 = 1

End Function

Private Function GetTargetColumnStartRow2(ByVal intColumn As Integer) As Integer
    Dim ret As Integer

    On Error GoTo overflow
    ' UsedRangeから検索、文字列があればその値
    ' 文字列が無ければ上に向かって移動
    If (Cells(2, intColumn).Value <> "") Then
        ret = 2
    Else
        ret = Cells(2, intColumn).End(xlDown).Row
    End If

    GetTargetColumnStartRow2 = ret
    Exit Function

overflow:
    GetTargetColumnStartRow2 = 1

End Function

Private Function GetUsedRangeEndRow() As Integer
    GetUsedRangeEndRow = Cells(1, 1).SpecialCells(xlCellTypeLastCell).Row
End Function

Private Function GetUsedRangeEndColumn() As Integer
    GetUsedRangeEndColumn = Cells(1, 1).SpecialCells(xlCellTypeLastCell).Column
End Function

