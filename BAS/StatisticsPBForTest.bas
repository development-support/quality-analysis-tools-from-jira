Attribute VB_Name = "StatisticsPBForTest"
Option Explicit


Sub PBExecuteForSim()
    Dim strJobName As String
    Dim strInSheetName As String
    Dim strAnalyzeSheetName As String
    Dim colRowLabel As Long
    Dim rowDate As Long
    Dim booEndMessage As Boolean

    Application.ScreenUpdating = False

    ' Parameter
    strJobName = "PB曲線集計"
    strInSheetName = strConstTestDataSheetName
    strAnalyzeSheetName = strConstPBAnalyzeSheetName

    Sheets(strInSheetName).Select
    colRowLabel = 1

    rowDate = GetTargetStringRow(Columns(colRowLabel), "Date")
    booEndMessage = False

    ' Statusバー表示
    JobBegin (strJobName & "中...")

    ' Logistics演算
    Call PBForSim(strInSheetName, _
                  strAnalyzeSheetName, _
                  colRowLabel, _
                  rowDate)

    ' Statusバー表示
    JobEnd strJobName & "処理終了", MsgBoxShow:=booEndMessage

End Sub

' Logistics演算
Private Sub PBForSim(ByVal strInSheetName As String, _
                     ByVal strAnalyzeSheetName As String, _
                     ByVal colRowLabel As Long, _
                     ByVal rowDate As Long)
    Dim varSettingParam As Variant
    Dim strByWeekOrDay As String
    Dim dtPredicted As Date
    Dim varTmp As Variant
    Dim dtStart As Date
    Dim dtEnd As Date
    Dim strDataFrequency As String
    Dim booGomperz As String

    Debug.Print "(PBForSim):strInSheetName = " & strInSheetName & _
                " strAnalyzeSheetName = " & strAnalyzeSheetName & _
                " colRowLabel = " & colRowLabel & " rowDate = " & rowDate

    ' パラメータ取得
    strByWeekOrDay = "Day"

    'Sheets(strInSheetName).Select
    'dtStart = WorksheetFunction.Min(Rows(GetTargetStringRow(Columns(colRowLabel), "Date", True)))
    'dtPredicted = WorksheetFunction.Max(Rows(GetTargetStringRow(Columns(colRowLabel), "Date", True)))
    'dtEnd = Cells(GetTargetStringRow(Columns(colRowLabel), "Current Report Date") + 1, colRowLabel).Value
    ' JIRA Dataより日付取得
    Sheets(strConstJIRASheetName).Select
    dtStart = WorksheetFunction.Min(Columns(GetTargetStringColumn(Rows(1), "Created")))
    dtPredicted = WorksheetFunction.Max(Columns(GetTargetStringColumn(Rows(1), "Created")))
    dtEnd = dtPredicted
    Debug.Print "(PBForSim):dtStart = " & dtStart & " dtPredicted = " & dtPredicted & _
                " dtEnd = " & dtEnd

    If (DateDiff("d", dtStart, dtEnd) <= 0) Then
        If (MsgBox("2日以上のバグのサンプルが必要です。" & vbCr & _
                   "JIRA Dataの日付を確認してください。", vbExclamation) = vbOK) Then
            Exit Sub
        End If
    End If

    ' 計算シート生成
    同名のワークシートを削除 strAnalyzeSheetName
    同名のワークシートが無ければ追加 strAnalyzeSheetName

    ' 日毎のバグを取得
    Call GetDailyTestsData(strInSheetName, _
                           strAnalyzeSheetName, _
                           dtStart, _
                           dtEnd, _
                           dtPredicted, _
                           strByWeekOrDay)

End Sub

' データの落とし込み
' 週毎の集計に対応する為、簡単に縦横変換はできない
Private Sub GetDailyTestsData(ByVal strInSheetName As String, _
                              ByVal strOutSheetName As String, _
                              ByVal dtStart As Date, _
                              ByVal dtEnd As Date, _
                              ByVal dtPredicted As Date, _
                              ByVal strByWeekOrDay As String)
    ' Total Tests Planned
    Call EndDayWrite(strOutSheetName, dtStart, dtPredicted, strByWeekOrDay, 1)

    Call ActualSamplingNoWrite(strOutSheetName, 1, 1, 2)

    Call NumberOfDailyTestsWrite(strInSheetName, _
                                 1, _
                                 "Total Tests Planned", _
                                 strOutSheetName, _
                                 1, _
                                 1, _
                                 1, _
                                 3)
    ' Planned Attempts
    Call NumberOfDailyTestsWrite(strInSheetName, _
                                 1, _
                                 "Planned Attempts", _
                                 strOutSheetName, _
                                 1, _
                                 1, _
                                 1, _
                                 4)
    ' Planned Pass
    Call NumberOfDailyTestsWrite(strInSheetName, _
                                 1, _
                                 "Planned Pass", _
                                 strOutSheetName, _
                                 1, _
                                 1, _
                                 1, _
                                 5)
    ' Actual Attempts
    Call EndDayWrite(strOutSheetName, dtStart, dtEnd, strByWeekOrDay, 6)

    Call NumberOfDailyTestsWrite(strInSheetName, _
                                 1, _
                                 "Actual Attempts", _
                                 strOutSheetName, _
                                 6, _
                                 1, _
                                 1, _
                                 7)
    ' Passed
    Call NumberOfDailyTestsWrite(strInSheetName, _
                                 1, _
                                 "Passed", _
                                 strOutSheetName, _
                                 6, _
                                 1, _
                                 1, _
                                 8)
    ' "Passed"の日付は"Total Tests Planned"の開始は同じで開始から終了の間に含まれるので削除
    Columns(6).Delete

    ' バーンダウンに変更
    Dim i As Long
    Dim rowEnd As Long
    Dim cntTotalTestsPlanned As Long
    ' テスト項目の総数を取得(途中で変わる可能性があるので最終日の値を取得)
    rowEnd = GetTargetColumnEndRow(1)
    cntTotalTestsPlanned = Cells(rowEnd, 3).Value

    ' Planned Attempts
    For i = 2 To rowEnd
        Cells(i, 4) = cntTotalTestsPlanned - Cells(i, 4)
    Next

    ' Planned Pass
    For i = 2 To rowEnd
        Cells(i, 5) = cntTotalTestsPlanned - Cells(i, 5)
    Next

    ' Actual Attempts
    ' 最終行を再取得
    rowEnd = GetTargetColumnEndRow(6)
    For i = 2 To rowEnd
        Cells(i, 6) = cntTotalTestsPlanned - Cells(i, 6)
    Next

    ' Passed
    ' 最終行を再取得
    rowEnd = GetTargetColumnEndRow(7)
    For i = 2 To rowEnd
        Cells(i, 7) = cntTotalTestsPlanned - Cells(i, 7)
    Next

    ' 日毎のバグを取得
    Call LogisticCalc(strOutSheetName, 2, 1, 8, False)

End Sub

' 実績日の書き込み
Private Sub EndDayWrite(ByVal varInSheet As Variant, _
                        ByVal dtStart As Date, _
                        ByVal dtEnd As Date, _
                        ByVal strByWeekOrDay As String, _
                        ByVal colOutTarget As Long, _
                        Optional ByVal booCheck As Boolean = True)
    Dim i As Integer
    
    Call TargetSheetSelect(varInSheet)

    If (strByWeekOrDay = "Day") Then
        Cells(1, colOutTarget).Value = "日付"

        For i = 0 To DateDiff("d", dtStart, dtEnd)
            Cells(i + 2, colOutTarget).Value = DateAdd("d", i, dtStart)
        Next
    Else
        Cells(1, colOutTarget).Value = "日付"

        For i = 0 To DateDiff("w", dtStart, dtEnd)
            Cells(i + 2, colOutTarget).Value = DateAdd("d", 7 * i, dtStart)
        Next
    End If

End Sub

' テスト項目データ取得
Private Sub NumberOfDailyTestsWrite(ByVal strInSheetName As String, _
                                    ByVal colHeader As Long, _
                                    ByVal strTargetString As String, _
                                    ByVal strOutSheetName As String, _
                                    ByVal colTarget As Long, _
                                    ByVal rowHeader As Long, _
                                    ByVal RowColTargetOffset As Long, _
                                    ByVal colOutTarget As Long)
    Dim i, j As Long
    Dim rowStart As Long
    Dim rowCur As Long
    Dim rowLast As Long
    Dim rowTarget As Long
    Dim dtTargetDate As Date

    ' Test Dataシートの対象行を抽出
    Sheets(strInSheetName).Select
    rowTarget = GetTargetStringRow(Columns(colHeader), strTargetString)

    ' 出力シート
    Sheets(strOutSheetName).Select
    ' 日付行のHeaderを除く開始と終わりの行を抽出
    rowStart = rowHeader + 1
    rowCur = rowStart
    rowLast = GetTargetColumnEndRow(colTarget)
    ' ヘッダを記載
    Cells(1, colOutTarget).Value = strTargetString

    ' Test Data シートの対象行の値を出力シートへコピー
    ' colTargetは日付
    Dim rowDate As Long
    Dim colDateEnd As Long

    Sheets(strInSheetName).Select
    rowDate = GetTargetStringRow(Columns(colHeader), "Date", True)
    colDateEnd = GetUsedRangeEndColumn

    For i = RowColTargetOffset + 1 To GetUsedRangeEndColumn
        If (Trim$(Cells(rowDate, i)) = "") Then
            colDateEnd = i - 1
            Exit For
        End If
    Next

    Sheets(strOutSheetName).Select

    For i = RowColTargetOffset + 1 To colDateEnd
        dtTargetDate = Sheets(strInSheetName).Cells(rowDate, i)
        'varTargetData = Sheets(strInSheetName).Cells(rowTarget, i)
        For j = rowCur To rowLast
            If (DateDiff("d", Cells(j, colTarget), dtTargetDate) = 0) Then
                If (Sheets(strInSheetName).Cells(rowTarget, i).Value <> "") Then
                    Cells(j, colOutTarget).Value = Trim$(Sheets(strInSheetName).Cells(rowTarget, i).Value)
                ' 値の入ってない行は0で埋める
                Else
                    Cells(j, colOutTarget).Value = 0
                End If
                rowCur = j
                Exit For
            End If
        Next
    Next

    ' 空白を前日の値で穴埋め（初期値があるのが前提)
    Sheets(strOutSheetName).Select
    rowLast = GetTargetColumnEndRow2(colOutTarget)

    For i = rowStart To rowLast
        If (Cells(i, colOutTarget) = "") Then
            Cells(i, colOutTarget) = Cells(i - 1, colOutTarget)
        End If
    Next

End Sub

' 実績サンプリング番号の書き込み
'　経過日数X
'  注：0から始まる
Private Sub ActualSamplingNoWrite(ByVal varInSheet As Variant, _
                                  ByVal colTarget As Long, _
                                  ByVal rowHeader As Long, _
                                  ByVal colOutTarget As Long)
    Dim i As Long
    Dim rowStart As Long
    Dim rowLast As Long

    rowStart = rowHeader + 1
    rowLast = GetTargetColumnEndRow(colTarget)

    Call TargetSheetSelect(varInSheet)

    Cells(1, colOutTarget).Value = "経過日数X"

    For i = rowStart To rowLast
        Cells(i, colOutTarget).Value = i - rowHeader - 1
    Next

End Sub

' ロジスティック曲線算出
Private Sub LogisticCalc(ByVal strInSheetName As String, _
                         ByVal colTarget As Long, _
                         ByVal rowHeader As Long, _
                         ByVal colOutTarget As Long, _
                         ByVal booGomperz As Boolean)
    Dim rowStart As Long
    Dim rowLast As Long
    Dim dblA As Double
    Dim dblB As Double
    Dim dblC As Double
    Dim strCalc As String
    Dim i As Long

    If (booGomperz) Then
        ' 注：名前定義してない
        dblA = Sheets(strConstGomperzAnalyzeSheetName).Cells(13, 12).Value
    Else
        dblA = Sheets(strConstConfigSheetName).Range("LogisticsParamaterA").Value
    End If

    dblB = Sheets(strConstConfigSheetName).Range("LogisticsParamaterB").Value
    dblC = Sheets(strConstConfigSheetName).Range("LogisticsParamaterC").Value

    Sheets(strInSheetName).Select
    rowStart = rowHeader + 1
    rowLast = GetTargetColumnEndRow(colTarget)

    If (booGomperz) Then
        Cells(1, colOutTarget).Value = "バグ数(Gomperz)"
    Else
        Cells(1, colOutTarget).Value = "バグ数(指標値)"
    End If

    ' a/{1+b*EXP(-c*X)}
    For i = rowStart To rowLast
        strCalc = "=" & dblA & "/(1 + " & dblB & "*EXP(-" & dblC & "*" & Cells(i, colTarget).Value & "))"
        Cells(i, colOutTarget).Value = Evaluate(strCalc)
    Next

End Sub


Private Sub JobBegin(Optional strMsg As String = "")

    varSaveCalculation = Application.Calculation
    varSaveScreenUpdating = Application.ScreenUpdating

    Application.Calculation = xlCalculationManual

    If strMsg <> "" Then
        Application.StatusBar = strMsg
    End If

    Application.ScreenUpdating = False

End Sub

Private Sub JobEnd(Optional strMsg As String = "", Optional MsgBoxShow As Boolean = False)

    If Not IsEmpty(varSaveCalculation) Then
        If Application.Calculation <> varSaveCalculation Then
            Application.Calculation = varSaveCalculation
        End If
        varSaveCalculation = Empty
    End If

    If Not IsEmpty(varSaveScreenUpdating) Then
        Application.ScreenUpdating = varSaveScreenUpdating
        varSaveScreenUpdating = Empty
    End If

    If (strMsg <> "") And (MsgBoxShow = True) Then
        Application.StatusBar = Replace(strMsg, vbCrLf, " ")
        MsgBox strMsg, vbOKOnly
    End If

    Application.StatusBar = False

End Sub

'　検索文字列の行番号を取得
Private Function GetTargetStringRow(ByVal rangeTarget As Range, _
                                    ByVal strTargetString As String, _
                                    Optional ByVal boofull As Boolean = False) As Long
    Dim rowReturn As Long
    Dim Foundcell As Range

    If (boofull) Then
        Set Foundcell = rangeTarget.Find(what:=strTargetString, _
                                         LookIn:=xlValues, _
                                         LookAt:=xlWhole, _
                                         MatchCase:=False, _
                                         MatchByte:=False)
    Else
        Set Foundcell = rangeTarget.Find(what:=strTargetString, _
                                         LookIn:=xlValues, _
                                         LookAt:=xlPart, _
                                         MatchCase:=False, _
                                         MatchByte:=False)
    End If

    If Foundcell Is Nothing Then
        rowReturn = 0
    Else
        rowReturn = Foundcell.Row
    End If

    GetTargetStringRow = rowReturn

End Function

'　検索文字列の列番号を取得
Private Function GetTargetStringColumn(ByVal rangeTarget As Range, _
                                       ByVal strTargetString As String) As Long
    Dim intReturn As Long
    Dim Foundcell As Range

    Set Foundcell = rangeTarget.Find(what:=strTargetString, _
                                     LookIn:=xlValues, _
                                     LookAt:=xlPart, _
                                     MatchCase:=False, _
                                     MatchByte:=False)
    If Foundcell Is Nothing Then
        intReturn = 0
    Else
        intReturn = Foundcell.Column
    End If

    GetTargetStringColumn = intReturn

End Function

Private Sub 同名のワークシートが無ければ追加(ByVal strOutSheetName As String)
    Dim i As Worksheet

    For Each i In Worksheets
        If (UCase(i.Name) = UCase(strOutSheetName)) Then
            Sheets(strOutSheetName).Select
            Exit Sub
        End If
    Next

    Sheets.Add After:=Sheets(Sheets(Sheets.Count).Name)
    ActiveSheet.Name = strOutSheetName

End Sub

Private Sub 同名のワークシートを削除(ByVal strOutSheetName As String)
    Dim i As Worksheet

    For Each i In Worksheets
        If (UCase(i.Name) = UCase(strOutSheetName)) Then
            Application.DisplayAlerts = False
            i.Delete
            Application.DisplayAlerts = True
            Exit For
        End If
    Next

End Sub

Private Sub 同名のワークシートを非表示(ByVal strOutSheetName As String)
    Dim i As Worksheet

    For Each i In Worksheets
        If (UCase(i.Name) = UCase(strOutSheetName)) Then
            i.Visible = False
            Exit For
        End If
    Next

End Sub

Private Sub 同名のグラフを削除(ByVal strOutGraphName As String)
    Dim i As Chart

    For Each i In Charts
        If (UCase(i.Name) = UCase(strOutGraphName)) Then
            Application.DisplayAlerts = False
            i.Delete
            Application.DisplayAlerts = True
            Exit For
        End If
    Next

End Sub

Private Sub TargetSheetSelect(ByVal varInSheet As Variant)

    If VarType(varInSheet) = vbString Then
        Sheets(varInSheet).Select
    Else
        varInSheet.Parent.Activate
        varInSheet.Select
    End If

End Sub

Private Function GetTargetColumnEndRow(ByVal colNum As Long, _
                                       Optional ByVal rowNum As Integer = 1) As Long
    Dim ret As Long

    On Error GoTo overflow
    ' 空白時は、2回実行、オーバーフロー時は1
    ' integerの範囲を超えたらオーバーフローとする
    If (Cells(rowNum, colNum).Value = "") Then
        ret = Cells(rowNum, colNum).End(xlDown).Row
        ret = Cells(ret, colNum).End(xlDown).Row
    Else
        ret = Cells(rowNum, colNum).End(xlDown).Row
    End If

    GetTargetColumnEndRow = ret
    Exit Function

overflow:
    GetTargetColumnEndRow = rowNum

End Function

Private Function GetTargetColumnEndRow2(ByVal colNum As Long) As Long
    Dim ret As Long

    On Error GoTo overflow
    ' UsedRangeから検索、文字列があればその値
    ' 文字列が無ければ上に向かって移動
    If (Cells(GetUsedRangeEndRow, colNum).Value <> "") Then
        ret = GetUsedRangeEndRow
    Else
        ret = Cells(GetUsedRangeEndRow, colNum).End(xlUp).Row
    End If

    GetTargetColumnEndRow2 = ret
    Exit Function

overflow:
    GetTargetColumnEndRow2 = 1

End Function

Private Function GetUsedRangeEndRow() As Long
    GetUsedRangeEndRow = Cells(1, 1).SpecialCells(xlCellTypeLastCell).Row
End Function

Private Function GetUsedRangeEndColumn() As Long
    GetUsedRangeEndColumn = Cells(1, 1).SpecialCells(xlCellTypeLastCell).Column
End Function

