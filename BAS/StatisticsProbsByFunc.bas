Attribute VB_Name = "StatisticsProbsByFunc"
Option Explicit
'-----------------------------------------------------------
' 【機能】
' 問題件数の分類毎の集計とグラフ化
'
'-----------------------------------------------------------

Private Const strTemplateSheetName    As String = "【書式】問題件数分類"

Private Const strGraphNamePrefix    As String = "CountByGrp"

Private Const strConstSplitSheetPrefix As String = "C_"
Private Const strConstProcessSheetPrefix As String = "P_"

Private strGroupingName() As String '分類項目名

Private Const intCountTableOffset As Long = 3

Private Const strGeneSourceAddr As String = "C1"  '実行ソース出力位置
Private Const strGeneTimeAddr As String = "C2" '実行日時出力位置

Private strGeneSource As String

'' レコード抽出条件
Private strTargetValue As String
Private intSelectorColumn As Long

'' 参照する定義名
Private Const strDefName_SelectField As String = "CountByGrp_参照項目"

' 集計表
Private Const strDefName_Axis0 As String = "CountByGrp_横軸"
Private Const strDefName_Axis1 As String = "CountByGrp_系列1"

Public _
Sub ProblemsByFuncExecute(Optional ByVal booEndMessage As Boolean = True)
    Const strJobName    As String = "問題件数分類集計"
    Dim wsData  As Worksheet    '入力データシート
    Dim boolEndStatus   As Boolean

    Dim strSheetName As String
    Dim varInputSheet As Variant '問題管理データシート/分割シート
    Dim strSplitName() As String
    Dim i As Long

    boolEndStatus = False

    If ProblemDataBaseTypeGet() = "Redmine" Then
        Select Case TargetProjectNameGet()
        Case "T200"
            Call 未実装(booEndMessage)
            Exit Sub
        End Select
    End If

    If booEndMessage Then
        If MsgBox(strJobName & "を開始します。", vbOKCancel) <> vbOK Then
            Exit Sub
        End If
    End If

    ' 前処理
    Call StatisticsLibrary2.JobBegin(strJobName & "中...")

    ' 入力シート取得
    Set wsData = 問題管理データシート取得()
    If wsData Is Nothing Then
        GoTo Exit_Proc
    End If

    If (ProblemDataBaseTypeGet() = "Redmine") And (TargetProjectNameGet() = "T3xx") Then
        If 分割シート取得(wsData, "カテゴリ", strConstSplitSheetPrefix, strSplitName, varInputSheet) = False Then
            GoTo Exit_Proc
        End If
    ElseIf ProblemDataBaseTypeGet() = "JIRA" Then
        If 分割シート取得(wsData, "カテゴリ", strConstSplitSheetPrefix, strSplitName, varInputSheet) = False Then
            GoTo Exit_Proc
        End If
    Else
        Set varInputSheet = wsData
    End If

    ' 問題件数分類集計
    Dim strGeneSource As String
    strGeneSource = TargetProjectNameGet()
    If IsArray(varInputSheet) Then
        Dim wsData2  As Worksheet    '分割データシート
        For i = 1 To UBound(varInputSheet)
            strSheetName = varInputSheet(i)
            Set wsData2 = StatisticsLibrary2.同名のシート取得(strSheetName)
            If ProblemDataBaseTypeGet() = "JIRA" Then
                If ProblemsByFuncByProcess(wsData2, strGeneSource, True, strSplitName(i)) = False Then
                    GoTo Exit_Proc
                End If
            ElseIf ProblemsByFuncCount(wsData2, strGeneSource, True, strSplitName(i)) = False Then
                GoTo Exit_Proc
            End If
        Next i

        Call 分割シート後処理(varInputSheet)

    ElseIf ProblemDataBaseTypeGet() = "JIRA" Then

    ElseIf ProblemsByFuncCount(wsData, strGeneSource) = False Then
        GoTo Exit_Proc
    End If

    ' 全体出力 ※JIRAの場合、分割出力の有無に関わらず全体出力する。
    If ProblemDataBaseTypeGet() = "JIRA" Then
        If ProblemsByFuncByProcess(wsData, strGeneSource) = False Then
            GoTo Exit_Proc
        End If
    End If

    boolEndStatus = True

Exit_Proc:
    ' 後処理
    If boolEndStatus = False Then
        Call StatisticsLibrary2.JobEnd(strJobName & "中止", MsgBoxShow:=False)
    Else
        Call StatisticsLibrary2.JobEnd(strJobName & "処理終了", MsgBoxShow:=booEndMessage)
    End If

    Erase strGroupingName
    Set wsData = Nothing
End Sub

Private Function ProblemsByFuncByProcess(ByVal wsData As Worksheet, _
                                         ByVal strGeneSource As String, _
                                         Optional ByVal IsSplitSheet As Boolean = False, _
                                         Optional ByVal strSplitName As String = "") As Boolean

    ProblemsByFuncByProcess = False

    Dim wsData2  As Worksheet
    Dim strSheetName As String
    Dim strAnalyzeSheetName As Variant
    Dim strSplitName2() As String
    Dim i As Long
    Dim strSourceSuffix As String
    Dim strNameSuffix As String

    strAnalyzeSheetName = StatisticsLibrary.ByProcessSheetGenerate( _
                                wsData, _
                                JobEndIfEmpty:=True, _
                                分割値格納配列:=strSplitName2, _
                                NewSheetNamePrefix:=strConstProcessSheetPrefix)
    If UBound(strAnalyzeSheetName) < 0 Then 'エラー発生
        Exit Function
    End If

    strSourceSuffix = "/" & strGeneSource
    strNameSuffix = ""
    If IsSplitSheet Then
        strSourceSuffix = "/" & strSplitName & strSourceSuffix
        strNameSuffix = "_" & strSplitName
    End If

    For i = 1 To UBound(strAnalyzeSheetName)
        strSheetName = strAnalyzeSheetName(i)
        Set wsData2 = StatisticsLibrary2.同名のシート取得(strSheetName)
        If ProblemsByFuncCount(wsData2, strSplitName2(i) & strSourceSuffix, True _
                    , strSplitName2(i) & strNameSuffix) = False Then
            GoTo Exit_Proc
        End If
    Next i

    ProblemsByFuncByProcess = True

    ' 工程別シートの削除
    For i = 1 To UBound(strAnalyzeSheetName)
        Call 同名のワークシートを削除(strAnalyzeSheetName(i))
    Next

Exit_Proc:
    Erase strAnalyzeSheetName
    Erase strSplitName2
End Function

' 出力シート名生成
Private _
Function ProblemsByFuncOutNameGet(ByVal strSuffix As String) As String
    ProblemsByFuncOutNameGet = 集計用出力シート名生成(strGraphNamePrefix & strSuffix)
End Function

' 出力書式シート表示／非表示
Public _
Sub ProblemsByFuncFormatSheetDisp()
    Call StatisticsLibrary2.FormatSheetVisibleReverse(strTemplateSheetName)
End Sub

Private _
Function ProblemsByFuncCount(ByVal wsData As Worksheet, _
                             ByVal strGeneSource As String, _
                             Optional ByVal IsSplitSheet As Boolean = False, _
                             Optional ByVal strSplitName As String = "") As Boolean

    Dim booJIRA  As Boolean
    Dim aryDataName()   As String   '抽出するデータの項目名
    Dim aryDataCol()    As Long     '抽出するデータの列番号
    Dim wsTempl As Worksheet
    Dim wsGraph As Worksheet
    Dim aryGroupList()  As String
    Dim strGraphName    As String   '出力シート名
    Dim strGroup As String   '出力対象グループ名
    Dim r2  As Range
    Dim r3  As Range
    Dim numCount    As Long ' 有効データ数
    Dim ii  As Long
    Dim j   As Long
    Dim numKeyCount As Long ' 分類項目のデータ数
    Dim rowSrcTop  As Long
    Dim rowSrcEnd  As Long

    Dim maxKeyCount  As Long ' 分類項目のデータ数の最大
    Dim intTableGap As Variant
    Dim intAxisTemplRowsCount As Long

    ProblemsByFuncCount = False

    '---------------------------------------------------
    ' グループ共通の前処理
    '---------------------------------------------------
    Select Case StatisticsLibrary2.RedmineSheetDataCount(wsData)
    Case Is < 0 '不正なシート
        Exit Function
    Case 0
        MsgBox "'" & wsData.Name & "'シートに有効なデータがありません"
        Exit Function
    End Select

    ' DB種別取得
    Select Case ProblemDataBaseTypeGet()
    Case ""
        Exit Function '不正な値
    Case "JIRA"
        booJIRA = True
    Case Else
        booJIRA = False
    End Select

    ' 出力書式シート取得
    If booJIRA Then
        Set wsTempl = StatisticsLibrary2.同名のシート取得(strTemplateSheetName & "(JIRA)")
    Else
        Set wsTempl = StatisticsLibrary2.同名のシート取得(strTemplateSheetName)
    End If
    If wsTempl Is Nothing Then
        GoTo Exit_Proc  '不正なシート
    End If

    ' Redmineシートから抽出するデータの項目名を取得
    If ProblemsByFuncSelectName(wsTempl, aryDataName) = False Then
        GoTo Exit_Proc  '不正なシート
    End If

    ' Redmineシートから抽出するデータの列番号を取得
    ReDim aryDataCol(UBound(aryDataName))
    If ProblemsByFuncSelectColumn(wsData, aryDataName, aryDataCol) = False Then
        GoTo Exit_Proc  '列番号取得ＮＧ
    End If

    ' レコード抽出条件設定
    If booJIRA Then
        strTargetValue = "Bug"
        intSelectorColumn = GetDataColumn(RedmineSheetHeader(wsData), "Issue Type")
        If intSelectorColumn < 1 Then
            GoTo Exit_Proc ' 不正な入力シート
        End If
    Else
        intSelectorColumn = GetProbemColumnByItemName("原因Gr")
    End If
    If intSelectorColumn < 1 Then
        GoTo Exit_Proc ' 不正な入力シート
    End If

    ' レコード数取得
    If StatisticsLibrary2.RedmineSheetDataRangeGet(wsData, rowSrcTop, rowSrcEnd) < 1 Then
        ProblemsByFuncCount = True
        GoTo Exit_Proc ' 有効データなし
    End If

    If booJIRA Then
        ReDim aryGroupList(1)
        aryGroupList(1) = "" '注) JIRAの場合、原因Grに該当するフィールドが無いので全体出力のみ。
    Else
        ' グループ名のリストを取得
        aryGroupList = UniqListGet(wsData.Cells(rowSrcTop, intSelectorColumn), rowSrcEnd)
        If Not IsSplitSheet Then
            ReDim Preserve aryGroupList(UBound(aryGroupList) + 1)
            aryGroupList(UBound(aryGroupList)) = "全体"
        End If
    End If

    '---------------------------------------------------
    ' グループ毎の処理
    '---------------------------------------------------
    For ii = 1 To UBound(aryGroupList)
        strGroup = aryGroupList(ii)

        '---------------------------------------------------
        ' 前処理
        '---------------------------------------------------
        '出力シート名生成
        If IsSplitSheet Then
            strGraphName = ProblemsByFuncOutNameGet("_" & Left(strSplitName, 12) & "_" & strGroup)
            
        ElseIf strGroup = "" Then
            strGraphName = ProblemsByFuncOutNameGet("")
        Else
            strGraphName = ProblemsByFuncOutNameGet("_" & strGroup)
        End If
        If strGraphName = "" Then
            GoTo Exit_Proc
        End If

        Call StatisticsLibrary.同名のワークシートを削除(strGraphName)

        ' 出力シート生成
        Set wsGraph = StatisticsLibrary2.FormatSheetCopy(wsTempl, strGraphName)
        If wsGraph Is Nothing Then
            GoTo Exit_Proc
        End If

        ' 生成日時とデータソースを設定
        With wsGraph.Range(strGeneSourceAddr)
            .NumberFormatLocal = "@"
            .Value = strGroup & "/" & strGeneSource
        End With
        With wsGraph.Range(strGeneTimeAddr)
            .Value = dateJobStart
        End With

        If strGroup = "全体" Then
            intSelectorColumn = 0
        ElseIf Not booJIRA Then
            strTargetValue = strGroup
        End If

        Call MyFilterModeClear(wsGraph) '注）フィルター選択されていると列コピーで全体がコピーされないため。

        ' 出力先取得
        Set r2 = StatisticsLibrary2.名前の参照範囲取得(wsGraph, strDefName_SelectField)  '抽出データの格納先
        If r2 Is Nothing Then
            GoTo Exit_Proc  '不正なシート
        End If

        Set r3 = StatisticsLibrary2.名前の参照範囲取得(wsGraph, strDefName_Axis0)    '集計結果の格納先
        If r3 Is Nothing Then
            GoTo Exit_Proc  '不正なシート
        End If

        intAxisTemplRowsCount = r3.Rows.Count - 1
        Set r3 = r3.Rows(1)

        '---------------------------------------------------
        ' Redmineシートからデータ抽出
        '---------------------------------------------------
        numCount = ProblemsByFuncDataSelect(wsData, aryDataCol, r2)
        If numCount < 0 Then
            GoTo Exit_Proc  '異常発生
        End If

        maxKeyCount = 0
        For j = 0 To UBound(strGroupingName)
            '---------------------------------------------------
            ' データ集計
            '---------------------------------------------------
            numKeyCount = ProblemsByFuncDataCount(r2, r3.Rows(1), intAxisTemplRowsCount, numCount, j)
            If numKeyCount < 0 Then
                GoTo Exit_Proc  '異常発生
            End If
            If maxKeyCount < numKeyCount Then
                maxKeyCount = numKeyCount
            End If

            '------------------------
            ' グラフ定義補整
            '------------------------
            If ProblemsByFuncGraphAdjust(wsGraph, numKeyCount, j) = False Then
                GoTo Exit_Proc  '異常発生
            End If
        Next j

        '-------------------------------------------------
        '   データ表の位置を補整
        '     ※データ表のフィルタ設定で集計表内の行が非表示になると
        '       その行がグラフに表示されなくなるため。
        '-------------------------------------------------
        intTableGap = r3.Row + maxKeyCount - r2.Row + 2
        If intTableGap > 0 Then
            wsGraph.Cells(1, r2.Column).Resize(intTableGap, r2.Cells.Count).Insert Shift:=xlShiftDown
        End If

        Call MySelectionReset(wsGraph)
        Call MyWsCalculate(wsGraph)
    Next ii

    ProblemsByFuncCount = True

Exit_Proc:
    Erase aryGroupList
    Erase aryDataName
    Erase aryDataCol
    Set wsTempl = Nothing

End Function

' Redmineデータシートから抽出するデータの名称格納位置を取得
Private _
Function ProblemsByFuncSelectName(ByVal ws As Worksheet, _
                                  ByRef aryDataName() As String) As Boolean
    Dim rr  As Range
    Dim r2  As Range
    Dim aa() As String
    Dim ii  As Long

    ProblemsByFuncSelectName = False

    Set rr = StatisticsLibrary2.名前の参照範囲取得(ws, strDefName_SelectField)
    If StatisticsLibrary2.MyRegionColumnSort(rr) = False Then
        GoTo Exit_Proc  '不正なシート
    ElseIf StatisticsLibrary2.Row2StringArray(rr, aa) = False Then
        GoTo Exit_Proc  '不正なシート
    End If

    ReDim aryDataName(UBound(aa))
    For ii = 0 To UBound(aa)
        aryDataName(ii) = aa(ii)
    Next ii

    ' 分類項目を設定
    Set r2 = StatisticsLibrary2.名前の参照範囲取得(ws, strDefName_Axis0)
    If r2 Is Nothing Then
        GoTo Exit_Proc  '不正なシート
    End If

    If r2.Columns.Count > 1 Then
        Call MsgBox("'" & ws.Name & "!" & strDefName_Axis0 & " の名前定義が不正です", vbCritical)
        GoTo Exit_Proc  '不正なシート
    ElseIf r2.Rows.Count > 1 Then
        Set r2 = r2.Rows(1)
    End If

    ii = 0
    Do While r2.Offset(0, 1).Value = "問題件数" Or r2.Offset(0, 1).Value = "Bug"
        ReDim Preserve strGroupingName(ii)
        strGroupingName(ii) = r2.Value
        ii = ii + 1
        Set r2 = r2.Offset(0, intCountTableOffset)
    Loop

    ProblemsByFuncSelectName = True

Exit_Proc:
    Erase aa

End Function

' Redmineデータシート抽出するデータの列番号を取得
Private _
Function ProblemsByFuncSelectColumn(ByVal wsData As Worksheet, _
                                    ByRef aryDataName() As String, _
                                    ByRef aryDataCol() As Long) As Boolean

    ProblemsByFuncSelectColumn = False

    If StatisticsLibrary2.RedmineSheetHeaderColumn(wsData, _
                                                   aryDataName, _
                                                   aryDataCol) = False Then
        ProblemsByFuncSelectColumn = False
    Else
        ProblemsByFuncSelectColumn = True
    End If

End Function

' 出力対象レコードか判定
Private _
Function IsTargetRecord(ByVal rndRecord As Range) As Boolean
    Dim strTmp  As String

    If intSelectorColumn > 0 Then
        strTmp = Trim(rndRecord.Columns(intSelectorColumn).Value)
        If strTmp <> strTargetValue Then
            IsTargetRecord = False
            Exit Function
        End If
    End If

    IsTargetRecord = True
End Function

' データシートから使用するデータを抽出
Private _
Function ProblemsByFuncDataSelect(ByVal wsData As Worksheet, _
                                  ByRef aryDataCol() As Long, _
                                  ByVal OutHeader As Range) As Long

    Dim rowDataTop  As Long
    Dim rowDataEnd  As Long
    Dim in_col  As Variant
    Dim in_row  As Long
    Dim out_col As Long
    Dim out_base    As Range
    Dim out_next    As Range
    Dim numCount    As Long ' 有効データ数

    ProblemsByFuncDataSelect = -1

    Set out_base = OutHeader.Offset(1)   '最初のデータ出力位置

    ' 出力域クリア   注）書式コピー用に一行目のデータは残す
    Call StatisticsLibrary2.ClearRangeToEndRow(out_base.Offset(1))

    '' 抽出対象の列の値を取り出して出力域に格納
    ' レコード数取得
    Call StatisticsLibrary2.RedmineSheetDataRangeGet(wsData, rowDataTop, rowDataEnd)

    Set out_next = out_base
    For in_row = rowDataTop To rowDataEnd
        ' 出力対象レコードか判定
        If IsTargetRecord(wsData.Rows(in_row)) Then
          If out_next.Row > out_base.Row Then
              out_base.Copy Destination:=out_next '書式等をコピー
          End If

          out_col = 0
          For Each in_col In aryDataCol
              out_col = out_col + 1
              If in_col > 0 Then
                  out_next.Columns(out_col).Value = wsData.Cells(in_row, in_col).Value
              End If
          Next in_col

          Set out_next = out_next.Offset(1)

        End If
    Next in_row

    numCount = out_next.Row - OutHeader.Row - 1
    If numCount < 1 Then  '有効データが無い場合
        out_base.ClearContents    '一行目のデータをクリア。
    End If

    ' 抽出したデータ数を返す。
    ProblemsByFuncDataSelect = numCount

End Function

' 集計
Private _
Function ProblemsByFuncDataCount(ByVal InHeader As Range, _
                                 ByVal OutHeaderBase As Range, _
                                 ByVal intOutDataBaseCount As Long, _
                                 ByVal numCount As Long, _
                                 ByVal intTargetGraphNo As Long) As Long

    Const strDefaultFunc    As String = "(未設定)"
    Dim numIdCol    As Long '「ID」列番号
    Dim numFuncCol  As Long '分類項目
    Dim rr  As Range
    Dim strTmp As String
    Dim OutHeader As Range
    Dim numResult As Long

    ProblemsByFuncDataCount = -1

    Set OutHeader = OutHeaderBase.Offset(0, intTargetGraphNo * intCountTableOffset).Resize(1, 2)

    If numCount < 1 Then  '有効データ無し
        numResult = 0
        GoTo Exit_Proc
    End If

    strTmp = strGroupingName(intTargetGraphNo)
    With InHeader.Offset(-1)
        numIdCol = StatisticsLibrary2.GetDataColumn(.Cells(1, 1), "ID") - .Column + 1
        If numIdCol < 1 Then Exit Function
    
        numFuncCol = StatisticsLibrary2.GetDataColumn(.Cells(1, 1), strTmp) - .Column + 1
        If numFuncCol < 1 Then Exit Function
    End With

    ' 分類項目の値が未設定の場合はダミー文字列を設定
    With InHeader.Columns(numFuncCol).Offset(1).Resize(numCount)
        For Each rr In .Cells
            If rr.Value = "" Then
                rr.Value = strDefaultFunc
                rr.Font.Color = vbRed
            End If
        Next rr
    End With

    '-------------------------------
    ' 分類項目の値毎の問題件数カウント
    '       ※書式シート内の関数で算出される
    '-------------------------------
    ' 出力域初期化
    If intOutDataBaseCount = 0 Then
        Call StatisticsLibrary2.ClearRangeToEndRow(OutHeader.Offset(2))
    Else
        ' 複数行が指定されている場合は、下に数式があるはずなので指定範囲の行数を調整する。
        Call MyRowsResize(OutHeader.Offset(1).Resize(intOutDataBaseCount), numCount)
        If numCount > 1 Then
            OutHeader.Offset(2).Resize(numCount - 1).Clear
        End If
    End If

    Set rr = OutHeader.Columns(1).Offset(1)
    If numCount > 1 Then
        rr.Copy Destination:=rr.Offset(1).Resize(numCount - 1) '一行目の書式をコピー
    End If

    ' 分類項目の値のリスト生成
    InHeader.Columns(numFuncCol).Offset(1).Resize(numCount).Copy _
        Destination:=rr '分類項目の値のリストをコピー

    If numCount > 1 Then
        ' 分類項目の値順にソート
        rr.Resize(numCount).Sort Header:=xlNo, key1:=rr, Orientation:=xlTopToBottom

        ' 重複除去
        OutHeader.Offset(1).Resize(numCount).RemoveDuplicates Columns:=1, Header:=xlNo
    End If

    Set rr = StatisticsLibrary2.GetEndToUp(OutHeader.Offset(numCount).Columns(1))
    numResult = rr.Row - OutHeader.Row  '分類項目の値の個数

    If numCount > numResult Then
        OutHeader.Offset(numResult + 1).Resize(numCount - numResult).Delete Shift:=xlShiftUp
    End If

    ' カウント式をコピー
    If numResult > 1 Then
        Set rr = OutHeader.Columns(2).Offset(1)
        rr.Copy Destination:=rr.Offset(1).Resize(numResult - 1) '一行目の式をコピー
    End If

'    ' 式を値に置き換える
'    With rr.Resize(numResult)
'        .Worksheet.Calculate
'        .Value = .Value
'    End With

Exit_Proc:
    If numResult < 1 Then
        ' 出力域クリア。一行目の罫線は残す。
        OutHeader.Offset(1).ClearContents
        Call StatisticsLibrary2.ClearRangeToEndRow(OutHeader.Offset(2))
    End If

    ProblemsByFuncDataCount = numResult  '有効データ数を返す。

End Function

' グラフの補整
Private _
Function ProblemsByFuncGraphAdjust(ByVal wsGraph As Worksheet, _
                                   ByVal numCount As Long, _
                                   ByVal intTargetGraphNo As Long) As Boolean

    Const numAxis   As Long = 1
    Dim rngAxis(numAxis) As Range
    Dim objChart As ChartObject
    Dim objSrs As Series
    Dim ss  As String
    Dim ii As Long
    Dim c1  As Range
    Dim varTmp As Variant
    Dim strTmp As String

    ProblemsByFuncGraphAdjust = False

    On Error GoTo Exit_Proc

    ' グラフを取り出す。
    strTmp = strGroupingName(intTargetGraphNo) & "別グラフ"
    Set objChart = Nothing
    For Each varTmp In wsGraph.ChartObjects
        If varTmp.Name = strTmp Then
            Set objChart = varTmp
            Exit For
        End If
    Next varTmp
    If objChart Is Nothing Then
        Call MsgBox("'" & wsGraph.Name & "'シートにグラフ「" & strTmp & "」がありません", vbCritical)
        GoTo Exit_Proc  '不正なシート
    End If

    For ii = 0 To numAxis
        Select Case ii
        Case 0
            ss = strDefName_Axis0
        Case 1
            ss = strDefName_Axis1
        End Select

        Set rngAxis(ii) = StatisticsLibrary2.名前の参照範囲取得(wsGraph, ss)
        If rngAxis(ii) Is Nothing Then
            GoTo Exit_Proc  '不正なシート
        ElseIf rngAxis(ii).Columns.Count > 1 Then
            Call MsgBox("'" & wsGraph.Name & "!" & ss & " の名前定義が不正です", vbCritical)
            GoTo Exit_Proc  '不正なシート
        ElseIf rngAxis(ii).Rows.Count > 1 Then
            Set rngAxis(ii) = rngAxis(ii).Rows(1)
        End If

        Set rngAxis(ii) = rngAxis(ii).Offset(0, intTargetGraphNo * intCountTableOffset)
    Next ii

    If numCount > 0 Then
        ' 値の範囲取得
        For ii = 0 To numAxis
            Set rngAxis(ii) = rngAxis(ii).Offset(1).Resize(numCount)
        Next ii

        If objChart.Chart.SeriesCollection.Count <> numAxis Then
            Call MsgBox("'" & wsGraph.Name & "'シートのグラフの系列数が " & numAxis & " ではありません", vbCritical)
            GoTo Exit_Proc  '不正なシート
        End If

        ' グラフ内の系列毎に処理する。
        For ii = 1 To objChart.Chart.SeriesCollection.Count
            ' データ範囲設定
            Set objSrs = objChart.Chart.SeriesCollection(ii)
            ss = rngAxis(ii).Rows(1).Offset(-1).Address(External:=True) '凡例に表示される名前
            ss = ss & "," & rngAxis(0).Address(External:=True)    '項目軸に表示されるラベル
            ss = ss & "," & rngAxis(ii).Address(External:=True)   'プロットされる値
            ss = ss & "," & ii     '系列のプロット順
            objSrs.Formula = "=SERIES(" & ss & ")"
        Next ii
    End If

    On Error GoTo 0

    ProblemsByFuncGraphAdjust = True

Exit_Proc:
    If Err.Number <> 0 Then
        Call MsgBox("グラフ補整でエラー発生。" & vbCrLf & vbCrLf & Err.Description, vbCritical)
    End If

    Set objChart = Nothing
    Set objSrs = Nothing
End Function
