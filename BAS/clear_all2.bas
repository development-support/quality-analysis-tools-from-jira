Attribute VB_Name = "clear_all2"
Sub clear_all_Click2()

  '
  ' 更新:2016.08.02
  '

  Dim MaxRow, MaxCol As Integer
  Dim ws As Worksheet

  Set ws = Worksheets(gJIRASheetName)

  'クリア範囲を指定
  ws.Activate
  ws.Range("A1") = " "

  '使用済み最終行
  With ActiveSheet.UsedRange
    MaxRow = .Find("*", , xlFormulas, , xlByRows, xlPrevious).Row
    MaxCol = .Find("*", , xlFormulas, , xlByColumns, xlPrevious).Column
  End With
 
  If MaxRow > 2 Then
    ' 1行目から最終行まで
    ws.Range("A1:" & Cells(MaxRow, MaxCol).Address) = ""
    ' 掛け線削除　追加:2016.08.02
    ws.Range("A1:" & Cells(MaxRow, MaxCol).Address).ClearFormats
    ws.Range("A1:" & Cells(MaxRow, MaxCol).Address).NumberFormatLocal = "@"
    ws.Range("A1:" & Cells(MaxRow, MaxCol).Address).Interior.ColorIndex = -4142
  End If

End Sub
