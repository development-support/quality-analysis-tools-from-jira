Attribute VB_Name = "StatisticsTookDays"
' *****************************
'   Version    Comment
'   2017.06.09 Create New
'   2017.06.13 "Rejected Count"が漏れたので修正
'   2018.10.04 スライサーにFunctional Areaを追加
' *****************************
Option Explicit

' 定数
Private Const strVersion As String = "2018.10.04"
Private Const strJobName As String = "仕掛日数別問題数集計"
Private Const strJiraSheetName As String = "Problem2"
Private Const strWorkSheetName As String = "仕掛日数別問題数集計_WorkTable"
Private Const strPivotTableName As String = "仕掛日数別問題数集計"
Private Const strPivotGraphName As String = "仕掛日数別問題数集計_PivotGraph"
Private Const strTitle As String = "Number of Issues by Turn Around Time"

'　仕掛日数別問題数集計
Public Sub TookDaysExecute(Optional ByVal booEndMessage As Boolean = True)
    Application.ScreenUpdating = False

    '    Dim booEndMessage As Boolean
    '    booEndMessage = True

    Dim boolEndStatus As Boolean
    boolEndStatus = False

    ' Statusバー表示
    StatisticsLibrary2.JobBegin (strJobName & "中...")

    ' シートコピー
    If Not SheetCopy(strJiraSheetName, strWorkSheetName) Then
        GoTo Exit_Proc
    End If

    ' 日付変換
    ConvertDate strWorkSheetName

    ' 集計データ追加
    DataAddForTAT strWorkSheetName

    ' ピボットテーブル生成
    PivotTableGenerate strWorkSheetName, strPivotTableName

    ' ピボットグラフ生成
    PivotGraphGenerate strPivotTableName, strPivotGraphName, strTitle

    同名のワークシートを非表示 strWorkSheetName

    実行ボタン生成

    ' コメント生成
    InsertComment strPivotTableName, strVersion

    boolEndStatus = True

Exit_Proc:
    ' 後処理
    If boolEndStatus = False Then
        Call StatisticsLibrary2.JobEnd(strJobName & "中止", MsgBoxShow:=False)
    Else
        Call StatisticsLibrary2.JobEnd(strJobName & "処理終了", MsgBoxShow:=booEndMessage)
    End If

End Sub
 
' Sheetコピー
Private Function SheetCopy(ByVal strJiraSheetName As String, _
                           ByVal strOutSheetName As String) As Boolean
    Dim wsData As Worksheet
    Dim boolEndStatus As Boolean
    Dim strSheetName As String

    SheetCopy = False

    If Not 同名のワークシートの存在Check(strJiraSheetName) Then
        Exit Function
    End If

    同名のワークシートを表示 strOutSheetName
    同名のワークシートを削除 strOutSheetName
    同名のワークシートが無ければコピー strJiraSheetName, strOutSheetName

    Set wsData = Nothing
    SheetCopy = True

End Function

' 日付変換
Private Sub ConvertDate(ByVal strInSheetName As String)
    Dim intEndColumn As Integer
    Dim intEndRow As Integer
    Dim i As Integer

    Sheets(strInSheetName).Select

    intEndColumn = GetTargetRowEndColumn(1)
    intEndRow = GetTargetColumnEndRow(1)

    ' 日付の変更
    ' Pivotで日付フィルタを可能とする為、空白->Emptyとする。
    Dim varTmp As Variant
    Dim intTmp As Integer
    Dim strDateTargets() As String
    ReDim strDateTargets(6) As String
    strDateTargets(0) = "Created"
    strDateTargets(1) = "Last Viewed"
    strDateTargets(2) = "Updated"
    strDateTargets(3) = "Resolved"
    strDateTargets(4) = "Due Date"
    strDateTargets(5) = "Target Fix Date"
    strDateTargets(6) = "Rejected"

    For Each varTmp In strDateTargets
        intTmp = GetTargetStringColumn(Rows(1), varTmp, True)
        If (intTmp > 0) Then
            For i = 2 To intEndRow
                If (Cells(i, intTmp) = "") Then
                    Cells(i, intTmp).Value = Empty
                End If
            Next
            Columns(intTmp).Replace what:="-", Replacement:="/", LookAt:=xlPart, _
                                    SearchOrder:=xlByRows, MatchCase:=False, _
                                    SearchFormat:=False, ReplaceFormat:=False
            Columns(intTmp).NumberFormatLocal = "yyyy/mm/dd"
        End If
    Next

End Sub

' 仕掛日数用データ追加
Private Sub DataAddForTAT(ByVal strInSheetName As String)
    Dim intEndColumn As Integer
    Dim intEndRow As Integer
    Dim intResolvedColumn As Integer
    Dim intCreatedColumn As Integer
    Dim datCreated As Date
    Dim datResolved As Date
    Dim i As Integer
    Dim intRejectedColumn As Integer
    Dim intRejectedCountColumn As Integer

    Sheets(strInSheetName).Select

    intEndColumn = GetTargetRowEndColumn(1)
    intEndRow = GetTargetColumnEndRow(1)

    ' TAT算出
    intCreatedColumn = GetTargetStringColumn(Rows(1), "Created", True)
    intResolvedColumn = GetTargetStringColumn(Rows(1), "Resolved", True)

    Columns(intEndColumn).Insert Shift:=xlToRight
    Cells(1, intEndColumn).Value = "TAT"

    For i = 2 To intEndRow
        If (Cells(i, intResolvedColumn) = "") Then
            datResolved = Format(Now, "yyyy/mm/dd")
        Else
            If IsDate(Format(Cells(i, intResolvedColumn), "yyyy/mm/dd")) Then
                datResolved = Format(Cells(i, intResolvedColumn), "yyyy/mm/dd")
            Else
                MsgBox """Resolved""に日付以外が入力されています。処理を中止します。行番号:" & i, vbCritical, "ErrorMessage"
                End
            End If
        End If

        If IsDate(Format(Cells(i, intCreatedColumn), "yyyy/mm/dd")) Then
            datCreated = Format(Cells(i, intCreatedColumn), "yyyy/mm/dd")
        Else
            MsgBox """Created""に日付以外が入力されています。処理を中止します。行番号:" & i, vbCritical, "ErrorMessage"
            End
        End If

        Cells(i, intEndColumn).Value = DateDiff("d", datCreated, datResolved) + 1
        Cells(i, intEndColumn).Borders.LineStyle = xlDash
    Next

    ' Rejected Count = 0時のRejectedを削除
    intRejectedColumn = GetTargetStringColumn(Rows(1), "Rejected", True)
    intRejectedCountColumn = GetTargetStringColumn(Rows(1), "Rejected Count", True)

    If (intRejectedCountColumn > 0) Then
        For i = 2 To intEndRow
            If (Cells(i, intRejectedCountColumn) = 0) And (Cells(i, intRejectedColumn) <> "") Then
                Cells(i, intRejectedColumn).Value = Empty
                Cells(i, intRejectedColumn).Interior.Color = 8420607
            End If
        Next
    End If

End Sub

' PivotTable生成
Private Sub PivotTableGenerate(ByVal strInSheetName As String, _
                               ByVal strOutTableName As String)
    Dim strTmp As String
    Dim strSliceTargets() As String
    Dim strStatusTargets() As String
    Dim i, j As Integer
    Dim varTmp As Variant
    Dim intRejectedCountColumn As Integer

    Sheets(strInSheetName).Select
    intRejectedCountColumn = GetTargetStringColumn(Rows(1), "Rejected Count", True)

    同名のワークシートを削除 strOutTableName

    ' Table生成
    ActiveSheet.ListObjects.Add(xlSrcRange, Range("A1").CurrentRegion, , xlYes).Name = strInSheetName
    Range(strInSheetName & "[#All]").Select
    Sheets.Add
    ActiveSheet.Name = strOutTableName

    ' PivotTable生成
    Dim objPivotCash As PivotCache
    Dim objPivotTable As PivotTable

    Set objPivotCash = ActiveWorkbook.PivotCaches.Create( _
        SourceType:=xlDatabase, _
        SourceData:=strInSheetName, _
        Version:=xlPivotTableVersion14)

    Set objPivotTable = objPivotCash.CreatePivotTable( _
        TableDestination:=strOutTableName & "!R52C1", _
        TableName:=strOutTableName, _
        DefaultVersion:=xlPivotTableVersion14)

    ' PivotTableパラメータ
    With objPivotTable
        
        With .PivotFields("Status")
            .Orientation = xlColumnField
            .Position = 1
        End With
        .AddDataField .PivotFields("TAT"), "データの個数 / TAT", xlCount
        With .PivotFields("TAT")
            .Orientation = xlRowField
            .Position = 1
        End With
        With .PivotFields("Key")
            .Orientation = xlRowField
            .Position = 2
        End With
        With .PivotFields("How-Found")
            .Orientation = xlRowField
            .Position = 3
        End With
        With .PivotFields("Component/s")
            .Orientation = xlRowField
            .Position = 4
        End With
        With .PivotFields("Assignee")
            .Orientation = xlRowField
            .Position = 5
        End With
        With .PivotFields("Created")
            .Orientation = xlRowField
            .Position = 6
        End With
        With .PivotFields("Resolved")
            .Orientation = xlRowField
            .Position = 7
        End With
        With .PivotFields("Due Date")
            .Orientation = xlRowField
            .Position = 8
        End With
        With .PivotFields("Target Fix Date")
            .Orientation = xlRowField
            .Position = 9
        End With
        With .PivotFields("Rejected")
            .Orientation = xlRowField
            .Position = 10
        End With
        With .PivotFields("Priority")
            .Orientation = xlRowField
            .Position = 11
        End With
        With .PivotFields("Severity")
            .Orientation = xlRowField
            .Position = 12
        End With
        If (intRejectedCountColumn > 0) Then
            With .PivotFields("Rejected Count")
                .Orientation = xlRowField
                .Position = 13
            End With
        End If

'        .PivotFields("TAT").AutoSort xlDescending, "データの個数 / TAT"
        .PivotSelect "TAT[All]", xlLabelOnly + xlFirstRow, True
        Selection.ShowDetail = False

        ' Statusの並べ替え
        ReDim strStatusTargets(5) As String
        strStatusTargets(0) = "open"
        strStatusTargets(1) = "in development"
        strStatusTargets(2) = "waiting to build"
        strStatusTargets(3) = "ready for test"
        strStatusTargets(4) = "rca_pending"
        strStatusTargets(5) = "done"

        j = 0
        For i = 0 To 5
            For Each varTmp In .PivotFields("Status").PivotItems
                If (LCase(varTmp.Name) = LCase(strStatusTargets(i))) Then
                    j = j + 1
                    varTmp.Position = j
                    Exit For
                End If
            Next
        Next

        ' Top-20
'        .PivotFields("Assignee").PivotFilters.Add _
'        Type:=xlTopCount, DataField:=.PivotFields("データの個数 / Key"), Value1:=20

        ' 小計を表示しない
        For Each varTmp In .PivotFields
            For i = 1 To 12
                varTmp.Subtotals(i) = False
            Next
        Next

    End With

    ' スライス
    ReDim strSliceTargets(7)
    strSliceTargets(0) = "Functional Area"
    strSliceTargets(1) = "Priority"
    strSliceTargets(2) = "Severity"
    strSliceTargets(3) = "Issue Type"
    strSliceTargets(4) = "How-Found"
    strSliceTargets(5) = ""
    strSliceTargets(6) = "Affects Version/s"
    strSliceTargets(7) = "Fix Version/s"

    For i = LBound(strSliceTargets) To UBound(strSliceTargets)
        If (i <> 5) Then
            strTmp = strSliceTargets(i)

            ActiveWorkbook.SlicerCaches.Add(objPivotTable, strTmp).Slicers.Add _
                ActiveSheet, , , , 12 + (i Mod 3) * 170, 740 + ((i \ 3) * 160), 144, 165.5
        End If
    Next

End Sub

' グラフ作成
Private Sub PivotGraphGenerate(ByVal strInSheetName As String, _
                               ByVal strOutGraphName As String, _
                               ByVal strTitle As String)
    Dim objPivotTable As PivotTable
    Dim objChartObject As ChartObject
    Dim objChart As Chart

    Sheets(strInSheetName).Select
    Set objPivotTable = ActiveSheet.PivotTables(strInSheetName)
    Set objChartObject = ActiveSheet.ChartObjects.Add(0, 12, 722, 336)
    Set objChart = objChartObject.Chart

    objChart.ChartType = xlColumnStacked
    objChart.SetSourceData objPivotTable.TableRange1
    objChartObject.Name = strOutGraphName

    ' 配色
    Dim ser As Series
    For Each ser In objChart.SeriesCollection
        ser.ApplyDataLabels
        If (LCase(ser.Name) = "done") Then
            ser.Format.Fill.ForeColor.RGB = RGB(0, 112, 192)
        ElseIf (LCase(ser.Name) = "rca_pending") Then
            ser.Format.Fill.ForeColor.RGB = RGB(0, 176, 80)
        ElseIf (LCase(ser.Name) = "ready for test") Then
            ser.Format.Fill.ForeColor.RGB = RGB(146, 208, 80)
        ElseIf (LCase(ser.Name) = "waiting to build") Then
            ser.Format.Fill.ForeColor.RGB = RGB(255, 255, 0)
        ElseIf (LCase(ser.Name) = "in development") Then
            ser.Format.Fill.ForeColor.ObjectThemeColor = msoThemeColorAccent6
        ElseIf (LCase(ser.Name) = "open") Then
            ser.Format.Fill.ForeColor.RGB = RGB(255, 0, 0)
        End If
    Next

    With objChart
        ' Title設定
        .HasTitle = True
        .ChartTitle.text = strTitle
        .ChartTitle.Format.TextFrame2.TextRange.Font.Size = 12
        .ChartTitle.Format.TextFrame2.TextRange.Font.Bold = msoTrue

        ' 縦軸のタイトル
        .Axes(xlValue, xlPrimary).HasTitle = True
        .Axes(xlValue, xlPrimary).AxisTitle.Characters.text = "Issues"
        .Axes(xlValue, xlPrimary).TickLabels.NumberFormatLocal = "G/標準"

        ' 横軸のタイトル
        '.Axes(xlCategory).TickLabels.Orientation = xlUpward

        .Axes(xlCategory, xlPrimary).HasTitle = True
        .Axes(xlCategory, xlPrimary).AxisTitle.Characters.text = "TAT(Days)"
        .Axes(xlCategory, xlPrimary).TickLabels.NumberFormatLocal = "G/標準"

    End With

End Sub

' コメント挿入
Private Sub InsertComment(ByVal strInSheetName As String, _
                          ByVal strVersion As String)
    Range("A31").Select
    ActiveCell.Value = strJobName
    ActiveCell.Offset(1, 0).Value = "　Version: " & strVersion
    ActiveCell.Offset(2, 0).Value = ""
    ActiveCell.Offset(3, 0).Value = "  仕掛日数(TAT)毎のIssueの数をStatusで分割した積み上げ棒グラフを作成します。"
    ActiveCell.Offset(4, 0).Value = ""
    ActiveCell.Offset(5, 0).Value = "　Statusでフィルタを行う場合は、グラフの'Status'のアイコンをクリック→フィルタ条件を設定して下さい。"
    ActiveCell.Offset(6, 0).Value = "　Status以外のフィルタを行う場合は、グラフ下のフィルタ又はグラフ右のスライサー※2からフィルタ条件を設定して下さい。"
    ActiveCell.Offset(7, 0).Value = ""
    ActiveCell.Offset(8, 0).Value = "  仕掛日数(TAT)毎のIssueのKey番号を確認する場合は、下記のPivotTableの該当TATの左のアイコンをクリック、"
    ActiveCell.Offset(9, 0).Value = "    又は任意のTATを選択→右クリック→'展開/折りたたみ'→'フィールド全体を展開'を選択して下さい。"
    ActiveCell.Offset(10, 0).Value = ""
    ActiveCell.Offset(11, 0).Value = "  下記のPivotTableを表形式で表示する場合、"
    ActiveCell.Offset(12, 0).Value = "    PivotTable選択→リボンの'ピボットテーブル ツール'-'デザイン'-'レポートのレイアウト'-'表形式で選択'を選択後、"
    ActiveCell.Offset(13, 0).Value = "    任意の行ラベルを右クリック→'展開/折り畳み'→'フィールド全体の展開'を選択して下さい。"
    ActiveCell.Offset(14, 0).Value = ""
    ActiveCell.Offset(15, 0).Value = "    ※1 表示する仕掛日数は変更可能です。"
    ActiveCell.Offset(16, 0).Value = "         下記のPivotTableにて、'行ラベル'(セル:C53)の右のフィルタのアイコンをクリック→'ラベルフィルタ'よりフィルタ条件を選択"
    ActiveCell.Offset(17, 0).Value = "    ※2 スライサーは追加可能です。"
    ActiveCell.Offset(18, 0).Value = "         下記のPivotTableを選択後、リボンの'ピボットテーブル ツール'-'オプション'-'スライサー'を選択し、"
    ActiveCell.Offset(19, 0).Value = "         必要となるスライサーを追加してください。"

    Range("A31:A50").Select
    With Selection.Font
        .Name = "ＭＳ Ｐゴシック"
        .Size = 10
    End With

    ' 枠線の削除
    Cells.Select
    ActiveWindow.DisplayGridlines = False
    Range("A1").Select

End Sub

Private Sub 実行ボタン生成()

    ActiveSheet.Buttons.Add(192.75, 360, 80, 24).Select
    Selection.OnAction = "TookDaysExecute"
    'Selection.Characters.text = strJobName
    Selection.Characters.text = "再集計"
    Selection.Characters.Font.Name = "ＭＳ Ｐゴシック"
    Selection.Characters.Font.Size = 10

End Sub

'　検索文字列の列番号を取得
Private Function GetTargetStringColumn(ByVal rangeTarget As Range, _
                                       ByVal strTargetString As String, _
                                       Optional boofull As Boolean = False) As Integer
    Dim intReturn As Integer
    Dim Foundcell As Range

    If (boofull) Then
        Set Foundcell = rangeTarget.Find(what:=strTargetString, _
                                        LookIn:=xlValues, _
                                        LookAt:=xlWhole, _
                                        MatchCase:=False, _
                                        MatchByte:=False)
    Else
        Set Foundcell = rangeTarget.Find(what:=strTargetString, _
                                        LookIn:=xlValues, _
                                        LookAt:=xlPart, _
                                        MatchCase:=False, _
                                        MatchByte:=False)
    End If

    If Foundcell Is Nothing Then
        intReturn = 0
    Else
        intReturn = Foundcell.Column
    End If

    GetTargetStringColumn = intReturn

End Function

