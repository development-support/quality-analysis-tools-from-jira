Attribute VB_Name = "StatisticsTtypematrix"
Option Explicit
'-----------------------------------------------------------
' 【機能】
'    各問題から検出工程､発見すべき工程､混入工程を抽出し､
'    　・発見工程−発見すべき工程
'    　・発見工程−混入工程
'      の各マトリックスに対応する件数を算出する
'    T型マトリックスに件数を設定する
'-----------------------------------------------------------

Private Const strTemplateSheetName  As String = "【書式】T型マトリクス"

Private Const strGraphNamePrefix  As String = "TTypeMatrix"

Private Const strGeneSourceAddr As String = "D1"  '実行ソース出力位置
Private Const strGeneTimeAddr As String = "D2" '実行日時出力位置

Private strGeneSource As String

'' 参照する定義名
' マトリクス定義
Private Const strDefName_ItemList As String = "TTypeMatrix_ItemList"
Private Const strDefName_IndexTab As String = "TTypeMatrix_IndexTab"
Private Const strDefName_DownAxis As String = "TTypeMatrix_DownAxis"
Private Const strDefName_LeftAxis As String = "TTypeMatrix_LeftAxis"
Private Const strDefName_RightAxis As String = "TTypeMatrix_RightAxis"
Private Const strDefName_LeftItemList As String = "TTypeMatrix_LeftItemList"
Private Const strDefName_RightItemList As String = "TTypeMatrix_RightItemList"

' 参照データ初期設定用
Private Const strDefName_SelectField As String = "TTypeMatrix_参照項目"

Private Type TTypeMatrixParamType
    numVertOrig     As Long '縦軸原点の行
    numLeftOrig     As Long '左軸原点の列
    numRightOrig    As Long '右軸原点の列
    
    '0:左軸, 1:縦軸, 2:右軸 ※strDefName_IndexTabの列に対応
    numMatrixSize(2)    As Long 'グラフの項目数
    dictItem(2) As Scripting.Dictionary '項目名⇒インデクスの変換表
End Type

Private intDataSelectType As Long '抽出条件種別

Private Sub matrixParamClear(matrixParam As TTypeMatrixParamType)
    Dim i
    For i = 0 To 2
        Set matrixParam.dictItem(i) = Nothing
    Next i
End Sub

Public _
Sub TTypeMatrixExecute(Optional ByVal booEndMessage As Boolean = True)
    Const strJobName    As String = "T型マトリックスの生成"
    Dim wsData  As Worksheet    '入力データシート
    Dim boolEndStatus   As Boolean

    If ProblemDataBaseTypeGet() = "Redmine" Then
        Select Case TargetProjectNameGet()
        Case "S100"
            intDataSelectType = 1

        Case "L100", "L120"
            intDataSelectType = 2

        Case Else
            Call 未実装(booEndMessage)
            Exit Sub
        End Select

    Else
        intDataSelectType = 3
    End If

    If booEndMessage Then
        If MsgBox(strJobName & "を開始します。", vbOKCancel) <> vbOK Then
            Exit Sub
        End If
    End If

    boolEndStatus = False

    ' 前処理
    Call StatisticsLibrary2.JobBegin(strJobName & "中...")

    ' 入力シート取得
    Set wsData = StatisticsLibrary2.問題管理データシート取得()
    If wsData Is Nothing Then
        GoTo Exit_Proc
    End If

    ' T型マトリックス生成
    If TTypeMatrixCreate(wsData) = False Then
        GoTo Exit_Proc
    End If

    boolEndStatus = True

Exit_Proc:
    ' 後処理
    If boolEndStatus = False Then
        Call StatisticsLibrary2.JobEnd(strJobName & "中止", MsgBoxShow:=False)
    Else
        Call StatisticsLibrary2.JobEnd(strJobName & "処理終了", MsgBoxShow:=booEndMessage)
    End If

    Set wsData = Nothing

End Sub

Private _
Function TTypeMatrixConfigGet(ByRef strGruop() As String, _
                              ByRef strTempl() As String) As Long
    Dim a1()  As String
    Dim a2()  As String
    Dim a3()  As String
    Dim i   As Long
    Dim j   As Long

    a1 = StatisticsLibrary.GetStringSettingParam("TTypeMatrix_setting", intGetColumn:=1)
    a2 = StatisticsLibrary.GetStringSettingParam("TTypeMatrix_setting", intGetColumn:=2)
    a3 = StatisticsLibrary.GetStringSettingParam("TTypeMatrix_setting", intGetColumn:=3)

    j = 0
    For i = 0 To UBound(a1)
        If a2(i) = "○" Then
            ReDim Preserve strGruop(j)
            strGruop(j) = a1(i)
            ReDim Preserve strTempl(j)
            strTempl(j) = a3(i)
            
            j = j + 1
        End If
    Next i

    Erase a1
    Erase a2
    Erase a3

    TTypeMatrixConfigGet = j '指定されたグループの数を返す。

End Function

' Ｔ型マトリックスの項目名リストを取得
Private _
Function TTypeMatrixItemListGet(ByVal ws As Worksheet, _
                                ByRef aResult() As Range) As Boolean
    TTypeMatrixItemListGet = False

    Set aResult(1) = StatisticsLibrary2.名前の参照範囲取得(ws, strDefName_ItemList)
    Set aResult(0) = StatisticsLibrary2.名前の参照範囲取得(ws, strDefName_LeftItemList, NoMsg:=True)
    Set aResult(2) = StatisticsLibrary2.名前の参照範囲取得(ws, strDefName_RightItemList, NoMsg:=True)

    Dim i As Integer
    For i = 0 To 2
        If aResult(i) Is Nothing Then
            If i = 1 Then
                Exit Function  '不正なGraphシート
            End If
        ElseIf aResult(i).Rows.Count > 1 And aResult(i).Columns.Count > 1 Then
            Call MsgBox("項目名リストは、１行または１列で指定する事", vbCritical)
            Exit Function '不正なGraphシート
        End If
    Next i

    TTypeMatrixItemListGet = True
End Function

'　Ｔ型マトリックスのインデックス値格納先を取得
Private _
Function TTypeMatrixIndexTabGet(ByVal ws As Worksheet) As Range
    Dim rr  As Range

    Set rr = StatisticsLibrary2.名前の参照範囲取得(ws, strDefName_IndexTab)
    If Not (rr Is Nothing) Then
        If rr.MergeCells Then ' 結合セル
            Set rr = Nothing
        ElseIf rr.Areas.Count > 1 Then
            Set rr = Nothing
        ElseIf rr.Rows.Count > 1 Then
            Set rr = Nothing
        Else
            Select Case rr.Columns.Count
            Case Is < 3
                Set rr = rr.Resize(1, 3)
            Case Is > 4
                Set rr = Nothing
            End Select
        End If

        If rr Is Nothing Then
            Call MsgBox(ws.Name & "シートの名前定義「" & strDefName_IndexTab & "」" & vbCrLf _
                            & "の参照範囲に誤りがあります", vbCritical)
        End If
    End If

    Set TTypeMatrixIndexTabGet = rr
End Function

'Redmineデータシートから抽出するデータの名称格納位置を取得
Private _
Function TTypeMatrixSelectName(ByVal ws As Worksheet) As Range
    Dim rr  As Range

    Set rr = StatisticsLibrary2.名前の参照範囲取得(ws, strDefName_SelectField)
    If StatisticsLibrary2.MyRegionColumnSort(rr) = False Then
        Set rr = Nothing
    End If

    Set TTypeMatrixSelectName = rr
End Function

' 出力シート名生成
Private _
Function TTypeMatrixOutNameGet(ByVal strTargetGroup As String) As String
    If strTargetGroup = "*" Then
        strTargetGroup = ""
    End If
    TTypeMatrixOutNameGet = 集計用出力シート名生成(strGraphNamePrefix & "_" & strTargetGroup)
End Function

' 出力書式シート表示／非表示
Public _
Sub TTypeMatrixFormatSheetDisp()
    Call StatisticsLibrary2.FormatSheetVisibleReverse(strTemplateSheetName)
End Sub

Public _
Function TTypeMatrixCreate(ByVal wsData As Worksheet) As Boolean
'    Dim strProjID       As String
    Dim aryDataCol()    As Long
    Dim wsTempl As Worksheet
    Dim wsGraph As Worksheet
    Dim strTempls() As String
    Dim strGroups() As String
    Dim strGraphName    As String   '出力シート名
    Dim strTemplName    As String   '書式シート名
    Dim strGroupName    As String   '出力対象グループ名
    Dim matrixParam As TTypeMatrixParamType
    Dim numCount    As Long ' 有効データ数
    Dim ii  As Long

    TTypeMatrixCreate = False

    '---------------------------------------------------
    ' グループ共通の前処理
    '---------------------------------------------------
    strGeneSource = TargetProjectNameGet()

    If TTypeMatrixConfigGet(strGroups, strTempls) < 1 Then
        TTypeMatrixCreate = True  ' 実行対象指定なし
        GoTo Exit_Proc
    End If

    Select Case StatisticsLibrary2.RedmineSheetDataCount(wsData)
    Case Is < 0 '不正なシート
        Exit Function
    Case 0
        MsgBox "'" & wsData.Name & "'シートに有効なデータがありません"
        Exit Function
    End Select

'    ' プロジェクトＩＤ取得
'    strProjID = StatisticsLibrary2.RedmineSheetProjectID(wsData)
'    If strProjID = "" Then
'        Exit Function
'    End If

    '---------------------------------------------------
    ' グループ毎の処理
    '---------------------------------------------------
    For ii = 0 To UBound(strGroups)
        strGroupName = strGroups(ii)
        strTemplName = strTempls(ii)

        '---------------------------------------------------
        ' 前処理
        '---------------------------------------------------
        strGraphName = TTypeMatrixOutNameGet(strGroupName) '出力シート名生成
        If strGraphName = "" Then
            GoTo Exit_Proc
        End If

        Call StatisticsLibrary.同名のワークシートを削除(strGraphName)

         ' 出力書式シート取得
        Set wsTempl = StatisticsLibrary2.同名のシート取得(strTemplName)
        If wsTempl Is Nothing Then
            GoTo Exit_Proc
        End If

        ' Redmineシートから抽出するデータの列番号を取得
        If TTypeMatrixSelectColumn(wsTempl, wsData, aryDataCol) = False Then
            GoTo Exit_Proc  '列番号取得ＮＧ
        End If

        ' Ｔ型マトリックスの軸定義取り出し＆項目名からインデクスの変換表を生成
        If TTypeMatrixAxisGet(wsTempl, matrixParam, 項目名辞書生成:=True) = False Then
            GoTo Exit_Proc
        End If

        ' 出力シート生成
        Set wsGraph = StatisticsLibrary2.FormatSheetCopy(wsTempl, strGraphName)
        If wsGraph Is Nothing Then
            GoTo Exit_Proc
        End If
        Set wsTempl = Nothing

        ' 生成日時とデータソースを設定
        With wsGraph.Range(strGeneSourceAddr)
            .NumberFormatLocal = "@"
            .Value = strGroupName & "/" & strGeneSource
        End With
'        With wsGraph.Range(strGeneTimeAddr)
'            .Value = dateJobStart
'        End With

        '---------------------------------------------------
        ' Redmineシートからデータ抽出
        '---------------------------------------------------
        numCount = TTypeMatrixDataSelect(wsData, aryDataCol, strGroupName, wsGraph, matrixParam)
        If numCount < 0 Then
            GoTo Exit_Proc  '異常発生
        End If

        '---------------------------------------------------
        ' T型マトリックスにカウント結果を記入する。
        '---------------------------------------------------
        If TTypeMatrixValueSet(wsGraph, matrixParam, False, numCount) = False Then
            GoTo Exit_Proc  '異常発生
        End If

        '---------------------------------------------------
        ' 後処理
        '---------------------------------------------------
        Call MyWsCalculate(wsGraph)

'        ' 式を値に置き換える
'        With wsGraph.UsedRange
'            .Value = .Value
'        End With

        Call matrixParamClear(matrixParam)

    Next ii

    TTypeMatrixCreate = True

Exit_Proc:
    Erase strGroups
    Erase strTempls
    Set wsTempl = Nothing
    Call matrixParamClear(matrixParam)

End Function

Public Sub TTypeMatrixUpdate()

    Dim wsDst As Worksheet
    Dim matrixParam As TTypeMatrixParamType
    Dim boolEndStatus   As Boolean
    Const strJobName As String = "T型マトリクス再計算"

    boolEndStatus = False

    Set wsDst = ActiveSheet

    Call StatisticsLibrary2.JobBegin(strJobName & "中...", booOnlyUpdate:=True)

    '---------------------------------------------------
    ' 前処理
    '---------------------------------------------------
    ' Ｔ型マトリックスの軸定義取り出し＆項目名からインデクスの変換表を生成
    If TTypeMatrixAxisGet(wsDst, matrixParam, 項目名辞書生成:=True) = False Then
        GoTo Exit_Proc
    End If

    '---------------------------------------------------
    ' T型マトリックスにカウント結果を記入する。
    '---------------------------------------------------
    Call TTypeMatrixValueSet(wsDst, matrixParam, True)

    Call MyWsCalculate(wsDst)
    boolEndStatus = True

Exit_Proc:
    ' 後処理
    If boolEndStatus = False Then
        Call StatisticsLibrary2.JobEnd(strJobName & "中止", MsgBoxShow:=False)
    Else
        Call StatisticsLibrary2.JobEnd(strJobName & "処理終了", MsgBoxShow:=True)
    End If
End Sub

' T型マトリックスにカウント結果を記入する。
Private _
Function TTypeMatrixValueSet(ByVal wsGraph As Worksheet, _
                             ByRef matrixParam As TTypeMatrixParamType, _
                             ByVal booOnlyDisp As Boolean, _
                             Optional ByVal numCount As Long = -1) As Boolean

    Dim orig_V As Long
    Dim orig_L As Long
    Dim orig_R As Long
    Dim num    As Long
    Dim index_V As Long
    Dim index_L As Long
    Dim index_R As Long
    Dim intDisp As Long
    Dim rngIndex  As Range
    Dim c1  As Range

    TTypeMatrixValueSet = False

    With matrixParam
        orig_V = .numVertOrig
        orig_L = .numLeftOrig
        orig_R = .numRightOrig
        num = .numMatrixSize(1)
    End With

    '初期化
    With wsGraph.Rows(orig_V + 1).Resize(num)
        num = matrixParam.numMatrixSize(0)
        .Columns(orig_L - num).Resize(, num).ClearContents

        num = matrixParam.numMatrixSize(2)
        .Columns(orig_R + 1).Resize(, num).ClearContents
    End With

    Set rngIndex = TTypeMatrixIndexTabGet(wsGraph)
    If rngIndex Is Nothing Then
        Exit Function
    End If

    If numCount < 0 Then
        With rngIndex
            numCount = MyDataEndRowGet(.Cells(1)) - .Row + 1
        End With
    End If

    If rngIndex.Columns.Count < 4 Then
        booOnlyDisp = False
    End If

    If booOnlyDisp Then
       Call MyWsCalculate(wsGraph)
    End If
    Set c1 = rngIndex
    If numCount > 0 Then
        Do While True
            Set c1 = c1.Offset(1)
            If c1.Columns(1).Value = "" Then
                Exit Do
            End If

            index_L = c1.Columns(1).Value
            index_V = c1.Columns(2).Value
            index_R = c1.Columns(3).Value
            intDisp = c1.Columns(4).Value

            With wsGraph.Rows(orig_V + index_V)
                If booOnlyDisp And (intDisp = 0) Then
                    ' 非表示レコード
                ElseIf StatisticsLibrary2.セル値を加算(.Columns(orig_L - index_L)) = False Then
                    Exit Function
                ElseIf StatisticsLibrary2.セル値を加算(.Columns(orig_R + index_R)) = False Then
                    Exit Function
                End If
            End With
        Loop
    End If

    TTypeMatrixValueSet = True

End Function

Public _
Sub TTypeMatrixDataDisp()
    Dim wsGraph As Worksheet
    Dim objSel  As Variant
    Dim rngSel  As Range
    Dim rngData     As Range
    Dim rngIndex    As Range
    Dim objAF   As AutoFilter
    Dim rngAF   As Range
    Dim matrixParam As TTypeMatrixParamType
    Dim index_V As Long
    Dim index_L As Long
    Dim index_R As Long
    Dim ofs As Long

    Set objSel = Application.Selection
    Set wsGraph = ActiveSheet

    Set rngData = StatisticsLibrary2.名前の参照範囲取得(wsGraph, strDefName_SelectField)
    If rngData Is Nothing Then
        Exit Sub
    End If

    rngData.Cells(1.1).Activate

    If TypeName(objSel) <> "Range" Then
        Exit Sub    ' 未選択
    End If

    Set rngSel = objSel
    If Not IsNumeric(rngSel.Value) Then
        Exit Sub    ' 有効範囲外
    End If

    ' Ｔ型マトリックスの軸定義取り出し
    If TTypeMatrixAxisGet(wsGraph, matrixParam) = False Then
        Exit Sub
    End If

    With matrixParam
        index_V = rngSel.Row - .numVertOrig

        Select Case index_V
        Case Is < 1
            Exit Sub    ' マトリックス範囲外
        Case Is > .numMatrixSize(1)
            Exit Sub     ' マトリックス範囲外
        End Select

        Select Case rngSel.Column
        Case Is < .numLeftOrig - .numMatrixSize(0)
            Exit Sub     ' マトリックス範囲外
        Case Is > .numRightOrig + .numMatrixSize(2)
            Exit Sub     ' マトリックス範囲外

        Case Is < .numLeftOrig
            index_L = .numLeftOrig - rngSel.Column
            index_R = 0
        Case Is > .numRightOrig
            index_L = 0
            index_R = rngSel.Column - .numRightOrig
        Case Else
            Exit Sub     ' マトリックス範囲外
        End Select
    End With

    If CLng(rngSel.Value) <= 0 Then
        index_V = 0
    End If

    Set rngIndex = TTypeMatrixIndexTabGet(wsGraph)  '抽出データのIndex欄取り出し
    If rngIndex Is Nothing Then
        Exit Sub
    End If

    Set objAF = wsGraph.AutoFilter
    If objAF Is Nothing Then
        Set rngAF = rngIndex
        rngAF.AutoFilter
        ofs = 0
    Else
        Set rngAF = objAF.Range.Rows(1)

        ' フィルター位置チェック
        With rngAF
            If .Columns.Count < 3 Then
                Exit Sub
            ElseIf .Row <> rngIndex.Row Then
                Exit Sub
            ElseIf .Column > rngIndex.Column Then
                Exit Sub
            ElseIf (.Column + .Columns.Count) < (rngIndex.Column + 3) Then
                Exit Sub
            End If

            ofs = rngIndex.Column - .Column
        End With

        If objAF.FilterMode Then
'            objAF.ShowAllData
            If index_V = 0 Then
                rngAF.AutoFilter Field:=2 + ofs
                rngAF.AutoFilter Field:=1 + ofs
                rngAF.AutoFilter Field:=3 + ofs
            End If
        End If

        Set objAF = Nothing
    End If

    With rngAF
        If index_V > 0 Then
            .AutoFilter Field:=2 + ofs, Criteria1:=index_V

            If index_L > 0 Then
                .AutoFilter Field:=3 + ofs
                .AutoFilter Field:=1 + ofs, Criteria1:=index_L
            Else
                .AutoFilter Field:=1 + ofs
                .AutoFilter Field:=3 + ofs, Criteria1:=index_R
            End If
        End If
    End With

End Sub

' 抽出するデータの列番号を取得
Private _
Function TTypeMatrixSelectColumn(ByVal wsGraph As Worksheet, _
                                 ByVal wsData As Worksheet, _
                                 ByRef aryDataCol() As Long) As Boolean

    Dim rr As Range
    Dim aa()  As String

    TTypeMatrixSelectColumn = False

    Set rr = TTypeMatrixSelectName(wsGraph)    '抽出するデータの名称格納位置
    If rr Is Nothing Then
       GoTo Exit_Proc   '不正な書式シート
    End If

    ' 抽出するデータの項目名を取得
    If StatisticsLibrary2.Row2StringArray(rr, aa) = False Then
        GoTo Exit_Proc
    End If

    ' 入力データの列番号を取得
    ReDim aryDataCol(UBound(aa))
    If StatisticsLibrary2.RedmineSheetHeaderColumn(wsData, aa, aryDataCol) = False Then
        GoTo Exit_Proc
    End If

    TTypeMatrixSelectColumn = True

Exit_Proc:
    Set rr = Nothing
    Erase aa

End Function

' データシートから使用するデータを抽出
Private _
Function TTypeMatrixDataSelect(ByVal wsData As Worksheet, _
                               ByRef aryDataCol() As Long, _
                               ByVal strTargetGroup As String, _
                               ByVal wsGraph As Worksheet, _
                               ByRef matrixParam As TTypeMatrixParamType) As Long

    Dim numDefaultItem As Long
    Dim rngData As Range
    Dim rngIndex    As Range
    Dim rowDataTop  As Long
    Dim rowDataEnd  As Long
    Dim in_row  As Long
    Dim aryDataVal()    As Variant
    Dim aryItemNo(2)    As Long 'Ｔ型マトリックス上の項番
    Dim aryAxisIndex(2) As Long
    Dim strAxisItemVal  As String
    Dim strBuildGrName  As String
    Dim ii  As Long
    Dim jj  As Long
    Dim ss  As String
    Dim c1  As Range
    Dim c2  As Range
    Dim idx As Variant
    Dim Flag    As Boolean
    Dim numDataMax  As Long '抽出列数 -1
    Dim numGrpIndex As Long
    Dim out_base1   As Range
    Dim out_base2   As Range
    Dim numCount    As Long ' 有効データ数

    TTypeMatrixDataSelect = -1

    Set rngData = TTypeMatrixSelectName(wsGraph)   '抽出データの格納先
    If rngData Is Nothing Then
        Exit Function   '不正な出力シート
    End If

    Set rngIndex = TTypeMatrixIndexTabGet(wsGraph) 'Ｔ型マトリックスのインデックス値格納先
    If rngIndex Is Nothing Then
        Exit Function   '不正な出力シート
    End If

    If rngData.Row <> rngIndex.Row Then
        Exit Function   '不正な出力シート
    End If

    ' インデクス値判定用の項目位置を取得
    With rngData.Offset(-1)
        For ii = 0 To 2
            Select Case ii
            Case 0
                ss = "左軸項目"
            Case 1
                ss = "縦軸項目"
            Case 2
                ss = "右軸項目"
            End Select

            jj = StatisticsLibrary2.GetDataColumn(.Cells(1, 1), ss)
            If jj = 0 Then
                Exit Function
            End If

            aryAxisIndex(ii) = jj - .Column
        Next ii

        ii = StatisticsLibrary2.GetDataColumn(.Cells(1, 1), "原因Gr", NoMsg:=True)
        If ii = 0 Then
            strBuildGrName = ""
        End If

        numGrpIndex = ii - .Column
    End With

    If StatisticsLibrary2.RedmineSheetDataRangeGet(wsData, rowDataTop, rowDataEnd) < 1 Then
        Exit Function
    End If

    numDataMax = UBound(aryDataCol)

    ReDim aryDataVal(numDataMax)
    For ii = 0 To numDataMax
        aryDataVal(ii) = ""
    Next ii

    Set c1 = StatisticsLibrary2.MyCurrentRegion(rngData).Rows(1)
    Set c2 = rngIndex.Rows(1)

    Set out_base1 = c1.Offset(1)
    Set out_base2 = c2.Offset(1)

    ' 出力域クリア   注）書式コピー用に一行目のデータは残す
    Call StatisticsLibrary2.ClearRangeToEndRow(out_base1.Offset(1))
    Call StatisticsLibrary2.ClearRangeToEndRow(out_base2.Offset(1))

    numCount = 0
    For in_row = rowDataTop To rowDataEnd
        ' 抽出対象の列の値を取り出す。
        With wsData.Rows(in_row)
            For jj = 0 To numDataMax
                If aryDataCol(jj) > 0 Then
                    aryDataVal(jj) = Trim(.Columns(aryDataCol(jj)).Value)
                End If
            Next jj
        End With

        If numGrpIndex >= 0 Then
            strBuildGrName = aryDataVal(numGrpIndex) '原因Gr
        End If

        ' Ｔ型マトリックスの出力対象レコードか判定
        Flag = IIf(strBuildGrName Like strTargetGroup, True, False)
        Select Case intDataSelectType
        Case 1
            ' 工程の何れかが軸項目の工程ならば、対象とする。
            For jj = 0 To 2
                strAxisItemVal = aryDataVal(aryAxisIndex(jj))   '軸項目
                idx = matrixParam.dictItem(jj)(strAxisItemVal)  'Ｔ型マトリックス上の項番を取り出す。

                If IsEmpty(idx) Then    '軸項目ではない
                    idx = 0 '抽出対象外。
                Else
                    Select Case strAxisItemVal
                    Case "ST1", "ST2", "ST", "IQT"
                        'ST,IQTは「(解析)原因作りこみG」が自Grなら対象とする。
                        If strBuildGrName <> strTargetGroup Then
                            idx = 0
                        End If
                    End Select

                    If idx > 0 Then
                        Flag = True
                    End If
                End If

                aryItemNo(jj) = idx
            Next jj

        Case 2, 3
            ' 「原因作りこみグループ」が自Grなら対象とする。工程名は置換を行う。
            If Flag Then
                For jj = 0 To 2
                    strAxisItemVal = aryDataVal(aryAxisIndex(jj)) '軸項目
                    If matrixParam.dictItem(jj) Is matrixParam.dictItem(1) Then
                        strAxisItemVal = StatisticsLibrary.GetProcessCategory(strAxisItemVal, NoMsg:=True)
                    End If

                    If strAxisItemVal = "" Then
                        idx = 0
                    Else
                        idx = matrixParam.dictItem(jj)(strAxisItemVal)  'Ｔ型マトリックス上の項番を取り出す。
                        If IsEmpty(idx) Then    '軸項目ではない
                            idx = 0
                        End If
                    End If

                    aryItemNo(jj) = idx
                Next jj
            End If
        End Select

        If Flag = True Then
            numCount = numCount + 1

            Set c1 = c1.Offset(1)
            Set c2 = c2.Offset(1)

            ' 書式等をコピー
            If c1.Row > out_base1.Row Then
                out_base1.Copy Destination:=c1.Cells(1, 1)
                out_base2.Copy Destination:=c2.Cells(1, 1)
            End If

            ' 抽出したデータを出力域に格納
            With c1.Cells(1, 1)
                For jj = 0 To numDataMax
                    .Offset(0, jj).Value = aryDataVal(jj)
                Next jj
            End With

            'Ｔ型マトリックスのインデクス値を出力域に格納
            With c2.Cells(1, 1)
                For jj = 0 To 2
                    numDefaultItem = matrixParam.numMatrixSize(jj)
                    .Offset(0, jj).Value = IIf(aryItemNo(jj) = 0, numDefaultItem, aryItemNo(jj))
                Next jj
            End With
        End If
    Next in_row

    If numCount < 1 Then  '有効データが無い場合
        out_base1.ClearContents  '一行目のデータをクリア。
        out_base2.ClearContents  '一行目のデータをクリア。
    End If

    Erase aryDataVal
    Erase aryItemNo

    TTypeMatrixDataSelect = numCount

End Function

' Ｔ型マトリックスの軸定義取り出し
Private _
Function TTypeMatrixAxisGet(ByVal wsGraph As Worksheet, _
                            ByRef matrixParam As TTypeMatrixParamType, _
                            Optional 項目名辞書生成 As Boolean = False) As Boolean

    Dim rngV   As Range    'グラフの縦軸開始位置
    Dim rngL   As Range    'グラフの左軸開始位置
    Dim rngR   As Range    'グラフの左軸開始位置
    Dim rngItemList(2) As Range
    Dim numI_Size(2)   As Long 'グラフの項目数
    Dim i As Integer

    TTypeMatrixAxisGet = False

    ' 縦軸の開始位置を取得
    Set rngV = StatisticsLibrary2.名前の参照範囲取得(wsGraph, strDefName_DownAxis)
    If rngV Is Nothing Then
        Exit Function   '不正なGraphシート
    End If

    ' 左軸の開始位置を取得
    Set rngL = StatisticsLibrary2.名前の参照範囲取得(wsGraph, strDefName_LeftAxis)
    If rngL Is Nothing Then
        Exit Function   '不正なGraphシート
    End If

    ' 右軸の開始位置を取得
    Set rngR = StatisticsLibrary2.名前の参照範囲取得(wsGraph, strDefName_RightAxis)
    If rngR Is Nothing Then
        Exit Function   '不正なGraphシート
    End If

    ' 項目リストを取得
    If TTypeMatrixItemListGet(wsGraph, rngItemList) = False Then
        Exit Function   '不正なGraphシート
    End If

    numI_Size(1) = rngItemList(1).Cells.Count

    If Not rngItemList(0) Is Nothing Then
        numI_Size(0) = rngItemList(0).Cells.Count

    ElseIf rngL.Column - numI_Size(1) < 0 Then
        Call MsgBox("'" & wsGraph.Name & "'シートの左軸の項目数が足りない", vbCritical)
        Exit Function   '不正なGraphシート
    Else
        numI_Size(0) = numI_Size(1)
    End If

    If Not rngItemList(2) Is Nothing Then
        numI_Size(2) = rngItemList(2).Cells.Count

    ElseIf rngR.Column + numI_Size(1) - 1 > wsGraph.Columns.Count Then
        Call MsgBox("'" & wsGraph.Name & "'シートの右軸の項目数が足りない", vbCritical)
        Exit Function   '不正なGraphシート
    Else
        numI_Size(2) = numI_Size(1)
    End If

    With matrixParam
        For i = 0 To 2
            .numMatrixSize(i) = numI_Size(i)
        Next i

        .numVertOrig = rngV.Row - 1
        .numLeftOrig = rngL.Column + 1
        .numRightOrig = rngR.Column - 1

        If 項目名辞書生成 = True Then
            ' 項目名⇒インデクスの変換表を生成
            Set .dictItem(1) = TTypeMatrixDictionary(rngItemList(1))

            If rngItemList(0) Is Nothing Then
                Set .dictItem(0) = .dictItem(1)
            Else
                Set .dictItem(0) = TTypeMatrixDictionary(rngItemList(0), Reverse:=True)
                If .dictItem(0) Is Nothing Then Exit Function
            End If

            If rngItemList(2) Is Nothing Then
                Set .dictItem(2) = .dictItem(1)
            Else
                Set .dictItem(2) = TTypeMatrixDictionary(rngItemList(2))
                If .dictItem(2) Is Nothing Then Exit Function
            End If
        End If
    End With

    TTypeMatrixAxisGet = True
End Function

' 項目名⇒インデクスの変換表を生成
Private _
Function TTypeMatrixDictionary(ByVal rngItemList As Range, _
                               Optional ByVal Reverse As Boolean = False) As Scripting.Dictionary

    Set TTypeMatrixDictionary = Nothing

    Dim dicItem As Scripting.Dictionary
    Dim i As Integer
    Dim k As Integer
    Dim c1  As Range

    ' 変換表作成
    Set dicItem = CreateObject("Scripting.Dictionary")

    If Reverse Then
        i = rngItemList.Cells.Count
        k = -1
    Else
        i = 1
        k = 1
    End If

    On Error Resume Next

    For Each c1 In rngItemList.Cells
        dicItem.Add Trim$(c1.Value), i
        If Err.Number <> 0 Then
            Call MsgBox("項目名の設定誤り: " & c1.Value & vbCrLf & vbCrLf _
                        & c1.Address(External:=True) & vbCrLf & vbCrLf _
                        & Err.Description, vbCritical)
            Err.Clear
            Exit Function
        End If

        i = i + k
    Next c1

    On Error GoTo 0

    Set TTypeMatrixDictionary = dicItem
End Function
