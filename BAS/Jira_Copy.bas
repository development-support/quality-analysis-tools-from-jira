Attribute VB_Name = "Jira_Copy"
Sub ボタン1_Click()
'
' 更新:2016.08.02
' 更新:2017.05.31  ver0.2.38
' 更新:2017.06.09  ver0.2.38
'

  Dim Target As Workbook
  Dim i As Long, MaxRow As Long, MaxCol As Long
  '格納する変数を定義します｡
  Dim strPath As String
  Dim wsJIRA As Worksheet
  Dim rEmptyRow As Range
  Dim LastRow As Long

  Set wsJIRA = Worksheets(gJIRASheetName)

  ' 追加:2017.06.09
  Application.ScreenUpdating = False

  Call clear_all_Click2

  'MsgBox "cler"

  ' 追加:2016.07.28
  Application.DisplayAlerts = False

  '変数にパスを格納します。
  strPath = ThisWorkbook.Path

  ' Open JIRA export data
  Set Target = Workbooks.Open(Filename:=strPath & "\JiraExport.xls")

  ThisWorkbook.Activate

  '使用済み最終行
  With Target.Sheets(1).UsedRange
    MaxRow = .Find("*", , xlFormulas, , xlByRows, xlPrevious).Row
    MaxCol = .Find("*", , xlFormulas, , xlByColumns, xlPrevious).Column
  End With

  For i = 4 To MaxRow - 1
    ' Copy JIRA export data to specified sheet
    Target.Sheets(1).Cells(i, 1).Resize(, MaxCol).Copy wsJIRA.Cells(i - 3, 1)
  Next

  '結合フィールドの列を削除する ※シート分割で未対応のため
  For i = MaxCol To 1 Step -1
    With wsJIRA.Columns(i)
      If .Rows(1).MergeCells Then
        .Delete
      End If
    End With
  Next i

  ' Close JIRA export data
  Target.Close SaveChanges:=False

  Call 日付変換

  If (Sheets(gConfigSheetName).Range("ConfigHistorySearch").Value = "あり") Then
    ' Status更新履歴取得
    Call JiraStatusGet
    ' Log Lalel
    ' wsJira.Cells(1, MaxCol + 1) = "StatusLog"
    ' wsJira.Cells(1, MaxCol + 2) = "Company"
    ' Else
    ' MsgBox "処理を中断します"
  End If

  Application.DisplayAlerts = True

  wsJIRA.Activate
  wsJIRA.Cells.RowHeight = 12
  wsJIRA.Cells.ColumnWidth = 15
  ' シート全体のフォント名/サイズを設定
  wsJIRA.Cells.Font.Name = "Meiryo UI"
  wsJIRA.Cells.Font.Size = 10
  ' シートの表示倍率を設定
  ActiveWindow.Zoom = 70

  LastRow = Cells(Rows.Count, 1).End(xlUp).Row
  With ActiveSheet
    For i = 2 To LastRow
      ' i行/1列目のセルが空白だった場合
      If IsEmpty(Cells(i, 1).Value) Then
        ' 最初の空白行
        If rEmptyRow Is Nothing Then
          Set rEmptyRow = .Rows(i).EntireRow
        ' 2件目以降の空白行
        Else
          Set rEmptyRow = Union(rEmptyRow, .Rows(i).EntireRow)
        End If
      End If
    Next i
  End With

  ' 空白行があれば一括で削除する
  If Not rEmptyRow Is Nothing Then
        rEmptyRow.Delete
  End If

  Call FormatChangeToDate

End Sub
