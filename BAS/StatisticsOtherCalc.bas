Attribute VB_Name = "StatisticsOtherCalc"
Option Explicit


Sub StatisticsOtherCalc()
    Application.ScreenUpdating = False
    
    Dim strJobName As String
    Dim strInSheetName As String
    Dim strAnalyzeSheetName As String
    Dim intRowLabelColumn As Integer
    Dim intDateRow As Integer
    Dim booEndMessage As Boolean
    
    Dim strMessage As String
    Dim booWarning As Boolean
    
    ' Statusバー表示
    strJobName = "その他のデータ集計"
    JobBegin (strJobName & "中...")

    ' JIRA関連
    Sheets(strConstJIRASearchResultSheetName).Select

    '問処発行数=
    Sheets(strConstConfigSheetName).Range("IssuesTotal").Value = GetUsedRangeEndRow - 1
    
    '対処済数=
    Sheets(strConstConfigSheetName).Range("IssuesResolved").Value = WorksheetFunction.CountA(Columns(5)) - 1
    If (Sheets(strConstConfigSheetName).Range("IssuesResolved").Value < 0) Then
        Sheets(strConstConfigSheetName).Range("IssuesResolved").Value = 0
    End If
    
    '残問処数=
    Sheets(strConstConfigSheetName).Range("IssuesRemain").Value = Sheets(strConstConfigSheetName).Range("IssuesTotal").Value - Sheets(strConstConfigSheetName).Range("IssuesResolved").Value
    
    '残問処数=
    'Sheets(strConstConfigSheetName).Range("IssuesRemain").Value = GetUsedRangeEndRow - 1 - WorksheetFunction.CountIf(Columns(3), "Done")
    
    '問処Close数=
    'Sheets(strConstConfigSheetName).Range("IssuesClose").Value = WorksheetFunction.CountIf(Columns(3), "Done")
    
    
    ' TestChart関連
    Sheets(strConstPBAnalyzeSheetName).Select

    ' 総項目数
    Sheets(strConstConfigSheetName).Range("TestsTotal").Value = Cells(GetTargetColumnEndRow2(GetTargetStringColumn(Rows(1), "Total Tests Planned")), 3)
    
    ' 残試行項目数=
    Sheets(strConstConfigSheetName).Range("TestsAttemptsRemain").Value = WorksheetFunction.Min(Columns(GetTargetStringColumn(Rows(1), "Actual Attempts")))
    
    ' 進捗率
    Sheets(strConstConfigSheetName).Range("TestsAttemptsProcess").Value = Format(1 - (Sheets(strConstConfigSheetName).Range("TestsAttemptsRemain").Value / Sheets(strConstConfigSheetName).Range("TestsTotal").Value), "0.000")

    ' 残パス項目数=
    Sheets(strConstConfigSheetName).Range("TestsRemain").Value = WorksheetFunction.Min(Columns(GetTargetStringColumn(Rows(1), "Passed")))
    
    '進捗率
    Sheets(strConstConfigSheetName).Range("TestsProcess").Value = Format(1 - (Sheets(strConstConfigSheetName).Range("TestsRemain").Value / Sheets(strConstConfigSheetName).Range("TestsTotal").Value), "0.000")

    ' Gomperz関連
    Sheets(strConstGomperzAnalyzeSheetName).Select
    ' 潜在バグ数check(EmptyかErrorだったらゴンペルツなし)
    If (Not ((IsEmpty(Sheets(strConstGomperzAnalyzeSheetName).Cells(13, 12)) Or _
              IsError(Sheets(strConstGomperzAnalyzeSheetName).Cells(13, 12))))) And _
       (Sheets(strConstConfigSheetName).Range("GomperzInsertSetting").Value = "あり") Then
        ' 潜在バグ数
        Sheets(strConstConfigSheetName).Range("Latent_Bug_Number").Value = Format(Cells(13, 12), "0.0")
        ' 潜在バグ数+5%
        Sheets(strConstConfigSheetName).Range("Latent_Bug_Number_p5").Value = Format(Cells(14, 12), "0.0")
        ' 潜在バグ数-5%
        Sheets(strConstConfigSheetName).Range("Latent_Bug_Number_m5").Value = Format(Cells(15, 12), "0.0")
        ' 収束率
        Sheets(strConstConfigSheetName).Range("Convergence_Rate").Value = Format(Cells(16, 12), "0.000")
    End If

    ' Current Report Date
    Sheets(strConstTestDataSheetName).Select
    Sheets(strConstConfigSheetName).Range("CurrentReportDate").Value = Format(Cells(GetTargetStringRow(Columns(1), "Current Report Date") + 1, 1).Value, "yy/mm/dd")
    
    ' 表記変更(英語⇔日本語)
    Sheets(strConstConfigSheetName).Select
    If (Range("LanguageSetting").Value = "英語") Then
        Range("SystemName").Offset(0, -1).Value = "System:"
        Range("SystemVersion").Offset(0, -1).Value = "Version:"
        Range("Group").Offset(0, -1).Value = "Group:"
        Range("TypeOfTests").Offset(0, -1).Value = "Type Of Tests:"
        
'        Range("IssuesTotal").Offset(0, 1).Value = "PTRs Number:"
'        Range("IssuesResolved").Offset(0, 1).Value = "Resolved PTRs Number:"
'        Range("IssuesRemain").Offset(0, 1).Value = "Remaining PTRs Number:"
'
'        Range("TestsTotal").Offset(0, 1).Value = "Total TCs Number:"
'        Range("TestsRemain").Offset(0, 1).Value = "Remaining TCs Number:"
'        Range("TestsProcess").Offset(0, 1).Value = "Progress Rate:"
        Range("IssuesTotal").Offset(0, -1).Value = "Bugs:"
        Range("IssuesResolved").Offset(0, -1).Value = "Resolved Bugs:"
        Range("IssuesRemain").Offset(0, -1).Value = "Remaining Bugs:"

        Range("TestsTotal").Offset(0, -1).Value = "Total Planned Tests:"
        Range("TestsAttemptsRemain").Offset(0, -1).Value = "Remaining Attempts Tests:"
        Range("TestsAttemptsProcess").Offset(0, -1).Value = "Progress Rate:"
        Range("TestsRemain").Offset(0, -1).Value = "Remaining Passed Tests:"
        Range("TestsProcess").Offset(0, -1).Value = "Progress Rate:"
        
'        Range("LogisticsTitle").Offset(0, 1).Value = "Incoming PTRs forecast(Logistics)"
'        Range("LogisticsParamaterA").Offset(0, 1).Value = "Target Bug Number (a):"
'        Range("LogisticsParamaterB").Offset(0, 1).Value = "Adjustment factor (b):"
'        Range("LogisticsParamaterC").Offset(0, 1).Value = "Adjustment factor (c):"
'
'        Range("GomperzTitle").Offset(0, 1).Value = "PTRs Learning forcast(Gomperz)"
'        Range("Latent_Bug_Number").Offset(0, 1).Value = "Latent Bug Number:"
'        Range("Latent_Bug_Number_p5").Offset(0, 1).Value = "Latent Bug Number +5%:"
'        Range("Latent_Bug_Number_m5").Offset(0, 1).Value = "Latent Bug Number -5%:"
'        Range("Convergence_Rate").Offset(0, 1).Value = "Convergence Rate:"
        
        Range("LogisticsTitle").Value = "Estimate Bugs of Planned(Logistics)"
        Range("LogisticsParamaterA").Offset(0, -1).Value = "Planned Bugs(a):"
        Range("LogisticsParamaterB").Offset(0, -1).Value = "Adjustment factor(b):"
        Range("LogisticsParamaterC").Offset(0, -1).Value = "Adjustment factor(c):"
        
        ' 潜在バグ数check(EmptyかErrorだったらゴンペルツなし)
        If (Not ((IsEmpty(Sheets(strConstGomperzAnalyzeSheetName).Cells(13, 12)) Or _
                  IsError(Sheets(strConstGomperzAnalyzeSheetName).Cells(13, 12))))) And _
           (Range("GomperzInsertSetting").Value = "あり") Then
            Range("GomperzTitle").Value = "Estimate Bugs of Learning(Gomperz)"
            Range("Latent_Bug_Number").Offset(0, -1).Value = "Latent Bugs:"
            Range("Latent_Bug_Number_p5").Offset(0, -1).Value = "Latent Bugs +5%:"
            Range("Latent_Bug_Number_m5").Offset(0, -1).Value = "Latent Bugs -5%:"
            Range("Convergence_Rate").Offset(0, -1).Value = "Convergence Rate:"
        Else
            Range("GomperzTitle").Value = Empty
            Range("Latent_Bug_Number").Offset(0, -1).Value = Empty
            Range("Latent_Bug_Number_p5").Offset(0, -1).Value = Empty
            Range("Latent_Bug_Number_m5").Offset(0, -1).Value = Empty
            Range("Convergence_Rate").Offset(0, -1).Value = Empty
            Range("Latent_Bug_Number").Value = Empty
            Range("Latent_Bug_Number_p5").Value = Empty
            Range("Latent_Bug_Number_m5").Value = Empty
            Range("Convergence_Rate").Value = Empty
        End If
        
        Range("CurrentReportDate").Offset(-1, -1).Value = "TestChart - CurrentReportDate:"
        
    Else
    
        Range("SystemName").Offset(0, -1).Value = "装置名:"
        Range("SystemVersion").Offset(0, -1).Value = "ターゲット:"
        Range("Group").Offset(0, -1).Value = "グループ:"
        Range("TypeOfTests").Offset(0, -1).Value = "試験工程:"
        
        Range("IssuesTotal").Offset(0, -1).Value = "バグ問処発行数:"
        Range("IssuesResolved").Offset(0, -1).Value = "バグ問処解決数:"
        Range("IssuesRemain").Offset(0, -1).Value = "残問処数:"
        
        Range("TestsTotal").Offset(0, -1).Value = "総テスト項目数:"
        Range("TestsAttemptsRemain").Offset(0, -1).Value = "残試行項目数:"
        Range("TestsAttemptsProcess").Offset(0, -1).Value = "進捗率:"
        Range("TestsRemain").Offset(0, -1).Value = "残パス項目数:"
        Range("TestsProcess").Offset(0, -1).Value = "進捗率:"
        
        Range("LogisticsTitle").Value = "バグ検出予測(Logistics)"
        Range("LogisticsParamaterA").Offset(0, -1).Value = "指標値(a):"
        Range("LogisticsParamaterB").Offset(0, -1).Value = "調整係数(b):"
        Range("LogisticsParamaterC").Offset(0, -1).Value = "調整係数(c):"
        
    ' 潜在バグ数check(EmptyかErrorだったらゴンペルツなし)
    If (Not ((IsEmpty(Sheets(strConstGomperzAnalyzeSheetName).Cells(13, 12)) Or _
              IsError(Sheets(strConstGomperzAnalyzeSheetName).Cells(13, 12))))) And _
            (Range("GomperzInsertSetting").Value = "あり") Then
            Range("GomperzTitle").Value = "バグ習熟予測(Gomperz)"
            Range("Latent_Bug_Number").Offset(0, -1).Value = "潜在バグ数:"
            Range("Latent_Bug_Number_p5").Offset(0, -1).Value = "潜在バグ数+5%:"
            Range("Latent_Bug_Number_m5").Offset(0, -1).Value = "潜在バグ数-5%:"
            Range("Convergence_Rate").Offset(0, -1).Value = "Convergence Rate:"
        Else
            Range("GomperzTitle").Value = Empty
            Range("Latent_Bug_Number").Offset(0, -1).Value = Empty
            Range("Latent_Bug_Number_p5").Offset(0, -1).Value = Empty
            Range("Latent_Bug_Number_m5").Offset(0, -1).Value = Empty
            Range("Convergence_Rate").Offset(0, -1).Value = Empty
            Range("Latent_Bug_Number").Value = Empty
            Range("Latent_Bug_Number_p5").Value = Empty
            Range("Latent_Bug_Number_m5").Value = Empty
            Range("Convergence_Rate").Value = Empty
        End If
        
        Range("CurrentReportDate").Offset(-1, -1).Value = "TestChart - CurrentReportDate:"
    
    End If
    
    ' 試行なし時の表記削除
    If (Sheets(strConstConfigSheetName).Range("TestDataAttemptsSetting").Value <> "あり") Then
        Sheets(strConstConfigSheetName).Range("TestsAttemptsRemain").Value = Empty
        Sheets(strConstConfigSheetName).Range("TestsAttemptsProcess").Value = Empty
        Sheets(strConstConfigSheetName).Range("TestsAttemptsRemain").Offset(0, -1).Value = Empty
        Sheets(strConstConfigSheetName).Range("TestsAttemptsProcess").Offset(0, -1).Value = Empty
    End If
    
    ' 入力データの確認
    booWarning = False
    Sheets(strConstTestDataSheetName).Select
    If (Sheets(strConstConfigSheetName).Range("TestDataAttemptsSetting").Value = "あり") Then
        ' Total Tests Planned と Planned Attempts
        If (WorksheetFunction.Max(Rows(GetTargetStringRow(Columns(1), "Planned Attempts"))) <> Sheets(strConstConfigSheetName).Range("TestsTotal").Value) Then
            booWarning = True
            If (Sheets(strConstConfigSheetName).Range("LanguageSetting").Value = "英語") Then
                strMessage = "The value of ""Total Test Planned"" on the last day of the test period differs from the maximum value of ""Planned Attempts""." & vbCr
            Else
                strMessage = "テスト期間の最終日の""Total Test Planned""の値と""Planned Attempts""の最大値が異なります." & vbCr
            End If
        End If
        ' Total Tests Planned と Actual Attempts
        If (WorksheetFunction.Max(Rows(GetTargetStringRow(Columns(1), "Actual Attempts"))) > Sheets(strConstConfigSheetName).Range("TestsTotal").Value) Then
            booWarning = True
            If (Sheets(strConstConfigSheetName).Range("LanguageSetting").Value = "英語") Then
                strMessage = strMessage & vbCr & "The maximum value of ""Actual Attempts"" is larger than the value of ""Total Test Planned"" on the last day of the test period." & vbCr
            Else
                strMessage = strMessage & vbCr & "テスト期間の最終日の""Total Test Planned""の値より""Planned Attempts""の最大値の方が大きいです." & vbCr
            End If
        End If
    End If
    
    ' Total Tests Planned と Planned Pass
    If (WorksheetFunction.Max(Rows(GetTargetStringRow(Columns(1), "Planned Pass"))) <> Sheets(strConstConfigSheetName).Range("TestsTotal").Value) Then
        booWarning = True
        If (Sheets(strConstConfigSheetName).Range("LanguageSetting").Value = "英語") Then
            strMessage = strMessage & vbCr & "The value of ""Total Test Planned"" on the last day of the test period differs from the maximum value of ""Planned Pass""." & vbCr
        Else
            strMessage = strMessage & vbCr & "テスト期間の最終日の""Total Test Planned""の値と""Planned Pass""の最大値が異なります." & vbCr
        End If
    End If
    ' Total Tests Planned と Passed
    If (WorksheetFunction.Max(Rows(GetTargetStringRow(Columns(1), "Passed"))) > Sheets(strConstConfigSheetName).Range("TestsTotal").Value) Then
        booWarning = True
        If (Sheets(strConstConfigSheetName).Range("LanguageSetting").Value = "英語") Then
            strMessage = strMessage & vbCr & "The maximum value of ""Passed"" is larger than the value of ""Total Test Planned"" on the last day of the test period." & vbCr
        Else
            strMessage = strMessage & vbCr & "テスト期間の最終日の""Total Test Planned""の値より""Passed""の最大値の方が大きいです." & vbCr
        End If
    End If
    
    If (booWarning) Then
        If (Sheets(strConstConfigSheetName).Range("LanguageSetting").Value = "英語") Then
            strMessage = strMessage & vbCr & "Please Check the TestChart." & vbCr
        Else
            strMessage = strMessage & vbCr & "TestChartを見直してください." & vbCr
        End If
        MsgBox strMessage, vbExclamation
    End If
    
    ' Statusバー表示
    booEndMessage = False
    JobEnd strJobName & "処理終了", MsgBoxShow:=booEndMessage


End Sub

Private Function GetUsedRangeEndRow() As Integer
    GetUsedRangeEndRow = Cells(1, 1).SpecialCells(xlCellTypeLastCell).Row
End Function

Private Function GetUsedRangeEndColumn() As Integer
    GetUsedRangeEndColumn = Cells(1, 1).SpecialCells(xlCellTypeLastCell).Column
End Function

Private Function GetTargetColumnEndRow2(ByVal intColumn As Integer) As Integer
    Dim ret As Integer
    On Error GoTo overflow
'    ' 空白時は、2回実行、オーバーフロー時は1
'    ' integerの範囲を超えたらオーバーフローとする
'    If (Cells(intRow, intColumn).Value = "") Then
'        ret = Cells(intRow, intColumn).End(xlDown).Row
'        ret = Cells(ret, intColumn).End(xlDown).Row
'    Else
'        ret = Cells(intRow, intColumn).End(xlDown).Row
'    End If
    ' UsedRangeから検索、文字列があればその値
    ' 文字列が無ければ上に向かって移動
    If (Cells(GetUsedRangeEndRow, intColumn).Value <> "") Then
        ret = GetUsedRangeEndRow
    Else
        ret = Cells(GetUsedRangeEndRow, intColumn).End(xlUp).Row
    End If
    GetTargetColumnEndRow2 = ret
    Exit Function
overflow:
    GetTargetColumnEndRow2 = 1
End Function


'　検索文字列の行番号を取得
Private Function GetTargetStringRow(ByVal rangeTarget As Range, ByVal strTargetString As String, Optional ByVal boofull As Boolean = False) As Integer
    Dim intReturn As Integer
    Dim Foundcell As Range
    If (boofull) Then
        Set Foundcell = rangeTarget.Find(what:=strTargetString, _
                                        LookIn:=xlValues, _
                                        LookAt:=xlWhole, _
                                        MatchCase:=False, _
                                        MatchByte:=False)
    Else
        Set Foundcell = rangeTarget.Find(what:=strTargetString, _
                                        LookIn:=xlValues, _
                                        LookAt:=xlPart, _
                                        MatchCase:=False, _
                                        MatchByte:=False)
    End If
    If Foundcell Is Nothing Then
        intReturn = 0
    Else
        intReturn = Foundcell.Row
    End If
    GetTargetStringRow = intReturn
        
End Function



'　検索文字列の列番号を取得
Private Function GetTargetStringColumn(ByVal rangeTarget As Range, ByVal strTargetString As String, Optional ByVal boofull As Boolean = False) As Integer
    Dim intReturn As Integer
    Dim Foundcell As Range
    If (boofull) Then
        Set Foundcell = rangeTarget.Find(what:=strTargetString, _
                                        LookIn:=xlValues, _
                                        LookAt:=xlWhole, _
                                        MatchCase:=False, _
                                        MatchByte:=False)
    Else
        Set Foundcell = rangeTarget.Find(what:=strTargetString, _
                                        LookIn:=xlValues, _
                                        LookAt:=xlPart, _
                                        MatchCase:=False, _
                                        MatchByte:=False)
    End If
    If Foundcell Is Nothing Then
        intReturn = 0
    Else
        intReturn = Foundcell.Column
    End If
    GetTargetStringColumn = intReturn
        
End Function



Private Sub JobBegin(Optional strMsg As String = "")

    varSaveCalculation = Application.Calculation
    varSaveScreenUpdating = Application.ScreenUpdating
    
    Application.Calculation = xlCalculationManual
    
    If strMsg <> "" Then
        Application.StatusBar = strMsg
    End If
    
    Application.ScreenUpdating = False
    
End Sub

Private Sub JobEnd(Optional strMsg As String = "", Optional MsgBoxShow As Boolean = False)

    If Not IsEmpty(varSaveCalculation) Then
        If Application.Calculation <> varSaveCalculation Then
            Application.Calculation = varSaveCalculation
        End If
        varSaveCalculation = Empty
    End If
    If Not IsEmpty(varSaveScreenUpdating) Then
        Application.ScreenUpdating = varSaveScreenUpdating
        varSaveScreenUpdating = Empty
    End If
    
    If (strMsg <> "") And (MsgBoxShow = True) Then
        Application.StatusBar = Replace(strMsg, vbCrLf, " ")
        MsgBox strMsg, vbOKOnly
    End If
    
    Application.StatusBar = False
End Sub


