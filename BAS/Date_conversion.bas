Attribute VB_Name = "Date_conversion"
Sub 日付変換()
Attribute 日付変換.VB_ProcData.VB_Invoke_Func = " \n14"
  '
  ' 日付変換 Macro
  '
  Dim MaxRow As Long


  '使用済み最終行
  With Worksheets(gJIRASheetName).UsedRange
    MaxRow = .Find("*", , xlFormulas, , xlByRows, xlPrevious).Row
  End With

  For i = 2 To MaxRow
    Created = Format(Range("K" & i), "yyyy/mm/dd")
    Range("K" & i).NumberFormatLocal = "@"
    Range("K" & i) = Created

    LastViewed = Format(Range("L" & i), "yyyy/mm/dd")
    Range("L" & i).NumberFormatLocal = "@"
    Range("L" & i) = LastViewed

    Updated = Format(Range("M" & i), "yyyy/mm/dd")
    Range("M" & i).NumberFormatLocal = "@"
    Range("M" & i) = Updated

    Resolved = Format(Range("N" & i), "yyyy/mm/dd")
    Range("N" & i).NumberFormatLocal = "@"
    Range("N" & i) = Resolved
  Next i

End Sub
 
Sub FormatChangeToDate()
  Dim MaxRow As Long

  '使用済み最終行
  With Worksheets(gJIRASheetName).UsedRange
    MaxRow = .Find("*", , xlFormulas, , xlByRows, xlPrevious).Row
  End With

  For i = 2 To MaxRow
    ' Created
    Created = Format(Range("K" & i), "yyyy/mm/dd")
    Range("K" & i).NumberFormatLocal = "yyyy/mm/dd"
    Range("K" & i) = Created
    ' LastViewed
    LastViewed = Format(Range("L" & i), "yyyy/mm/dd")
    Range("L" & i).NumberFormatLocal = "yyyy/mm/dd"
    Range("L" & i) = LastViewed
    ' Updated
    Updated = Format(Range("M" & i), "yyyy/mm/dd")
    Range("M" & i).NumberFormatLocal = "yyyy/mm/dd"
    Range("M" & i) = Updated
    ' Resolved
    Resolved = Format(Range("N" & i), "yyyy/mm/dd")
    Range("N" & i).NumberFormatLocal = "yyyy/mm/dd"
    Range("N" & i) = Resolved
  Next i

End Sub
