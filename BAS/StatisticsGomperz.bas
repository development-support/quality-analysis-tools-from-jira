Attribute VB_Name = "StatisticsGomperz"
Option Explicit

' Ver0.2.28 2016/09/02 Gomperz曲線をZN準拠に変更
'                      集計期間の後端のバグが増加しない期間が演算に反映されて無かったので修正

Private Const strConstSplitSheetPrefix As String = "C_"
Private Const strConstProcessSheetPrefix As String = "P_"

Private Const strJobName As String = "ゴンペルツ曲線生成"
Private Const strConstGraphNamePrefix As String = "G_gpz_" ' "gomperz"
Private Const strConstDataNamePrefix As String = "Data_gpz_" ' "gomperz"

Sub GomperzExecute(Optional ByVal booEndMessage As Boolean = True)
    Dim wsProblem   As Worksheet '問題管理データシート
    Dim strSheetName As String
    Dim varInputSheet As Variant '問題管理データシート/分割シート
    Dim strSplitName() As String
    Dim i As Long
    
    If ProblemDataBaseTypeGet() = "Redmine" Then
        Select Case TargetProjectNameGet()
        Case "T200"
            Call 未実装(booEndMessage)
            Exit Sub
        End Select
    End If
    
    If booEndMessage Then
        If MsgBox(strJobName & "を開始します。", vbOKCancel) <> vbOK Then
            Exit Sub
        End If
    End If
    
    ' Statusバー表示
    StatisticsLibrary2.JobBegin (strJobName & "中...")
    
    Set wsProblem = 問題管理データシート取得()
    If wsProblem Is Nothing Then
        booEndMessage = False
        GoTo Exit_Proc
    End If
    
    If (ProblemDataBaseTypeGet() = "Redmine") And (TargetProjectNameGet() = "T3xx") Then
        If 分割シート取得(wsProblem, "カテゴリ", strConstSplitSheetPrefix, strSplitName, varInputSheet) = False Then
            booEndMessage = False
            GoTo Exit_Proc
        End If
    ElseIf ProblemDataBaseTypeGet() = "JIRA" Then
        If 分割シート取得(wsProblem, "カテゴリ", strConstSplitSheetPrefix, strSplitName, varInputSheet) = False Then
            booEndMessage = False
            GoTo Exit_Proc
        End If
    Else
        Set varInputSheet = wsProblem
    End If
    
    If IsArray(varInputSheet) Then
        Dim wsProblem2  As Worksheet    '分割データシート
        For i = 1 To UBound(varInputSheet)
            strSheetName = varInputSheet(i)
            Set wsProblem2 = StatisticsLibrary2.同名のシート取得(strSheetName)
            If gomperzMain(wsProblem2, True, strSplitName(i)) = False Then
                booEndMessage = False
                Exit For
            End If
        Next i
        
        Call 分割シート後処理(varInputSheet)
        
    ElseIf ProblemDataBaseTypeGet() = "JIRA" Then
    
    ElseIf gomperzMain(wsProblem) = False Then
        booEndMessage = False
    End If
    
    ' 全体出力 ※JIRAの場合、分割出力の有無に関わらず全体出力する。
    If ProblemDataBaseTypeGet() = "JIRA" Then
        If gomperzMain(wsProblem) = False Then
            booEndMessage = False
        End If
    End If
    
Exit_Proc:
    ' Statusバー表示
    StatisticsLibrary2.JobEnd strJobName & "処理終了", MsgBoxShow:=booEndMessage
    
End Sub

Private _
Function gomperzMain(ByVal wsProblem As Worksheet, _
                     Optional ByVal IsSplitSheet As Boolean = False, _
                     Optional ByVal strSplitName As String = "") As Boolean

    Dim strAnalyzeSheetName() As String
    Dim strProcessID() As String
    Dim intTargetColumn As Integer
    Dim i As Integer
    
    Dim strGeneSource As String
    Dim strSep As String
    Dim strDataSheetName As String ' 計算シート
    Dim strGraphName As String
    Dim strGraphTitle As String
    Dim strResultSheetName As String
    
    gomperzMain = False
    
    strGeneSource = TargetProjectNameGet()
    If IsSplitSheet Then
        strGeneSource = strSplitName & "/" & strGeneSource
        strSep = "_" & strSplitName
    Else
        strSep = ""
    End If
    
    Call DeleteGraphByChartName(strConstGraphNamePrefix & "*")
    
    ' 問題管理シートから工程別シート生成
    strAnalyzeSheetName = ByProcessSheetGenerate(wsProblem _
                                                , JobEndIfEmpty:=IIf(IsSplitSheet, False, True) _
                                                , DeleteIfEmpty:=True _
                                                , 分割値格納配列:=strProcessID _
                                                , NewSheetNamePrefix:=strConstProcessSheetPrefix)
    Select Case UBound(strAnalyzeSheetName)
    Case Is < 0  'エラー発生
        GoTo Exit_Proc
    Case 0  ' 有効データなし
        gomperzMain = True
        GoTo Exit_Proc
    End Select
    
    '' 参照する項目の列番号を取得
    ' 開始日の列番号
    intTargetColumn = StatisticsLibrary2.GetProbemColumnByItemName("問題発生日")
    If intTargetColumn < 1 Then
        GoTo Exit_Proc
    End If
    
    ' gomperz計算
    For i = 1 To UBound(strAnalyzeSheetName)
        strDataSheetName = 集計用出力シート名生成(strConstDataNamePrefix & strProcessID(i) & strSep)
        strGraphName = 集計用出力シート名生成(strConstGraphNamePrefix & strProcessID(i)) ' 注) グラフシートはグラフ結合後に削除される
        strGraphTitle = strProcessID(i) & IIf(IsSplitSheet, "/" & strSplitName, "")
        
        gomperz strAnalyzeSheetName(i), _
                strProcessID(i), _
                strGraphTitle, _
                strGraphName, _
                strDataSheetName, _
                intTargetColumn
    Next
    
    ' 工程別シートの削除
    For i = 1 To UBound(strAnalyzeSheetName)
        同名のワークシートを削除 strAnalyzeSheetName(i)
    Next
    
    ' グラフ結合
    strResultSheetName = 集計用出力シート名生成("Graph_Gomperz" & strSep)
    JointGraphByChartName strResultSheetName, 集計用出力シート名生成(strConstGraphNamePrefix) & "*", strGeneSource
    
    gomperzMain = True
    
Exit_Proc:
    Erase strAnalyzeSheetName
    
End Function

' ゴンペルツ計算
Private Sub gomperz(ByVal strRedmineSheetName As String, _
                    ByVal strProcessID As String, _
                    ByVal strGraphTitle As String, _
                    ByVal strGraphName As String, _
                    ByVal strAnalyzeSheetName As String, _
                    ByVal intTargetColumn As Integer)

    Dim varSettingParam As Variant
    Dim strEnableProcess() As String
    
    Dim strByWeekOrDay As String
    Dim dtPredicted As Date
    
    Dim varTmp As Variant
    Dim dtStart As Date
    Dim dtEnd As Date
    Dim dtProcessStart  As Date '工程の開始日
    Dim dtProcessEnd    As Date '工程の終了(予定)日
    
    ' ゴンペルツ曲線の設定を取得
    varSettingParam = GetStringSettingParam("gomperz_setting")
    If (varSettingParam(3) = "週") Then
        strByWeekOrDay = "Week"
    Else
        strByWeekOrDay = "Day"
    End If
    
    If Not (IsDate(varSettingParam(4))) Then
        If Not (varSettingParam(4) = "") Then
            If (MsgBox(strJobName + "設定: 日付の形式が異なります。", vbExclamation) = vbOK) Then
                End
            End If
        End If
    End If
    
    ' パラメータ取得
    Sheets(strRedmineSheetName).Select
    If (GetTargetColumnEndRow(intTargetColumn) < StatisticsLibrary2.numRedmineSheetDataRow) Then
        Exit Sub ' 有効データが無い
    End If
        
    ' 問題発生日の最小と最大を取得。
    varTmp = GetTargetColumnDateMinMax(intTargetColumn)
    dtStart = varTmp(1)
    dtEnd = varTmp(2)
    If Not (dtEnd > dtStart) Then
        If (MsgBox(strProcessID & "のサンプル数が不足しています。", vbExclamation) = vbOK) Then
            Exit Sub
        End If
    End If

    ' 処理対象工程の開始日と終了(予定)日を取得
    strEnableProcess = GetEnableProcessName(strProcessID)  '「試験工程」
    varTmp = StatisticsLibrary.GetProcessPeriod(strEnableProcess)
    dtProcessStart = varTmp(1)
    dtProcessEnd = varTmp(2)
    
    ' 工程開始日の指定がある場合は、工程開始日からグラフを作成。
    If Not DateIsInitial(dtProcessStart) Then
        ' ただし、工程開始日より前の問題発生がある場合は、工程開始日を無視する。
        If dtStart > dtProcessStart Then
            dtStart = dtProcessStart
        End If
    End If
    ' 工程終了日の指定がある場合は、工程終了日設定までの問題を集計
    ' 試験の最後で問題が起きない区間を実績に反映する為、追加
    ' 工程終了日の指定がない場合は、問題発生日までの問題を集計する為、変更なし
    ' また、問題発生日が工程終了日を超えた場合は、問題発生日の最終日を
    ' 工程終了日とする(何もしない）
    If Not DateIsInitial(dtProcessEnd) Then
        ' 問題発生日が工程終了日以下
        If dtEnd <= dtProcessEnd Then
            ' 集計処理実行時が、問題発生日を超えている場合
            If (Format(Now, "yyyy/mm/dd") > dtEnd) Then
                ' 処理日が工程終了日設定を超えている場合
                If (Format(Now, "yyyy/mm/dd") > dtProcessEnd) Then
                    dtEnd = dtProcessEnd
                ' 処理日が工程終了日設定未満の場合
                Else
                    dtEnd = Format(Now, "yyyy/mm/dd")
                End If
            End If
        End If
    End If
    
    If (varSettingParam(4) = "") Then
        ' 予測日の指定が無い場合、
        If Not DateIsInitial(dtProcessEnd) Then
            ' 工程終了日が指定されており、かつ、
            ' 工程終了日より後の問題発生がない場合は、工程終了日を予測日とする。
            If dtEnd < dtProcessEnd Then
                dtPredicted = dtProcessEnd
            End If
        End If
    
        If DateIsInitial(dtPredicted) Then
            dtPredicted = DateAdd("d", 14, dtEnd)
        End If

    ElseIf Not IsDate(varSettingParam(4)) Then
        Call MsgBox("設定: 予測日の設定が日付ではありませ。", vbExclamation)
        End
    Else
        dtPredicted = varSettingParam(4)
        If (dtPredicted <= dtEnd) Then
            If (MsgBox(strJobName & "設定: 予測日の設定が、" & strProcessID & vbCrLf _
                    & "の問題発生日以前となっています。" & vbCrLf _
                    & "この設定を無視して処理を続けます。", vbOKCancel + vbInformation) = vbCancel) Then
                End
            End If
            dtPredicted = DateAdd("d", 14, dtEnd)
        End If
    End If
    
    Erase varSettingParam
    
    ' 計算シート生成
    StatisticsGomperz.SheetGenerate strAnalyzeSheetName
    
    ' 日毎のバグを取得
    GetDailyBugData strRedmineSheetName, _
                    strAnalyzeSheetName, _
                    dtStart, _
                    dtEnd, _
                    strByWeekOrDay, _
                    intTargetColumn

    ' ゴンペルツパラメータ演算
    If (GetGomperzParameter(strAnalyzeSheetName, 1)) Then
        ' ゴンペルツ曲線生成
        GomperzGenerate strAnalyzeSheetName, 1, dtStart, dtPredicted, strByWeekOrDay
        ' グラフ用テーブル生成
        GraphTableGenerate strAnalyzeSheetName, 1, dtStart, dtPredicted, strByWeekOrDay, True
        ' グラフ化
        GenerateGraph strGraphName, strGraphTitle, strAnalyzeSheetName, 1, 26, True
    Else
        ' グラフ用テーブル生成
        GraphTableGenerate strAnalyzeSheetName, 1, dtStart, dtPredicted, strByWeekOrDay, False
        ' グラフ化
        GenerateGraph strGraphName, strGraphTitle, strAnalyzeSheetName, 1, 26, False
    End If

    ' 計算用シートの非表示
    同名のワークシートを非表示 strAnalyzeSheetName

End Sub

' 計算用シート生成
Private Sub SheetGenerate(ByVal strOutSheetName As String)

    '同名のワークシートをクリア strOutSheetName
    同名のワークシートを削除 strOutSheetName
    同名のワークシートが無ければ追加 strOutSheetName

End Sub

' データの落とし込み
Private Sub GetDailyBugData(ByVal strInSheetName As String, _
                            ByVal strOutSheetName As String, _
                            ByVal dtStart As Date, _
                            ByVal dtEnd As Date, _
                            ByVal strByWeekOrDay As String, _
                            ByVal intTargetColumn As Integer)

    EndDayWrite strOutSheetName, _
                dtStart, _
                dtEnd, _
                strByWeekOrDay, _
                1

    ActualSamplingNoWrite strOutSheetName, _
                            1, _
                            1, _
                            2

    NumberOfDailyBugWrite strInSheetName, _
                            strOutSheetName, _
                            intTargetColumn, _
                            1, _
                            dtStart, _
                            dtEnd, _
                            strByWeekOrDay, _
                            3

End Sub

' 実績日の書き込み
Private Sub EndDayWrite(ByVal varInSheet As Variant, _
                        ByVal dtStart As Date, _
                        ByVal dtEnd As Date, _
                        ByVal strByWeekOrDay As String, _
                        ByVal intOutTargetColumn As Integer, _
                        Optional ByVal booCheck As Boolean = True)
    Dim i As Integer

    Call TargetSheetSelect(varInSheet)

    If (strByWeekOrDay = "Day") Then
        If (DateDiff("d", dtStart, dtEnd) <= 0) And booCheck Then
            If (MsgBox("start_date列のサンプル数が不足しています。2日以上のサンプルが必要です。", vbExclamation) = vbOK) Then
                End
            End If
        End If
        Cells(1, intOutTargetColumn).Value = "日付"
        For i = 0 To DateDiff("d", dtStart, dtEnd)
            Cells(i + 2, intOutTargetColumn).Value = DateAdd("d", i, dtStart)
        Next
    Else
        If (DateDiff("w", dtStart, dtEnd) <= 0) And booCheck Then
            If (MsgBox("start_date列のサンプル数が不足しています。2週以上のサンプルが必要です。", vbExclamation) = vbOK) Then
               End
            End If
        End If
        Cells(1, intOutTargetColumn).Value = "日付"
        For i = 0 To DateDiff("w", dtStart, dtEnd)
            Cells(i + 2, intOutTargetColumn).Value = DateAdd("d", 7 * i, dtStart)
        Next
    End If

End Sub

' 実績サンプリング番号の書き込み
' 時間(ti)
Private Sub ActualSamplingNoWrite(ByVal varInSheet As Variant, _
                                  ByVal intTargetColumn As Integer, _
                                  ByVal intHeaderRow As Integer, _
                                  ByVal intOutTargetColumn As Integer)
    Dim i As Integer
    Dim intStartRow As Integer
    Dim intLastRow As Integer

    intStartRow = intHeaderRow + 1
    intLastRow = GetTargetColumnEndRow(intTargetColumn)

    Call TargetSheetSelect(varInSheet)
    Cells(1, intOutTargetColumn).Value = "時間(ti)"
    For i = intStartRow To intLastRow
        Cells(i, intOutTargetColumn).Value = i - intHeaderRow
    Next
End Sub

' 検出件数(fi)
Private Sub NumberOfDailyBugWrite(ByVal strInSheetName As String, _
                                  ByVal strOutSheetName As String, _
                                  ByVal intTargetColumn As Integer, _
                                  ByVal intHeaderRow As Integer, _
                                  ByVal dtStart As Date, _
                                  ByVal dtEnd As Date, _
                                  ByVal strByWeekOrDay As String, _
                                  ByVal intOutTargetColumn As Integer)
    Dim intCumulativeValue As Integer
    Dim i As Integer
    Dim j As Integer
    Dim intStartRow As Integer

    intStartRow = intHeaderRow + 1

    Sheets(strInSheetName).Select
    Columns(intTargetColumn).Select
    Selection.NumberFormatLocal = "yyyy-mm-dd"
    intCumulativeValue = 0

    Sheets(strOutSheetName).Select
    Cells(1, intOutTargetColumn).Value = "検出件数(fi)"

    For i = 0 To DateDiff("d", dtStart, dtEnd)
        Sheets(strInSheetName).Select
        intCumulativeValue = intCumulativeValue + WorksheetFunction.CountIf(Columns(intTargetColumn), DateAdd("d", i, dtStart))
        If (strByWeekOrDay = "Day") Then
            Sheets(strOutSheetName).Select
            Cells(i + intStartRow, intOutTargetColumn).Value = intCumulativeValue
            intCumulativeValue = 0
        Else
            If (i Mod 7 = 6) Or (i = DateDiff("d", dtStart, dtEnd)) Then
                Sheets(strOutSheetName).Select
                Cells(j + intStartRow, intOutTargetColumn).Value = intCumulativeValue
                j = j + 1
                intCumulativeValue = 0
            End If
       End If
    Next

    Sheets(strInSheetName).Select
    Columns(intTargetColumn).Select
    Selection.NumberFormatLocal = "@"

End Sub

' ゴンペルツパラメータ取得
Private Function GetGomperzParameter(ByVal strInOutSheetName As String, _
                                     ByVal intHeaderRow As Integer) As Boolean
    Dim ret As Boolean

    ret = False

    ' バグ数=0を除く
    WriteExceptFor0 strInOutSheetName, intHeaderRow, 2, 4

    ' 累積検出件数Fi(Σfi)
    NumberOfCumlativeBugWrite strInOutSheetName, _
                            5, _
                            intHeaderRow, _
                            6

    ' ゴンペルツパラメータ算出
    ret = GomperzParameterCalc(strInOutSheetName, intHeaderRow)

    ' 不一致係数算出
    If (ret) Then
        ret = MismatchFactorCalc(strInOutSheetName, intHeaderRow)
    End If

    GetGomperzParameter = ret

End Function

' 検出件数0を除いた時間(ti)
' 検出件数0を除いた検出件数(fi)
Private Sub WriteExceptFor0(ByVal strInOutSheetName As String, _
                            ByVal intHeaderRow As Integer, _
                            ByVal intInTiColumn As Integer, _
                            ByVal intOutTiColumn As Integer)
    Dim i, j As Integer
    Dim intStartRow As Integer
    Dim intLastRow As Integer

    Sheets(strInOutSheetName).Select
    intStartRow = intHeaderRow + 1
    intLastRow = GetTargetColumnEndRow(intInTiColumn)

    Cells(1, intOutTiColumn).Value = "時間(ti)-検出件数0を除く"
    Cells(1, intOutTiColumn + 1).Value = "検出件数(fi)-検出件数0を除く"

    For i = intStartRow To intLastRow
        If (Cells(i, intInTiColumn + 1).Value <> 0) Then
            j = j + 1
            Cells(j + intHeaderRow, intOutTiColumn).Value = Cells(i, intInTiColumn).Value
            Cells(j + intHeaderRow, intOutTiColumn + 1).Value = Cells(i, intInTiColumn + 1).Value
        End If
    Next

End Sub

' 累積検出件数Fi(Σfi)
Private Sub NumberOfCumlativeBugWrite(ByVal strInOutSheetName As String, _
                                      ByVal intTargetColumn As Integer, _
                                      ByVal intHeaderRow As Integer, _
                                      ByVal intOutTargetColumn As Integer)
    Dim i As Integer

    Sheets(strInOutSheetName).Select
    Cells(1, intOutTargetColumn).Value = "累積検出件数Fi(Σfi)"

    For i = 1 + intHeaderRow To GetTargetColumnEndRow(intTargetColumn)
        If (i = 1 + intHeaderRow) Then
            Cells(i, intOutTargetColumn).Value = Cells(i, intTargetColumn).Value
        Else
            Cells(i, intOutTargetColumn).Value = Cells(i - 1, intOutTargetColumn).Value + Cells(i, intTargetColumn).Value
        End If
    Next

End Sub

'　実績値から算出
'  ZN102-006準拠
Private Function GomperzParameterCalc(ByVal strInSheetName As String, _
                                      ByVal intHeaderRow As Integer) As Boolean
    Dim i As Integer
    Dim tmp As String
    Dim intStartRow As Integer
    Dim intLastRow As Integer

    On Error GoTo ErrorHandler

    GomperzParameterCalc = False

    Sheets(strInSheetName).Select

    intStartRow = intHeaderRow + 1
    intLastRow = GetTargetColumnEndRow(4)

    Cells(intHeaderRow, 7).Value = "ti^2"
    Cells(intHeaderRow, 8).Value = "Yi=ln(fi/Fi)"
    Cells(intHeaderRow, 9).Value = "ti×Yi"

    For i = intStartRow To intLastRow
        '　ti^2
        Cells(i, 7).Value = Cells(i, 4).Value ^ 2
        ' Yi=ln(fi/Fi)
        tmp = "LN(" & Cells(i, 5).Address & "/" & Cells(i, 6).Address & ")"
        Cells(i, 8).Value = Evaluate(tmp)
        ' ti×Yi
        Cells(i, 9).Value = Cells(i, 4).Value * Cells(i, 8).Value
    Next

    ' ゴンペルツパラメータ演算
    Cells(1, 11).Value = "ゴンペルツパラメータ"
    Cells(1, 12).Value = "値"
    ' Σti
    Cells(2, 11).Value = "Σti"
    tmp = "SUM(" & Cells(intStartRow, 4).Address & ":" & Cells(intLastRow, 4).Address & ")"
    Cells(2, 12).Value = Evaluate(tmp)
    ' Σfi
    Cells(3, 11).Value = "Σfi"
    Cells(3, 12).Value = Cells(intLastRow, 6).Value
    ' Σti^2
    Cells(4, 11).Value = "Σti^2"
    tmp = "SUM(" & Cells(intStartRow, 7).Address & ":" & Cells(intLastRow, 7).Address & ")"
    Cells(4, 12).Value = Evaluate(tmp)
    ' ΣYi
    Cells(5, 11).Value = "ΣYi"
    tmp = "SUM(" & Cells(intStartRow, 8).Address & ":" & Cells(intLastRow, 8).Address & ")"
    Cells(5, 12).Value = Evaluate(tmp)
    ' Σti×Yi
    Cells(6, 11).Value = "Σti×Yi"
    tmp = "SUM(" & Cells(intStartRow, 9).Address & ":" & Cells(intLastRow, 9).Address & ")"
    Cells(6, 12).Value = Evaluate(tmp)
    ' n
    ' fi=0以外の個数
    Cells(7, 11).Value = "n"
    Cells(7, 12).Value = intLastRow - intHeaderRow
    ' D
    ' D = (n*Σ(ti×Yi) - ΣtiΣYi) / (n*Σ(ti^2) - (Σti)^2)
    ' ここはfi=0以外をのぞかないのかな?
    ' 除いてよいらしい。もう一度再計算しよう・・。8/26
    Cells(8, 11).Value = "D"
    Cells(8, 12).Value = (Cells(7, 12).Value * Cells(6, 12).Value - Cells(2, 12).Value * Cells(5, 12).Value) / (Cells(7, 12).Value * Cells(4, 12).Value - Cells(2, 12) ^ 2)
    ' C
    ' C = (ΣYi - D*Σti) / n
    Cells(9, 11).Value = "C"
    Cells(9, 12).Value = (Cells(5, 12).Value - Cells(8, 12).Value * Cells(2, 12).Value) / Cells(7, 12).Value
    ' a
    ' a = exp(exp(C)/D)
    Cells(10, 11).Value = "a"
    tmp = "EXP(EXP(" & Cells(9, 12).Address & ")/" & Cells(8, 12).Address & ")"
    Cells(10, 12).Value = Evaluate(tmp)
    ' b
    ' b = exp(D)
    Cells(11, 11).Value = "b"
    tmp = "EXP(" & Cells(8, 12).Address & ")"
    Cells(11, 12).Value = Evaluate(tmp)
    ' t
    Cells(12, 11).Value = "t"
    Cells(12, 12).Value = Cells(GetTargetColumnEndRow(2), 2).Value
    ' N
    ' N=exp(ln(Σfi)-b^t*ln(a))
    Cells(13, 11).Value = "N"
    tmp = "EXP(LN(" & Cells(3, 12).Address & ")-((" & Cells(11, 12).Address & "^" & Cells(12, 12).Address & ")*(LN(" & Cells(10, 12).Address & "))))"
    Cells(13, 12).Value = Evaluate(tmp)
    ' 95%限界最少バグ数
    Cells(14, 11).Value = "95%限界最少バグ数"
    tmp = Cells(13, 12).Address & "+1.64*SQRT(" & Cells(13, 12).Address & ")"
    Cells(14, 12).Value = Evaluate(tmp)
    ' 95%限界最大バグ数
    Cells(15, 11).Value = "95%限界最大バグ数"
    tmp = Cells(13, 12).Address & "-1.64*SQRT(" & Cells(13, 12).Address & ")"
    Cells(15, 12).Value = Evaluate(tmp)
    ' 収束率F/N
    Cells(16, 11).Value = "収束率"
    Cells(16, 12).Value = Format((Cells(3, 12).Value / Cells(13, 12).Value), "0.000 %")

    GomperzParameterCalc = True

    Exit Function
ErrorHandler:
'    MsgBox "現状データではゴンペルツ演算できません"
'    End
End Function

' 不一致係数の算出
Private Function MismatchFactorCalc(ByVal strInSheetName As String, _
                                    ByVal intHeaderRow As Integer) As Boolean
    Dim i As Integer
    Dim intStartRow As Integer
    Dim intLastRow As Integer
    Dim a As Double
    Dim b As Double
    Dim n As Double
    Dim t As Integer
    Dim tmp As String

    On Error GoTo ErrorHandler

    MismatchFactorCalc = False

    Sheets(strInSheetName).Select

    intStartRow = intHeaderRow + 1
    intLastRow = GetTargetColumnEndRow(1)
    
    ' t,fiをコピー,Σfiを算出
    Cells(1, 14).Value = "時間(ti)"
    Cells(1, 15).Value = "検出件数(fi)"
    Cells(1, 16).Value = "累積検出件数Fi(Σfi)"
    For i = intStartRow To intLastRow
        ' t
        Cells(i, 14).Value = Cells(i, 2).Value
        ' fi
        Cells(i, 15).Value = Cells(i, 3).Value
        ' Σfi
        If (i = intStartRow) Then
            Cells(i, 16).Value = Cells(i, 3).Value
        Else
            Cells(i, 16).Value = Cells(i - 1, 16).Value + Cells(i, 3).Value
        End If
    Next

    ' N
    n = Cells(13, 12).Value
    ' a
    a = Cells(10, 12).Value
    ' b
    b = Cells(11, 12).Value

    ' ゴンペルツ曲線:F=N×(a^(b^t))
    Cells(1, 17).Value = "F"
    Cells(1, 18).Value = "Σfiの2乗"
    Cells(1, 19).Value = "Fの2乗"
    Cells(1, 20).Value = "誤差の2乗"

    For i = intStartRow To intLastRow
        ' t
        t = Cells(i, 14).Value
        ' ゴンペルツ曲線(予測値):F=N×(a^(b^t))
        Cells(i, 17).Value = n * (a ^ (b ^ t))
        ' 実績値:Σfiの2乗
        Cells(i, 18).Value = Cells(i, 16).Value ^ 2
        ' 予測値:Fの2乗
        Cells(i, 19).Value = Cells(i, 17).Value ^ 2
        ' 誤差:(実測値Σfi-予測値F)の2乗
        Cells(i, 20).Value = (Cells(i, 16).Value - Cells(i, 17).Value) ^ 2
    Next

    ' 不一致係数
    ' = Σ誤差の2乗^(1/2) / (Σ実績値の2乗^(1/2) + Σ予測値の2乗^(1/2))
    Cells(17, 11).Value = "不一致係数"
    tmp = "SUM(" & Cells(intStartRow, 20).Address & ":" & Cells(intLastRow, 20).Address & ")^0.5" & _
          "/" & _
          "(SUM(" & Cells(intStartRow, 18).Address & ":" & Cells(intLastRow, 18).Address & ")^0.5 +" & _
          " SUM(" & Cells(intStartRow, 19).Address & ":" & Cells(intLastRow, 19).Address & ")^0.5)"
    Cells(17, 12).Value = "=" & tmp

    MismatchFactorCalc = True

    Exit Function
ErrorHandler:
'    MsgBox "現状データではゴンペルツ演算できません"
'    End
End Function

' ゴンペルツ曲線生成
Private Sub GomperzGenerate(ByVal strInSheetName As String, _
                            ByVal intHeaderRow As Integer, _
                            ByVal dtStart As Date, _
                            ByVal dtEnd As Date, _
                            ByVal strByWeekOrDay As String)
    Dim i As Integer
    Dim intStartRow As Integer
    Dim intLastRow As Integer
    Dim a As Double
    Dim b As Double
    Dim n As Double
    Dim t As Integer

    EndDayWrite strInSheetName, dtStart, dtEnd, strByWeekOrDay, 22
    ActualSamplingNoWrite strInSheetName, 22, 1, 23

    Sheets(strInSheetName).Select

    intStartRow = intHeaderRow + 1
    intLastRow = GetTargetColumnEndRow(23)

    ' N
    n = Cells(13, 12).Value
    ' a
    a = Cells(10, 12).Value
    ' b
    b = Cells(11, 12).Value

    ' ゴンペルツ曲線(予測値):F=N×(a^(b^t))
    Cells(1, 24).Value = "F"
    For i = intStartRow To intLastRow
        ' t
        t = Cells(i, 23).Value
        ' ゴンペルツ曲線(予測値):F=N×(a^(b^t))
        Cells(i, 24).Value = n * (a ^ (b ^ t))
    Next

End Sub

' グラフ用テーブル生成
Private Sub GraphTableGenerate(ByVal strInSheetName As String, _
                               ByVal intHeaderRow As Integer, _
                               ByVal dtStart As Date, _
                               ByVal dtEnd As Date, _
                               ByVal strByWeekOrDay As String, _
                               ByVal booGomperz As Boolean)
    Dim i As Integer
    Dim intStartRow As Integer
    Dim intLastRow As Integer

    Sheets(strInSheetName).Select

    ' 16列目をコピー
    intStartRow = intHeaderRow + 1

    ' 日付
    EndDayWrite strInSheetName, dtStart, dtEnd, strByWeekOrDay, 26

    '　実績値:累積検出件数Fi(Σfi)
    Cells(1, 27).Value = "実測値"
    intLastRow = GetTargetColumnEndRow(3)
    For i = intStartRow To intLastRow
        ' Σfi
        If (i = intStartRow) Then
            Cells(i, 27).Value = Cells(i, 3).Value
        Else
            Cells(i, 27).Value = Cells(i - 1, 27).Value + Cells(i, 3).Value
        End If
    Next

    '　ゴンペルツ曲線(予測値):F=N×(a^(b^t))
    If (booGomperz) Then
        Cells(1, 28).Value = "予測値"
        intLastRow = GetTargetColumnEndRow(24)
        For i = intStartRow To intLastRow
            Cells(i, 28).Value = Cells(i, 24).Value
        Next
    End If

End Sub

' グラフ
Private Sub GenerateGraph(ByVal strGraphName As String, _
                          ByVal strTitle As String, _
                          ByVal strInSheetName As String, _
                          ByVal intHeaderRow As Integer, _
                          ByVal intTargetColumn As Integer, _
                          ByVal booGomperz As Boolean)

    Dim intStartRow As Integer
    Dim intLastRow As Integer

    Dim XRange As Range
    Dim ActualRange As Range
    Dim PredictedRange As Range
    Dim TestActualRange As Range
    Dim TestPredictedRange As Range
    Dim dblMismatchCoefficient As Double
    Dim strRateOfConvergence As String
    Dim dblPredictNumOfBug As Double
    Dim intNumOfActualPoint As Integer
    Dim intNumOfPredictedPoint As Integer
    Dim intNumOfTestActualPoint As Integer
    Dim intNumOfTestPredictedPoint As Integer
    Dim i As Integer
    Dim z As Series

    Sheets(strInSheetName).Select

    intStartRow = intHeaderRow + 1
    intLastRow = GetTargetColumnEndRow(intTargetColumn)

    Set XRange = Range(Cells(intStartRow, intTargetColumn), Cells(intLastRow, intTargetColumn))
    Set ActualRange = Range(Cells(intStartRow, intTargetColumn + 1), Cells(intLastRow, intTargetColumn + 1))

    intNumOfActualPoint = GetTargetColumnEndRow(intTargetColumn + 1) - intHeaderRow

    If (booGomperz) Then
        Set PredictedRange = Range(Cells(intStartRow, intTargetColumn + 2), Cells(intLastRow, intTargetColumn + 2))
        dblMismatchCoefficient = Round(Sheets(strInSheetName).Cells(17, 12).Value, 6)
        strRateOfConvergence = Format(Sheets(strInSheetName).Cells(16, 12).Value, "0.000 %")
        dblPredictNumOfBug = Round(Sheets(strInSheetName).Cells(13, 12).Value, 2)
        intNumOfPredictedPoint = GetTargetColumnEndRow(intTargetColumn + 2) - intHeaderRow
    End If

    同名のグラフを削除 strGraphName

    Dim mychart As Chart
    Set mychart = Charts.Add(After:=Sheets(Sheets(Sheets.Count).Name))
    ' 上記で最後のシートの後ろに置きたいが、仕様でおけない(Beforeと同じ)ので置き換え
    Sheets(mychart.Index).Move After:=Sheets(Sheets.Count)

    With mychart
        ' グラフType設定
        .ChartType = xlLineMarkers
        ' グラフ名設定
        .Name = strGraphName
        ' Title設定
        .HasTitle = True
        .ChartTitle.text = "Gomperz curve" & "[" & strTitle & "]"
        .ChartTitle.Format.TextFrame2.TextRange.Font.Size = 16
        .ChartTitle.Format.TextFrame2.TextRange.Font.Bold = msoTrue
        '.ChartTitle.Format.TextFrame2.TextRange.Font.Name = "Arial"

        ' 初回は削除して新規作成(Seriesの初期値が変動するため・・・)
        For Each z In .SeriesCollection
          z.Delete
        Next
        .SeriesCollection.NewSeries

        .SeriesCollection(1).XValues = XRange
        .SeriesCollection(1).Values = ActualRange
        .SeriesCollection(1).Name = "Actual Bugs"

        .Axes(xlCategory, xlPrimary).HasTitle = False
        '.Axes(xlCategory, xlPrimary).AxisTitle.Characters.Text = ""
        .Axes(xlCategory, xlPrimary).TickLabels.NumberFormatLocal = "m/d"
        .Axes(xlValue, xlPrimary).HasTitle = True
        .Axes(xlValue, xlPrimary).AxisTitle.Characters.text = "Bugs"
        .Axes(xlValue, xlPrimary).TickLabels.NumberFormatLocal = "G/標準"

        ' 実績バグの線に対する設定
        With .SeriesCollection(1)
            .Format.Line.ForeColor.RGB = RGB(255, 0, 0)
            .Format.Fill.ForeColor.RGB = RGB(255, 0, 0)
            .Format.Line.DashStyle = msoLineSolid
            .Border.Weight = xlMedium
            .MarkerStyle = xlSquare
            .Smooth = False
            .MarkerSize = 7
            .Shadow = False
            .Points(intNumOfActualPoint).ApplyDataLabels
            .Points(intNumOfActualPoint).DataLabel.NumberFormat = "#,##0_ "
        End With

        If (booGomperz) Then
            .SeriesCollection.NewSeries
            .SeriesCollection(2).Values = PredictedRange
            .SeriesCollection(2).Name = "Estimate Bugs"

            ' 予測バグの線に対する設定
            With .SeriesCollection(2)
                .Format.Line.ForeColor.RGB = RGB(255, 124, 128)
                .Format.Line.DashStyle = msoLineSysDash
                .Border.Weight = xlMedium
                .MarkerStyle = xlNone
                .Smooth = False
                .MarkerSize = 7
                .Shadow = False
                .Points(intNumOfPredictedPoint).ApplyDataLabels
                .Points(intNumOfPredictedPoint).DataLabel.NumberFormat = "#,##0_ "
            End With
        End If

        ' プロットエリアの背景色
        .PlotArea.Format.Fill.ForeColor.RGB = RGB(255, 255, 204)

        ' テキストボックス
        If (booGomperz) Then
'            With .TextBoxes.Add(75, 40, 125, 55)
            With .TextBoxes.Add(75, 60, 250, 110)
                '.Select
                '.AutoSize = True
                .text = "[Mismatch coefficient： " & dblMismatchCoefficient & " ]" & vbCr & _
                        "[Convergence Rate：          " & strRateOfConvergence & " ]" & vbCr & _
                        "[Latent Bugs(t->∞): " & dblPredictNumOfBug & " ]" & vbCr
                .ShapeRange.TextFrame2.TextRange.Font.NameComplexScript = "Meiryo UI"
                .ShapeRange.TextFrame2.TextRange.Font.NameFarEast = "Meiryo UI"
                .ShapeRange.TextFrame2.TextRange.Font.Size = 9
            End With
        Else
            With .TextBoxes.Add(75, 40, 125, 55)
                '.Select
                '.AutoSize = True
                .text = "現状データではゴンペルツ演算できません"
                .Font.Color = vbRed
                .ShapeRange.TextFrame2.TextRange.Font.NameComplexScript = "Meiryo UI"
                .ShapeRange.TextFrame2.TextRange.Font.NameFarEast = "Meiryo UI"
                .ShapeRange.TextFrame2.TextRange.Font.Size = 9
            End With
        End If

    End With

End Sub

Private Sub 同名のワークシートが無ければ追加(ByVal strOutSheetName As String)
    Dim i As Worksheet

    For Each i In Worksheets
        If (UCase(i.Name) = UCase(strOutSheetName)) Then
            Sheets(strOutSheetName).Select
            Exit Sub
        End If
    Next

    Sheets.Add After:=Sheets(Sheets(Sheets.Count).Name)
    ActiveSheet.Name = strOutSheetName

End Sub
