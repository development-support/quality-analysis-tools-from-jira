Attribute VB_Name = "JiraGet_Func"
Option Explicit

' *******************************************
'   Date        Version    Comment
'   2017.06.09  0.2.38     Board未使用に変更
' *******************************************

Public Sub JiraGet(Optional ByVal booEndMessage As Boolean = True)
Attribute JiraGet.VB_ProcData.VB_Invoke_Func = " \n14"
  '
  ' JiraGet Macro
  '
  Const strJobName As String = ""
  Dim Filename As String
  Dim FillID As String
  Dim i As Long

  '格納する変数を定義します｡
  Dim strPath As String

  Application.Calculation = xlCalculationManual

  '変数にパスを格納します。
  strPath = ActiveWorkbook.Path

  Filename = "\JiraExport.xls"

  FillID = Sheets(gConfigSheetName).Range("ConfigFilterID").Value

  strPath = strPath & Filename

  'バッチファイル起動してPOST実行
  Dim ret As Long
    
  With CreateObject("Wscript.Shell")
    ' all
    ret = .Run("curl -Lkv -o " & strPath & " -v -H ""Content-Type: application/json"" -X GET -u okuda:lc2011SDAP ""http://rtx-swtl-jira.fnc.net.local/sr/jira.issueviews:searchrequest-html-all-fields/" & FillID & "/SearchRequest-" & FillID & ".html?", 7, True)
    ' select
    ' ret = .Run("curl -Lkv -o " & strPath & " -v -H ""Content-Type: application/json"" -X GET -u okuda:lc2011SDAP ""http://rtx-swtl-jira.fnc.net.local/sr/jira.issueviews:searchrequest-excel-current-fields/" & FillID & "/SearchRequest-" & FillID & ".xls?", 7, True)
    ' curl -Lkv -o %1 -X GET --basic --user okuda:lc2011SDAP "http://rtx-swtl-jira.fnc.net.local/sr/jira.issueviews:searchrequest-excel-all-fields/%2/SearchRequest-%2.xls?"
  End With

  'If ret <> 0 Then MsgBox "失敗しました"

  Call ボタン1_Click

  Application.Calculation = xlCalculationAutomatic

  ' 2018/10/10追加
  Call StatisticsLibrary2.JobEnd(strJobName & "処理終了", MsgBoxShow:=booEndMessage)

End Sub

